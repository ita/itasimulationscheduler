/*
 * ----------------------------------------------------------------
 *
 *		Room Acoustics for Virtual ENvironments (RAVEN)
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2013-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *		ravenegg
 *
 *		Command line application to perform a simulation using
 *		a task input file to generate a result impulse response
 *
 */

// getopt (simple command option parser)
#ifndef __GNU_LIBRARY__
#	define __GNU_LIBRARY__
#endif
#include "getopt.h"

#include <ITA/SimulationScheduler/RoomAcoustics/raven/raven_simulation_result.h>
#include <ITA/SimulationScheduler/RoomAcoustics/raven/simulation_task.h>
#include <ITA/SimulationScheduler/RoomAcoustics/raven/simulator.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/types.h>
#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITAFileSystemUtils.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <RG_Vector.h>
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>
#include <conio.h>
#include <iostream>
#include <memory>
#include <omp.h>
#include <string>


using namespace std;
using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;
using namespace Raven;

void normalize_0dB( ITASampleFrame* );

void LoadTask( CSimulationTask& oTask, const std::string& sPath );

FieldOfDuty ParseSimulationType( const std::string& sShortSimType );

void syntax_error( )
{
	cout << "Syntax: ravenegg -i RPFFILE -t TASKFILE -r RESULTFILE [-n (normalize)]" << endl;
	cout << "Scene of task file may only include a single source and a single receiver" << endl;
}

int main( int argc, char* argv[] )
{
	if( argc < 7 )
	{
		cout << "Too few arguments given (" << argc << ")" << endl;
		syntax_error( );
		return 255;
	}

	string sRPFFile, sTaskPath, sResultPath;
	bool bNormalize = false;

	int c;
	while( ( c = getopt( argc, argv, "i:nr:t:" ) ) != -1 )
	{
		switch( c )
		{
			case 'i':
				sRPFFile = optarg;
				break;
			case 'n':
				bNormalize = true;
				break;
			case 'r':
				sResultPath = optarg;
				break;
			case 't':
				sTaskPath = optarg;
				break;
			default:
				char x   = c;
				string s = "1";
				s[0]     = x;
				cout << "Parameter parse error, could not understand '" << s << "'" << endl;
				syntax_error( );
				return 255;
		}
	}

	try
	{
		CSimulationTask oTask;
		CRavenSimulationResult oResult;

		cout << "Loading task from file with path '" << sTaskPath << "'" << endl;
		LoadTask( oTask, sTaskPath );

		if( oTask.oScene.GetSourceMap( ).size( ) != 1 )
		{
			cerr << "[ERROR] Please provide a single source in task file" << endl;
			return 255;
		}

		if( oTask.oScene.GetReceiverMap( ).size( ) != 1 )
		{
			cerr << "[ERROR] Please provide a single receiver in task file, portals are not supported by simulator yet" << endl;
			return 255;
		}

		cout << "Task acknowledged" << endl;
		cout << " --- Task scene: " << endl << oTask.oScene.ToString( ) << endl;

		auto pRavenSimulator = std::make_shared<CSimulator>( oTask.eSimulationType, sRPFFile );

		cout << "Performing simulation" << endl;
		pRavenSimulator->Compute( &oTask, &oResult );

		cout << "Exporting result to '" << sResultPath << "'" << endl;
		if( bNormalize )
			normalize_0dB( oResult.vcspResult[0]->psfResult.get( ) );

		writeAudiofile( sResultPath, oResult.vcspResult[0]->psfResult.get( ), 44100.0 );

		if( oResult.vcspResult[0]->bEntitiesInSameRoom )
			cout << "Entities (source and receiver) were in the same room." << endl;
		else
			cout << "Entities (source and receiver) were not in the same room." << endl;

		cout << "Store Profile data";

		int c = _getch( );

		if( c == 'y' )
			StoreProfilerData( "SimulationProfile.json" );
	}
	catch( ITAException& err )
	{
		cout << "[ERROR]: " << err << endl;
		return 255;
	}

	return 0;
}

void normalize_0dB( ITASampleFrame* p )
{
	float fGlobalPeak = 0.0f;
	for( int i = 0; i < p->length( ); i++ )
	{
		ITASampleBuffer& p1 = ( *p )[0];
		ITASampleBuffer& p2 = ( *p )[1];
		float fSample1      = p1[i];
		float fSample2      = p2[i];
		fGlobalPeak         = max( fGlobalPeak, fSample1 );
		fGlobalPeak         = max( fGlobalPeak, fSample2 );
	}
	if( fGlobalPeak != 0.0f )
		p->mul_scalar( 1 / fGlobalPeak );
}

void LoadTask( CSimulationTask& oTask, const std::string& sPath )
{
	oTask.uiID                  = INIFileReadUInt( sPath, "Task", "ID", 0 );
	std::string sSimulationType = INIFileReadString( sPath, "Task", "Simulation", "DS+ER_IS+DD_RT" );
	auto eSimulationType        = ParseSimulationType( sSimulationType );
	oTask.eSimulationType       = eSimulationType;
	oTask.oScene.LoadFormIni( sPath );

	return;
}

FieldOfDuty ParseSimulationType( const std::string& sShortSimType )
{
	if( sShortSimType == "DS" )
		return FieldOfDuty::directSound;
	else if( sShortSimType == "ER_IS" )
		return FieldOfDuty::earlyReflections;
	else if( sShortSimType == "DD_RT" )
		return FieldOfDuty::diffuseDecay;

	ITA_EXCEPT_INVALID_PARAMETER( "Simulation Type not supported." );
}
