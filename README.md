# ITASimulationScheduler

ITASimulationScheduler is a C++ library and application collection to run distributed acoustic simulations.
It comes with scheduling features for real-time and sequential (offline) task procedures using threads and/or MPI.

## Configuration

The runtime aspects of the simulation scheduler are fully configurable via `json` and `ini` files.
Note, that `json` is preferred here, since it is easier to map the underlying structure through nesting.

In the following, a brief overview of the configuration options will be given.

### Scheduler config

The scheduler forms the basis of this framework.
In its configuration, the workers that are available to the scheduler and the optional update filtering can be configured.
The configuration and concept of which will be covered later.
A scheduler can have zero or more worker, however at least one should be present for any simulation to be run.
When a scheduler has multiple worker, it distributes incoming simulations to these workers.
In case all available worker are busy, i.e. running a simulation, the incoming update is kept and given to a worker as soon as possible.
However, in case `ReplaceUpdates` is true, the pending update can be overwritten by a newer one.

The configuration keys look like this:

```json
{
    "@ReplaceUpdates" : true,    // bool; If true, a new incoming update will replace an older pending update.
    "@Type" : "LocalScheduler",  // string; Type of the scheduler; Currently only "LocalScheduler" supported.
    "FilterNetwork" : { ... },   // object; optional; Configuration for the update filtering.
    "Worker0" : { ... }          // object; optional; Configuration for a worker.
                                 // In oder to add more worker, add more key object pairs with the key containing "Worker".
                                 // For example: "Worker0", "Worker1" and "Worker2".
}
```

### Worker configuration

A worker represents an object, that can run a single blocking simulation.
Currently, only three specialized workers are available:

- OutdoorAcousticsWorkerThread -- Runs outdoor simulations using different simulators.
- RAVENWorkerThread -- Runs room acoustic simulations using RAVEN.
- RemoteWorker -- Connects to the `remote-worker` application and runs the simulation on a separate computer using RPC.

The common key for all workers is **`@Type`** which is a string, that sets the type of worker.

#### Remote Worker

The remote worker can be used to offload a simulation to a separate computer.
To do so, it connects to a `remote-worker` application and forwards any simulation requests to it.
Once the simulation is finished, the remote worker receives the result.
This process is done though remote procedure calls (RPC).

The configuration for this worker looks like this:

```json
{
    "@Type" : "RemoteWorker",
    "@Address": "localhost:50052", // Address of the computer running the simulation. The format is "<ip address>:<port number>".
    "Worker0" : { ... }            // object; optional; Configuration for a worker. Same as for the scheduler.
                                   // In oder to add more worker, add more key object pairs with the key containing "Worker".
                                   // For example: "Worker0", "Worker1" and "Worker2".
                                   // Note, that the configuration must match the folder structure on the remote computer.
}
```

The configuration of the remote worker includes the configuration for the actual worker that will be running the simulation.
This will be forwarded to the `remote-worker` application where the configured workers will be created.
The remote worker can also be configured to host multiple workers if the computing resources allow for it.
Make sure, that the included worker are configured for the folder structure on the remote computer as well as for the correct simulation (for example: using the RAVEN worker thread with the master simulation controller would adapt the filed of duty, this cannot be done with the remote worker.).

### Filter network configuration

This scheduling framework implements a filtering system for the incoming scene change updates.
The intend behind this, is to filter or remove updates that should or may not be simulated.
As a result of the reduction in updates, the workers do not have to work as much.
The hope is, that this improves the real-time capabilities of the simulation.

In order to have full control over which updates are filtered, a network of these filters can be constructed to describe more complex relations.
In this object, this network can be configured.

The configuration keys look like this:

```json
{
    "@StartFilter" : "FilterName", // string; Name of the filter to start the evaluation from. Note: the filters are named via the "@Name" key.
    "Filter0" : { ... }            // object; optional; Configuration for a filter.
                                   // In oder to add more filter, add more key object pairs with the key containing "Filter".
                                   // For example: "Filter0", "Filter1" and "Filter2".
}
```

#### Possible Filter Configs

Listed below are the possible filter configs with all their respective keys.

Common keys are:

- **`Name`** : The name by which the filter can be referred to.
- **`Type`** : The type of filter to be used.
  Currently available are:
  - RateFilter -- Only allows an update to pass with the configured rate.
  - DistanceFilter -- Allows update to pass when the distance between source and receiver is smaller than the configured distance.
  - RotationFilter -- Allows update to pass when the rotation exceeds the configured amount.
  - TranslationFilter -- Allows update to pass when the translation exceeds the configured amount.
  - ZoneFilter -- Allows update to pass when source or receiver reside inside of the configured zones.
  - PerceptiveRotationFilter -- Allows update to pass when the rotation exceeds the localization accuracy of humans hearing.
- **`NextFilter`** : Name of the next filter to be evaluated after this filter.
Multiple comma separated names are supported to created branches in the evaluation.
- **`UsageMode`** : How the filter should be used: As a filter (0) or condition (1).
- **`InvertCondition`** : In case the filter is used as a condition, the result can be inverted.

```json
{
    "Filter0" : {
        "@Name" : "Unknown",
        "@Type" : "RateFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "@Rate" : 1                    // float; Update Rate in Herz, i.e. how many updates per second are passed through.
    },
    "Filter1" : {
        "@Name" : "Unknown",
        "@Type" : "DistanceFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "@DistanceThreshold" : 5       // float; Distance below which the update is passed through.
    },
    "Filter2" : {
        "@Name" : "Unknown",
        "@Type" : "RotationFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "@ReceiverThreshold" : 2.5,    // float; Rotation threshold for the receiver in degree.
        "@SourceThreshold" : 2.5,      // float; Rotation threshold for the receiver in degree.
        "@RotationMode" : 1            // int (enum); Mode for rotation calculation, relative (0) or absolute (1).
                                       // Relative uses the rotation of source and receiver relative to each other. (Should only be used for direct sound paths)
                                       // Absolute uses the absolute rotation of source and receiver.
    },
    "Filter3" : {
        "@Name" : "Unknown",
        "@Type" : "TranslationFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "@MinimumTranslation" : 5,     // float; Threshold above which the update is passed through.
        "@TranslationMode" : 1         // int (enum); Mode for translation calculation, relative (0) or absolute (1).
                                       // Relative uses the quotient of new and previous distance between source and receiver
                                       // Absolute uses the absolute change in translation of source or receiver.
    },
    "Filter4" : {
        "@Name" : "Unknown",
        "@Type" : "ZoneFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "Zones" : {                    // object; Definition of the axis aligned cuboid zones of the filter. The filter can have more than one zone.
            "Zone0first" : [0, 0, 0],  // 3 element float array, OpenGL Coordinates; First point defining the cuboid of zone 1.
            "Zone0second" : [1, 1, 1]  // 3 element float array, OpenGL Coordinates; Second point defining the cuboid of zone 1.
        }                              // To add more zones, add more keys and three element arrays with the key following the schema
                                       // "Zone[linearly increasing counter, starting at 0][first|second]"
    },
    "Filter5" : {
        "@Name" : "Unknown",
        "@Type" : "PerceptiveRotationFilter",
        "@NextFilter" : "",
        "@UsageMode" : 0,
        "@InvertCondition" : false,
        "@StrengthFactor" : 1          // float; Factor with which the localization uncertainty is multiplied before it is used as a threshold.
                                       // With it, the strength of this filter can be adapted.
    }
}
```

### Variable substitution

In the configuration files, a variable substitution can be performed.
In the top level of the json file, under the **`Variables`** key, these variables can be specified like this:

```json
{
    "Variables" : {
        "VariableName" : 3.141,
        "VariableString" : "A variable can be any type you want!!"
    },

    "@RandomKey" : "${VariableString}",
    "@Pi" : "${VariableName}"
}
```

After their definition, the variables can be used in the configuration by using a string value `"${Variable name}"` using the variables keys as the name.
See the example above.

### Room acoustic module

The room acoustic module adds additional configurable elements.

#### Master simulation controller

For room acoustic simulations, it is often preferable to simulate the direct sound, early reflections and diffuse decay separately.
To model this behavior, the **Master simulation controller** is introduced.
It combines three separate scheduler, each for the direct sound, early reflections and diffuse decay.
Each incoming update is distributed to each of the three schedulers, so that it can be processed separately depending on the part of the RIR.
Its configuration looks like this:

```json
{
    "@ReplaceUpdates" : true, // Same as in the scheduler itself.
    "DSScheduler" : { ... },  // Configuration for the direct sound scheduler.
    "ERScheduler" : { ... },  // Configuration for the early reflection scheduler.
    "DDScheduler" : { ... }   // Configuration for the diffuse decay scheduler.
}
```

#### RAVEN worker thread

```json
{
    "@Type" : "RavenWorkerThread",
    "@FieldOfDuty" : 0,                           // int (enum); RIR part to simulate, direct sound (0), early reflections (1), diffuse decay (2).
                                                  // When used in conjunction with the master simulation controller, this field will be overwritten with the 
                                                  // corresponding scheduler RIR part/ field of duty.
    "@RavenProjectFilePath" : "Path/to/rpf/file", // Path to the rpf file, either absolute or relative to the current working directory.
}
```

In theory, every rpf (RAVEN project file) should work here.
However, it is recommended, that the simulation parameters defined in the rpf file are meaningful in a real-time context.
This means, a reasonable order of image sources and a reasonable number of rays.

Additionally, some parameters in the rpf file are redundant to the settings from the simulation scheduler.
Some of these are:

- `simulationTypeIS` and `simulationTypeRT` -- this is already defined via the field of duty
- `generateRIR` and `generateBRIR` -- currently hard coded to binarual, however this might later be configurable.
- `keepOutputFiles`, `exportFilter`, `exportHistogram`, `logPerformance`, `exportWallHitList` and `exportPlaneWaveList` -- we do not care for any file output.
- `[PrimarySources]` and `[Receiver]` sections -- source and receiver information is given via the scheduler interface.

## Quick build guide

Follow instructions from Wiki pages of the [ITACoreLibs](https://git.rwth-aachen.de/ita/ITACoreLibs) project.

### With room acoustic module RAVEN

To include RAVEN, access to the repository must be granted.

It is also recommended, to build RAVEN with the CMake option `ITA_RAVEN_WITH_OPENMP_ENABLED` set to `OFF`.
Otherwise, each simulator is allowed to access as many cores as available.
As a result, the audio thread might be compromised, however, the simulation also takes longer.

## Adding new filters or workers

To add filters or worker to this project, derive your from the fitting interface class and add a corresponding config struct, also derived from the fitting interface config.
In order to use the new filter or worker, it can be added to their corresponding factory class:

- at compile time by adding it to the map in the correspoding cpp file.
- at run time by calling the register function of the factory.

## License

Copyright 2019-2024 Institute of Technical Acoustics, RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
