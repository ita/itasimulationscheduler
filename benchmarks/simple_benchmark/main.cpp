#include "ITA/SimulationScheduler/profiler.h"

#include <CLI/App.hpp>
#include <CLI/Config.hpp>
#include <CLI/Formatter.hpp>
#include <ITA/SimulationScheduler/dummy_worker.h>
#include <ITA/SimulationScheduler/scheduler.h>
#include <filesystem>
#include <memory>

#ifndef WITH_PROFILER
#	error This Benchmark needs the profiler activated
#endif

VistaVector3D VistaVectorFromJSON( const nlohmann::json& data )
{
	assert( data.is_object( ) );

	return VistaVector3D( data.at( "x" ), data.at( "y" ), data.at( "z" ) );
}

std::unique_ptr<ITA::SimulationScheduler::C3DObject> ObjectFromJSON( const nlohmann::json& positionData, const nlohmann::json& forwardData,
                                                                     ITA::SimulationScheduler::C3DObject::Type type )
{
	const auto position = VistaVectorFromJSON( positionData );
	const auto forward  = VistaVectorFromJSON( forwardData );

	VistaQuaternion orientation;
	orientation.SetFromViewAndUpDir( forward, VistaVector3D( 0, 1, 0 ) );

	return std::make_unique<ITA::SimulationScheduler::C3DObject>( position, orientation, type, 0 );
}

int main( int argc, char** argv )
{
	PROFILER_NAME_THREAD_MAIN( );
	PROFILER_EVENT_COUNT( "START" );

	CLI::App app {
		R"(
Benchmark that uses a simple local scheduler with (a) dummy worker.
As such, the system can be benchmarked using the profiler.

A filter network can be given via json file.

Its update rate is given via the command line as well as the time the worker(s) need to simulate.

The movement of the single source and single receiver can be input through a json file.
The format is: {"sourceMovement":   [{"x":__,"y":__,"z":__},...],
                "receiverMovement": [{"x":__,"y":__,"z":__},...],
                "sourceForward":    [{"x":__,"y":__,"z":__},...],
                "receiverForward":  [{"x":__,"y":__,"z":__},...],
                "rate":             __
               }
Where rate is the rate of the sample points. Resampling to the update rate is done via sample and hold.
		)"
	};

	std::string networkConfigFilename = "./filter_network_config.json";
	app.add_option( "-c,--network-config", networkConfigFilename, "Filter network configuration file. Default: " + networkConfigFilename )->check( CLI::ExistingFile );

	std::string movementFilename = "./movement.json";
	app.add_option( "-m,--movement", movementFilename, "Movement data file. Default: " + movementFilename )->check( CLI::ExistingFile );

	std::string profileOutput = "./profile.json";
	app.add_option( "-o,--output", profileOutput, "Movement data file. Default: " + profileOutput )->check( !CLI::ExistingFile );

	int workerCount = 1;
	app.add_option( "-w,--worker-count", workerCount, "Number of workers. Default: " + std::to_string( workerCount ) )->check( CLI::PositiveNumber );

	double workerDuration;
	app.add_option( "-d,--worker-duration", workerDuration, "Worker 'simulation' duration." )->required( )->check( CLI::PositiveNumber );

	double updateRate = 44100. / 512.; // audio interface sampling rate / buffer length
	app.add_option( "-r,--update-rate", updateRate, "Update rate in seconds for the scheduler. Default: " + std::to_string( updateRate ) + "s" )
	    ->check( CLI::PositiveNumber );

	CLI11_PARSE( app, argc, argv );

	const std::filesystem::path movementDataFile( movementFilename );

	if( movementDataFile.extension( ) != ".json" )
	{
		std::cerr << "Movement data file has to be a json file.\n";
		return 1;
	}

	if( !exists( movementDataFile ) )
		return app.exit( CLI::FileError::Missing( movementDataFile.generic_string( ) ) );

	const std::filesystem::path networkConfigFile( networkConfigFilename );

	if( networkConfigFile.extension( ) != ".json" )
	{
		std::cerr << "Config file has to be a json file.\n";
		return 1;
	}

	if( !exists( networkConfigFile ) )
		return app.exit( CLI::FileError::Missing( networkConfigFile.generic_string( ) ) );

	ITA::SimulationScheduler::CScheduler::LocalSchedulerConfig schedulerConfig;

	for( int i = 0; i < workerCount; ++i )
	{
		auto workerConfig   = std::make_shared<ITA::SimulationScheduler::CDummyWorker::DummyWorkerConfig>( );
		workerConfig->dTime = workerDuration;
		schedulerConfig.vpWorkerConfigs.push_back( workerConfig );
	}

	schedulerConfig.oFilterNetworkConfig.Load( ITA::SimulationScheduler::Utils::JSONConfigUtils::LoadJSONConfig( networkConfigFile.string( ) ) );

	std::ifstream movementFileStream( movementDataFile );

	auto movementJson = nlohmann::json::parse( movementFileStream );

	movementFileStream.close( );

	if( !movementJson.contains( "sourceMovement" ) || !movementJson.contains( "receiverMovement" ) || !movementJson.contains( "sourceForward" ) ||
	    !movementJson.contains( "receiverForward" ) || !movementJson.contains( "rate" ) )
	{
		std::cerr << "Movement data does not contain the correct keys.\n";
		return 1;
	}

	if( !( movementJson.at( "sourceMovement" ).size( ) == movementJson.at( "receiverMovement" ).size( ) &&
	       movementJson.at( "sourceMovement" ).size( ) == movementJson.at( "sourceForward" ).size( ) &&
	       movementJson.at( "sourceMovement" ).size( ) == movementJson.at( "receiverForward" ).size( ) ) )
	{
		std::cerr << "Movement data does not contain the same number of points for all keys.\n";
		return 1;
	}

	const auto numberOfPoints          = movementJson.at( "sourceMovement" ).size( );
	const auto originalDuration        = static_cast<double>( numberOfPoints ) / movementJson.at( "rate" ).get<double>( );
	const auto resampledNumberOfPoints = static_cast<std::size_t>( std::round( originalDuration * updateRate ) );

	std::vector<std::pair<std::unique_ptr<ITA::SimulationScheduler::C3DObject>, std::unique_ptr<ITA::SimulationScheduler::C3DObject>>> movementDataPoints;
	movementDataPoints.reserve( resampledNumberOfPoints );

	for( std::size_t i = 0; i < resampledNumberOfPoints; i++ )
	{
		const auto indexMappedOriginal =
		    std::min( numberOfPoints, static_cast<std::size_t>( std::floor( static_cast<double>( i ) / updateRate * movementJson.at( "rate" ).get<double>( ) ) ) );

		movementDataPoints.emplace_back( ObjectFromJSON( movementJson.at( "sourceMovement" )[indexMappedOriginal],
		                                                 movementJson.at( "sourceForward" )[indexMappedOriginal], ITA::SimulationScheduler::C3DObject::Type::source ),
		                                 ObjectFromJSON( movementJson.at( "receiverMovement" )[indexMappedOriginal],
		                                                 movementJson.at( "receiverForward" )[indexMappedOriginal],
		                                                 ITA::SimulationScheduler::C3DObject::Type::receiver ) );
	}

	ITA::SimulationScheduler::CScheduler scheduler( schedulerConfig );

	{
		const auto start = std::chrono::high_resolution_clock::now( );
		while( std::chrono::duration<double>( std::chrono::high_resolution_clock::now( ) - start ).count( ) < 0.1 )
		{
		}
	}

	const auto begin = std::chrono::high_resolution_clock::now( );

	for( auto&& [sourceReceiverPair, index]: ITA::SimulationScheduler::Utils::Enumerate( movementDataPoints ) )
	{
		auto&& [source, receiver] = sourceReceiverPair;
		auto update               = std::make_unique<ITA::SimulationScheduler::CUpdateScene>( );
		update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );
		scheduler.PushUpdate( std::move( update ) );

		const auto start = std::chrono::high_resolution_clock::now( );

		std::cout << "\r" << static_cast<int>( static_cast<double>( index ) / static_cast<double>( movementDataPoints.size( ) ) * 100. ) << '%';

		while( std::chrono::duration<double>( std::chrono::high_resolution_clock::now( ) - start ).count( ) < 1 / updateRate )
		{
		}
	}

	while( scheduler.IsBusy( ) )
	{
	}

	std::cout << "\r100%\nElapsed time: " << std::chrono::duration<double>( std::chrono::high_resolution_clock::now( ) - begin ).count( ) << "s\n";

	ITA::SimulationScheduler::StoreProfilerData( profileOutput );

	return 0;
}