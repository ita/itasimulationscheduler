%% clear
clc
clear

%% load stuff

load('RWTHColorsSorted.mat');

files = dir('filterEvalResults*.txt');

%% start

content = fileread(files(1).name);

lines = splitlines(content);

lines = strrep(lines,': avg=',', ');
lines = strrep(lines,'stddev=','');
lines = strrep(lines,'min=','');
lines = strrep(lines,'max=','');
lines = strrep(lines,'cycles=','');
lines = strrep(lines,'ns,','e-9,');
lines = strrep(lines,'us,','e-6,');
lines = strrep(lines,'ms,','e-3,');
lines = strrep(lines,'s','');

data = split(lines,', ');

tab = cell2table(data,...
    'VariableNames',{'name','mean','stddev','min','max','cycles'});

for i = 2:width(tab)
    if iscell(tab.(i))
        tab.(i) = str2double(tab.(i));
    end
end

%% plot

yAxisDimension = 1e-6;

h = figure;

colororder(RWTHColorsSorted);

h.Position = [0, 0, 1440, 1080] * .9;

b = bar(tab.mean / yAxisDimension);

xticklabels(tab.name)
xtickangle(45)

hold on;

xtips = b.XEndPoints;
ytips = b.YEndPoints;

lab = string(num2str(tab.mean,'%10.2e'));

errorbar(xtips,reshape(tab.mean / yAxisDimension,1,[]),reshape(tab.stddev / yAxisDimension,1,[]),'k','linestyle','none','linewidth',1)

tmpY = ylim;

text(xtips(ytips > 0),repmat(tmpY(2),size(xtips)),lab,'HorizontalAlignment','center',...
    'VerticalAlignment','middle')

ylim(ylim * 1.1);
yticks = yticks / yAxisDimension;
ylabel('Filter Evaluation Runtime in us')
title('Filter Evaluation Runtime Comparison');

%%  save fig 

savefig(h,'RuntimesNoProfiler.fig','compact');
exportgraphics(h,'RuntimesNoProfiler.png');
exportgraphics(h,'RuntimesNoProfiler.pdf');

close all;

%% start

content = fileread(files(2).name);

lines = splitlines(content);

lines = strrep(lines,': avg=',', ');
lines = strrep(lines,'stddev=','');
lines = strrep(lines,'min=','');
lines = strrep(lines,'max=','');
lines = strrep(lines,'cycles=','');
lines = strrep(lines,'ns,','e-9,');
lines = strrep(lines,'us,','e-6,');
lines = strrep(lines,'ms,','e-3,');
lines = strrep(lines,'s','');

data = split(lines,', ');

tab = cell2table(data,...
    'VariableNames',{'name','mean','stddev','min','max','cycles'});

for i = 2:width(tab)
    if iscell(tab.(i))
        tab.(i) = str2double(tab.(i));
    end
end

%% plot

yAxisDimension = 1e-6;

h = figure;

colororder(RWTHColorsSorted);

h.Position = [0, 0, 1440, 1080] * .9;

b = bar(tab.mean / yAxisDimension);

xticklabels(tab.name)
xtickangle(45)

hold on;

xtips = b.XEndPoints;
ytips = b.YEndPoints;

lab = string(num2str(tab.mean,'%10.2e'));

errorbar(xtips,reshape(tab.mean / yAxisDimension,1,[]),reshape(tab.stddev / yAxisDimension,1,[]),'k','linestyle','none','linewidth',1)

tmpY = ylim;

text(xtips(ytips > 0),repmat(tmpY(2),size(xtips)),lab,'HorizontalAlignment','center',...
    'VerticalAlignment','middle')

ylim(ylim * 1.1);
yticks = yticks / yAxisDimension;
ylabel('Filter Evaluation Runtime in us')
title('Filter Evaluation Runtime Comparison');

%%  save fig 

savefig(h,'RuntimesWithProfiler.fig','compact');
exportgraphics(h,'RuntimesWithProfiler.png');
exportgraphics(h,'RuntimesWithProfiler.pdf');

close all;

%% start

content = fileread(files(3).name);

lines = splitlines(content);

lines = strrep(lines,': avg=',', ');
lines = strrep(lines,'stddev=','');
lines = strrep(lines,'min=','');
lines = strrep(lines,'max=','');
lines = strrep(lines,'cycles=','');
lines = strrep(lines,'ns,','e-9,');
lines = strrep(lines,'us,','e-6,');
lines = strrep(lines,'ms,','e-3,');
lines = strrep(lines,'s','');

data = split(lines,', ');

tab = cell2table(data,...
    'VariableNames',{'name','mean','stddev','min','max','cycles'});

for i = 2:width(tab)
    if iscell(tab.(i))
        tab.(i) = str2double(tab.(i));
    end
end

%% plot

yAxisDimension = 1e-6;

h = figure;

colororder(RWTHColorsSorted);

h.Position = [0, 0, 1440, 1080] * .9;

b = bar(tab.mean / yAxisDimension);

xticklabels(tab.name)
xtickangle(45)

hold on;

xtips = b.XEndPoints;
ytips = b.YEndPoints;

lab = string(num2str(tab.mean,'%10.2e'));

errorbar(xtips,reshape(tab.mean / yAxisDimension,1,[]),reshape(tab.stddev / yAxisDimension,1,[]),'k','linestyle','none','linewidth',1)

tmpY = ylim;

text(xtips(ytips > 0),repmat(tmpY(2),size(xtips)),lab,'HorizontalAlignment','center',...
    'VerticalAlignment','middle')

ylim(ylim * 1.1);
yticks = yticks / yAxisDimension;
ylabel('Filter Evaluation Runtime in us')
title('Filter Evaluation Runtime Comparison');

%%  save fig 

savefig(h,'RuntimesWithProfilerOpt.fig','compact');
exportgraphics(h,'RuntimesWithProfilerOpt.png');
exportgraphics(h,'RuntimesWithProfilerOpt.pdf');

close all;



