cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (filter_evaluation_time)

add_executable (${PROJECT_NAME} main.cpp)
target_link_libraries (${PROJECT_NAME} PRIVATE ITASimulationScheduler::ITASimulationScheduler)

set_property (TARGET ${PROJECT_NAME} PROPERTY FOLDER "Benchmarks/ITASimulationScheduler")

install (TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
