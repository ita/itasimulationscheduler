#include <ITA/SimulationScheduler/3d_object.h>
#include <ITA/SimulationScheduler/AudibilityFilter/distance_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/perceptive_rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/translation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/zone_filter.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <ITAStopWatch.h>
#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>


using namespace ITA::SimulationScheduler;
using namespace AudibilityFilter;


int main( )
{
	const int runs = 500000;

	CUpdateScene prevUpdate;
	CUpdateScene newUpdate;

	C3DObject source( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	C3DObject receiver( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );

	prevUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiver ) );
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiver ) );

	CRateFilter::RateFilterConfig rateFilterConfig;
	CRateFilter rateFilter( rateFilterConfig );

	CPerceptiveRotationFilter::PerceptiveRotationFilterConfig perceptiveRotationFilterConfig;
	CPerceptiveRotationFilter perceptiveRotationFilter( perceptiveRotationFilterConfig );

	CDistanceFilter::DistanceFilterConfig distanceFilterConfig;
	CDistanceFilter distanceFilter( distanceFilterConfig );

	CRotationFilter::RotationFilterConfig rotationFilterConfig;
	rotationFilterConfig.eMode = CRotationFilter::RotationModes::absolute;
	CRotationFilter absoluteRotationFilter( rotationFilterConfig );
	rotationFilterConfig.eMode = CRotationFilter::RotationModes::relative;
	CRotationFilter relativeRotationFilter( rotationFilterConfig );

	CTranslationFilter::TranslationFilterConfig translationFilterConfig;
	translationFilterConfig.eMode = CTranslationFilter::TranslationModes::absolute;
	CTranslationFilter absoluteTranslationFilter( translationFilterConfig );
	translationFilterConfig.eMode = CTranslationFilter::TranslationModes::relative;
	CTranslationFilter relativeTranslationFilter( translationFilterConfig );

	CZoneFilter::ZoneFilterConfig zoneFilterConfig;
	CZoneFilter zeroZoneFilter( zoneFilterConfig );
	zoneFilterConfig.vpZones.emplace_back( VistaVector3D( 0, 0, 0 ), VistaVector3D( 1, 1, 1 ) );
	CZoneFilter oneZoneFilter( zoneFilterConfig );
	zoneFilterConfig.vpZones.emplace_back( VistaVector3D( 0, 0, 0 ), VistaVector3D( 1, 1, 1 ) );
	CZoneFilter twoZoneFilter( zoneFilterConfig );
	zoneFilterConfig.vpZones.emplace_back( VistaVector3D( 0, 0, 0 ), VistaVector3D( 1, 1, 1 ) );
	CZoneFilter threeZoneFilter( zoneFilterConfig );

	ITAStopWatch rateFilterSW;
	ITAStopWatch perceptiveRotationFilterSW;
	ITAStopWatch distanceFilterSW;
	ITAStopWatch absoluteRotationFilterSW;
	ITAStopWatch relativeRotationFilterSW;
	ITAStopWatch absoluteTranslationFilterSW;
	ITAStopWatch relativeTranslationFilterSW;
	ITAStopWatch zeroZoneFilterSW;
	ITAStopWatch oneZoneFilterSW;
	ITAStopWatch twoZoneFilterSW;
	ITAStopWatch threeZoneFilterSW;

	std::vector<std::pair<IAudibilityFilter*, ITAStopWatch*>> filterVector;
	filterVector.emplace_back( &rateFilter, &rateFilterSW );
	filterVector.emplace_back( &perceptiveRotationFilter, &perceptiveRotationFilterSW );
	filterVector.emplace_back( &distanceFilter, &distanceFilterSW );
	filterVector.emplace_back( &absoluteRotationFilter, &absoluteRotationFilterSW );
	filterVector.emplace_back( &relativeRotationFilter, &relativeRotationFilterSW );
	filterVector.emplace_back( &absoluteTranslationFilter, &absoluteTranslationFilterSW );
	filterVector.emplace_back( &relativeTranslationFilter, &relativeTranslationFilterSW );
	filterVector.emplace_back( &zeroZoneFilter, &zeroZoneFilterSW );
	filterVector.emplace_back( &oneZoneFilter, &oneZoneFilterSW );
	filterVector.emplace_back( &twoZoneFilter, &twoZoneFilterSW );
	filterVector.emplace_back( &threeZoneFilter, &threeZoneFilterSW );

	for( auto& filter: filterVector )
	{
		for( int i = 0; i < runs; ++i )
		{
			filter.second->start( );
			auto result = filter.first->ChangeIsAudible( prevUpdate, newUpdate );
			filter.second->stop( );
		}
	}

	std::stringstream ss;

	ss << "Rate Filter: " << rateFilterSW.ToString( ) << "\n";
	ss << "Perceptive Rotation: " << perceptiveRotationFilterSW.ToString( ) << "\n";
	ss << "Distance Filter: " << distanceFilterSW.ToString( ) << "\n";
	ss << "Absolute RotationFilter: " << absoluteRotationFilterSW.ToString( ) << "\n";
	ss << "RelativeRotation Filter: " << relativeRotationFilterSW.ToString( ) << "\n";
	ss << "Absolute Translation Filter: " << absoluteTranslationFilterSW.ToString( ) << "\n";
	ss << "Relative Translation Filter: " << relativeTranslationFilterSW.ToString( ) << "\n";
	ss << "Zero Zone Filter: " << zeroZoneFilterSW.ToString( ) << "\n";
	ss << "One Zone Filter: " << oneZoneFilterSW.ToString( ) << "\n";
	ss << "Two Zone Filter: " << twoZoneFilterSW.ToString( ) << "\n";
	ss << "Three Zone Filter: " << threeZoneFilterSW.ToString( );

	std::cout << ss.str( );

	std::ofstream myfile;
	myfile.open( "filterEvalResults.txt" );
	myfile << ss.str( );
	myfile.close( );

	return 0;
}