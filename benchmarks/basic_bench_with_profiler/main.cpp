#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>
#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>
#include <ITA/SimulationScheduler/RoomAcoustics/raven/worker_thread.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/utils/utils.h>
#include <VistaBase/VistaTimeUtils.h>
#include <filesystem>

using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;
using namespace AudibilityFilter;
using namespace Raven;
using namespace IHTA::Profiler;

struct ResultDump : IResultHandler
{
	void PostResultReceived( std::unique_ptr<CSimulationResult> result ) override
	{
		resultVector.push_back( result.release( ) );

		const auto rirResult = dynamic_cast<const CRIRSimulationResult*>( resultVector.back( ) );

		if( rirResult )
		{
			if( rirResult->eResultType == FieldOfDuty::directSound )
				dsResultVector.push_back( rirResult );
			if( rirResult->eResultType == FieldOfDuty::earlyReflections )
				erResultVector.push_back( rirResult );
			if( rirResult->eResultType == FieldOfDuty::diffuseDecay )
				ddResultVector.push_back( rirResult );
		}
	}

	std::vector<CSimulationResult*> resultVector = std::vector<CSimulationResult*>( );

	std::vector<const CRIRSimulationResult*> dsResultVector;
	std::vector<const CRIRSimulationResult*> erResultVector;
	std::vector<const CRIRSimulationResult*> ddResultVector;
};

int main( )
{
	PROFILER_NAME_THREAD_MAIN( );
	PROFILER_EVENT_COUNT( "START" );

	auto oConfig = CMasterSimulationController::MasterSimulationControllerConfig( );

	auto schedulerConfig = std::make_shared<CScheduler::LocalSchedulerConfig>( );

	auto workerConfig                   = std::make_shared<CWorkerThread::WorkerThreadConfig>( );
	workerConfig->sRavenProjectFilePath = "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/referenceNEW.rpf";

	schedulerConfig->vpWorkerConfigs.push_back( workerConfig );

	auto filterConfig         = std::make_shared<CRateFilter::RateFilterConfig>( );
	filterConfig->dRate       = 0.5;
	filterConfig->sFilterName = "Filter";

	CFilterNetwork::FilterNetworkConfig networkConfig;
	networkConfig.vpFilterConfigs.push_back( filterConfig );
	networkConfig.sStartFilter = filterConfig->sFilterName;

	schedulerConfig->oFilterNetworkConfig = networkConfig;

	oConfig.oDSSchedulerConfig = schedulerConfig;
	oConfig.oERSchedulerConfig = schedulerConfig;
	oConfig.oDDSchedulerConfig = schedulerConfig;

	std::ofstream myfile;
	myfile.open( "CompleteTest_Master_Test_Config.json" );
	myfile << Utils::JSONConfigUtils::WriteVistaPropertyListToJSON( oConfig.Store( ) ).dump( );
	myfile.close( );

	auto masterController = std::make_unique<CMasterSimulationController>( oConfig );

	const auto resultHandler = std::make_unique<ResultDump>( );

	masterController->AttachResultHandler( resultHandler.get( ) );

	auto update   = std::make_unique<CUpdateScene>( );
	auto source   = std::make_unique<C3DObject>( VistaVector3D( 1, 1.5, 1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 1 );
	auto receiver = std::make_unique<C3DObject>( VistaVector3D( -0.5, 1.5, -0.5 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 1 );

	update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

	masterController->PushUpdate( std::move( update ) );

	// We need to wait a bit for the workers to start working
	VistaTimeUtils::Sleep( 500 );

	// wait till we have all results
	while( resultHandler->resultVector.size( ) != 3 )
		VistaTimeUtils::Sleep( 250 );


	StoreProfilerData( "profiler.json" );

	return 0;
}