// Header include
#include <ITA/SimulationScheduler/simulation_result.h>

// simulation scheduler include
#include <ITA/SimulationScheduler/Utils/utils.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		int CSimulationResult::Serialize( IVistaSerializer& pSerializer ) const
		{
			int returnVal = 0;

			returnVal += pSerializer.WriteSerializable( *sourceReceiverPair.source );
			returnVal += pSerializer.WriteSerializable( *sourceReceiverPair.receiver );

			return returnVal;
		}

		int CSimulationResult::DeSerialize( IVistaDeSerializer& pDeserializer )
		{
			int returnVal = 0;

			auto source   = new C3DObject( );
			auto receiver = new C3DObject( );
			returnVal += pDeserializer.ReadSerializable( *source );
			sourceReceiverPair.source = source;
			returnVal += pDeserializer.ReadSerializable( *receiver );
			sourceReceiverPair.receiver = receiver;

			return returnVal;
		}

		std::string CSimulationResult::GetSignature( ) const { return "CSimulationResult"; }

		std::unique_ptr<CSimulationResult> CSimulationResult::Clone( ) const { return std::make_unique<CSimulationResult>( *this ); }
	} // namespace SimulationScheduler
} // namespace ITA