// Header include
#include <ITA/SimulationScheduler/update_scene.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

// ITA includes
#include <ITAException.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		CUpdateScene::CUpdateScene( double dTimeStamp ) : m_dTimeStamp( dTimeStamp ) {}

		CUpdateScene::CUpdateScene( const CUpdateScene& other ) : IUpdateMessage( other ), m_dTimeStamp( other.m_dTimeStamp )
		{
			if( other.m_pSource )
				m_pSource = std::make_unique<C3DObject>( *other.m_pSource );
			if( other.m_pReceiver )
				m_pReceiver = std::make_unique<C3DObject>( *other.m_pReceiver );
		}
		CUpdateScene& CUpdateScene::operator=( const CUpdateScene& other )
		{
			if( this == &other )
				return *this;
			IUpdateMessage::operator=( other );
			if( other.m_pSource )
				m_pSource = std::make_unique<C3DObject>( *other.m_pSource );
			if( other.m_pReceiver )
				m_pReceiver = std::make_unique<C3DObject>( *other.m_pReceiver );
			m_dTimeStamp = other.m_dTimeStamp;
			return *this;
		}
		SourceReceiverPair CUpdateScene::GetSourceReceiverPair( ) const
		{
			if( m_pReceiver == nullptr, m_pSource == nullptr )
				ITA_EXCEPT_INVALID_PARAMETER( "Source and receiver is not set." )

			SourceReceiverPair pair;

			pair.source   = m_pSource.get( );
			pair.receiver = m_pReceiver.get( );

			return pair;
		}
		void CUpdateScene::SetSourceReceiverPair( std::unique_ptr<C3DObject> pSource, std::unique_ptr<C3DObject> pReceiver )
		{
			if( pSource->GetType( ) == C3DObject::Type::source )
				m_pSource = std::move( pSource );
			else
				ITA_EXCEPT_INVALID_PARAMETER( "Wrong type given" );

			if( pReceiver->GetType( ) == C3DObject::Type::receiver )
				m_pReceiver = std::move( pReceiver );
			else
				ITA_EXCEPT_INVALID_PARAMETER( "Wrong type given" );
		}
		double ITA::SimulationScheduler::CUpdateScene::GetTimeStamp( ) const { return m_dTimeStamp; }
		bool CUpdateScene::IsEqualTolerance( const CUpdateScene& obj, double dTolerance ) const
		{
			return m_pSource->IsEqualTolerance( *obj.m_pSource, dTolerance ) && m_pReceiver->IsEqualTolerance( *obj.m_pReceiver, dTolerance );
		}
		int CUpdateScene::Serialize( IVistaSerializer& pSerializer ) const
		{
			int returnVal = 0;

			returnVal += pSerializer.WriteInt32( m_iID );
			returnVal += pSerializer.WriteFloat64( m_dTimeStamp );
			returnVal += pSerializer.WriteSerializable( *m_pReceiver );
			returnVal += pSerializer.WriteSerializable( *m_pSource );

			return returnVal;
		}
		int CUpdateScene::DeSerialize( IVistaDeSerializer& pDeserializer )
		{
			int returnVal = 0;

			returnVal += pDeserializer.ReadInt32( m_iID );
			returnVal += pDeserializer.ReadFloat64( m_dTimeStamp );
			if( m_pReceiver == nullptr && m_pSource == nullptr )
			{
				C3DObject tmp;
				returnVal += pDeserializer.ReadSerializable( tmp );
				m_pReceiver = std::make_unique<C3DObject>( tmp );

				returnVal += pDeserializer.ReadSerializable( tmp );
				m_pSource = std::make_unique<C3DObject>( tmp );
			}
			else
			{
				returnVal += pDeserializer.ReadSerializable( *m_pReceiver );
				returnVal += pDeserializer.ReadSerializable( *m_pSource );
			}

			return returnVal;
		}
		std::string CUpdateScene::GetSignature( ) const { return "CUpdateScene"; }
	} // namespace SimulationScheduler
} // namespace ITA