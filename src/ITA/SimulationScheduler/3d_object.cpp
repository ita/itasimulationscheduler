// Header include
#include <ITA/SimulationScheduler/3d_object.h>

// std includes
#include <cmath>

// simulation scheduler include
#include "../src/ITA/SimulationScheduler/serialization_helper.h"

#include <ITA/SimulationScheduler/Utils/utils.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		C3DObject::C3DObject( const VistaVector3D& position, const VistaQuaternion& orientation, const Type type, const int id )
		    : m_iID( id )
		    , m_eType( type )
		    , m_v3Position( position )
		    , m_qOrientation( orientation )
		{
		}
		const VistaVector3D& C3DObject::GetPosition( ) const { return m_v3Position; }
		const VistaQuaternion& C3DObject::GetOrientation( ) const { return m_qOrientation; }
		unsigned int C3DObject::GetId( ) const { return m_iID; }
		C3DObject::Type C3DObject::GetType( ) const { return m_eType; }
		bool C3DObject::IsEqualTolerance( const C3DObject& obj, const double dTolerance ) const
		{
			if( obj.m_eType == m_eType && obj.m_iID == m_iID )
			{
				return ( abs( m_v3Position[0] - obj.m_v3Position[0] ) <= dTolerance && abs( m_v3Position[1] - obj.m_v3Position[1] ) <= dTolerance &&
				         abs( m_v3Position[2] - obj.m_v3Position[2] ) <= dTolerance && abs( m_v3Position[3] - obj.m_v3Position[3] ) <= dTolerance ) &&
				       ( abs( m_qOrientation[0] - obj.m_qOrientation[0] ) <= dTolerance && abs( m_qOrientation[1] - obj.m_qOrientation[1] ) <= dTolerance &&
				         abs( m_qOrientation[2] - obj.m_qOrientation[2] ) <= dTolerance && abs( m_qOrientation[3] - obj.m_qOrientation[3] ) <= dTolerance );
			}
			return false;
		}
		int C3DObject::Serialize( IVistaSerializer& pSerializer ) const
		{
			int returnVal = 0;
			returnVal += SerializationHelper::WriteVistaVector3D( pSerializer, m_v3Position );

			returnVal += SerializationHelper::WriteVistaQuaternion( pSerializer, m_qOrientation );

			returnVal += pSerializer.WriteInt32( m_iID );

			returnVal += pSerializer.WriteInt32( AsInteger( m_eType ) );

			return returnVal;
		}
		int C3DObject::DeSerialize( IVistaDeSerializer& pDeserializer )
		{
			int returnVal = 0;
			int tmp;
			returnVal += SerializationHelper::ReadVistaVector3D( pDeserializer, m_v3Position );

			returnVal += SerializationHelper::ReadVistaQuaternion( pDeserializer, m_qOrientation );

			returnVal += pDeserializer.ReadInt32( m_iID );

			returnVal += pDeserializer.ReadInt32( tmp );
			m_eType = Type( tmp );

			return returnVal;
		}
		std::string C3DObject::GetSignature( ) const { return "C3DObject"; }
	} // namespace SimulationScheduler
} // namespace ITA
