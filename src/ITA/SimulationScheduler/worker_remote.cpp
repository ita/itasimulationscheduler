// Header include
#include "ITA/SimulationScheduler/worker_remote.h"

#include "configuration_keys.h"
#include "remote-worker.grpc.pb.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>
#include <ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>
#include <ITADebug.h>
#include <ITAException.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/security/credentials.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		///
		/// \brief Decoders.
		///
		/// Transform the ITASimulationScheduler data structures to gRPC data structures.
		/// \{
		C3DObject* decode( const RemoteWorkerRPC::Object3D& objIn )
		{
			const VistaVector3D position( objIn.position( ).x( ), objIn.position( ).y( ), objIn.position( ).z( ) );
			const VistaQuaternion rotation( objIn.quaternion( ).x( ), objIn.quaternion( ).y( ), objIn.quaternion( ).z( ), objIn.quaternion( ).w( ) );

			C3DObject::Type type;
			if( objIn.type( ) == RemoteWorkerRPC::Object3D_Type_RECEIVER )
				type = C3DObject::Type::receiver;
			else if( objIn.type( ) == RemoteWorkerRPC::Object3D_Type_SOURCE )
				type = C3DObject::Type::source;

			const auto objOut = new C3DObject( position, rotation, type, objIn.id( ) );

			return objOut;
		}

		ITASampleFrame decode( const RemoteWorkerRPC::SampleFrame& frameIn )
		{
			ITASampleFrame frameOut( frameIn.buffers_size( ), frameIn.nsamples( ) );

			for( auto channel = 0; channel < frameOut.channels( ); channel++ )
			{
				frameOut[channel].WriteSamples( frameIn.buffers( ).Get( channel ).samples( ).data( ), frameIn.buffers( ).Get( channel ).samples_size( ) );
			}

			return frameOut;
		}

		std::unique_ptr<CSimulationResult> decode( const RemoteWorkerRPC::SimulationResult& resultIn )
		{
			if( resultIn.has_rir_simulation_result( ) )
			{
				auto resultOut = std::make_unique<RoomAcoustics::CRIRSimulationResult>( );

				switch( resultIn.rir_simulation_result( ).result_type( ) )
				{
					case RemoteWorkerRPC::RIRSimulationResult::FieldOfDuty::RIRSimulationResult_FieldOfDuty_DIRECT_SOUND:
						resultOut->eResultType = RoomAcoustics::FieldOfDuty::directSound;
						break;
					case RemoteWorkerRPC::RIRSimulationResult::FieldOfDuty::RIRSimulationResult_FieldOfDuty_EARLY_REFLECTIONS:
						resultOut->eResultType = RoomAcoustics::FieldOfDuty::earlyReflections;
						break;
					case RemoteWorkerRPC::RIRSimulationResult::FieldOfDuty::RIRSimulationResult_FieldOfDuty_DIFFUSE_DECAY:
						resultOut->eResultType = RoomAcoustics::FieldOfDuty::diffuseDecay;
						break;
					default:
						ITA_EXCEPT_INVALID_PARAMETER( "Unhandled RIR simulation result type" )
				}

				resultOut->bSameRoom = resultIn.rir_simulation_result( ).same_room( );

				resultOut->iLeadingZeros  = resultIn.ir_simulation_result( ).leading_zero( );
				resultOut->iTrailingZeros = resultIn.ir_simulation_result( ).trailing_zeros( );
				resultOut->bZerosStripped = resultIn.ir_simulation_result( ).zeros_stripped( );
				resultOut->sfResult       = decode( resultIn.ir_simulation_result( ).result( ) );

				resultOut->dTimeStamp         = resultIn.time_stamp( );
				resultOut->sourceReceiverPair = { decode( resultIn.source( ) ), decode( resultIn.receiver( ) ) };

				return std::move( resultOut );
			}

			return nullptr;
		}
		/// \}


		///
		/// \brief Encoders.
		///
		/// Transform the gRPC data structures to ITASimulationScheduler data structures.
		/// \{
		RemoteWorkerRPC::Object3D encode( const C3DObject* objIn )
		{
			RemoteWorkerRPC::Object3D objOut;

			objOut.set_id( objIn->GetId( ) );
			objOut.set_type( objIn->GetType( ) == C3DObject::Type::source ? ITA::SimulationScheduler::RemoteWorkerRPC::Object3D_Type_SOURCE :
                                                                            ITA::SimulationScheduler::RemoteWorkerRPC::Object3D_Type_RECEIVER );

			objOut.mutable_position( )->set_x( objIn->GetPosition( )[0] );
			objOut.mutable_position( )->set_y( objIn->GetPosition( )[1] );
			objOut.mutable_position( )->set_z( objIn->GetPosition( )[2] );

			objOut.mutable_quaternion( )->set_x( objIn->GetOrientation( )[0] );
			objOut.mutable_quaternion( )->set_y( objIn->GetOrientation( )[1] );
			objOut.mutable_quaternion( )->set_z( objIn->GetOrientation( )[2] );
			objOut.mutable_quaternion( )->set_w( objIn->GetOrientation( )[3] );

			return objOut;
		}
		/// \}

		///
		/// \brief Remote worker implementation.
		///
		class CWorkerRemote::Impl
		{
		public:
			///
			/// \brief Constructor for the Impl.
			/// \param pParent the parent scheduler of the worker.
			/// \param channel the gRPC channel used to communicate with the remote computer..
			///
			Impl( ISchedulerInterface* pParent, std::shared_ptr<::grpc::Channel> channel )
			    : m_pStub( RemoteWorkerRPC::RemoteWorker::NewStub( channel ) )
			    , m_pParentScheduler( pParent )
			{
				m_tSimulationResultReceiveThread = std::thread(
				    [this]( )
				    {
					    ::grpc::ClientContext context;
					    google::protobuf::Empty argument;
					    RemoteWorkerRPC::SimulationResult result;
					    const std::unique_ptr<::grpc::ClientReader<RemoteWorkerRPC::SimulationResult>> reader( m_pStub->ResultFinished( &context, argument ) );
					    while( reader->Read( &result ) )
					    {
						    if( this->m_pParentScheduler )
							    this->m_pParentScheduler->HandleSimulationFinished( decode( result ) );
					    }
					    ::grpc::Status status = reader->Finish( );

					    if( !status.ok( ) )
					    {
						    ITA_EXCEPT1( ITAException::UNKNOWN, status.error_message( ) );
					    }
				    } );
			}

			///
			/// \brief Destructor for the Impl.
			///
			/// Sends the shutdown command to the remote computer and waits for the simulation receive thread to finish
			/// (which should happen when the remote computer is shut down).
			///
			~Impl( )
			{
				shutdown( );
				m_tSimulationResultReceiveThread.join( );
			}

			///
			/// \brief Implementation of CWorkerRemote::pushUpdate.
			///
			/// Encodes the update via protobuf and sends it to the remote computer to be simulated.
			/// \param pUpdate the new update for the worker.
			///
			void pushUpdate( std::unique_ptr<CUpdateScene> pUpdate ) const
			{
				RemoteWorkerRPC::UpdateMessage update;
				update.set_id( pUpdate->GetID( ) );
				auto&& sceneUpdate = update.mutable_scene( );
				sceneUpdate->set_time_stamp( pUpdate->GetTimeStamp( ) );

				sceneUpdate->mutable_source( )->CopyFrom( encode( pUpdate->GetSourceReceiverPair( ).source ) );
				sceneUpdate->mutable_receiver( )->CopyFrom( encode( pUpdate->GetSourceReceiverPair( ).receiver ) );

				google::protobuf::Empty reply;
				::grpc::ClientContext context;
				m_pStub->PushUpdate( &context, update, &reply );
			}

			///
			/// \brief Initiate the workers on the remote computer.
			///
			/// Sends the configuration for the worker to the remote computer to initiate them.
			///
			[[nodiscard]] bool initWorker( const WorkerRemoteConfig& config ) const
			{
				ITA::SimulationScheduler::RemoteWorkerRPC::InitMessage message;

				message.set_config( ITA::SimulationScheduler::Utils::JSONConfigUtils::WriteVistaPropertyListToJSON( config.Store( ) ).dump( ) );

				RemoteWorkerRPC::Reply reply;
				::grpc::ClientContext context;
				m_pStub->InitWorker( &context, message, &reply );

				return reply.success( );
			}

			///
			/// \brief Implementation of CWorkerRemote::IsBusy.
			///
			[[nodiscard]] bool isBusy( ) const
			{
				const google::protobuf::Empty argument;
				google::protobuf::BoolValue reply;
				::grpc::ClientContext context;
				m_pStub->IsBusy( &context, argument, &reply );
				return reply.value( );
			}

			///
			/// \brief Implementation of CWorkerRemote::reset.
			///
			void reset( ) const
			{
				const google::protobuf::Empty argument;
				RemoteWorkerRPC::Reply reply;
				::grpc::ClientContext context;
				m_pStub->Reset( &context, argument, &reply );

				if( !reply.success( ) )
				{
					ITA_EXCEPT1( ITAException::UNKNOWN, reply.error( ) )
				}
			}

			///
			/// \brief Implementation of CWorkerRemote::shutdown.
			///
			void shutdown( ) const
			{
				const google::protobuf::Empty argument;
				RemoteWorkerRPC::Reply reply;
				::grpc::ClientContext context;
				m_pStub->Shutdown( &context, argument, &reply );

				if( !reply.success( ) )
				{
					ITA_EXCEPT1( ITAException::UNKNOWN, reply.error( ) )
				}
			}

		private:
			///
			/// \brief Pointer to the RPC stub.
			///
			std::unique_ptr<ITA::SimulationScheduler::RemoteWorkerRPC::RemoteWorker::Stub> m_pStub;

			///
			/// \brief Thread handling the receiving of results from the remote worker and passing it tho the scheduler.
			///
			/// The thread runs until the stream from the remote worker is finished.
			/// It gets joind in the destruction of Impl.
			///
			std::thread m_tSimulationResultReceiveThread;

			///
			/// \brief Pointer to the parent scheduler.
			///
			/// Via this pointer the result of the simulation is passed to the parent scheduler.
			///
			ISchedulerInterface* m_pParentScheduler;
		};

		CWorkerRemote::WorkerRemoteConfig::WorkerRemoteConfig( ) : WorkerConfig( GetType( ) ) {}

		VistaPropertyList CWorkerRemote::WorkerRemoteConfig::Store( ) const
		{
			auto oProperties = WorkerConfig::Store( );

			auto counter = 0;

			for( const auto& workerConfig: vpWorkerConfigs )
			{
				oProperties.SetValue( schedulerWorkerSectionName + std::to_string( counter++ ), workerConfig->Store( ) );
			}

			oProperties.SetValue( remoteWorkerAddress, sRemoteAddress );

			return oProperties;
		}

		void CWorkerRemote::WorkerRemoteConfig::Load( const VistaPropertyList& oProperties )
		{
			WorkerConfig::Load( oProperties );

			for( const auto& oProperty: oProperties )
			{
				if( oProperty.first.find( schedulerWorkerSectionName ) != std::string::npos && oProperty.second.GetPropertyType( ) == VistaProperty::PROPT_PROPERTYLIST )
				{
					auto configProperties = oProperty.second.GetPropertyListConstRef( );
					auto type             = configProperties.GetValue<std::string>( workerTypeKey );

					auto workerConfig = CWorkerFactory::CreateConfig( type );

					if( !workerConfig )
						ITA_EXCEPT_INVALID_PARAMETER( "Could not create simulation worker thread of type '" + type + "'" );

					workerConfig->Load( configProperties );
					vpWorkerConfigs.push_back( workerConfig );
				}
			}

			oProperties.GetValue( remoteWorkerAddress, sRemoteAddress );
		}

		CWorkerRemote::CWorkerRemote( const WorkerRemoteConfig& oConfig, ISchedulerInterface* pParent ) : IWorkerInterface( pParent )
		{
			if( oConfig.sRemoteAddress.empty( ) )
			{
				ITA_EXCEPT_INVALID_PARAMETER( "Required remote address for the CWorkerRemote was not given. Cannot contruct it." )
			}

			pImpl = std::make_unique<Impl>( m_pParentScheduler, ::grpc::CreateChannel( oConfig.sRemoteAddress, ::grpc::InsecureChannelCredentials( ) ) );

			if( !pImpl->initWorker( oConfig ) )
			{
				ITA_EXCEPT1( ITAException::UNKNOWN, "Initiaion of the remote worker faild." )
			}
		}

		CWorkerRemote::~CWorkerRemote( ) = default;

		bool CWorkerRemote::IsBusy( ) { return pImpl->isBusy( ); }

		std::string CWorkerRemote::GetType( ) { return "RemoteWorker"; }

		void CWorkerRemote::PushUpdate( std::unique_ptr<CUpdateScene> pUpdate ) { pImpl->pushUpdate( std::move( pUpdate ) ); }

		void CWorkerRemote::Reset( ) { pImpl->reset( ); }

		void CWorkerRemote::Shutdown( ) { pImpl->shutdown( ); }

		std::unique_ptr<IWorkerInterface> CWorkerRemote::CreateWorker( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
		{
			return std::make_unique<CWorkerRemote>( dynamic_cast<const WorkerRemoteConfig&>( *pConfig ), pParent );
		}

	} // namespace SimulationScheduler
} // namespace ITA