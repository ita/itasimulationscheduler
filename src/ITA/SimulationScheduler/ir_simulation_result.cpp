// Header include
#include <ITA/SimulationScheduler/ir_simulation_result.h>

// simulation scheduler include
#include "../src/ITA/SimulationScheduler/serialization_helper.h"

#include <ITA/SimulationScheduler/Utils/utils.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		int CIRSimulationResult::Serialize( IVistaSerializer& pSerializer ) const
		{
			int returnVal = 0;

			returnVal += CSimulationResult::Serialize( pSerializer );

			returnVal += SerializationHelper::WriteITASampleFrame( pSerializer, &sfResult, sfResult.GetLength( ), 0 );
			returnVal += pSerializer.WriteInt32( iLeadingZeros );
			returnVal += pSerializer.WriteInt32( iTrailingZeros );
			returnVal += pSerializer.WriteBool( bZerosStripped );

			return returnVal;
		}

		int CIRSimulationResult::DeSerialize( IVistaDeSerializer& pDeserializer )
		{
			int returnVal = 0;

			returnVal += CSimulationResult::DeSerialize( pDeserializer );

			returnVal += SerializationHelper::ReadITASampleFrame( pDeserializer, sfResult );
			returnVal += pDeserializer.ReadInt32( iLeadingZeros );
			returnVal += pDeserializer.ReadInt32( iTrailingZeros );
			returnVal += pDeserializer.ReadBool( bZerosStripped );

			return returnVal;
		}

		std::string CIRSimulationResult::GetSignature( ) const { return "CIRSimulationResult"; }

		std::unique_ptr<CSimulationResult> CIRSimulationResult::Clone( ) const { return std::make_unique<CIRSimulationResult>( *this ); }
	} // namespace SimulationScheduler
} // namespace ITA