// Header include
#include <ITA/SimulationScheduler/scheduler.h>
#include <random>

// simulation scheduler includes
#include "../src/ITA/SimulationScheduler/configuration_keys.h"
#include "../src/ITA/SimulationScheduler/replacement_filter.h"

#include <ITA/SimulationScheduler/RoomAcoustics/room_acoustics_worker_interface.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/result_handler.h>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>

// ITA include
#include <ITAException.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		std::pair<unsigned, unsigned> mapKeyFromUpdate( const std::unique_ptr<CUpdateScene>& pUpdate )
		{
			return std::make_pair( pUpdate->GetSourceReceiverPair( ).receiver->GetId( ), pUpdate->GetSourceReceiverPair( ).source->GetId( ) );
		}

		CScheduler::LocalSchedulerConfig::LocalSchedulerConfig( ) : SchedulerConfig( GetType( ) ) {}

		VistaPropertyList CScheduler::LocalSchedulerConfig::Store( ) const
		{
			VistaPropertyList oProperties = SchedulerConfig::Store( );

			auto counter = 0;

			for( const auto& workerConfig: vpWorkerConfigs )
			{
				oProperties.SetValue( schedulerWorkerSectionName + std::to_string( counter++ ), workerConfig->Store( ) );
			}

			oProperties.SetValue( schedulerFilterNetworkSectionName, oFilterNetworkConfig.Store( ) );

			oProperties.SetValue( schedulerReplaceUpdatesKey, bReplaceUpdates );

			return oProperties;
		}

		void CScheduler::LocalSchedulerConfig::Load( const VistaPropertyList& oProperties )
		{
			SchedulerConfig::Load( oProperties );

			for( const auto& oProperty: oProperties )
			{
				if( oProperty.first.find( schedulerWorkerSectionName ) != std::string::npos && oProperty.second.GetPropertyType( ) == VistaProperty::PROPT_PROPERTYLIST )
				{
					auto configProperties = oProperty.second.GetPropertyListConstRef( );
					auto type             = configProperties.GetValue<std::string>( workerTypeKey );

					auto workerConfig = CWorkerFactory::CreateConfig( type );

					if( !workerConfig )
						ITA_EXCEPT_INVALID_PARAMETER( "Could not create simulation worker thread of type '" + type + "'" );

					workerConfig->Load( configProperties );
					vpWorkerConfigs.push_back( workerConfig );
				}
			}

			VistaPropertyList filterNetworkConfig;

			oProperties.GetValue( schedulerFilterNetworkSectionName, filterNetworkConfig );

			oFilterNetworkConfig.Load( filterNetworkConfig );

			oProperties.GetValue( schedulerReplaceUpdatesKey, bReplaceUpdates );
		}

		CScheduler::CScheduler( const LocalSchedulerConfig& pConfig ) : m_evTriggerLoop( VistaThreadEvent::NON_WAITABLE_EVENT )
		{
			m_bReplaceUpdates = pConfig.bReplaceUpdates;

			for( const auto& workerConfig: pConfig.vpWorkerConfigs )
			{
				m_vWorker.push_back( std::move( CWorkerFactory::CreateWorker( workerConfig, this ) ) );
			}

			m_pFilterNetwork = std::make_unique<AudibilityFilter::CFilterNetwork>( pConfig.oFilterNetworkConfig );

			Run( );
		}

		CScheduler::~CScheduler( )
		{
			m_bStopIndicated = true;
			m_evTriggerLoop.SignalEvent( );

			while( m_bStopACK != true )
				VistaTimeUtils::Sleep( 250 );

			m_evTriggerLoop.SignalEvent( );

			StopGently( true );
		}

		void CScheduler::PushUpdate( std::unique_ptr<IUpdateMessage> pUpdateMessage )
		{
			// PROFILER_FUNCTION ( );
			PROFILER_VALUE( "Enqueued Update to " + GetThreadName( ), pUpdateMessage->GetID( ) );
			m_qUpdateQueue.push( std::move( pUpdateMessage ) );
			m_evTriggerLoop.SignalEvent( );
		}

		void CScheduler::AttachResultHandler( IResultHandler* pResultHandler ) { m_vpResultHandlers.push_back( pResultHandler ); }

		void CScheduler::DetachResultHandler( IResultHandler* pResultHandler )
		{
			const auto iterator = std::remove( m_vpResultHandlers.begin( ), m_vpResultHandlers.end( ), pResultHandler );
			m_vpResultHandlers.erase( iterator );
		}

		void CScheduler::HandleSimulationFinished( std::unique_ptr<CSimulationResult> pResult )
		{
			for( auto handler: m_vpResultHandlers )
				handler->PostResultReceived( pResult->Clone( ) );
		}

		bool CScheduler::IsBusy( ) const
		{
			auto busy = false;

			for( const auto& worker: m_vWorker )
			{
				busy |= worker->IsBusy( );
			}

			return busy;
		}

		std::string CScheduler::GetType( ) { return "LocalScheduler"; }

		void CScheduler::ProcessUpdateScene( std::unique_ptr<CUpdateScene> pUpdate )
		{
			// PROFILER_FUNCTION ( );
			PROFILER_VALUE( "Processed Update Scenes", pUpdate->GetID( ) );

			auto bAudible = true;

			// Check if a previous update exists.
			auto mapKey = mapKeyFromUpdate( pUpdate );

			if( m_mPreviousStates.find( mapKey ) != m_mPreviousStates.end( ) )
			{
				// Test if the update is audible.
				bAudible = m_pFilterNetwork->EvaluateNetwork( *m_mPreviousStates.at( mapKey ), *pUpdate );
			}

			if( bAudible )
			{
				PROFILER_VALUE( "Audible Update Scenes", pUpdate->GetID( ) );
				m_qPendingUpdateQueue.push_back( std::move( pUpdate ) );
			}
			else
			{
				PROFILER_VALUE( "Inaudible Update Scenes", pUpdate->GetID( ) );
			}
		}

		void CScheduler::ProcessUpdateConfig( std::unique_ptr<CUpdateConfig> pUpdate )
		{
			// PROFILER_FUNCTION ( );
			PROFILER_VALUE( "Processed Update Configs", pUpdate->GetID( ) );

			switch( pUpdate->GetType( ) )
			{
				case CUpdateConfig::ConfigChangeType::changeDirectivity:
					m_qPendingConfigUpdateQueue.push_back( std::move( pUpdate ) );					
					break;
				case CUpdateConfig::ConfigChangeType::changeHRTF:
					ITA_EXCEPT_NOT_IMPLEMENTED;
					break;
				case CUpdateConfig::ConfigChangeType::resetAll:
					Reset( );
					break;
				case CUpdateConfig::ConfigChangeType::shutdown:
					Shutdown( );
					break;
				default:
					ITA_EXCEPT_INVALID_PARAMETER( "Unkown type of config change" );
			}
		}

		void CScheduler::Reset( )
		{
			// signal the reset to the workers
			for( auto& worker: m_vWorker )
			{
				// todo this is very inefficient, if one worker is running a very long simulation, the workers after it in the vector are not reset. Still, the thread
				// todo loop is not running since it is waiting here.
				while( worker->IsBusy( ) )
					VistaTimeUtils::Sleep( 250 );

				worker->Reset( );
			}

			// The loop was on hold, clear all update lists.
			m_qUpdateQueue.clear( );
			m_lUpdateList.clear( );
			m_qPendingUpdateQueue.clear( );
			m_qPendingConfigUpdateQueue.clear( );

			// start the loop again.
			m_bResetIndicated = true;
			m_evTriggerLoop.SignalEvent( );
		}

		bool CScheduler::LoopBody( )
		{
			// Wait for trigger
			m_evTriggerLoop.WaitForEvent( true );
			m_evTriggerLoop.ResetThisEvent( );

			PROFILER_FUNCTION( );

			if( m_bResetIndicated )
			{
				PROFILER_EVENT_COUNT( "Reset" );
				m_bResetIndicated = false;
			}

			// Handle the stop of the loop
			if( m_bStopIndicated )
			{
				IndicateLoopEnd( );
				PROFILER_EVENT_COUNT( "Loop Stop" );
				m_bStopACK = true;
				return false;
			}

			// get the updates from the queue to a list.
			std::unique_ptr<IUpdateMessage> updateMessage;

			PROFILER_SECTION( "Updates queue -> list" );
			while( m_qUpdateQueue.try_pop( updateMessage ) )
			{
				m_lUpdateList.push_back( std::move( updateMessage ) );
			}
			PROFILER_END_SECTION( )

			PROFILER_SECTION( "Process Updates" );
			// process the updates
			for( auto& update: m_lUpdateList )
			{
				const auto rawUpdate = update.release( );
				if( const auto updateConfig = dynamic_cast<CUpdateConfig*>( rawUpdate ) )
				{
					ProcessUpdateConfig( std::unique_ptr<CUpdateConfig>( updateConfig ) );

					if( m_bResetIndicated )
					{
						return true;
					}

					if( m_lUpdateList.empty( ) )
						break;
				}
				else if( const auto updateScene = dynamic_cast<CUpdateScene*>( rawUpdate ) )
					ProcessUpdateScene( std::unique_ptr<CUpdateScene>( updateScene ) );
				else
					ITA_EXCEPT_INVALID_PARAMETER( "Unkown type of UpdateMessage" );
			}
			PROFILER_END_SECTION( )

			// Now, the list only contains non valid unique_ptr, we can clear it.
			m_lUpdateList.clear( );

			// replace old updates
			if( m_bReplaceUpdates )
			{
				CReplacementFilter::FilterReplace( m_qPendingUpdateQueue );
				BalanceUpdateRequests( );
			}

			PROFILER_SECTION( "Distribute SceneUpdates to Worker" );
			// distribute updates to the worker
			for( auto workerIter = m_vWorker.begin( ); !m_qPendingUpdateQueue.empty( ) && workerIter != m_vWorker.end( ); ++workerIter )
			{
				auto& worker = *workerIter;
				if( !worker->IsBusy( ) )
				{
					// get the update out of the list.
					auto update = std::move( m_qPendingUpdateQueue.front( ) );
					m_qPendingUpdateQueue.pop_front( );

					PROFILER_VALUE( "Update Scence To Simulator", update->GetID( ) );

					// save a copy for the filters.
					auto mapKey               = mapKeyFromUpdate( update );
					m_mPreviousStates[mapKey] = new CUpdateScene( *update );

					// post the update to the worker.
					worker->PushUpdate( std::move( update ) );
				}
			}

			PROFILER_END_SECTION( );
			PROFILER_SECTION( "Distribute ConfigUpdates to workers" );
			
			for( auto& update: m_qPendingConfigUpdateQueue)
			{
				const auto rawConfigUpdate = update.get( );

				for( auto workerIter = m_vWorker.begin( ); workerIter != m_vWorker.end( ); ++workerIter )
				{
					auto& worker = *workerIter;
		
					worker->PushConfigUpdate( std::make_unique<CUpdateConfig>( *rawConfigUpdate ) );
				}
			}
			PROFILER_END_SECTION( );

			// Now all ConfigUpdates are distributed, so m_qPendingConfigUpdateQueue contains only non-valid ptrs -> clear it
			m_qPendingConfigUpdateQueue.clear( );

			PROFILER_VALUE( "Number of Pending Updates After Loop", m_qPendingUpdateQueue.size( ) );

			// If we still have updates, we'd want the loop to go again to see if a worker can simulate the pending updates.
			if( !m_qPendingUpdateQueue.empty( ) || !m_qPendingConfigUpdateQueue.empty( ) )
				m_evTriggerLoop.SignalEvent( );

			PROFILER_END_SECTION( );

			return true;
		}

		void CScheduler::PreLoop( )
		{
#ifdef WITH_PROFILER
			if( !m_vWorker.empty( ) )
			{
				const auto roomAcousticWorker = dynamic_cast<RoomAcoustics::IRoomAcousticsWorkerInterface*>( m_vWorker.front( ).get( ) );

				if( roomAcousticWorker )
				{
					switch( roomAcousticWorker->GetFieldOfDuty( ) )
					{
						case RoomAcoustics::FieldOfDuty::directSound:
							SetThreadName( "Direct Sound Scheduler" );
							break;
						case RoomAcoustics::FieldOfDuty::earlyReflections:
							SetThreadName( "Early Reflections Scheduler" );
							break;
						case RoomAcoustics::FieldOfDuty::diffuseDecay:
							SetThreadName( "Diffuse Decay Scheduler" );
							break;
						default:;
					}
					PROFILER_NAME_THREAD( GetThreadName( ) );
					return;
				}
			}

			SetThreadName( "Scheduler" );
			PROFILER_NAME_THREAD( GetThreadName( ) );
#endif
		}

		std::vector<IWorkerInterface*> CScheduler::GetWorker( ) const
		{
			std::vector<IWorkerInterface*> vec;

			for( const auto& worker: m_vWorker )
			{
				vec.push_back( worker.get( ) );
			}

			return vec;
		}

		AudibilityFilter::CFilterNetwork* CScheduler::GetFilterNetwork( ) const { return m_pFilterNetwork.get( ); }

		void CScheduler::Shutdown( )
		{
			m_qUpdateQueue.clear( );
			m_qPendingUpdateQueue.clear( );
			m_qPendingConfigUpdateQueue.clear( );
			m_lUpdateList.clear( );

			// signal the reset to the workers
			for( auto& worker: m_vWorker )
			{
				worker->Shutdown( );
			}
		}

		void CScheduler::BalanceUpdateRequests( )
		{
			std::sort( m_qPendingUpdateQueue.begin( ), m_qPendingUpdateQueue.end( ),
			           [&]( const std::unique_ptr<CUpdateScene>& lhs, const std::unique_ptr<CUpdateScene>& rhs )
			           {
				           const auto lhsMapKey = mapKeyFromUpdate( lhs );
				           const auto rhsMapKey = mapKeyFromUpdate( rhs );

				           if( m_mPreviousStates.find( lhsMapKey ) == m_mPreviousStates.end( ) && m_mPreviousStates.find( rhsMapKey ) != m_mPreviousStates.end( ) )
					           return true;

				           return false;
			           } );

			auto priorityEndIterator = std::find_if( m_qPendingUpdateQueue.begin( ), m_qPendingUpdateQueue.end( ),
			                                         [&]( const std::unique_ptr<CUpdateScene>& update )
			                                         {
				                                         const auto mapKey = mapKeyFromUpdate( update );
				                                         return m_mPreviousStates.find( mapKey ) != m_mPreviousStates.end( );
			                                         } );

#if 0
			std::random_device rd;
			std::mt19937 g( rd( ) );
			std::shuffle( priorityEndIterator, m_qPendingUpdateQueue.end( ), g );
#else
			std::sort( priorityEndIterator, m_qPendingUpdateQueue.end( ),
			           [&]( const std::unique_ptr<CUpdateScene>& lhs, const std::unique_ptr<CUpdateScene>& rhs )
			           {
				           const auto lhsMapKey = mapKeyFromUpdate( lhs );
				           const auto rhsMapKey = mapKeyFromUpdate( rhs );

				           if( m_mPreviousStates.find( lhsMapKey )->second->GetTimeStamp( ) < m_mPreviousStates.find( rhsMapKey )->second->GetTimeStamp( ) )
					           return true;

				           return false;
			           } );
#endif
		}
	} // namespace SimulationScheduler
} // namespace ITA