#ifndef INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_REPLACEMENT_FILTER
#define INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_REPLACEMENT_FILTER

// std includes
#include <deque>
#include <list>
#include <memory>

// API includes
#include <ITA/SimulationScheduler/definitions.h>

// simulation scheduler includes
#include <ITA/SimulationScheduler/update_scene.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		struct ITA_SIMULATION_SCHEDULER_API CReplacementFilter
		{
			///
			/// \brief Remove outdated CUpdateScene for a update list.
			/// \param updateList update list from which outdated CSceneUpdates will be removed.
			///
			static inline void FilterReplace( std::list<std::unique_ptr<IUpdateMessage>>& updateList );
			static inline void FilterReplace( std::list<std::unique_ptr<CUpdateScene>>& updateList );
			static inline void FilterReplace( std::deque<std::unique_ptr<CUpdateScene>>& updateList );
		};
	} // namespace SimulationScheduler
} // namespace ITA

#endif // INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_ROOM_ACOUSTICS_REPLACEMENT_FILTER
