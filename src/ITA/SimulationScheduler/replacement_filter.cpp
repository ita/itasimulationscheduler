#include "replacement_filter.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <algorithm>
#include <map>

void ITA::SimulationScheduler::CReplacementFilter::FilterReplace( std::list<std::unique_ptr<IUpdateMessage>>& updateList )
{
	PROFILER_FUNCTION( );
	// Revers traversal of the updates, as the oldest updates are at the front,
	// if a newer update removed them, the iterators are still valid.
	for( auto updateIter = updateList.rbegin( ); updateIter != updateList.rend( ); ++updateIter )
	{
		if( auto updateScene = dynamic_cast<CUpdateScene*>( updateIter->get( ) ) )
		{
			updateList.remove_if( [&]( const std::unique_ptr<IUpdateMessage>& update ) -> bool {
				if( const auto updateSceneLambda = dynamic_cast<CUpdateScene*>( update.get( ) ) )
				{
					if( updateScene->GetSourceReceiverPair( ).receiver->GetId( ) == updateSceneLambda->GetSourceReceiverPair( ).receiver->GetId( ) &&
					    updateScene->GetSourceReceiverPair( ).source->GetId( ) == updateSceneLambda->GetSourceReceiverPair( ).source->GetId( ) &&
					    updateScene->GetTimeStamp( ) > updateSceneLambda->GetTimeStamp( ) )
					{
						PROFILER_VALUE( "Removed Updates", updateSceneLambda->GetID( ) );
						return true;
					}
					return false;
				}

				return false;
			} );
		}
	}
	PROFILER_END_SECTION( );
}

void ITA::SimulationScheduler::CReplacementFilter::FilterReplace( std::list<std::unique_ptr<CUpdateScene>>& updateList )
{
	PROFILER_FUNCTION( );
	// Revers traversal of the updates, as the oldest updates are at the front,
	// if a newer update removed them, the iterators are still valid.
	for( auto updateIter = updateList.rbegin( ); updateIter != updateList.rend( ); ++updateIter )
	{
		updateList.remove_if( [&]( const std::unique_ptr<CUpdateScene>& update ) -> bool {
			if( ( *updateIter )->GetSourceReceiverPair( ).receiver->GetId( ) == update->GetSourceReceiverPair( ).receiver->GetId( ) &&
			    ( *updateIter )->GetSourceReceiverPair( ).source->GetId( ) == update->GetSourceReceiverPair( ).source->GetId( ) &&
			    ( *updateIter )->GetTimeStamp( ) > update->GetTimeStamp( ) )
			{
				PROFILER_VALUE( "Removed Updates", update->GetID( ) );
				return true;
			}
			return false;
		} );
	}
	PROFILER_END_SECTION( );
}

void ITA::SimulationScheduler::CReplacementFilter::FilterReplace( std::deque<std::unique_ptr<CUpdateScene>>& updateList )
{
	PROFILER_FUNCTION( );

	std::sort( updateList.begin( ), updateList.end( ),
	           []( const std::unique_ptr<CUpdateScene>& lhs, const std::unique_ptr<CUpdateScene>& rhs )
	           {
		           if( lhs->GetSourceReceiverPair( ).receiver->GetId( ) == rhs->GetSourceReceiverPair( ).receiver->GetId( ) &&
		               lhs->GetSourceReceiverPair( ).source->GetId( ) == rhs->GetSourceReceiverPair( ).source->GetId( ) && lhs->GetTimeStamp( ) > rhs->GetTimeStamp( ) )
			           return true;

		           return false;
	           } );

	std::map<std::tuple<unsigned, unsigned>, double> updateMemory;

	const auto removeIter =
	    std::remove_if( updateList.begin( ), updateList.end( ),
	                    [&]( const std::unique_ptr<CUpdateScene>& update )
	                    {
		                    const auto key = std::tuple { update->GetSourceReceiverPair( ).receiver->GetId( ), update->GetSourceReceiverPair( ).source->GetId( ) };

		                    if( updateMemory.count( key ) == 1 && update->GetTimeStamp( ) < updateMemory[key] )
		                    {
			                    PROFILER_VALUE( "Removed Updates", update->GetID( ) );
			                    return true;
		                    }

		                    updateMemory[key] = update->GetTimeStamp( );
		                    return false;
	                    } );

	updateList.erase( removeIter, updateList.end( ) );

	PROFILER_END_SECTION( );
}
