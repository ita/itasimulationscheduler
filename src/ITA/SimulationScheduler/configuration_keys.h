#ifndef INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_CONFIGURATION_KEYS
#define INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_CONFIGURATION_KEYS

// API includes
#include <ITA/SimulationScheduler/definitions.h>


// simulation scheduler includes
#include <ITA/SimulationScheduler/3d_object.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		const std::string schedulerConfigSectionName       = "Scheduler";
		const std::string audibilityFilterSectionName      = "AudibilityFilter";
		const std::string audibilityFilterOrderSectionName = "AudibilityFilterOrder";

		const std::string schedulerReplaceUpdatesKey        = "@ReplaceUpdates";
		const std::string schedulerWorkerSectionName        = "Worker";
		const std::string schedulerFilterNetworkSectionName = "FilterNetwork";
		const std::string workerTypeKey                     = "@Type";
		const std::string schedulerTypeKey                  = "@Type";

		const std::string dummyWorkerTime         = "@Time";
		const std::string dummyWorkerReturnResult = "@ReturnResult";
		const std::string remoteWorkerAddress     = "@Address";

		namespace RoomAcoustics
		{
			const std::string masterSimulationControllerConfigSectionName      = "MasterSimulationScheduler";
			const std::string masterSimulationControllerDSSchedulerSectionName = "DSScheduler";
			const std::string masterSimulationControllerERSchedulerSectionName = "ERScheduler";
			const std::string masterSimulationControllerDDSchedulerSectionName = "DDScheduler";

			const std::string workerThreadConfigName  = "RavenWorkerThread";
			const std::string ravenProjectFilePathKey = "@RavenProjectFilePath";

			const std::string workerFieldOfDuty = "@FieldOfDuty";

		} // namespace RoomAcoustics

		namespace OutdoorAcoustics
		{
			const std::string WORKER_SIMULATOR_SECTION_NAME = "Simulator";
			const std::string SIMULATOR_TYPE_KEY            = "@SimulatorType";
			const std::string CLIENT_COORDINATE_SYSTEM_KEY  = "@ClientCoordinateSystem";

			namespace ART
			{
				const std::string STRATIFIED_ATMOSPHERE_KEY = "@StratifiedAtmosphere";
			}

			namespace Urban
			{
				const std::string GEOMETRY_FILE_PATH    = "@GeometryFilePath";
				const std::string SPEED_OF_SOUND        = "@SpeedOfSound";
				const std::string MAX_REFLECTION_ORDER  = "@MaxReflectionOrder";
				const std::string MAX_DIFFRACTION_ORDER = "@MaxDiffractionOrder";

			} // namespace Urban

			namespace FreeField
			{
				const std::string SPEED_OF_SOUND = "@SpeedOfSound";
			}
		} // namespace OutdoorAcoustics

		namespace AudibilityFilter
		{
			const std::string translationFilterTranslationModeKey = "@TranslationMode";
			const std::string translationFilterThresholdKey       = "@Threshold";

			const std::string rotationFilterSourceThresholdKey   = "@SourceThreshold";
			const std::string rotationFilterReceiverThresholdKey = "@ReceiverThreshold";
			const std::string rotationFilterRotationModeKey      = "@RotationMode";

			const std::string distanceFilterDistanceThresholdKey = "@DistanceThreshold";

			const std::string rateFilterRateKey = "@Rate";

			const std::string perceptiveRotationFilterStrengthFactorKey = "@StrengthFactor";

			const std::string audibilityFilterNameKey            = "@Name";
			const std::string audibilityFilterTypeKey            = "@Type";
			const std::string audibilityFilterNextFilterKey      = "@NextFilter";
			const std::string audibilityFilterUsageModeKey       = "@UsageMode";
			const std::string audibilityFilterInvertConditionKey = "@InvertCondition";

			const std::string filterNetworkFilterSectionName = "Filter";
			const std::string filterNetworkFilterStartKey    = "@StartFilter";

		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA

#endif // INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_CONFIGURATION_KEYS
