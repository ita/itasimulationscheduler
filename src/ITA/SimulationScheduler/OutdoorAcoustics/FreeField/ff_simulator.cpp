// Header include
#include "../../configuration_keys.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/FreeField/ff_simulator.h>
#include <ITA/SimulationScheduler/Utils/data_type_utils.h>
#include <ITA/SimulationScheduler/profiler.h>

// ITA Base
#include <ITAConstants.h>
#include <ITAException.h>

// STD
#include <memory>
#include <ostream>

using namespace ITA;
using namespace SimulationScheduler;
using namespace OutdoorAcoustics;
using namespace FreeField;


#pragma region CONFIG

VistaPropertyList CFreeFieldSimulator::FreeFieldSimulatorConfig::Store( ) const
{
	auto oProperties = ISimulatorInterface::SimulatorConfig::Store( );
	return oProperties;
}

void CFreeFieldSimulator::FreeFieldSimulatorConfig::Load( const VistaPropertyList& oProperties )
{
	ISimulatorInterface::SimulatorConfig::Load( oProperties );
	oProperties.GetValueOrDefault( SPEED_OF_SOUND, dSpeedOfSound, ITAConstants::DEFAULT_SPEED_OF_SOUND_D );
}

#pragma endregion

#pragma region SIMULATOR

CFreeFieldSimulator::CFreeFieldSimulator( const FreeFieldSimulatorConfig& oConfig ) : m_oInitialConfig( oConfig ) {}

std::unique_ptr<ISimulatorInterface> CFreeFieldSimulator::Create( std::shared_ptr<const ISimulatorInterface::SimulatorConfig> pConfig )
{
	std::shared_ptr<const FreeFieldSimulatorConfig> pConfigGenericShared = std::dynamic_pointer_cast<const FreeFieldSimulatorConfig>( pConfig );
	if( !pConfigGenericShared )
		ITA_EXCEPT_INVALID_PARAMETER( "Pointer to invalid config given." );

	const FreeFieldSimulatorConfig& oConf( *pConfigGenericShared );
	return std::make_unique<CFreeFieldSimulator>( oConf );
}

std::unique_ptr<COutdoorSimulationResult> CFreeFieldSimulator::Compute( std::unique_ptr<CUpdateScene> pUpdateScene )
{
	PROFILER_FUNCTION( );
	m_pSceneUpdate = std::move( pUpdateScene );

	PROFILER_SECTION( "SceneAdaption" );

	// Don't change order of sensor and emitter update calls
	auto v3SensorPos = m_pSceneUpdate->GetSourceReceiverPair( ).receiver->GetPosition( );
	auto v3EmitterPos = m_pSceneUpdate->GetSourceReceiverPair( ).source->GetPosition( );
	
	const auto sSourceID   = std::to_string( m_pSceneUpdate->GetSourceReceiverPair( ).source->GetId( ) );
	const auto sReceiverID = std::to_string( m_pSceneUpdate->GetSourceReceiverPair( ).receiver->GetId( ) );

	PROFILER_END_SECTION( );


	PROFILER_SECTION( "Simulation" );

	const VistaVector3D vWFNormal = v3SensorPos - v3EmitterPos;
	const double dDistance        = vWFNormal.GetLength( );
	if( dDistance <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Resulting propagation path length was equal or less than zero, source id = " + sReceiverID + ", receiver id = " + sReceiverID );

	PROFILER_END_SECTION( );


	auto pResult                = std::make_unique<COutdoorSimulationResult>( );
	pResult->sourceReceiverPair = m_pSceneUpdate->GetSourceReceiverPair( );
	pResult->dTimeStamp         = m_pSceneUpdate->GetTimeStamp( );

	PROFILER_SECTION( "Modelling" );

	pResult->voPathProperties.resize( 1 );
	auto& oPathProp( pResult->voPathProperties[1] );

	// Static properties
	oPathProp.sID               = "ds";
	oPathProp.iDiffractionOrder = 0;
	oPathProp.iReflectionOrder  = 0;

	// @todo move to a function that applies propagation models
	oPathProp.dPropagationDelay = dDistance / m_oInitialConfig.dSpeedOfSound;
	oPathProp.dSpreadingLoss    = 1 / dDistance;
	oPathProp.oAirAttenuationSpectrum.SetIdentity( );
	oPathProp.oGeoAttenuationSpectrum.SetIdentity( );
	oPathProp.v3ReceiverWaveFrontNormal = vWFNormal;
	oPathProp.v3SourceWaveFrontNormal   = vWFNormal;

	PROFILER_END_SECTION( );

	return std::move( pResult );
}

#pragma endregion
