// Header include
#include "../../configuration_keys.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/Urban/urban_simulator.h>
#include <ITA/SimulationScheduler/Utils/data_type_utils.h>
#include <ITA/SimulationScheduler/profiler.h>

// ITAGeo
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils.h>

// ITA Base
#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAFileSystemUtils.h>

// STD
#include <memory>
#include <ostream>

using namespace ITA;
using namespace SimulationScheduler;
using namespace OutdoorAcoustics;
using namespace Urban;

using namespace ITAGeo;
using namespace ITAPropagationPathSim;

#pragma region CONFIG

VistaPropertyList CUrbanSimulator::UrbanSimulatorConfig::Store( ) const
{
	auto oProperties = ISimulatorInterface::SimulatorConfig::Store( );
	oProperties.SetValue( CLIENT_COORDINATE_SYSTEM_KEY, AsInteger( eClientCoordinateSystem ) );
	oProperties.SetValue( GEOMETRY_FILE_PATH, sJSONGeometryFilePath );
	return oProperties;
}

void CUrbanSimulator::UrbanSimulatorConfig::Load( const VistaPropertyList& oProperties )
{
	ISimulatorInterface::SimulatorConfig::Load( oProperties );

	if( !oProperties.HasProperty( GEOMETRY_FILE_PATH ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Urban simulator configuration is missing a geometry file path" );
	oProperties.GetValue( GEOMETRY_FILE_PATH, sJSONGeometryFilePath );

	if( oProperties.HasProperty( CLIENT_COORDINATE_SYSTEM_KEY ) )
		eClientCoordinateSystem = static_cast<CoordinateSystem>( oProperties.GetValue<int>( CLIENT_COORDINATE_SYSTEM_KEY ) );
	else
		eClientCoordinateSystem = CoordinateSystem::Cartesian;

	oProperties.GetValueOrDefault( SPEED_OF_SOUND, dSpeedOfSound, ITAConstants::DEFAULT_SPEED_OF_SOUND_D );

	oProperties.GetValueOrDefault( MAX_DIFFRACTION_ORDER, iMaxDiffractionOrder, 1 );
	oProperties.GetValueOrDefault( MAX_REFLECTION_ORDER, iMaxReflectionOrder, 1 );
}

#pragma endregion

#pragma region SIMULATOR

CUrbanSimulator::CUrbanSimulator( const UrbanSimulatorConfig& oConfig ) : m_oInitialConfig( oConfig )
{
	m_eClientCoordinateSystem = oConfig.eClientCoordinateSystem;

	try
	{
		m_pMeshModelList = std::make_shared<CModel>( );

		if( !m_pMeshModelList->Load( oConfig.sJSONGeometryFilePath ) )
			ITA_EXCEPT_INVALID_PARAMETER( "Could not load mes model froom file path " + oConfig.sJSONGeometryFilePath );

		m_pSensor  = std::make_shared<ITAGeo::CSensor>( );
		m_pEmitter = std::make_shared<ITAGeo::CEmitter>( );

		// Configuration

		CombinedModel::CPathEngine::CSimulationConfig oConf;
		oConf.bFilterIntersectedPaths              = true;
		oConf.bFilterIlluminatedRegionDiffraction  = true;
		oConf.bFilterEmitterToEdgeIntersectedPaths = true;
		oConf.bFilterSensorToEdgeIntersectedPaths  = true;

		CombinedModel::CPathEngine::CAbortionCriteria oAbort;
		oAbort.iMaxDiffractionOrder = oConfig.iMaxDiffractionOrder;
		oAbort.iMaxReflectionOrder  = oConfig.iMaxReflectionOrder;

		m_pPathEngine = std::make_shared<CombinedModel::CPathEngine>( );

		m_pPathEngine->SetSimulationConfiguration( oConf );
		m_pPathEngine->SetAbortionCriteria( oAbort );

		m_pPathEngine->InitializePathEnvironment( m_pMeshModelList->GetOpenMesh( ) );
	}
	catch( const ITAException& err )
	{
		throw err;
	}
	catch( ... )
	{
		ITA_EXCEPT_INVALID_PARAMETER( "Urban Simulator: Unhandled error while loading configuration." );
	}
}

std::unique_ptr<ISimulatorInterface> CUrbanSimulator::Create( std::shared_ptr<const ISimulatorInterface::SimulatorConfig> pConfig )
{
	std::shared_ptr<const UrbanSimulatorConfig> pConfigGenericShared = std::dynamic_pointer_cast<const UrbanSimulatorConfig>( pConfig );
	if( !pConfigGenericShared )
		ITA_EXCEPT_INVALID_PARAMETER( "Pointer to invalid config given." );

	const UrbanSimulatorConfig& oConf( *pConfigGenericShared );
	return std::make_unique<CUrbanSimulator>( oConf );
}

std::unique_ptr<COutdoorSimulationResult> CUrbanSimulator::Compute( std::unique_ptr<CUpdateScene> pUpdateScene )
{
	PROFILER_FUNCTION( );
	m_pSceneUpdate = std::move( pUpdateScene );

	PROFILER_SECTION( "SceneAdaption" );

	// Don't change order of sensor and emitter update calls
	const VistaVector3D v3SensorPosUserContext( m_pSceneUpdate->GetSourceReceiverPair( ).receiver->GetPosition( ) );
	VistaVector3D& v3SensorPosSim( m_pSensor->v3InteractionPoint );

	if( m_eClientCoordinateSystem == CoordinateSystem::OpenGL )
		v3SensorPosSim = OpenGLToISO( v3SensorPosUserContext );
	else
		v3SensorPosSim = v3SensorPosSim;
	m_pSensor->sName = "id" + std::to_string( m_pSceneUpdate->GetSourceReceiverPair( ).receiver->GetId( ) );
	m_pPathEngine->UpdateTargetEntity( m_pSensor );

	const VistaVector3D v3EmitterPosUserContext( m_pSceneUpdate->GetSourceReceiverPair( ).source->GetPosition( ) );
	VistaVector3D& v3EmitterPosSim( m_pEmitter->v3InteractionPoint );
	if( m_eClientCoordinateSystem == CoordinateSystem::OpenGL )
		v3EmitterPosSim = OpenGLToISO( v3EmitterPosUserContext );
	else
		v3EmitterPosSim = v3EmitterPosUserContext;
	m_pEmitter->sName = "id" + std::to_string( m_pSceneUpdate->GetSourceReceiverPair( ).source->GetId( ) );
	m_pPathEngine->UpdateOriginEntity( m_pEmitter );

	PROFILER_END_SECTION( );


	PROFILER_SECTION( "Simulation" );

	CPropagationPathList oPathList;
	m_pPathEngine->ConstructPropagationPaths( oPathList );
	ITAGeoUtils::AddIdentifier( oPathList );


	auto pVisualisationMeshModel = std::make_shared<ITAGeo::SketchUp::CModel>( ); // @todo only perform if requested (once?)
	pVisualisationMeshModel->Load( m_oInitialConfig.sJSONGeometryFilePath );

	PROFILER_END_SECTION( );


	auto pResult                = std::make_unique<COutdoorSimulationResult>( );
	pResult->sourceReceiverPair = m_pSceneUpdate->GetSourceReceiverPair( );
	pResult->dTimeStamp         = m_pSceneUpdate->GetTimeStamp( );

	PROFILER_SECTION( "Modelling" );

	pResult->voPathProperties.resize( oPathList.size( ) );
	for( size_t i = 0; i < oPathList.size( ); i++ )
	{
		const CPropagationPath& oSimulationPath( oPathList[i] );
		auto& oPathProp( pResult->voPathProperties[i] );

		if( oSimulationPath.size( ) < 2 )
			ITA_EXCEPT_INVALID_PARAMETER( "Result propagation path must have at least two propagation anchors, but received less, id = " + oSimulationPath.sIdentifier );

		if( oSimulationPath.GetLength( ) <= 0.0f )
			ITA_EXCEPT_INVALID_PARAMETER( "Result propagation path length was equal or less tan zero, id = " + oSimulationPath.sIdentifier );

		pVisualisationMeshModel->AddPropagationPathVisualization( oSimulationPath, oSimulationPath.sIdentifier );

		// Static properties
		oPathProp.sID               = oSimulationPath.sIdentifier;
		oPathProp.iDiffractionOrder = oSimulationPath.GetNumDiffractions( );
		oPathProp.iReflectionOrder  = oSimulationPath.GetNumReflections( );

		// @todo move to a function that applies propagation models
		oPathProp.dPropagationDelay = oSimulationPath.GetLength( ) / m_oInitialConfig.dSpeedOfSound;
		oPathProp.dSpreadingLoss    = 1 / oSimulationPath.GetLength( );
		oPathProp.oAirAttenuationSpectrum.SetIdentity( );
		oPathProp.oGeoAttenuationSpectrum.SetIdentity( );
		oPathProp.v3ReceiverWaveFrontNormal = oSimulationPath.GetReceiverWaveFrontNormal( );
		oPathProp.v3SourceWaveFrontNormal   = oSimulationPath.GetSourceWaveFrontNormal( );
	}

	PROFILER_END_SECTION( );

	pVisualisationMeshModel->Store( "urban_simulator_" + oPathList.sIdentifier + ".skp" );

	return std::move( pResult );
}

#pragma endregion
