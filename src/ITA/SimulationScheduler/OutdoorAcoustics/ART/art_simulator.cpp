// Header include
#include "../../configuration_keys.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/ART/art_simulator.h>
#include <ITA/SimulationScheduler/Utils/data_type_utils.h>
#include <ITA/SimulationScheduler/profiler.h>

// ITAGeo
#include <ITAGeo/Utils/JSON/Atmosphere.h>
#include <ITAPropagationModels/Atmosphere/AirAttenuation.h>

// ITA Base
#include <ITAException.h>
#include <ITAFileSystemUtils.h>

// STD
#include <ostream>

using namespace ITAPropagationPathSim::AtmosphericRayTracing;
using namespace ITAPropagationModels::Atmosphere;
namespace ITA
{
	namespace SimulationScheduler
	{
		namespace OutdoorAcoustics
		{
			namespace ART
			{
//------CONFIG------
#pragma region CONFIG

				VistaPropertyList CARTSimulator::ARTSimulatorConfig::Store( ) const
				{
					auto oProperties = ISimulatorInterface::SimulatorConfig::Store( );
					oProperties.SetValue( CLIENT_COORDINATE_SYSTEM_KEY, AsInteger( eClientCoordinateSystem ) );
					oProperties.SetValue( STRATIFIED_ATMOSPHERE_KEY, sJsonAtmosphere );
					return oProperties;
				}
				void CARTSimulator::ARTSimulatorConfig::Load( const VistaPropertyList& oProperties )
				{
					ISimulatorInterface::SimulatorConfig::Load( oProperties );
					eClientCoordinateSystem = static_cast<CoordinateSystem>( oProperties.GetValue<int>( CLIENT_COORDINATE_SYSTEM_KEY ) );
					if( oProperties.HasProperty( STRATIFIED_ATMOSPHERE_KEY ) )
						oProperties.GetValue( STRATIFIED_ATMOSPHERE_KEY, sJsonAtmosphere );
					else
						sJsonAtmosphere = "";
				}

#pragma endregion


//------SIMULATOR------
#pragma region SIMULATOR

				CARTSimulator::CARTSimulator( const ARTSimulatorConfig& oConfig )
				{
					m_eClientCoordinateSystem = oConfig.eClientCoordinateSystem;
					if( oConfig.sJsonAtmosphere.empty( ) )
						std::cout << "ART Simulator: Atmosphere undefined, using default atmosphere." << std::endl;
					else
					{
						try
						{
							// Assume file if contains path separator and no {
							if( oConfig.sJsonAtmosphere.find( PATH_SEPARATOR ) != std::string::npos && oConfig.sJsonAtmosphere.find( "{" ) == std::string::npos )
								ITAGeo::Utils::JSON::Import( m_oAtmosphere, oConfig.sJsonAtmosphere );
							// Assume JSON formatted string
							else
								ITAGeo::Utils::JSON::Decode( m_oAtmosphere, oConfig.sJsonAtmosphere );
						}
						catch( const ITAException& err )
						{
							throw err;
						}
						catch( ... )
						{
							ITA_EXCEPT_INVALID_PARAMETER( "ART Simulator: Unhandled error while loading atmosphere." );
						}
					}

					m_oEngine.eigenraySettings.rayTracing.maxReflectionOrder = 1; // Always expect exactly 2 eigenrays
					m_oEngine.simulationSettings.bMultiThreading             = false;
				}

				std::unique_ptr<ISimulatorInterface> CARTSimulator::Create( std::shared_ptr<const ISimulatorInterface::SimulatorConfig> pConfig )
				{
					std::shared_ptr<const ARTSimulatorConfig> pConfigGenericShared = std::dynamic_pointer_cast<const ARTSimulatorConfig>( pConfig );
					if( !pConfigGenericShared )
						ITA_EXCEPT_INVALID_PARAMETER( "Pointer to oinvalid config given." );

					const ARTSimulatorConfig& oConf( *pConfigGenericShared );
					return std::make_unique<CARTSimulator>( oConf );
				}


				std::unique_ptr<COutdoorSimulationResult> CARTSimulator::Compute( std::unique_ptr<CUpdateScene> pUpdateScene )
				{
					PROFILER_FUNCTION( );
					m_pSceneUpdate = std::move( pUpdateScene );

					const SourceReceiverPair oSRPair = m_pSceneUpdate->GetSourceReceiverPair( );
					VistaVector3D v3SourcePos        = oSRPair.source->GetPosition( );
					VistaVector3D v3ReceiverPos      = oSRPair.receiver->GetPosition( );
					if( m_eClientCoordinateSystem == CoordinateSystem::OpenGL )
					{
						v3SourcePos   = OpenGLToCart( v3SourcePos );
						v3ReceiverPos = OpenGLToCart( v3ReceiverPos );
					}

					// Simulate
					PROFILER_SECTION( "Finding Eigenrays" );
					std::vector<std::shared_ptr<CRay> > vpEigenrays = m_oEngine.Run( m_oAtmosphere, v3SourcePos, v3ReceiverPos );
					PROFILER_END_SECTION( );

					if( vpEigenrays.size( ) != 2 )
						ITA_EXCEPT_INVALID_PARAMETER( std::string( "Expected 2 eigenrays but found " + std::to_string( vpEigenrays.size( ) ) + "." ) );


					PROFILER_SECTION( "Eigenrays to OutdoorSimulationResult" );

					auto pResult                = std::make_unique<COutdoorSimulationResult>( );
					pResult->sourceReceiverPair = oSRPair;
					pResult->dTimeStamp         = m_pSceneUpdate->GetTimeStamp( );
					pResult->voPathProperties.resize( vpEigenrays.size( ) );

					for( int idx = 0; idx < vpEigenrays.size( ); idx++ )
					{
						pResult->voPathProperties[idx].sID = std::to_string( idx );

						pResult->voPathProperties[idx].dPropagationDelay = vpEigenrays[idx]->LastTimeStamp( );
						pResult->voPathProperties[idx].dSpreadingLoss    = vpEigenrays[idx]->SpreadingLoss( );
						pResult->voPathProperties[idx].iReflectionOrder  = vpEigenrays[idx]->ReflectionOrder( );
						pResult->voPathProperties[idx].iDiffractionOrder = 0; // No diffraction

						pResult->voPathProperties[idx].oAirAttenuationSpectrum =
						    ITAPropagationModels::Atmosphere::AirAttenuationSpectrum( *vpEigenrays[idx], m_oAtmosphere );
						// pResult->voPathProperties[idx].oGeoAttenuationSpectrum = ...; //Default is identity. To be changed in future. Probably reading data from
						// config.

						pResult->voPathProperties[idx].v3SourceWaveFrontNormal   = vpEigenrays[idx]->InitialDirection( );
						pResult->voPathProperties[idx].v3ReceiverWaveFrontNormal = vpEigenrays[idx]->LastWavefrontNormal( );
						if( m_eClientCoordinateSystem == CoordinateSystem::OpenGL )
						{
							pResult->voPathProperties[idx].v3SourceWaveFrontNormal   = CartToOpenGL( pResult->voPathProperties[idx].v3SourceWaveFrontNormal );
							pResult->voPathProperties[idx].v3ReceiverWaveFrontNormal = CartToOpenGL( pResult->voPathProperties[idx].v3ReceiverWaveFrontNormal );
						}
					}

					PROFILER_END_SECTION( );

					return std::move( pResult );
				}
				VistaVector3D CARTSimulator::OpenGLToCart( const VistaVector3D& v3In ) { return VistaVector3D( -v3In[Vista::Z], -v3In[Vista::X], v3In[Vista::Y] ); }
				VistaVector3D CARTSimulator::CartToOpenGL( const VistaVector3D& v3In ) { return VistaVector3D( -v3In[Vista::Y], v3In[Vista::Z], -v3In[Vista::X] ); }
#pragma endregion

			} // namespace ART
		}     // namespace OutdoorAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA