// Header include
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulator_interface.h>


// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/FreeField/ff_simulator.h>
#ifdef WITH_ART
#	include <ITA/SimulationScheduler/OutdoorAcoustics/ART/art_simulator.h>
#endif
#ifdef WITH_URBAN
#	include <ITA/SimulationScheduler/OutdoorAcoustics/Urban/urban_simulator.h>
#endif


namespace ITA
{
	namespace SimulationScheduler
	{
		namespace OutdoorAcoustics
		{
//-------CONFIG-------
//--------------------
#pragma region CONFIG

			ISimulatorInterface::SimulatorConfig::SimulatorConfig( ) : SimulatorConfig( "Unknown" ) {}

			ISimulatorInterface::SimulatorConfig::SimulatorConfig( const std::string& sType ) : m_sSimulatorType( sType ), sSimulatorType( m_sSimulatorType ) {}

			ISimulatorInterface::SimulatorConfig::SimulatorConfig( const SimulatorConfig& oOther ) : SimulatorConfig( oOther.sSimulatorType ) {}

			ISimulatorInterface::SimulatorConfig& ISimulatorInterface::SimulatorConfig::operator=( const SimulatorConfig& other )
			{
				if( this == &other )
					return *this;
				IConfig::operator=( other );
				m_sSimulatorType = other.m_sSimulatorType;
				return *this;
			}

			VistaPropertyList ISimulatorInterface::SimulatorConfig::Store( ) const
			{
				VistaPropertyList oProperties;
				oProperties.SetValue( SIMULATOR_TYPE_KEY, m_sSimulatorType );
				return oProperties;
			}

			void ISimulatorInterface::SimulatorConfig::Load( const VistaPropertyList& oProperties )
			{
				oProperties.GetValue<std::string>( SIMULATOR_TYPE_KEY, m_sSimulatorType );
			}

#pragma endregion


//-------FACTORY-------
//---------------------
#pragma region FACTORY

			std::map<std::string, std::pair<CSimulatorFactory::CreateCallback, CSimulatorFactory::ConfigCreateCallback>>
			    CSimulatorFactory::m_mRegisteredSimulatorTypes = {
				    { OutdoorAcoustics::FreeField::CFreeFieldSimulator::GetType( ),
				      { OutdoorAcoustics::FreeField::CFreeFieldSimulator::Create,
				        std::make_shared<OutdoorAcoustics::FreeField::CFreeFieldSimulator::FreeFieldSimulatorConfig> } },
#ifdef WITH_ART
				    { OutdoorAcoustics::ART::CARTSimulator::GetType( ),
				      { OutdoorAcoustics::ART::CARTSimulator::Create, std::make_shared<OutdoorAcoustics::ART::CARTSimulator::ARTSimulatorConfig> } },
#endif
#ifdef WITH_URBAN
				    { OutdoorAcoustics::Urban::CUrbanSimulator::GetType( ),
				      { OutdoorAcoustics::Urban::CUrbanSimulator::Create, std::make_shared<OutdoorAcoustics::Urban::CUrbanSimulator::UrbanSimulatorConfig> } },
#endif
			    };


			void CSimulatorFactory::RegisterSimulator( const std::string& sType, CreateCallback createFunction, ConfigCreateCallback configCreateFunction )
			{
				m_mRegisteredSimulatorTypes[sType] = { createFunction, configCreateFunction };
			}

			void CSimulatorFactory::UnregisterSimulator( const std::string& sType ) { m_mRegisteredSimulatorTypes.erase( sType ); }

			std::unique_ptr<ISimulatorInterface> CSimulatorFactory::CreateSimulator( const std::shared_ptr<ISimulatorInterface::SimulatorConfig>& pConfig )
			{
				if( !pConfig )
					return nullptr;

				auto it = m_mRegisteredSimulatorTypes.find( pConfig->sSimulatorType );
				if( it != m_mRegisteredSimulatorTypes.end( ) )
				{
					// call the creation callback to construct this derived type
					std::unique_ptr<ISimulatorInterface> pSimGeneric( it->second.first( pConfig ) );
					return pSimGeneric;
				}
				return nullptr;
			}

			std::shared_ptr<ISimulatorInterface::SimulatorConfig> CSimulatorFactory::CreateConfig( const std::string& sType )
			{
				auto it = m_mRegisteredSimulatorTypes.find( sType );
				if( it != m_mRegisteredSimulatorTypes.end( ) )
				{
					// call the creation callback to construct this derived type
					return it->second.second( );
				}
				return nullptr;
			}

#pragma endregion
		} // namespace OutdoorAcoustics
	}     // namespace SimulationScheduler
} // namespace ITA
