// Header include
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_worker_thread.h>

// Simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>

// ITA includes
#include "ITAException.h"

#include <ITADebug.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace OutdoorAcoustics
		{
#ifdef WITH_PROFILER
			std::size_t CWorkerThread::iWorkerMaxID = 0;
#endif

			CWorkerThread::WorkerThreadConfig::WorkerThreadConfig( ) : WorkerThreadConfig( "Unkown" ) {}

			CWorkerThread::WorkerThreadConfig::WorkerThreadConfig( const std::string& sType ) : WorkerConfig( GetType( ) ) {}

			VistaPropertyList CWorkerThread::WorkerThreadConfig::Store( ) const
			{
				auto oProperties = WorkerConfig::Store( );

				if( pSimulatorConfig )
					oProperties.SetValue( WORKER_SIMULATOR_SECTION_NAME, pSimulatorConfig->Store( ) );

				return oProperties;
			}

			void CWorkerThread::WorkerThreadConfig::Load( const VistaPropertyList& oProperties )
			{
				WorkerConfig::Load( oProperties );

				for( const auto& oProperty: oProperties )
				{
					if( oProperty.first.find( WORKER_SIMULATOR_SECTION_NAME ) != std::string::npos &&
					    oProperty.second.GetPropertyType( ) == VistaProperty::PROPT_PROPERTYLIST )
					{
						const VistaPropertyList configProperties = oProperty.second.GetPropertyListConstRef( );
						const auto sType                         = configProperties.GetValue<std::string>( SIMULATOR_TYPE_KEY );

						pSimulatorConfig = CSimulatorFactory::CreateConfig( sType );

						if( !pSimulatorConfig ) //  || pSimulatorConfig->sSimulatorType.compare( sType ) != 0
							ITA_EXCEPT_INVALID_PARAMETER( "Could not create simulation worker thread of type '" + sType + "'" );

						pSimulatorConfig->Load( configProperties );
					}
				}
			}

			CWorkerThread::CWorkerThread( const WorkerThreadConfig& oConfig, ISchedulerInterface* pParent )
			    : IWorkerInterface( pParent )
			    , m_evTriggerLoop( VistaThreadEvent::NON_WAITABLE_EVENT )
			{
				m_pSimulator = CSimulatorFactory::CreateSimulator( oConfig.pSimulatorConfig );

				Run( );
			}

			CWorkerThread::~CWorkerThread( )
			{
				m_bStopIndicated = true;
				m_evTriggerLoop.SignalEvent( );

				// Wait for the end of the simulation
				while( IsBusy( ) )
					VistaTimeUtils::Sleep( 100 );

				StopGently( true );
			}

			void CWorkerThread::PushUpdate( std::unique_ptr<CUpdateScene> pUpdate )
			{
				// PROFILER_FUNCTION ( );
				PROFILER_VALUE( "Update to Worker", pUpdate->GetID( ) );

				// auto pTask = CreateTaskFromUpdate ( std::move ( pUpdate ) );

				assert( !IsBusy( ) );
				m_bBusy = true;

				m_pUpdate = std::move( pUpdate );

				// Stop if reset is running.
				if( m_bResetIndicated )
				{
					m_pUpdate = nullptr;
					m_bBusy   = false;
					return;
				}

				// Start the loop and simulation.
				m_evTriggerLoop.SignalEvent( );
			}

			void CWorkerThread::Reset( )
			{
				// Signal reset
				m_bResetIndicated = true;

				// Wait for the end of the simulation
				while( IsBusy( ) )
					VistaTimeUtils::Sleep( 100 );

				// Reset the simulator
				if( m_pSimulator )
					m_pSimulator->Reset( );

				// Reset the flags
				m_bResetIndicated = false;
			}

			void CWorkerThread::Shutdown( )
			{
				m_bStopIndicated = true;
				m_evTriggerLoop.SignalEvent( );

				// Wait for the end of the simulation
				while( IsBusy( ) )
					VistaTimeUtils::Sleep( 100 );

				StopGently( true );
			}

			std::unique_ptr<IWorkerInterface> CWorkerThread::CreateWorker( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
			{
				return std::make_unique<CWorkerThread>( dynamic_cast<const WorkerThreadConfig&>( *pConfig ), pParent );
			}

			void CWorkerThread::SetSimulator( std::unique_ptr<ISimulatorInterface> pSimulator ) { m_pSimulator = std::move( pSimulator ); }

			bool CWorkerThread::LoopBody( )
			{
				// Wait for trigger
				m_evTriggerLoop.WaitForEvent( true );
				m_evTriggerLoop.ResetThisEvent( );

				PROFILER_FUNCTION( );

				if( m_bStopIndicated )
				{
					IndicateLoopEnd( );
					PROFILER_EVENT_COUNT( "Loop Stop" );
					return false;
				}

				assert( !m_bResetIndicated );
				assert( m_pSimulator );
				assert( m_pUpdate );

				const unsigned int uiUpdateID = m_pUpdate->GetID( );

				// R_INFO ( "raven::WorkerThread:\tStarting simulation on Task %i, SimType %i\n", pTask->uiID, pTask->eSimulationType );

				PROFILER_SECTION( "Compute Update/Task" );
				PROFILER_VALUE( "Compute Update/Task", uiUpdateID );
				std::unique_ptr<COutdoorSimulationResult> pResult = nullptr;
				try
				{
					// Simulate (blocking)
					DEBUG_PRINTF( "[Outdoor Thread Worker]\t Simulation Started\n" );
					pResult   = m_pSimulator->Compute( std::move( m_pUpdate ) );
					m_pUpdate = nullptr;
					DEBUG_PRINTF( "[Outdoor Thread Worker]\t Simulation Finished\n" );
				}
				catch( ITAException& e )
				{
					m_pUpdate = nullptr;
					std::cerr << "[ ERROR ][ SimScheduler ][ OutdoorWorkerThread ] " << e.ToString( );
				}
				PROFILER_VALUE( "Update/Task Simulated", uiUpdateID );
				PROFILER_END_SECTION( );


				try
				{
					if( pResult )
						m_pParentScheduler->HandleSimulationFinished( std::move( pResult ) );
				}
				catch( ITAException& e )
				{
					std::cerr << e.ToString( );
				}

				PROFILER_VALUE( "Result handled", uiUpdateID );

				// Worker is free again
				m_bBusy = false;

				return true;
			}

			void CWorkerThread::PreLoop( )
			{
#ifdef WITH_PROFILER
				const std::string sThreadName = "Outdoor Worker Thread " + std::to_string( iWorkerMaxID++ );

				SetThreadName( sThreadName );
				PROFILER_NAME_THREAD( sThreadName );
#else
				SetThreadName( "Outdoor Worker Thread" );
#endif
			}
		} // namespace OutdoorAcoustics
	}     // namespace SimulationScheduler
} // namespace ITA