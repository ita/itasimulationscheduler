// Header include
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>

// simulation scheduler include
//#include <ITA/SimulationScheduler/Utils/utils.h>
#include "../serialization_helper.h"

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::OutdoorAcoustics;

const int MAX_PATHID_STRING_LENGTH = 10000;

int COutdoorSimulationResult::CPathProperty::Serialize( IVistaSerializer& pSerializer ) const
{
	int returnVal = 0;

	returnVal += pSerializer.WriteString( sID );

	returnVal += pSerializer.WriteDouble( dPropagationDelay );
	returnVal += pSerializer.WriteDouble( dSpreadingLoss );
	returnVal += pSerializer.WriteInt32( iReflectionOrder );
	returnVal += pSerializer.WriteInt32( iDiffractionOrder );

	returnVal += SimulationScheduler::SerializationHelper::WriteITAThirdOctaveFactorMagnitudeSpectrum( pSerializer, oGeoAttenuationSpectrum );
	returnVal += SimulationScheduler::SerializationHelper::WriteITAThirdOctaveFactorMagnitudeSpectrum( pSerializer, oAirAttenuationSpectrum );

	returnVal += SimulationScheduler::SerializationHelper::WriteVistaVector3D( pSerializer, v3SourceWaveFrontNormal );
	returnVal += SimulationScheduler::SerializationHelper::WriteVistaVector3D( pSerializer, v3ReceiverWaveFrontNormal );

	return returnVal;
}

int COutdoorSimulationResult::CPathProperty::DeSerialize( IVistaDeSerializer& pDeserializer )
{
	int returnVal = 0;

	returnVal += pDeserializer.ReadString( sID, MAX_PATHID_STRING_LENGTH );

	returnVal += pDeserializer.ReadDouble( dPropagationDelay );
	returnVal += pDeserializer.ReadDouble( dSpreadingLoss );
	returnVal += pDeserializer.ReadInt32( iReflectionOrder );
	returnVal += pDeserializer.ReadInt32( iDiffractionOrder );

	returnVal += SimulationScheduler::SerializationHelper::ReadITAThirdOctaveFactorMagnitudeSpectrum( pDeserializer, oGeoAttenuationSpectrum );
	returnVal += SimulationScheduler::SerializationHelper::ReadITAThirdOctaveFactorMagnitudeSpectrum( pDeserializer, oAirAttenuationSpectrum );

	returnVal += SimulationScheduler::SerializationHelper::ReadVistaVector3D( pDeserializer, v3SourceWaveFrontNormal );
	returnVal += SimulationScheduler::SerializationHelper::ReadVistaVector3D( pDeserializer, v3ReceiverWaveFrontNormal );

	return returnVal;
}

int COutdoorSimulationResult::Serialize( IVistaSerializer& pSerializer ) const
{
	int returnVal = 0;

	returnVal += pSerializer.WriteInt32( (int)voPathProperties.size( ) );
	for( const CPathProperty& rPath: voPathProperties )
		returnVal += rPath.Serialize( pSerializer );

	return returnVal;
}

int COutdoorSimulationResult::DeSerialize( IVistaDeSerializer& pDeserializer )
{
	int returnVal = 0;

	int iNumPaths;
	returnVal += pDeserializer.ReadInt32( iNumPaths );
	if( iNumPaths < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Number of path properties must be >= 0." );

	voPathProperties.resize( iNumPaths );
	for( CPathProperty& oPathProp: voPathProperties )
		returnVal += oPathProp.DeSerialize( pDeserializer );

	return returnVal;
}

std::unique_ptr<CSimulationResult> COutdoorSimulationResult::Clone( ) const
{
	return std::make_unique<COutdoorSimulationResult>( *this );
}