// Header include
#include "ITA/SimulationScheduler/update_message.h"

#include "ITASimulationScheduler_instrumentation.h"

namespace ITA
{
	namespace SimulationScheduler
	{
		unsigned int IUpdateMessage::updateMaxID = 0;

		IUpdateMessage::IUpdateMessage( )
		{
			m_iID = updateMaxID++;
			ITASIMULATIONSCHEDULER_TRACE( "Create update message {} with ID {}", fmt::ptr(this), m_iID );
		}

		IUpdateMessage::~IUpdateMessage( )
		{
			ITASIMULATIONSCHEDULER_TRACE( "Delete update message {} with ID {}", fmt::ptr(this), m_iID );
		}

		unsigned IUpdateMessage::GetID( ) const
		{
			return m_iID;
		}
	} // namespace SimulationScheduler
} // namespace ITA