#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_worker_thread.h>
#include <ITA/SimulationScheduler/dummy_worker.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>
#include <ITA/SimulationScheduler/worker_remote.h>
#include <ITA\SimulationScheduler\worker_interface.h>
#ifdef WITH_RAVEN
#	include <ITA/SimulationScheduler/RoomAcoustics/Raven/worker_thread.h>
#endif

#include "configuration_keys.h"

namespace ITA
{
	namespace SimulationScheduler
	{
		// todo This can fail if the name changes .. Fix this/make it bullet proof
		std::map<std::string, std::pair<CWorkerFactory::CreateCallback, CWorkerFactory::ConfigCreateCallback>> CWorkerFactory::m_mWorkers = {
#ifdef WITH_RAVEN
			{ RoomAcoustics::Raven::CWorkerThread::GetType( ),
			  { RoomAcoustics::Raven::CWorkerThread::CreateWorker, std::make_shared<RoomAcoustics::Raven::CWorkerThread::WorkerThreadConfig> } },
#endif
			{ OutdoorAcoustics::CWorkerThread::GetType( ),
			  { OutdoorAcoustics::CWorkerThread::CreateWorker, std::make_shared<OutdoorAcoustics::CWorkerThread::WorkerThreadConfig> } },
			{ CDummyWorker::GetType( ), { CDummyWorker::CreateWorker, std::make_shared<CDummyWorker::DummyWorkerConfig> } },
			{ CWorkerRemote::GetType( ), { CWorkerRemote::CreateWorker, std::make_shared<CWorkerRemote::WorkerRemoteConfig> } }
		};

		IWorkerInterface::WorkerConfig::WorkerConfig( ) : sWorkerType( m_sWorkerType ) { m_sWorkerType = "Unknown"; }

		IWorkerInterface::WorkerConfig::WorkerConfig( std::string sType ) : sWorkerType( m_sWorkerType ) { m_sWorkerType = sType; }

		IWorkerInterface::WorkerConfig::WorkerConfig( const WorkerConfig& other ) : IConfig( other ), sWorkerType( m_sWorkerType ), m_sWorkerType( other.m_sWorkerType )
		{
		}

		IWorkerInterface::WorkerConfig& IWorkerInterface::WorkerConfig::operator=( const WorkerConfig& other )
		{
			if( this == &other )
				return *this;
			IConfig::operator=( other );
			m_sWorkerType    = other.m_sWorkerType;
			return *this;
		}

		VistaPropertyList IWorkerInterface::WorkerConfig::Store( ) const
		{
			VistaPropertyList oProperties;
			oProperties.SetValue( workerTypeKey, m_sWorkerType );
			return oProperties;
		}

		void IWorkerInterface::WorkerConfig::Load( const VistaPropertyList& oProperties ) { oProperties.GetValue<std::string>( workerTypeKey, m_sWorkerType ); }

		IWorkerInterface::IWorkerInterface( ISchedulerInterface* pParent ) : m_pParentScheduler( pParent ) {}

		void CWorkerFactory::RegisterWorker( const std::string& type, CreateCallback createFunction, ConfigCreateCallback configCreateFunction )
		{
			m_mWorkers[type] = { createFunction, configCreateFunction };
		}

		void CWorkerFactory::UnregisterWorker( const std::string& type ) { m_mWorkers.erase( type ); }

		std::unique_ptr<IWorkerInterface> CWorkerFactory::CreateWorker( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
		{
			auto it = m_mWorkers.find( pConfig->sWorkerType );
			if( it != m_mWorkers.end( ) )
			{
				// call the creation callback to construct this derived type
				return it->second.first( pConfig, pParent );
			}
			return nullptr;
		}

		std::shared_ptr<IWorkerInterface::WorkerConfig> CWorkerFactory::CreateConfig( const std::string& sType )
		{
			auto it = m_mWorkers.find( sType );
			if( it != m_mWorkers.end( ) )
			{
				// call the creation callback to construct this derived type
				return it->second.second( );
			}
			return nullptr;
		}
	} // namespace SimulationScheduler
} // namespace ITA
