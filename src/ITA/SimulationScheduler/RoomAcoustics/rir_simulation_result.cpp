// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h>

// simulation scheduler include
#include <ITA/SimulationScheduler/Utils/utils.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			int CRIRSimulationResult::Serialize( IVistaSerializer& pSerializer ) const
			{
				int returnVal = 0;

				returnVal += CIRSimulationResult::Serialize( pSerializer );

				returnVal += pSerializer.WriteInt32( AsInteger( eResultType ) );
				returnVal += pSerializer.WriteBool( bSameRoom );

				return returnVal;
			}

			int CRIRSimulationResult::DeSerialize( IVistaDeSerializer& pDeserializer )
			{
				int returnVal = 0;

				returnVal += CIRSimulationResult::DeSerialize( pDeserializer );

				int tmp;
				returnVal += pDeserializer.ReadInt32( tmp );
				eResultType = FieldOfDuty( tmp );

				returnVal += pDeserializer.ReadBool( bSameRoom );

				return returnVal;
			}

			std::string CRIRSimulationResult::GetSignature( ) const { return "CRIRSimulationResult"; }

			std::unique_ptr<CSimulationResult> CRIRSimulationResult::Clone( ) const { return std::make_unique<CRIRSimulationResult>( *this ); }
		} // namespace RoomAcoustics
	}     // namespace SimulationScheduler
} // namespace ITA