// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>

// simulation scheduler includes
#include "../src/ITA/SimulationScheduler/configuration_keys.h"
#include "../src/ITA/SimulationScheduler/replacement_filter.h"

#include <ITA/SimulationScheduler/RoomAcoustics/room_acoustics_worker_interface.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/scheduler.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>
#include <ITA/SimulationScheduler/update_config.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <ITA/SimulationScheduler/worker_remote.h>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>

// ITA includes
#include <ITAException.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			VistaPropertyList CMasterSimulationController::MasterSimulationControllerConfig::Store( ) const
			{
				VistaPropertyList oProperties;

				oProperties.SetValue( masterSimulationControllerDSSchedulerSectionName, oDSSchedulerConfig->Store( ) );
				oProperties.SetValue( masterSimulationControllerERSchedulerSectionName, oERSchedulerConfig->Store( ) );
				oProperties.SetValue( masterSimulationControllerDDSchedulerSectionName, oDDSchedulerConfig->Store( ) );

				oProperties.SetValue( schedulerReplaceUpdatesKey, bReplaceUpdates );

				return oProperties;
			}

			void CMasterSimulationController::MasterSimulationControllerConfig::Load( const VistaPropertyList& oProperties )
			{
				VistaPropertyList schedulerConfig;

				oProperties.GetValue( masterSimulationControllerDSSchedulerSectionName, schedulerConfig );
				if( schedulerConfig.GetValue<std::string>( schedulerTypeKey ) == CScheduler::GetType( ) )
				{
					oDSSchedulerConfig = std::make_shared<CScheduler::LocalSchedulerConfig>( );
					oDSSchedulerConfig->Load( schedulerConfig );
				}
				schedulerConfig.clear( );

				oProperties.GetValue( masterSimulationControllerERSchedulerSectionName, schedulerConfig );
				if( schedulerConfig.GetValue<std::string>( schedulerTypeKey ) == CScheduler::GetType( ) )
				{
					oERSchedulerConfig = std::make_shared<CScheduler::LocalSchedulerConfig>( );
					oERSchedulerConfig->Load( schedulerConfig );
				}
				schedulerConfig.clear( );

				oProperties.GetValue( masterSimulationControllerDDSchedulerSectionName, schedulerConfig );
				if( schedulerConfig.GetValue<std::string>( schedulerTypeKey ) == CScheduler::GetType( ) )
				{
					oDDSchedulerConfig = std::make_shared<CScheduler::LocalSchedulerConfig>( );
					oDDSchedulerConfig->Load( schedulerConfig );
				}
				schedulerConfig.clear( );

				oProperties.GetValue( schedulerReplaceUpdatesKey, bReplaceUpdates );
			}

			CMasterSimulationController::CMasterSimulationController( const MasterSimulationControllerConfig& pConfig )
			    : m_evTriggerLoop( VistaThreadEvent::NON_WAITABLE_EVENT )
			{
				m_bReplaceUpdates = pConfig.bReplaceUpdates;

				if( pConfig.oDSSchedulerConfig && pConfig.oDSSchedulerConfig->sSchedulerType == CScheduler::GetType( ) )
					m_pDSScheduler = std::make_unique<CScheduler>(
					    SetWorkerFieldOfDuty( dynamic_cast<const CScheduler::LocalSchedulerConfig&>( *pConfig.oDSSchedulerConfig ), FieldOfDuty::directSound ) );

				if( pConfig.oERSchedulerConfig && pConfig.oERSchedulerConfig->sSchedulerType == CScheduler::GetType( ) )
					m_pERScheduler = std::make_unique<CScheduler>(
					    SetWorkerFieldOfDuty( dynamic_cast<const CScheduler::LocalSchedulerConfig&>( *pConfig.oERSchedulerConfig ), FieldOfDuty::earlyReflections ) );

				if( pConfig.oDDSchedulerConfig && pConfig.oDDSchedulerConfig->sSchedulerType == CScheduler::GetType( ) )
					m_pDDScheduler = std::make_unique<CScheduler>(
					    SetWorkerFieldOfDuty( dynamic_cast<const CScheduler::LocalSchedulerConfig&>( *pConfig.oDDSchedulerConfig ), FieldOfDuty::diffuseDecay ) );

				Run( );
			}

			CMasterSimulationController::~CMasterSimulationController( )
			{
				m_bStopIndicated = true;
				m_evTriggerLoop.SignalEvent( );

				while( m_bStopACK != true )
					VistaTimeUtils::Sleep( 250 );

				m_evTriggerLoop.SignalEvent( );

				StopGently( true );
			}

			void CMasterSimulationController::PushUpdate( std::unique_ptr<IUpdateMessage> pUpdateMessage )
			{
				// PROFILER_FUNCTION ( );
				PROFILER_VALUE( "Enqueued Update to Master Simulation Controller", pUpdateMessage->GetID( ) );
				m_qUpdateQueue.push( std::move( pUpdateMessage ) );
				m_evTriggerLoop.SignalEvent( );
			}

			void CMasterSimulationController::AttachResultHandler( IResultHandler* pResultHandler ) const
			{
				m_pDSScheduler->AttachResultHandler( pResultHandler );
				m_pERScheduler->AttachResultHandler( pResultHandler );
				m_pDDScheduler->AttachResultHandler( pResultHandler );
			}

			void CMasterSimulationController::DetachResultHandler( IResultHandler* pResultHandler ) const
			{
				m_pDSScheduler->DetachResultHandler( pResultHandler );
				m_pERScheduler->DetachResultHandler( pResultHandler );
				m_pDDScheduler->DetachResultHandler( pResultHandler );
			}

			bool CMasterSimulationController::IsBusy( ) { return m_pDSScheduler->IsBusy( ) | m_pERScheduler->IsBusy( ) | m_pDDScheduler->IsBusy( ); }


			bool CMasterSimulationController::LoopBody( )
			{
				// Wait for trigger
				m_evTriggerLoop.WaitForEvent( true );
				m_evTriggerLoop.ResetThisEvent( );

				PROFILER_FUNCTION( );

				// Handle the stop of the loop
				if( m_bStopIndicated )
				{
					IndicateLoopEnd( );
					PROFILER_EVENT_COUNT( "Loop Stop" );
					m_bStopACK = true;
					return false;
				}

				// get the updates from the queue to a list.
				std::unique_ptr<IUpdateMessage> updateMessage;

				PROFILER_SECTION( "Updates queue -> list" );
				while( m_qUpdateQueue.try_pop( updateMessage ) )
				{
					m_lUpdateList.push_back( std::move( updateMessage ) );
				}
				PROFILER_END_SECTION( );

				if( m_bReplaceUpdates )
					FilterReplace( );

				PROFILER_SECTION( "Distribute Updates" );
				for( auto& update: m_lUpdateList )
				{
					const auto rawUpdate = update.get( );

					PROFILER_VALUE( "Update Distributed", rawUpdate->GetID( ) );

					if( const auto rawUpdateScene = dynamic_cast<CUpdateScene*>( rawUpdate ) )
					{
						// We make three copies of the update message, one for each scheduler.
						m_pDSScheduler->PushUpdate( std::make_unique<CUpdateScene>( *rawUpdateScene ) );
						m_pERScheduler->PushUpdate( std::make_unique<CUpdateScene>( *rawUpdateScene ) );
						m_pDDScheduler->PushUpdate( std::make_unique<CUpdateScene>( *rawUpdateScene ) );
					}
					else
					{
						const auto rawUpdateConfig = dynamic_cast<CUpdateConfig*>( rawUpdate );

						// We make three copies of the update message, one for each scheduler.
						m_pDSScheduler->PushUpdate( std::make_unique<CUpdateConfig>( *rawUpdateConfig ) );
						m_pERScheduler->PushUpdate( std::make_unique<CUpdateConfig>( *rawUpdateConfig ) );
						m_pDDScheduler->PushUpdate( std::make_unique<CUpdateConfig>( *rawUpdateConfig ) );
					}
				}
				PROFILER_END_SECTION( );

				// Now, the list only contains non valid unique_ptr, we can clear it.
				m_lUpdateList.clear( );

				return true;
			}

			void CMasterSimulationController::PreLoop( )
			{
				SetThreadName( "Master Simulation Controller" );
				PROFILER_NAME_THREAD( GetThreadName( ) );
			}

			void CMasterSimulationController::FilterReplace( ) { CReplacementFilter::FilterReplace( m_lUpdateList ); }

			void CMasterSimulationController::SetScheduler( std::unique_ptr<ISchedulerInterface> DSScheduler, std::unique_ptr<ISchedulerInterface> ERScheduler,
			                                                std::unique_ptr<ISchedulerInterface> DDScheduler )
			{
				m_pDSScheduler = std::move( DSScheduler );
				m_pERScheduler = std::move( ERScheduler );
				m_pDDScheduler = std::move( DDScheduler );
			}

			CScheduler::LocalSchedulerConfig CMasterSimulationController::SetWorkerFieldOfDuty( const CScheduler::LocalSchedulerConfig& oConfig, FieldOfDuty eFOD )
			{
				auto configCopy = oConfig;

				for( auto&& workerConfig: configCopy.vpWorkerConfigs )
				{
					if( auto roomAcousticWorker = std::dynamic_pointer_cast<IRoomAcousticsWorkerInterface::RoomAcousticsWorkerInterfaceConfig>( workerConfig ) )
					{
						roomAcousticWorker->eFOD = eFOD;

						workerConfig = roomAcousticWorker;
					}
					else if( auto remoteWorker = std::dynamic_pointer_cast<CWorkerRemote::WorkerRemoteConfig>( workerConfig ) )
					{
					}
					else
					{
						ITA_EXCEPT_INVALID_PARAMETER( "Worker is not a room acoustics worker." )
					}
				}

				return configCopy;
			}
		} // namespace RoomAcoustics
	} // namespace SimulationScheduler
} // namespace ITA