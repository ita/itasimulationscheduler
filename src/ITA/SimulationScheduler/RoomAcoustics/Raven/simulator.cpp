// Header include
#include "ITA/SimulationScheduler/RoomAcoustics/Raven/simulator.h"
#include "ITASimulationScheduler_instrumentation.h"

#include <ITA/SimulationScheduler/Utils/utils.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <RD_instrumentation.h>

#include <nlohmann/json.hpp>

#include <ITAException.h>

#include <string>
#include <tuple>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			namespace Raven
			{
#define RAVENSIMULATOR_REQUIRE_UNINITIALISED                                                                               \
	{                                                                                                                      \
		if( operator<( m_eStatus, SimulatorStatus::uninitialized ) )                                                       \
			ITA_EXCEPT1( MODAL_EXCEPTION, "Uninitialized mode required for this method, use Reset() prior to this call" ); \
	};
#define RAVENSIMULATOR_REQUIRE_IDLE                                                                                          \
	{                                                                                                                        \
		if( operator<( m_eStatus, SimulatorStatus::idle ) )                                                                  \
			ITA_EXCEPT1( MODAL_EXCEPTION, "Idle mode required for this method, use Reset() and Init() prior to this call" ); \
	};
#define RAVENSIMULATOR_REQUIRE_SCENE                                                                                     \
	{                                                                                                                    \
		if( operator<( m_eStatus, SimulatorStatus::sceneLoaded ) )                                                       \
			ITA_EXCEPT1( MODAL_EXCEPTION, "Loaded scene required for this method, use LoadScene() prior to this call" ); \
	};
#define RAVENSIMULATOR_REQUIRE_FILTER_ENGINE                                                                                           \
	{                                                                                                                                  \
		if( operator<( m_eStatus, SimulatorStatus::filterEngineLoaded ) )                                                              \
			ITA_EXCEPT1( MODAL_EXCEPTION, "Loaded filter engine required for this method, probably an error in LoadScene() occured" ); \
	};


				const int g_iMaxNumSources   = 1;
				const int g_iMaxNumReceivers = 1;

				CSimulator::CSimulator( FieldOfDuty eFieldOfDuty, const std::string& sRavenProjectFilePath )
				    : m_eStatus( SimulatorStatus::uninitialized )
				    , m_pRavenMasterController( nullptr )
				    , m_sRavenProjectFilePath( sRavenProjectFilePath )
				    , m_eFieldOfDuty( eFieldOfDuty )
				{
					m_pRavenMasterController = std::make_unique<R_MasterController>( );

					m_pRavenMasterController->m_currentConfig_General.applicationMode = NET_MODE;

					Init( ); // Danach: IDLE

					if( !m_sRavenProjectFilePath.empty( ) )
						CSimulator::LoadScene( m_sRavenProjectFilePath );
				}

				CSimulator::~CSimulator( )
				{
					m_pRavenMasterController->killProject( NET_MODE, false );
					DeregisterWorkerThread( );
				}

				void CSimulator::Reset( )
				{
					PROFILER_EVENT_COUNT( "Reset" );
					m_oCurScene.Reset( );
					m_oNewScene.Reset( );

					m_mapReceivers.clear( );
					m_mapSources.clear( );

					for( int i = 0; i < g_iMaxNumReceivers; i++ )
						m_mapStaticReceiversList[i] = false;

					for( int i = 0; i < g_iMaxNumSources; i++ )
						m_mapStaticSourcesList[i] = false;

					Init( );

					if( !m_sRavenProjectFilePath.empty( ) )
						CSimulator::LoadScene( m_sRavenProjectFilePath );
				}

				void CSimulator::LoadScene( const std::string& sFileName )
				{
					PROFILER_FUNCTION( );

					if( sFileName.empty( ) )
						return;

					// Initialisieren, falls das noch nicht geschehen ist
					if( m_eStatus == SimulatorStatus::uninitialized )
						Init( );

					if( m_eStatus != SimulatorStatus::idle )
						Reset( );

					RAVENSIMULATOR_REQUIRE_IDLE;

					if( m_pRavenMasterController->loadSceneFromProjectFile( sFileName, false, false, true, false ) == EXIT_FAILURE )
					{
						m_pRavenMasterController->killProject( NET_MODE, false );
						ITA_EXCEPT_INVALID_PARAMETER( "project File could not be loaded" );
					}

					// Simulationskonfiguration Raven
					t_simulation_config_Simulation& scs( m_pRavenMasterController->m_currentConfig_Simulation );

					if( m_eFieldOfDuty == FieldOfDuty::directSound )
					{
						scs.ISOrder_PS = 0;
						scs.ISOrder_SS = 0;
					}

					scs.generateBRIR               = 1;
					scs.noAirAbsorption            = 0;
					scs.simulationIS               = 1;
					scs.simulationRT               = 1;
					scs.simulationAR               = 1;
					scs.ISSkipImageSourcesPS       = 1;
					scs.ISSkipImageSourcesSS       = 1;
					scs.filtersUseDiskInsteadOfRam = false;
					scs.exportFilter               = 0;
					scs.exportHistogram            = 0;
					scs.exportWallHitList          = 0;

					t_simulation_config_Visualization& scv( m_pRavenMasterController->m_currentConfig_Visualization );
					scv.m_calcEnergyQuantities        = 0;
					scv.m_calcRoomAcousticParameters  = 0;
					scv.m_exportVisualizationData     = 0;
					scv.m_fileName_Export             = "";
					scv.m_frameRate                   = 256; // Irgendwas, RavenSimulator kennt keine FrameRate oder BlockSize
					scv.m_logVisualizationPerformance = 0;
					scv.m_simLength_ParticleView      = 1500;
					scv.m_simStep_ParticleView        = 5;
					scv.m_simVoxelSize                = 1;
					scv.m_visMode                     = 0;

					t_simulation_config_EdgeDiffraction& sce( m_pRavenMasterController->m_currentConfig_EdgeDiffraction );
					sce.DiffractionEdgeEnds.clear( );
					sce.DiffractionEdgeNames.clear( );
					sce.DiffractionEdgeNorms.clear( );
					sce.DiffractionEdgeStarts.clear( );
					sce.DiffractionEdgeStates.clear( );
					sce.DiffractionEdgeThetas.clear( );
					sce.EDenabled    = 0;
					sce.EDISOrder    = 0;
					sce.EDOrder      = 0;
					sce.PreEDISOrder = 0;

					m_pRavenMasterController->initLODEngine( );

					m_eStatus = SimulatorStatus::sceneLoaded;

					bool bSourcesPresent   = m_pRavenMasterController->getAllPrimarySources( ).size( ) > 0;
					bool bReceiversPresent = m_pRavenMasterController->getAllReceivers( ).size( ) > 0;

					if( bSourcesPresent || bReceiversPresent )
					{
						Reset( );
						ITA_EXCEPT1(
						    CONFIG_ERROR,
						    "Neither sources nor receivers can be added to the scene using the Raven Project File, please remove the relevant lines from the project" )
					}

					// Add static number of sound sources
					int iAssignedRoomID = -1;
					m_mapStaticSourcesList.clear( );
					for( int i = 0; i < g_iMaxNumSources; i++ )
					{
						std::string sName          = "Raven Simulator Primary Source " + IntToString( i );
						float fSoundLeveldB        = 100;
						int iRavenNewSoundSourceID = m_pRavenMasterController->addPrimarySource( sName, RG_Vector( 0, 0, 0 ), RG_Vector( 0, 0, -1 ), RG_Vector( 0, 1, 0 ),
						                                                                         "", "", fSoundLeveldB, 0, 0, 0, iAssignedRoomID );
						m_mapStaticSourcesList.insert( std::pair<int, bool>( iRavenNewSoundSourceID, false ) );
					}

					// Add and initialize static number of sound receivers
					iAssignedRoomID = -1;
					m_mapStaticReceiversList.clear( );
					for( int i = 0; i < g_iMaxNumReceivers; i++ )
					{
						std::string sName       = "Raven Simulator Receiver " + IntToString( i );
						int iRavenNewReceiverID = m_pRavenMasterController->addReceiver( sName, RG_Vector( 0, 1.7f, 0 ), RG_Vector( 0, 0, -1 ), RG_Vector( 0, 1, 0 ),
						                                                                 RECEIVER_ACTIVE, iAssignedRoomID );
						m_pRavenMasterController->initReceiver( iRavenNewReceiverID );
						m_mapStaticReceiversList.insert( std::pair<int, bool>( iRavenNewReceiverID, false ) );
					}

					// Post-init of sound source
					std::map<int, bool>::const_iterator cit = m_mapStaticSourcesList.begin( );
					while( cit != m_mapStaticSourcesList.end( ) )
					{
						int iRavenSoundSourceID = cit->first;

						// Initialize source 
						if( EXIT_SUCCESS != m_pRavenMasterController->initPrimarySource( iRavenSoundSourceID ) )
							ITA_EXCEPT1( UNKNOWN, "Could not initialise primary source" );

						// Initialize image source
						if( EXIT_SUCCESS != m_pRavenMasterController->initImageSources( iRavenSoundSourceID, true, true, false, true ) )
							ITA_EXCEPT1( UNKNOWN, "Could not initialise image sources" );

						cit++;
					}

					// Initialize Ray Tracing
					if( EXIT_SUCCESS != m_pRavenMasterController->initRavenRayTracerObjects_Receiver( ) )
						ITA_EXCEPT1( UNKNOWN, "Could not initialize Raven ray tracer objects for the receiver" );

					if( EXIT_SUCCESS != m_pRavenMasterController->initRavenFilterController( ) )
						ITA_EXCEPT1( UNKNOWN, "Could not initialize filter controller" );

					// Artificial reverberation init
					m_pRavenMasterController->initRavenFilterControllerAR( );

					m_eStatus = SimulatorStatus::filterEngineLoaded;

					PROFILER_EVENT_COUNT( "Scene loaded" );
				}

				void CSimulator::RegisterWorkerThread( IRavenWorkerInterface* pParentThread )
				{
					m_pParentThread = std::move( pParentThread );
				}

				void CSimulator::DeregisterWorkerThread( )
				{
					m_pParentThread = nullptr;
				}

				void CSimulator::Compute( CSimulationTask* pTask, CRavenSimulationResult* pResult )
				{
					PROFILER_FUNCTION( );

					assert( pTask );

					pTask->oProfiler.dComputeTime = 0;

					PROFILER_SECTION( "Adapt Scene" );
					std::vector<int> viSourcesOutsideRoom;
					std::vector<int> viReceiversOutsideRoom;
					SetScene( pTask->oScene );
					AdaptScene( viSourcesOutsideRoom, viReceiversOutsideRoom );
					PROFILER_END_SECTION( );

					// Simulate

					switch( m_eFieldOfDuty )
					{
						case FieldOfDuty::directSound:
							ComputeDirectSound( pResult->vcspResult );
							break;
						case FieldOfDuty::earlyReflections:
							ComputeImageSources( pResult->vcspResult, ITABase::MixingMethod::OVERWRITE );
							break;
						case FieldOfDuty::diffuseDecay:
							ComputeRayTracing( pResult->vcspResult, ITABase::MixingMethod::ADD );
							break;
						default:
							break;
					}

					// Inside or outside room?
					for( auto& pCSP: pResult->vcspResult )
					{
						pCSP->bEntitiesInSameRoom = true;

						for( auto j: viSourcesOutsideRoom )
						{
							if( pCSP->iSourceID == j )
							{
								pCSP->bEntitiesInSameRoom = false;
								break;
							}
						}
						for( auto j: viReceiversOutsideRoom )
						{
							if( pCSP->iReceiverID == j )
							{
								pCSP->bEntitiesInSameRoom = false;
								break;
							}
						}
					}
				}

				void CSimulator::WriteToRpf( const std::string& sFileName ) const { m_pRavenMasterController->writeProjectFileFromScene( sFileName ); }

				void CSimulator::Init( )
				{
					PROFILER_FUNCTION( );
					m_pRavenMasterController->clearCurrentConfig( NET_MODE, true );

					m_pRavenMasterController->initLODEngine( );
					// todo : do we need this?
					m_pRavenMasterController->initPortalsAndPortalSenders( );

					if( m_eStatus == SimulatorStatus::uninitialized )
						m_pRavenMasterController->initStaticCopyOfGeometry( );

					// Completely reset raven to be on the safe side (Also loads some default values from config)
					m_pRavenMasterController->killProject( NET_MODE, false );

					m_eStatus = SimulatorStatus::idle;
				}

				void CSimulator::SetScene( const CRavenScene& oScene )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;

					if( oScene.GetSourceMap( ).size( ) > m_mapStaticSourcesList.size( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Too many receivers" );
					if( oScene.GetReceiverMap( ).size( ) > m_mapStaticReceiversList.size( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Too many receivers" );

					m_oNewScene.Reset( );

					// set the new scene
					m_oNewScene = oScene;

					assert( m_oNewScene.GetSourceMap( ).size( ) == oScene.GetSourceMap( ).size( ) );
					assert( m_oNewScene.GetReceiverMap( ).size( ) == oScene.GetReceiverMap( ).size( ) );
				}

				void CSimulator::AdaptScene( std::vector<int>& viSourcesOutsideRoom, std::vector<int>& viReceiversOutsideRoom )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;

					CRavenScene::CSceneDifference oDiff = m_oCurScene.Difference( m_oNewScene );

					viSourcesOutsideRoom.clear( );
					viReceiversOutsideRoom.clear( );

					// Update internal lists

					// Sources
					for( int i = 0; i < (int)oDiff.sourceDifferences.viDelObjects.size( ); i++ )
					{
						int iID = oDiff.sourceDifferences.viDelObjects[i];
						RemoveSource( iID );
					}
					for( int i = 0; i < (int)oDiff.sourceDifferences.viNewObjects.size( ); i++ )
					{
						int iID = oDiff.sourceDifferences.viNewObjects[i];
						AddSource( iID );
						auto eFlag              = CRavenScene::CSourceState::DifferenceFlags::position | CRavenScene::CSourceState::DifferenceFlags::orientation;
						bool bInsideCurrentRoom = SetSourceState( iID, m_oNewScene.GetSourceMap( ).at( iID ), eFlag );
						if( !bInsideCurrentRoom )
							viSourcesOutsideRoom.push_back( iID );
					}
					for( int i = 0; i < (int)oDiff.sourceDifferences.viModObjects.size( ); i++ )
					{
						int iID                 = oDiff.sourceDifferences.viModObjects[i];
						auto eDifferenceFlags   = m_oCurScene.GetSourceMap( ).at( iID ).Compare( m_oNewScene.GetSourceMap( ).at( iID ) );
						bool bInsideCurrentRoom = SetSourceState( iID, m_oNewScene.GetSourceMap( ).at( iID ), eDifferenceFlags );
						if( !bInsideCurrentRoom )
							viSourcesOutsideRoom.push_back( iID );
					}

					// Receivers
					for( int i = 0; i < (int)oDiff.receiverDifferences.viDelObjects.size( ); i++ )
					{
						int iID = oDiff.receiverDifferences.viDelObjects[i];
						RemoveReceiver( iID );
					}
					for( int i = 0; i < (int)oDiff.receiverDifferences.viNewObjects.size( ); i++ )
					{
						int iID = oDiff.receiverDifferences.viNewObjects[i];
						AddReceiver( iID );
						auto eFlag              = CRavenScene::CReceiverState::DifferenceFlags::position | CRavenScene::CReceiverState::DifferenceFlags::orientation;
						bool bInsideCurrentRoom = SetReceiverState( iID, m_oNewScene.GetReceiverMap( ).at( iID ), eFlag );
						if( !bInsideCurrentRoom )
							viReceiversOutsideRoom.push_back( iID );
					}
					for( int i = 0; i < (int)oDiff.receiverDifferences.viModObjects.size( ); i++ )
					{
						int iID                 = oDiff.receiverDifferences.viModObjects[i];
						auto eDifferenceFlags   = m_oCurScene.GetReceiverMap( ).at( iID ).Compare( m_oNewScene.GetReceiverMap( ).at( iID ) );
						bool bInsideCurrentRoom = SetReceiverState( iID, m_oNewScene.GetReceiverMap( ).at( iID ), eDifferenceFlags );

						auto state = m_oNewScene.GetReceiverState( iID );
						m_oNewScene.SetReceiverState( iID, state );
						// getReceiverMap ( ).at ( iID ).bInsideRoom = bInsideCurrentRoom;
						if( !bInsideCurrentRoom )
							viReceiversOutsideRoom.push_back( iID );
					}

					// Switch to new scene and clean up
					m_oCurScene = m_oNewScene;
					m_oNewScene.Reset( );
				}

				void CSimulator::ComputeDirectSoundAudibility( std::vector<std::unique_ptr<CRavenSimulationResult::ComplexSimulationSoundPath> >& vpResult )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_FILTER_ENGINE;

					const int iInitialFilterSize = 1;

					const int iNumReceivers = (int)m_mapReceivers.size( );
					const int iNumSources   = (int)m_mapSources.size( );
					vpResult.reserve( vpResult.size( ) + iNumReceivers * iNumSources ); // Portals still missing!!!!

					std::map<int, int>::const_iterator rcit = m_mapReceivers.begin( );
					while( rcit != m_mapReceivers.end( ) )
					{
						int iReceiverID                         = rcit->first;
						std::map<int, int>::const_iterator scit = m_mapSources.begin( );
						while( scit != m_mapSources.end( ) )
						{
							int iSourceID = scit->first;

							// TODO: später aus Pool holen
							auto pCSSP            = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
							pCSSP->iNumChannels   = 2;
							pCSSP->iReceiverID    = iReceiverID;
							pCSSP->iSourceID      = iSourceID;
							pCSSP->bEmpty         = true;
							pCSSP->iLeadingZeros  = 0;
							pCSSP->iTailingZeros  = 0;
							pCSSP->bZerosStripped = false;
							pCSSP->psfResult      = std::make_unique<ITASampleFrame>( 2, iInitialFilterSize, true );

							ComputeDirectSoundAudibility( iSourceID, iReceiverID, pCSSP->bDirectSoundAudible );

							vpResult.push_back( std::move( pCSSP ) );

							++scit;
						}
						++rcit;
					}
				}

				void CSimulator::ComputeDirectSoundAudibility( const int iSourceID, const int iReceiverID, bool& bAudibility )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_SCENE;

					std::map<int, int>::iterator it = m_mapSources.find( iSourceID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );
					unsigned int iRavenSoundSourceID = (unsigned int)it->second;

					it = m_mapReceivers.find( iReceiverID );
					if( it == m_mapReceivers.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );
					unsigned int iRavenReceiverID = (unsigned int)it->second;

					int iRoomID = 0;
					std::list<unsigned int> liInvolvedRooms;
					liInvolvedRooms.push_back( (unsigned int)iRoomID );
					std::list<RE_AudibleImageSource> loAudibleImageSources;

					PROFILER_SECTION( "Compute DS Audibility" );

					int iLevelOfDetail = 0;

					auto iAccelerationMode = m_pRavenMasterController->m_currentConfig_Simulation.accelerationType;

					bAudibility = m_pRavenMasterController->computeAudibilityOfDirectSound( iRavenSoundSourceID, iRavenReceiverID, iAccelerationMode, iLevelOfDetail );

					PROFILER_END_SECTION( );

					return;
				}

				void CSimulator::ComputeDirectSound( std::vector<std::unique_ptr<CRavenSimulationResult::ComplexSimulationSoundPath> >& vpResult,
				                                     const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_FILTER_ENGINE;

					t_simulation_config_Simulation& scs( m_pRavenMasterController->m_currentConfig_Simulation );

					const double dSampleRate = m_pRavenMasterController->m_currentConfig_Simulation.samplingFrequency;
					const float fReverbTime =
					    m_pRavenMasterController->m_currentConfig_Simulation.filterLength_DetectionSphere / 1000; // the filter length of raven is in ms
					const int iInitialFilterSize = (int)( fReverbTime * dSampleRate );

					const int iNumReceivers = (int)m_mapReceivers.size( );
					const int iNumSources   = (int)m_mapSources.size( );
					vpResult.reserve( vpResult.size( ) + iNumReceivers * iNumSources ); // Portals still missing!!!!

					std::map<int, int>::const_iterator rcit = m_mapReceivers.begin( );
					while( rcit != m_mapReceivers.end( ) )
					{
						int iReceiverID                         = rcit->first;
						std::map<int, int>::const_iterator scit = m_mapSources.begin( );
						while( scit != m_mapSources.end( ) )
						{
							int iSourceID = scit->first;

							auto pCSSP            = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
							pCSSP->iNumChannels   = 2;
							pCSSP->iReceiverID    = iReceiverID;
							pCSSP->iSourceID      = iSourceID;
							pCSSP->bEmpty         = true;
							pCSSP->iLeadingZeros  = 0;
							pCSSP->iTailingZeros  = 0;
							pCSSP->bZerosStripped = false;
							pCSSP->psfResult      = std::make_unique<ITASampleFrame>( 2, iInitialFilterSize, true ); // TODO new Element from Pool!

							ComputeDirectSound( iSourceID, iReceiverID, *pCSSP->psfResult, iResultMixingMode );

							// Determine leading and trailing zeros, then deactivate Empty-Flag
							int n = (int)pCSSP->psfResult->length( );
							for( int i = 0; i < n; i++ )
							{
								if( ( ( *pCSSP->psfResult )[0][i] != 0.0f ) || ( ( *pCSSP->psfResult )[1][i] != 0.0f ) )
								{
									pCSSP->iLeadingZeros = i;
									pCSSP->bEmpty        = false;
									break;
								}
							}
							if( !pCSSP->bEmpty )
							{
								for( int j = 0; j < n; j++ )
								{
									if( ( ( *pCSSP->psfResult )[0][n - j - 1] != 0.0f ) || ( ( *pCSSP->psfResult )[1][n - j - 1] != 0.0f ) )
									{
										pCSSP->iTailingZeros = j;
										break;
									}
								}
							}

							vpResult.push_back( std::move( pCSSP ) );

							++scit;
						}
						++rcit;
					}
				}

				void CSimulator::ComputeDirectSound( const int iSourceID, const int iReceiverID, ITASampleFrame& oFilter, const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_SCENE;

					std::map<int, int>::iterator it = m_mapSources.find( iSourceID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );
					unsigned int iRavenSoundSourceID = (unsigned int)it->second;

					it = m_mapReceivers.find( iReceiverID );
					if( it == m_mapReceivers.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );
					unsigned int iRavenReceiverID = (unsigned int)it->second;

					int iRoomID = 0;
					std::list<unsigned int> liInvolvedRooms;
					liInvolvedRooms.push_back( (unsigned int)iRoomID );

					PROFILER_SECTION( "Compute DS" );
					t_simulationPaths_Vis_IS tmpIS;
					if( EXIT_SUCCESS != m_pRavenMasterController->computeAudibleImageSources( iRavenSoundSourceID, MODE_BSP, true, false, false, false, tmpIS ) )
						ITA_EXCEPT1( UNKNOWN, "Could not compute audible image sources" );
					PROFILER_END_SECTION( );

					PROFILER_SECTION( "Compute DS Filter" );
					bool bSkipDirectSound = false; // !!!
					if( EXIT_SUCCESS != m_pRavenMasterController->buildBRIRFilter_IS( iRavenSoundSourceID, false, false, bSkipDirectSound ) )
						ITA_EXCEPT1( UNKNOWN, "Could not build BRIR filter for image sources" );
					PROFILER_END_SECTION( );

					RF_Filter* pTmpFilterCh1;
					RF_Filter* pTmpFilterCh2;
					m_pRavenMasterController->getBRIRFilter_Pointer( pTmpFilterCh1, pTmpFilterCh2, iRavenSoundSourceID, iRavenReceiverID, SIM_PS2R, 1, 0 );

					int iResultFilterSize = (int)pTmpFilterCh1->m_filterCoefficients.size( );
					R_INFO( "IS filter size = {}", iResultFilterSize );

					if( iResultFilterSize == 0 )
					{
						oFilter.zero( );
					}
					else
					{
						assert( oFilter.channels( ) == 2 );
						assert( !pTmpFilterCh1->m_filterCoefficients.empty( ) );
						assert( !pTmpFilterCh2->m_filterCoefficients.empty( ) );

						if( iResultFilterSize > oFilter.GetLength( ) )
							R_WARN( "Simulation result filter size is greater than requested filter length, will crop result." );

						int iMaxFilterSize = std::min( iResultFilterSize, oFilter.GetLength( ) );

						float* pfDataCh1 = &( pTmpFilterCh1->m_filterCoefficients[0] );
						float* pfDataCh2 = &( pTmpFilterCh2->m_filterCoefficients[0] );

						if( iResultMixingMode == ITABase::MixingMethod::ADD )
						{
							oFilter[0].AddSamples( pfDataCh1, iMaxFilterSize );
							oFilter[1].AddSamples( pfDataCh2, iMaxFilterSize );
						}
						else
						{
							oFilter[0].WriteSamples( pfDataCh1, iMaxFilterSize );
							oFilter[1].WriteSamples( pfDataCh2, iMaxFilterSize );
						}
					}
				}

				void CSimulator::ComputeImageSources( std::vector<std::unique_ptr<CRavenSimulationResult::ComplexSimulationSoundPath> >& vpResult,
				                                      const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_FILTER_ENGINE;

					const double dSampleRate = m_pRavenMasterController->m_currentConfig_Simulation.samplingFrequency;
					const float fReverbTime =
					    m_pRavenMasterController->m_currentConfig_Simulation.filterLength_DetectionSphere / 1000; // the filter length of raven is in ms
					const int iInitialFilterSize = (int)( fReverbTime * dSampleRate );

					const int iNumReceivers = (int)m_mapReceivers.size( );
					const int iNumSources   = (int)m_mapSources.size( );
					vpResult.reserve( vpResult.size( ) + iNumReceivers * iNumSources ); // Portals still missing!!!!

					std::map<int, int>::const_iterator rcit = m_mapReceivers.begin( );
					while( rcit != m_mapReceivers.end( ) )
					{
						int iReceiverID                         = rcit->first;
						std::map<int, int>::const_iterator scit = m_mapSources.begin( );
						while( scit != m_mapSources.end( ) )
						{
							int iSourceID = scit->first;

							auto pCSSP            = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
							pCSSP->iNumChannels   = 2;
							pCSSP->iReceiverID    = iReceiverID;
							pCSSP->iSourceID      = iSourceID;
							pCSSP->bEmpty         = true;
							pCSSP->iLeadingZeros  = 0;
							pCSSP->iTailingZeros  = 0;
							pCSSP->bZerosStripped = false;
							pCSSP->psfResult      = std::make_unique<ITASampleFrame>( 2, iInitialFilterSize, true ); // TODO new Element from Pool!

							ComputeImageSources( iSourceID, iReceiverID, *pCSSP->psfResult, iResultMixingMode );

							// Determine leading and trailing zeros and deactivate empty flag
							int n = (int)pCSSP->psfResult->length( );
							for( int i = 0; i < n; i++ )
							{
								if( ( ( *pCSSP->psfResult )[0][i] != 0.0f ) || ( ( *pCSSP->psfResult )[1][i] != 0.0f ) )
								{
									pCSSP->iLeadingZeros = i;
									pCSSP->bEmpty        = false;
									break;
								}
							}
							if( !pCSSP->bEmpty )
							{
								for( int j = 0; j < n; j++ )
								{
									if( ( ( *pCSSP->psfResult )[0][n - j - 1] != 0.0f ) || ( ( *pCSSP->psfResult )[1][n - j - 1] != 0.0f ) )
									{
										pCSSP->iTailingZeros = j;
										break;
									}
								}
							}

							vpResult.push_back( std::move( pCSSP ) );

							++scit;
						}
						++rcit;
					}
				}

				void CSimulator::ComputeImageSources( const int iSourceID, const int iReceiverID, ITASampleFrame& oFilter, const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_SCENE;

					std::map<int, int>::iterator it = m_mapSources.find( iSourceID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );
					unsigned int iRavenSoundSourceID = (unsigned int)it->second;

					it = m_mapReceivers.find( iReceiverID );
					if( it == m_mapReceivers.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );
					unsigned int iRavenReceiverID = (unsigned int)it->second;

					int iRoomID = 0;
					std::list<unsigned int> liInvolvedRooms;
					liInvolvedRooms.push_back( (unsigned int)iRoomID );

					PROFILER_SECTION( "Compute ER" );
					t_simulationPaths_Vis_IS tmpIS;
					if( EXIT_SUCCESS != m_pRavenMasterController->computeAudibleImageSources( iRavenSoundSourceID, MODE_BSP, true, false, false, false, tmpIS ) )
						ITA_EXCEPT1( UNKNOWN, "Could not compute audible image sources" );
					PROFILER_END_SECTION( );

					PROFILER_SECTION( "Compute ER Filter" );
					bool bSkipDirectSound = true; // !!!
					if( EXIT_SUCCESS != m_pRavenMasterController->buildBRIRFilter_IS( iRavenSoundSourceID, false, false, bSkipDirectSound ) )
						ITA_EXCEPT1( UNKNOWN, "Could not build BRIR filter for image sources" );
					PROFILER_END_SECTION( );

					RF_Filter* pTmpFilterCh1;
					RF_Filter* pTmpFilterCh2;
					m_pRavenMasterController->getBRIRFilter_Pointer( pTmpFilterCh1, pTmpFilterCh2, iRavenSoundSourceID, iRavenReceiverID, SIM_PS2R, 1, 0 );

					int iResultFilterSize = (int)pTmpFilterCh1->m_filterCoefficients.size( );
					R_INFO( "IS filter size = {}", iResultFilterSize );

					if( iResultFilterSize == 0 )
					{
						oFilter.zero( );
					}
					else
					{
						assert( oFilter.channels( ) == 2 );
						assert( !pTmpFilterCh1->m_filterCoefficients.empty( ) );
						assert( !pTmpFilterCh2->m_filterCoefficients.empty( ) );

						if( iResultFilterSize > oFilter.GetLength( ) )
							R_WARN( "Simulation result filter size is greater than requested filter length, will crop result." );

						int iMaxFilterSize = std::min( iResultFilterSize, oFilter.GetLength( ) );

						float* pfDataCh1 = &( pTmpFilterCh1->m_filterCoefficients[0] );
						float* pfDataCh2 = &( pTmpFilterCh2->m_filterCoefficients[0] );

						if( iResultMixingMode == ITABase::MixingMethod::ADD )
						{
							oFilter[0].AddSamples( pfDataCh1, iMaxFilterSize );
							oFilter[1].AddSamples( pfDataCh2, iMaxFilterSize );
						}
						else
						{
							oFilter[0].WriteSamples( pfDataCh1, iMaxFilterSize );
							oFilter[1].WriteSamples( pfDataCh2, iMaxFilterSize );
						}
					}
				}

				void CSimulator::ComputeRayTracing( std::vector<std::unique_ptr<CRavenSimulationResult::ComplexSimulationSoundPath> >& vpResult,
				                                    const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_FILTER_ENGINE;

					const double dSampleRate = m_pRavenMasterController->m_currentConfig_Simulation.samplingFrequency;
					const float fReverbTime =
					    m_pRavenMasterController->m_currentConfig_Simulation.filterLength_DetectionSphere / 1000; // the filter length of raven is in ms
					const int iInitialFilterSize = (int)( fReverbTime * dSampleRate );

					const int iNumReceivers = (int)m_mapReceivers.size( );
					const int iNumSources   = (int)m_mapSources.size( );

					std::vector<ITASampleFrame*> vpsfResults;

					std::vector<int> viReceiverIDs;
					std::map<int, int>::const_iterator rcit = m_mapReceivers.begin( );
					while( rcit != m_mapReceivers.end( ) )
						viReceiverIDs.push_back( ( rcit++ )->first );

					std::map<int, int>::const_iterator scit = m_mapSources.begin( );
					while( scit != m_mapSources.end( ) )
					{
						int iSourceID = scit->first;

						// prepare results
						for( int i = 0; i < iNumReceivers; i++ )
						{
							auto pCSSP            = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
							pCSSP->iNumChannels   = 2;
							pCSSP->iReceiverID    = viReceiverIDs[i];
							pCSSP->iSourceID      = iSourceID;
							pCSSP->bEmpty         = true;
							pCSSP->iLeadingZeros  = 0;
							pCSSP->iTailingZeros  = 0;
							pCSSP->bZerosStripped = false;
							pCSSP->psfResult      = std::make_unique<ITASampleFrame>( 2, iInitialFilterSize, true ); // TODO new Element from Pool!
							vpsfResults.push_back( pCSSP->psfResult.get( ) ); // todo This is not nice ownership is wierd ... fix this with refactoring
							vpResult.push_back( std::move( pCSSP ) );
						}

						ComputeRayTracing( iSourceID, viReceiverIDs, vpsfResults, iResultMixingMode );

//#if RAVENSIMULATOR_RESULT_PRESERVE_ZEROS == 0
#if 1
						// check results and save information
						for( int i = 0; i < iNumReceivers; i++ )
						{
							auto& pcspCurrentPath            = vpResult[i];
							ITASampleFrame* psfCurrentResult = pcspCurrentPath->psfResult.get( );
							int n                            = (int)pcspCurrentPath->psfResult->length( );

							for( int k = 0; k < n; k++ )
							{
								if( ( ( *pcspCurrentPath->psfResult )[0][k] != 0.0f ) || ( ( *pcspCurrentPath->psfResult )[1][k] != 0.0f ) )
								{
									pcspCurrentPath->iLeadingZeros = k;
									pcspCurrentPath->bEmpty        = false;
									break;
								}
							}
							if( !pcspCurrentPath->bEmpty )
							{
								for( int j = 0; j < n; j++ )
								{
									if( ( ( *pcspCurrentPath->psfResult )[0][n - j - 1] != 0.0f ) || ( ( *pcspCurrentPath->psfResult )[1][n - j - 1] != 0.0f ) )
									{
										pcspCurrentPath->iTailingZeros = j;
										break;
									}
								}
							}
						}

						++scit;
					}
#endif // RAVENSIMULATOR_RESULT_PRESERVE_ZEROS == 0

					return;
				}

				void CSimulator::ComputeRayTracing( const int iSourceID, const std::vector<int>& viReceiverIDs, std::vector<ITASampleFrame*>& vpResult,
				                                    const int iResultMixingMode )
				{
					PROFILER_FUNCTION( );
					RAVENSIMULATOR_REQUIRE_SCENE;

					assert( !viReceiverIDs.empty( ) && ( viReceiverIDs.size( ) == vpResult.size( ) ) );

					auto it = m_mapSources.find( iSourceID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );
					const auto iRavenSoundSourceID = static_cast<unsigned int>( it->second );

					// Raven aufräumen
					m_pRavenMasterController->clearAllRTObjects( );

					const auto iNumberRandomNumbers  = static_cast<unsigned int>( 50e3 );
					const auto iNumberRandomVectors  = static_cast<unsigned int>( 10e3 );
					const unsigned int iDistribution = RNG_EvenlyDistributed_EqSphere;
					RE_PrimarySource* pPrimarySource = m_pRavenMasterController->getPrimarySource( iRavenSoundSourceID );
					pPrimarySource->initParticles( iNumberRandomNumbers, iNumberRandomVectors, iDistribution, false );

					PROFILER_SECTION( "Compute RT" );
					t_simulationPaths_Vis_RT tmpRT;

					unsigned int uiRayTracingAccelerationMode = m_pRavenMasterController->m_currentConfig_Simulation.accelerationType;

					if( EXIT_SUCCESS != m_pRavenMasterController->startRayTracing( iRavenSoundSourceID, false, false, false, uiRayTracingAccelerationMode, true, false,
					                                                               false, tmpRT, false ) )
						ITA_EXCEPT1( UNKNOWN, "Could not start Raven ray tracing" );
					PROFILER_END_SECTION( );


					PROFILER_SECTION( "Compute RT Filter" );
					if( EXIT_SUCCESS != m_pRavenMasterController->buildBRIRFilter_RT( iRavenSoundSourceID, false ) )
						ITA_EXCEPT1( UNKNOWN, "Could not build Raven ray tracer impulse response filter" );
					PROFILER_END_SECTION( );


					for( int i = 0; i < viReceiverIDs.size( ); i++ )
					{
						int iReceiverID = viReceiverIDs[i];
						it              = m_mapReceivers.find( iReceiverID );
						if( it == m_mapReceivers.end( ) )
							ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );
						const auto iRavenReceiverID = static_cast<unsigned int>( it->second );

						RF_Filter* pTmpFilterCh1;
						RF_Filter* pTmpFilterCh2;
						m_pRavenMasterController->getBRIRFilter_Pointer( pTmpFilterCh1, pTmpFilterCh2, iRavenSoundSourceID, iRavenReceiverID, SIM_PS2R, 0, 1 );

						int iFilterSize = pTmpFilterCh1->m_filterCoefficients.size( );

						if( iFilterSize > vpResult[0]->length( ) )
						{
							R_WARN( "Filter size of target filter object is too small, truncating to fit requested size. Increase ReverberationTime." );
							iFilterSize = vpResult[0]->length( );
						}

						float* pfDataCh1 = &( pTmpFilterCh1->m_filterCoefficients[0] );
						float* pfDataCh2 = &( pTmpFilterCh2->m_filterCoefficients[0] );

						if( iResultMixingMode == ITABase::MixingMethod::ADD )
						{
							( *vpResult[i] )[0].AddSamples( pfDataCh1, iFilterSize );
							( *vpResult[i] )[1].AddSamples( pfDataCh2, iFilterSize );
						}
						else
						{
							( *vpResult[i] )[0].WriteSamples( pfDataCh1, iFilterSize );
							( *vpResult[i] )[1].WriteSamples( pfDataCh2, iFilterSize );
						}

						float fFilterEnergy[2] = { 0, 0 };
						for( int n = 0; n < iFilterSize; n++ )
						{
							fFilterEnergy[0] += pow( ( *vpResult[i] )[0][n], 2.0f );
							fFilterEnergy[1] += pow( ( *vpResult[i] )[1][n], 2.0f );
						}

						float fFilterEnergyMaximum = 5;
						if( fFilterEnergy[0] > fFilterEnergyMaximum || fFilterEnergy[1] > fFilterEnergyMaximum )
						{
							R_WARN( "Ray Tracing result filter energy: ({:0.2}, {:0.2} ) TOO HIGH in RavenSimulator, storing to Problembaer_RT.wav", fFilterEnergy[0],
							        fFilterEnergy[1] );

							vpResult[i]->Store( "Problembaer_RT.wav" );

							m_pRavenMasterController->writeProjectFileFromScene( "Problembaer.rpf" );
						}
						else
						{
							R_INFO( "Ray Tracing result filter energy: ({:0.2}, {:0.2} )", fFilterEnergy[0], fFilterEnergy[1] );
						}
					}
				}

				void CSimulator::AddReceiver( const int iID )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;

					if( iID < 0 )
						ITA_EXCEPT1( INVALID_PARAMETER, "Invalid receiver ID " + IntToString( iID ) );

					if( m_mapReceivers.end( ) != m_mapReceivers.find( iID ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID already exists" );

					auto cit                = m_mapStaticReceiversList.begin( );
					int iInternalReceiverID = -1;
					while( cit != m_mapStaticReceiversList.end( ) )
					{
						if( cit->second == false )
						{
							// Unused ... so utilize new ID
							iInternalReceiverID = cit->first;
							cit->second         = true;
							m_mapReceivers.insert( std::pair<int, int>( iID, iInternalReceiverID ) );

							return;
						}

						++cit;
					}

					// All Slots occupied, as none is unused
					if( iInternalReceiverID == -1 )
						ITA_EXCEPT1( MODAL_EXCEPTION, "Maximum number of sound receivers reached" );
				}

				void CSimulator::RemoveReceiver( const int iID )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;
					std::map<int, int>::iterator it = m_mapReceivers.find( iID );
					if( it == m_mapReceivers.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );

					unsigned int iRavenReceiverID = (unsigned int)it->second;
					m_mapReceivers.erase( it );
					m_mapStaticReceiversList[iRavenReceiverID] = false; // Mark slot as unused
				}

				bool CSimulator::SetReceiverState( const int iID, const CRavenScene::CReceiverState& oState, const CRavenScene::CReceiverState::DifferenceFlags eFlags )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;
					std::map<int, int>::iterator it = m_mapReceivers.find( iID );
					if( it == m_mapReceivers.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Receiver ID not found" );

					unsigned int iRavenReceiverID = (unsigned int)it->second;
					bool bInsideRoom              = true;
					if( IsSet( eFlags, CRavenScene::CReceiverState::DifferenceFlags::position ) )
					{
						int iAssignedRoom;
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewReceiverPosition_IS( iRavenReceiverID, oState.vPos, iAssignedRoom ) )
							bInsideRoom = false;
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewReceiverPosition_RT( iRavenReceiverID, oState.vPos, iAssignedRoom ) )
							bInsideRoom = false;
					}
					if( IsSet( eFlags, CRavenScene::CReceiverState::DifferenceFlags::orientation ) )
					{
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewReceiverOrientation_IS( iRavenReceiverID, oState.vView, oState.vUp ) )
							ITA_EXCEPT1( UNKNOWN, "Could not set receiver orientation in SetReceiverState" );
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewReceiverOrientation_RT( iRavenReceiverID, oState.vView, oState.vUp ) )
							ITA_EXCEPT1( UNKNOWN, "Could not set receiver orientation in SetReceiverState" );
					}
					/*if ( isSet ( eFlags, CRavenScene::CReceiverState::DifferenceFlags::HRIR ) )
					{
					    std::string sGlobalRavenHRIR = m_pRavenMasterController->m_currentConfig_General.projectPath_HRTFDB;
					    if ( oState.sHRIRFilename.size ( ) > 0 && sGlobalRavenHRIR != sHRIRFilename )
					    {
					        R_WARN ( "Raven Simulator HRIR changed to '%s', forcing reinitialization of Raven Filter Engine\n", sHRIRFilename.c_str ( ) );
					        m_pRavenMasterController->m_currentConfig_General.projectPath_HRTFDB = sHRIRFilename;
					        // Raven Filter Controller neu aufsetzen
					        if ( EXIT_SUCCESS != m_pRavenMasterController->initRavenFilterController ( ) )
					            ITA_EXCEPT1 ( UNKNOWN, "Could not initialize filter controller" );
					    }
					}*/

					return bInsideRoom;
				}

				void CSimulator::AddSource( const int iID )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;

					if( iID < 0 )
						ITA_EXCEPT1( INVALID_PARAMETER, "Invalid sound source ID " + IntToString( iID ) );

					if( m_mapSources.end( ) != m_mapSources.find( iID ) )
						ITASIMULATIONSCHEDULER_ERROR("Error in simulator.cpp: Trying to add sound source id that already exists.");

					std::map<int, bool>::iterator cit = m_mapStaticSourcesList.begin( );
					int iInternalSoundSourceID        = -1;
					while( cit != m_mapStaticSourcesList.end( ) )
					{
						if( cit->second == false )
						{
							// Unbenutzt ... also mit der neuen ID besetzen
							iInternalSoundSourceID = cit->first;
							cit->second            = true;
							m_mapSources.insert( std::pair<int, int>( iID, iInternalSoundSourceID ) );

							return;
						}

						++cit;
					}

					if( iInternalSoundSourceID == -1 )
						ITA_EXCEPT1( MODAL_EXCEPTION, "Maximum number of sound sources reached" );
				}

				void CSimulator::RemoveSource( const int iID )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;
					const auto it = m_mapSources.find( iID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );

					const unsigned int iRavenSoundSourceID = it->second;
					m_mapSources.erase( it );
					m_mapStaticSourcesList[iRavenSoundSourceID] = false; // Mark slot as unused
				}

				bool CSimulator::SetSourceState( const int iID, const CRavenScene::CSourceState& oState, const CRavenScene::CSourceState::DifferenceFlags& eFlags )
				{
					RAVENSIMULATOR_REQUIRE_SCENE;
					std::map<int, int>::iterator it = m_mapSources.find( iID );
					if( it == m_mapSources.end( ) )
						ITA_EXCEPT1( INVALID_PARAMETER, "Sound source ID not found" );

					std::string sDirectivityPath =  m_pParentThread->GetSoundSourceDirectivity( iID );

					unsigned int iRavenSoundSourceID = (unsigned int)it->second;
					bool bInsideRoom                 = true;

					SetSourceDirectivity( iRavenSoundSourceID, sDirectivityPath );

					if( IsSet( eFlags, CRavenScene::CSourceState::DifferenceFlags::position ) )
					{
						int iAssignedRoom = -1;
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewPrimarySourcePosition_IS( iRavenSoundSourceID, oState.vPos, iAssignedRoom ) )
							bInsideRoom = false;
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewPrimarySourcePosition_RT( iRavenSoundSourceID, oState.vPos, iAssignedRoom ) )
							bInsideRoom = false;
					}
					if( IsSet( eFlags, CRavenScene::CSourceState::DifferenceFlags::orientation ) )
					{
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewPrimarySourceOrientation_IS( iRavenSoundSourceID, oState.vView, oState.vUp ) )
							ITA_EXCEPT1( UNKNOWN, "Could not set source orientation" );
						if( EXIT_SUCCESS != m_pRavenMasterController->setNewPrimarySourceOrientation_RT( iRavenSoundSourceID, oState.vView, oState.vUp ) )
							ITA_EXCEPT1( UNKNOWN, "Could not set source orientation" );
					}
					/*if ( isSet ( eFlags, CRavenScene::CSourceState::DifferenceFlags::DIR ) )
					{
					    if ( !doesFileExist ( sDirectivityFilenameSubstituted ) || sDirectivityFilenameSubstituted.empty ( ) )
					        sDirectivityFilenameSubstituted = "NoDirectivity";
					    m_pRavenMasterController->setDirectivityOfPrimarySource ( iRavenSoundSourceID, sDirectivityFilenameSubstituted, "" );
					}*/

					return bInsideRoom;
				}

				void CSimulator::SetSourceDirectivity( const int iRavenSoundSourceID, const std::string& sPathToDirectivity )
				{
					if( ( !doesFileExist( sPathToDirectivity ) && sPathToDirectivity != "NoDirectivity" ) || sPathToDirectivity.empty( ) )
					{
						ITA_EXCEPT_INVALID_PARAMETER( "Specified directivity was not found, or path was empty" );
						return;
					}

					// The empty string at the end denotes the path to a full-spectrum Directivity DFT file. RAVEN expects this string to be present and as 
					// the feature is not used in VA, an empty string is passed (see discussion in ITASimulationScheduler MR !7 )
					m_pRavenMasterController->setDirectivityOfPrimarySource( iRavenSoundSourceID, sPathToDirectivity, "" );
				}

				void CSimulator::SubstituteMacros( std::string& s )
				{
					// RavenDataBasePath
					// s = SubstituteMacro ( s, "RavenDataBasePath", m_sRavenDataBasePath );
					// ... mehr, wenn gewünscht
				}
			} // namespace Raven
		}     // namespace RoomAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA