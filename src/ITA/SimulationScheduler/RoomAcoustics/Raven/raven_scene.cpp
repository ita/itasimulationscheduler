// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/Raven/raven_scene.h>

// ITA includes
#include <ITAASCIITable.h>
#include <ITAConfigUtils.h>
#include <ITAException.h>
#include <ITAStringUtils.h>

// std includes
#include <cassert>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			namespace Raven
			{
				CRavenScene::CReceiverState::DifferenceFlags CRavenScene::CReceiverState::Compare( const CReceiverState& oState ) const
				{
					auto differenceFlags = DifferenceFlags::none;
					if( vPos != oState.vPos )
						differenceFlags |= DifferenceFlags::position;
					if( ( vView != oState.vView ) || ( vUp != oState.vUp ) )
						differenceFlags |= DifferenceFlags::orientation;

					return differenceFlags;
				}

				CRavenScene::~CRavenScene( ) { Reset( ); }

				void CRavenScene::AddSource( int iSoundSourceID )
				{
					// Make sure the ID isn't already used
					assert( m_mSources.find( iSoundSourceID ) == m_mSources.end( ) );

					m_mSources[iSoundSourceID] = CSourceState( );
				}

				void CRavenScene::RemoveSource( int iSoundSourceID )
				{
					auto it = m_mSources.find( iSoundSourceID );
					// Make sure the ID exists
					assert( it != m_mSources.end( ) );
					m_mSources.erase( it );
				}

				void CRavenScene::SetSourceState( int iSoundSourceID, const CSourceState& oState )
				{
					auto it = m_mSources.find( iSoundSourceID );
					// Make sure the ID exists
					assert( it != m_mSources.end( ) );
					it->second = oState;
				}

				const CRavenScene::CSourceState& CRavenScene::GetSourceState( int iSoundSourceID ) const
				{
					auto cit = m_mSources.find( iSoundSourceID );
					// Make sure the ID exists
					assert( cit != m_mSources.end( ) );
					return cit->second;
				}

				void CRavenScene::AddReceiver( int iReceiverID )
				{
					// Make sure the ID isn't already used
					assert( m_mReceivers.find( iReceiverID ) == m_mReceivers.end( ) );

					m_mReceivers[iReceiverID] = CReceiverState( );
				}

				void CRavenScene::RemoveReceiver( int iReceiverID )
				{
					auto it = m_mReceivers.find( iReceiverID );
					// Make sure the ID exists
					assert( it != m_mReceivers.end( ) );
					m_mReceivers.erase( it );
				}

				void CRavenScene::SetReceiverState( int iReceiverID, const CReceiverState& oState )
				{
					auto it = m_mReceivers.find( iReceiverID );
					// Make sure the ID exists
					assert( it != m_mReceivers.end( ) );
					it->second = oState;
				}

				const CRavenScene::CReceiverState& CRavenScene::GetReceiverState( int iReceiverID ) const
				{
					auto cit = m_mReceivers.find( iReceiverID );
					// Make sure the ID exists
					assert( cit != m_mReceivers.end( ) );
					return cit->second;
				}

				CRavenScene::CSceneDifference CRavenScene::Difference( const CRavenScene& oScene ) const
				{
					CSceneDifference oDifference;
					oDifference.receiverDifferences = DifferenceReceiver( m_mReceivers, oScene.m_mReceivers );
					oDifference.sourceDifferences   = DifferenceSource( m_mSources, oScene.m_mSources );
					return oDifference;
				}

				bool CRavenScene::IsEqual( const CRavenScene& oScene ) const
				{
					auto oDiff = Difference( oScene );

					for( auto iID: oDiff.sourceDifferences.viModObjects )
					{
						if( GetSourceState( iID ).Compare( oScene.GetSourceState( iID ) ) != CSourceState::DifferenceFlags::none )
							return true;
					}

					for( auto iID: oDiff.receiverDifferences.viModObjects )
					{
						if( GetReceiverState( iID ).Compare( oScene.GetReceiverState( iID ) ) != CReceiverState::DifferenceFlags::none )
							return true;
					}

					return false;
				}

				std::string CRavenScene::ToString( ) const
				{
					std::ostringstream ss;

					ss << "Number of sources: " << m_mSources.size( ) << std::endl;

					for( const auto& iter: m_mSources )
					{
						ss << " + Source " << iter.first << " has state (pos: " << iter.second.vPos.x << ", " << iter.second.vPos.y << ", " << iter.second.vPos.z << ")"
						   << std::endl;
					}

					ss << "Number of receivers: " << m_mReceivers.size( ) << std::endl;

					for( const auto& iter: m_mReceivers )
					{
						ss << " + Receiver " << iter.first << " has state (pos: " << iter.second.vPos.x << ", " << iter.second.vPos.y << ", " << iter.second.vPos.z << ")"
						   << std::endl;
					}

					return ss.str( );
				}

				void CRavenScene::LoadFormIni( const std::string& sPath )
				{
					Reset( );

					INIFileUseFile( sPath );
					INIFileUseSection( "Scene" );

					std::vector<std::string> vsKeys( INIFileGetKeys( "Scene" ) );

					for( const auto& vsKey: vsKeys )
					{
						std::string sKey( toLowercase( vsKey ) );

						std::string sPrefix = "source";
						std::size_t iFound  = sKey.find( sPrefix );
						if( iFound != std::string::npos )
						{
							std::string sID( &( sKey[iFound + sPrefix.size( )] ) );
							int iID = StringToInt( sID );

							std::vector<std::string> vsSource( INIFileReadStringList( sKey ) );

							int iNumParams = 3 + 3 + 3 + 1;
							if( vsSource.size( ) != iNumParams )
								ITA_EXCEPT1( INVALID_PARAMETER,
								             "Invalid source '" + sKey + "' in '" + sPath + "', list must have " + IntToString( iNumParams ) + " elements" );

							CSourceState oState;
							oState.vPos.x  = std::stof( vsSource[0] );
							oState.vPos.y  = std::stof( vsSource[1] );
							oState.vPos.z  = std::stof( vsSource[2] );
							oState.vView.x = std::stof( vsSource[3] );
							oState.vView.y = std::stof( vsSource[4] );
							oState.vView.z = std::stof( vsSource[5] );
							oState.vUp.x   = std::stof( vsSource[6] );
							oState.vUp.y   = std::stof( vsSource[7] );
							oState.vUp.z   = std::stof( vsSource[8] );

							AddSource( iID );
							SetSourceState( iID, oState );
						}

						sPrefix = "receiver";
						iFound  = sKey.find( sPrefix );
						if( iFound != std::string::npos )
						{
							std::string sID( &( sKey[iFound + sPrefix.size( )] ) );
							int iID = StringToInt( sID );

							std::vector<std::string> vsReceiver( INIFileReadStringList( sKey ) );

							int iNumParams = 3 + 3 + 3 + 1;
							if( vsReceiver.size( ) != iNumParams )
								ITA_EXCEPT1( INVALID_PARAMETER,
								             "Invalid receiver '" + sKey + "' in '" + sPath + "', list must have " + IntToString( iNumParams ) + " elements" );

							CReceiverState oState;
							oState.vPos.x  = std::stof( vsReceiver[0] );
							oState.vPos.y  = std::stof( vsReceiver[1] );
							oState.vPos.z  = std::stof( vsReceiver[2] );
							oState.vView.x = std::stof( vsReceiver[3] );
							oState.vView.y = std::stof( vsReceiver[4] );
							oState.vView.z = std::stof( vsReceiver[5] );
							oState.vUp.x   = std::stof( vsReceiver[6] );
							oState.vUp.y   = std::stof( vsReceiver[7] );
							oState.vUp.z   = std::stof( vsReceiver[8] );

							AddReceiver( iID );
							SetReceiverState( iID, oState );
						}
					}
				}

				void CRavenScene::StoreToIni( const std::string& sPath, int iFloatingPointPrecision ) const
				{
					INIFileUseFile( sPath );

					for( const auto& iter: m_mSources )
					{
						int iID                    = iter.first;
						const CSourceState& oState = iter.second;
						std::string sKey           = "Source" + IntToString( iID );
						std::vector<std::string> vsSource;
						vsSource.push_back( FloatToString( oState.vPos.x, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vPos.y, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vPos.z, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vView.x, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vView.y, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vView.z, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vUp.x, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vUp.y, iFloatingPointPrecision ) );
						vsSource.push_back( FloatToString( oState.vUp.z, iFloatingPointPrecision ) );
						INIFileWriteStringList( sPath, "SCENE", sKey, vsSource );
					}

					for( const auto& iter: m_mReceivers )
					{
						int iID                      = iter.first;
						const CReceiverState& oState = iter.second;
						std::string sKey             = "Receiver" + IntToString( iID );
						std::vector<std::string> vsReceiver;
						vsReceiver.push_back( FloatToString( oState.vPos.x, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vPos.y, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vPos.z, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vView.x, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vView.y, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vView.z, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vUp.x, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vUp.y, iFloatingPointPrecision ) );
						vsReceiver.push_back( FloatToString( oState.vUp.z, iFloatingPointPrecision ) );
						INIFileWriteStringList( sPath, "SCENE", sKey, vsReceiver );
					}
				}

				const std::map<int, CRavenScene::CSourceState>& CRavenScene::GetSourceMap( ) const { return m_mSources; }

				const std::map<int, CRavenScene::CReceiverState>& CRavenScene::GetReceiverMap( ) const { return m_mReceivers; }

				CRavenScene::CSceneDifference::DifferenceVectors CRavenScene::DifferenceReceiver( const std::map<int, CReceiverState>& mOld,
				                                                                                  const std::map<int, CReceiverState>& mNew )
				{
					// Clear container
					std::vector<int> viNewElems;
					std::vector<int> viModElems;
					std::vector<int> viDelElems;

					// Preparations for assertions
					int totalNumEntities = mOld.size( ) + mNew.size( );

					// Intersection test
					auto oldit = mOld.begin( );
					auto newit = mNew.begin( );
					while( true )
					{
						// End condition: no further elements in old list or new list or both
						if( oldit == mOld.end( ) && newit != mNew.end( ) )
						{
							// All elements of new list are new
							while( newit != mNew.end( ) )
							{
								viNewElems.push_back( newit->first );
								newit++;
							}

							break;
						}
						else if( newit == mNew.end( ) && oldit != mOld.end( ) )
						{
							// All elements of old list have been removed
							while( oldit != mOld.end( ) )
							{
								viDelElems.push_back( oldit->first );
								oldit++;
							}

							break;
						}
						else if( oldit != mOld.end( ) && newit != mNew.end( ) )
						{
							int iOldID = oldit->first;
							int iNewID = newit->first;

							if( iOldID < iNewID )
							{
								// Old id missing in new list -> removed
								viDelElems.push_back( iOldID );

								// Continue to reach equivalent new id
								oldit++;
							}
							else if( iOldID > iNewID )
							{
								// New id missing in old list
								viNewElems.push_back( iNewID );

								// Continue to reach equivalent new id
								newit++;
							}
							else
							{
								// Old and new id are equivalent
								assert( iOldID == iNewID );
								viModElems.push_back( iOldID );

								// Continue on both maps
								oldit++;
								newit++;
							}
						}
						else
						{
							// Both lists brought to end
							break;
						}
					}


					// Assert correct number of entities after comparison
					assert( totalNumEntities == viNewElems.size( ) + viDelElems.size( ) + 2 * viModElems.size( ) );

					return { viNewElems, viModElems, viDelElems };
				}

				CRavenScene::CSceneDifference::DifferenceVectors CRavenScene::DifferenceSource( const std::map<int, CSourceState>& mOld,
				                                                                                const std::map<int, CSourceState>& mNew )
				{
					// Clear container
					std::vector<int> viNewElems;
					std::vector<int> viModElems;
					std::vector<int> viDelElems;

					// Preparations for assertions
					int totalNumEntities = mOld.size( ) + mNew.size( );

					// Intersection test
					auto oldit = mOld.begin( );
					auto newit = mNew.begin( );
					while( true )
					{
						// End condition: no further elements in old list or new list or both
						if( oldit == mOld.end( ) && newit != mNew.end( ) )
						{
							// All elements of new list are new
							while( newit != mNew.end( ) )
							{
								viNewElems.push_back( newit->first );
								++newit;
							}

							break;
						}
						else if( newit == mNew.end( ) && oldit != mOld.end( ) )
						{
							// All elements of old list have been removed
							while( oldit != mOld.end( ) )
							{
								viDelElems.push_back( oldit->first );
								oldit++;
							}

							break;
						}
						else if( oldit != mOld.end( ) && newit != mNew.end( ) )
						{
							int iOldID = oldit->first;
							int iNewID = newit->first;

							if( iOldID < iNewID )
							{
								// Old id missing in new list -> removed
								viDelElems.push_back( iOldID );

								// Continue to reach equivalent new id
								oldit++;
							}
							else if( iOldID > iNewID )
							{
								// New id missing in old list
								viNewElems.push_back( iNewID );

								// Continue to reach equivalent new id
								newit++;
							}
							else
							{
								// Old and new id are equivalent
								assert( iOldID == iNewID );
								viModElems.push_back( iOldID );

								// Continue on both maps
								oldit++;
								newit++;
							}
						}
						else
						{
							// Both lists brought to end
							break;
						}
					}


					// Assert correct number of entities after comparison
					assert( totalNumEntities == int( viNewElems.size( ) + viDelElems.size( ) + 2 * viModElems.size( ) ) );

					return { viNewElems, viModElems, viDelElems };
				}

				void CRavenScene::Reset( )
				{
					m_mSources.clear( );
					m_mReceivers.clear( );
				}

				CRavenScene::CSourceState::DifferenceFlags CRavenScene::CSourceState::Compare( const CSourceState& oState ) const
				{
					auto differenceFlags = DifferenceFlags::none;

					if( vPos != oState.vPos )
						differenceFlags |= DifferenceFlags::position;
					if( ( vView != oState.vView ) || ( vUp != oState.vUp ) )
						differenceFlags |= DifferenceFlags::orientation;

					return differenceFlags;
				}

				bool CRavenScene::CSceneDifference::EntitiesChanged( ) const
				{
					if( !receiverDifferences.viNewObjects.empty( ) )
						return true;
					if( !receiverDifferences.viDelObjects.empty( ) )
						return true;
					if( !sourceDifferences.viNewObjects.empty( ) )
						return true;
					if( !sourceDifferences.viDelObjects.empty( ) )
						return true;

					return false;
				}
			} // namespace Raven
		}     // namespace RoomAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA