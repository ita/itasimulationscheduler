// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/Raven/raven_worker_interface.h>

// simulation scheduler includes
#include "../../configuration_keys.h"
#include "ITA/SimulationScheduler/RoomAcoustics/Raven/worker_thread.h"

#include <ITA/SimulationScheduler/RoomAcoustics/Raven/raven_simulation_result.h>
#include <ITA/SimulationScheduler/RoomAcoustics/Raven/simulation_task.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>

// ITA includes
#include "ITADebug.h"

#include <ITAException.h>


namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			namespace Raven
			{
				IRavenWorkerInterface::RavenWorkerConfig::RavenWorkerConfig( std::string sType ) : RoomAcousticsWorkerInterfaceConfig( sType ) {}

				VistaPropertyList IRavenWorkerInterface::RavenWorkerConfig::Store( ) const { return RoomAcousticsWorkerInterfaceConfig::Store( ); }

				void IRavenWorkerInterface::RavenWorkerConfig::Load( const VistaPropertyList& oProperties ) { RoomAcousticsWorkerInterfaceConfig::Load( oProperties ); }

				IRavenWorkerInterface::IRavenWorkerInterface( const RavenWorkerConfig& oConfig, ISchedulerInterface* pParent )
				    : IRoomAcousticsWorkerInterface( oConfig, pParent )
				{
				}

				IRavenWorkerInterface::~IRavenWorkerInterface( ) {}

				std::unique_ptr<CSimulationTask> IRavenWorkerInterface::CreateTaskFromUpdate( std::unique_ptr<CUpdateScene> pUpdate ) const
				{
					PROFILER_FUNCTION( );

					auto pTask             = std::make_unique<CSimulationTask>( );
					pTask->eSimulationType = m_eFieldOfDuty;
					pTask->uiID            = pUpdate->GetID( );

					// This is the previous implementation of this. Was is faster to only set certain members?
					/*auto eFlags = pTask->oConfig.compare ( *m_pRavenConfig );
					switch ( m_eFieldOfDuty )
					{
					    case FieldOfDuty::directSound:
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::scene_FileName ) )
					            pTask->oConfig.oGeometry.sSceneFileName = m_pRavenConfig->oGeometry.sSceneFileName;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::simulationSettings_AccelerationType ) )
					            pTask->oConfig.oSimulationSettings.eAccelerationType = m_pRavenConfig->oSimulationSettings.eAccelerationType;
					        break;
					    case FieldOfDuty::earlyReflections:
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::scene_FileName ) )
					            pTask->oConfig.oGeometry.sSceneFileName = m_pRavenConfig->oGeometry.sSceneFileName;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_Humidity ) )
					            pTask->oConfig.oRoom.fHumidity = m_pRavenConfig->oRoom.fHumidity;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_StaticPressure ) )
					            pTask->oConfig.oRoom.fStaticPressure = m_pRavenConfig->oRoom.fStaticPressure;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_Temperature ) )
					            pTask->oConfig.oRoom.fTemperature = m_pRavenConfig->oRoom.fTemperature;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::simulationSettings_SampleRate ) )
					            pTask->oConfig.oSimulationSettings.dSampleRate = m_pRavenConfig->oSimulationSettings.dSampleRate;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::simulationSettings_AccelerationType ) )
					            pTask->oConfig.oSimulationSettings.eAccelerationType = m_pRavenConfig->oSimulationSettings.eAccelerationType;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::filterEngine_MagnitudeResolution ) )
					            pTask->oConfig.oFilterEngine.eMagnitudesResolution = m_pRavenConfig->oFilterEngine.eMagnitudesResolution;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::isOrder_PrimarySources ) )
					            pTask->oConfig.oImageSource.iOrderPrimarySource = m_pRavenConfig->oImageSource.iOrderPrimarySource;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::isOrder_SecondarySources ) )
					            pTask->oConfig.oImageSource.iOrderSecondarySource = m_pRavenConfig->oImageSource.iOrderSecondarySource;
					        break;
					    case FieldOfDuty::diffuseDecay:
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::scene_FileName ) )
					            pTask->oConfig.oGeometry.sSceneFileName = m_pRavenConfig->oGeometry.sSceneFileName;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_Humidity ) )
					            pTask->oConfig.oRoom.fHumidity = m_pRavenConfig->oRoom.fHumidity;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_StaticPressure ) )
					            pTask->oConfig.oRoom.fStaticPressure = m_pRavenConfig->oRoom.fStaticPressure;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::room_Temperature ) )
					            pTask->oConfig.oRoom.fTemperature = m_pRavenConfig->oRoom.fTemperature;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::simulationSettings_SampleRate ) )
					            pTask->oConfig.oSimulationSettings.dSampleRate = m_pRavenConfig->oSimulationSettings.dSampleRate;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::simulationSettings_AccelerationType ) )
					            pTask->oConfig.oSimulationSettings.eAccelerationType = m_pRavenConfig->oSimulationSettings.eAccelerationType;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::filterEngine_MagnitudeResolution ) )
					            pTask->oConfig.oFilterEngine.eMagnitudesResolution = m_pRavenConfig->oFilterEngine.eMagnitudesResolution;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_EnergyLossThreshold ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fEnergyLossThreshold = m_pRavenConfig->oRayTracer.oSphereDetector.fEnergyLossThreshold;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_FilterLength ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fFilterLength = m_pRavenConfig->oRayTracer.oSphereDetector.fFilterLength;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_TimeSlotResolution ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fResolutionTimeSlots = m_pRavenConfig->oRayTracer.oSphereDetector.fResolutionTimeSlots;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_NumberOfParticles ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.uliNumParticles = m_pRavenConfig->oRayTracer.oSphereDetector.uliNumParticles;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_Radius ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fRadius = m_pRavenConfig->oRayTracer.oSphereDetector.fRadius;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_AzimuthResolution ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fResolutionAzimuth = m_pRavenConfig->oRayTracer.oSphereDetector.fResolutionAzimuth;
					        if ( isSet ( eFlags, CRavenConfig::DifferenceFlags::rayTracing_SphereDetector_ElevationResolution ) )
					            pTask->oConfig.oRayTracer.oSphereDetector.fResolutionElevation = m_pRavenConfig->oRayTracer.oSphereDetector.fResolutionElevation;
					        break;
					    default:
					        ITA_EXCEPT_INVALID_PARAMETER ( "Unkonwn field of duty." );
					}*/

					// scene
					const auto sourceReceiverPair = pUpdate->GetSourceReceiverPair( );

					auto sourceState             = CRavenScene::CSourceState( );
					const auto sourcePosition    = sourceReceiverPair.source->GetPosition( );
					const auto sourceOrientation = sourceReceiverPair.source->GetOrientation( );
					sourceState.vPos             = RG_Vector( sourcePosition[0], sourcePosition[1], sourcePosition[2] );
					sourceState.vView = RG_Vector( sourceOrientation.GetViewDir( )[0], sourceOrientation.GetViewDir( )[1], sourceOrientation.GetViewDir( )[2] );
					sourceState.vUp   = RG_Vector( sourceOrientation.GetUpDir( )[0], sourceOrientation.GetUpDir( )[1], sourceOrientation.GetUpDir( )[2] );
					pTask->oScene.AddSource( sourceReceiverPair.source->GetId( ) );
					pTask->oScene.SetSourceState( sourceReceiverPair.source->GetId( ), sourceState );

					auto receiverState             = CRavenScene::CReceiverState( );
					const auto receiverPosition    = sourceReceiverPair.receiver->GetPosition( );
					const auto receiverOrientation = sourceReceiverPair.receiver->GetOrientation( );
					receiverState.vPos             = RG_Vector( receiverPosition[0], receiverPosition[1], receiverPosition[2] );
					receiverState.vView = RG_Vector( receiverOrientation.GetViewDir( )[0], receiverOrientation.GetViewDir( )[1], receiverOrientation.GetViewDir( )[2] );
					receiverState.vUp   = RG_Vector( receiverOrientation.GetUpDir( )[0], receiverOrientation.GetUpDir( )[1], receiverOrientation.GetUpDir( )[2] );
					pTask->oScene.AddReceiver( sourceReceiverPair.receiver->GetId( ) );
					pTask->oScene.SetReceiverState( sourceReceiverPair.receiver->GetId( ), receiverState );

					return pTask;
				}

				std::unique_ptr<CRIRSimulationResult> IRavenWorkerInterface::ConvertSimulationResult( std::unique_ptr<CRavenSimulationResult> pResult,
				                                                                                      CSimulationTask* pTask )
				{
					PROFILER_FUNCTION( );

					if( pResult->vcspResult.size( ) != 1 )
					{
						DEBUG_PRINTF( "[Worker             ]\t Expected one Result\n" );
						ITA_EXCEPT_INVALID_PARAMETER( "Expected simulation result vector with only one result.\n" )
					}

					const auto& soundPath = pResult->vcspResult.front( );
	
					if( soundPath->bEmpty == true )
					{
						DEBUG_PRINTF( "[Worker             ]\t Result Is Empty\n" );
					}

					if( soundPath->psfResult->GetNumChannels( ) != soundPath->iNumChannels )
					{
						ITA_EXCEPT_INVALID_PARAMETER( "The channel count does not match." );
					}

					auto returnResult = std::make_unique<CRIRSimulationResult>( );

					returnResult->eResultType    = pTask->eSimulationType;
					returnResult->bZerosStripped = soundPath->bZerosStripped;
					returnResult->iLeadingZeros  = soundPath->iLeadingZeros;
					returnResult->iTrailingZeros = soundPath->iTailingZeros;
					returnResult->bSameRoom      = soundPath->bEntitiesInSameRoom;
					returnResult->sfResult       = *soundPath->psfResult;

					if( pTask->oScene.GetSourceMap( ).size( ) != 1 )
					{
						ITA_EXCEPT_INVALID_PARAMETER( "Task has more than one source." );
					}
					if( pTask->oScene.GetReceiverMap( ).size( ) != 1 )
					{
						ITA_EXCEPT_INVALID_PARAMETER( "Task has more than one receiver." );
					}

					const auto sourceIter   = pTask->oScene.GetSourceMap( ).begin( );
					const auto receiverIter = pTask->oScene.GetReceiverMap( ).begin( );

					auto sourceOrientation = VistaQuaternion( );
					sourceOrientation.SetFromViewAndUpDir( VistaVector3D( sourceIter->second.vView.components ), VistaVector3D( sourceIter->second.vUp.components ) );
					auto receiverOrientation = VistaQuaternion( );
					receiverOrientation.SetFromViewAndUpDir( VistaVector3D( receiverIter->second.vView.components ),
					                                         VistaVector3D( receiverIter->second.vUp.components ) );

					const auto source =
					    new C3DObject( VistaVector3D( sourceIter->second.vPos.components ), sourceOrientation, C3DObject::Type::source, sourceIter->first );

					const auto receiver =
					    new C3DObject( VistaVector3D( receiverIter->second.vPos.components ), receiverOrientation, C3DObject::Type::receiver, receiverIter->first );

					returnResult->sourceReceiverPair.source   = source;
					returnResult->sourceReceiverPair.receiver = receiver;

					return returnResult;
				}
			} // namespace Raven
		}     // namespace RoomAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA