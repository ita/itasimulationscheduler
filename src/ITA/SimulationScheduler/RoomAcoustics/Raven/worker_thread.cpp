// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/Raven/worker_thread.h>

// Simulation scheduler includes
#include "../../configuration_keys.h"

#include <ITA/SimulationScheduler/RoomAcoustics/Raven/simulator.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>

#include <nlohmann/json.hpp>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>

// ITA includes
#include "ITAException.h"

#include <ITADebug.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			namespace Raven
			{
#ifdef WITH_PROFILER
				std::size_t CWorkerThread::iWorkerMaxID = 0;
#endif

				CWorkerThread::WorkerThreadConfig::WorkerThreadConfig( ) : RavenWorkerConfig( GetType( ) ) {}

				VistaPropertyList CWorkerThread::WorkerThreadConfig::Store( ) const
				{
					auto oProperties = RavenWorkerConfig::Store( );

					oProperties.SetValue( ravenProjectFilePathKey, sRavenProjectFilePath );

					return oProperties;
				}

				void CWorkerThread::WorkerThreadConfig::Load( const VistaPropertyList& oProperties )
				{
					RavenWorkerConfig::Load( oProperties );

					oProperties.GetValue( ravenProjectFilePathKey, sRavenProjectFilePath );
				}

				CWorkerThread::CWorkerThread( const WorkerThreadConfig& oConfig, ISchedulerInterface* pParent )
				    : IRavenWorkerInterface( oConfig, pParent )
				    , m_evTriggerLoop( VistaThreadEvent::NON_WAITABLE_EVENT )
				{
					m_pSimulator = std::make_unique<CSimulator>( m_eFieldOfDuty, oConfig.sRavenProjectFilePath );
					m_pSimulator->RegisterWorkerThread( this );
					Run( );
				}

				CWorkerThread::~CWorkerThread( )
				{
					m_bStopIndicated = true;
					m_evTriggerLoop.SignalEvent( );

					// Wait for the end of the simulation
					while( IsBusy( ) )
						VistaTimeUtils::Sleep( 100 );

					m_viSourceVector.clear( );
					StopGently( true );
				}

				bool CWorkerThread::IsBusy( ) { return m_pCurrentTask.get( ) != nullptr; }

				std::string CWorkerThread::GetSoundSourceDirectivity( int iSourceID )
				{
					// Find the first tuple in m_viSourceVector, whose first element is equal to iSourceID
					const auto it = std::find_if( m_viSourceVector.begin(), m_viSourceVector.end(),
                        [iSourceID]( const std::tuple<int, std::string>& tuple ) {
                           return std::get<0>( tuple ) == iSourceID;
                        });

					if( it == m_viSourceVector.end( ) )
					{
						ITA_EXCEPT_INVALID_PARAMETER("Couldn't find Source with specified ID in worker_thread's SourceVector");
					}

					return std::get<1>( *it );
				}

				void CWorkerThread::PushConfigUpdate( std::unique_ptr<CUpdateConfig> pUpdate )
				{
					int iSourceID;

					std::string sUpdatePayload;
					std::string sDirectivityFilePath;
					
					nlohmann::json jUpdatePayload;

					std::vector< std::tuple< int, std::string > >::iterator it;

					switch( pUpdate->GetType( ) )
					{
						case ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity:
							sUpdatePayload = pUpdate->GetPayload( );

							try
							{
								jUpdatePayload = nlohmann::json::parse( sUpdatePayload );
								iSourceID = jUpdatePayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyObjId ];
								sDirectivityFilePath = jUpdatePayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyDirectivityFilePath ];
							}
							catch( nlohmann::json::exception& e )
							{
								throw ITAException( ITAException::PARSE_ERROR, __FUNCTION__, e.what( ) );
							}

							it = std::find_if( m_viSourceVector.begin(), m_viSourceVector.end(),

							// Find the first tuple in m_viSourceVector, whose first element is equal to iSourceID
                           	[iSourceID]( const std::tuple<int, std::string>& tuple ) {
                               return std::get<0>( tuple ) == iSourceID;
                           	});

							if( it == m_viSourceVector.end( ) )
							{
								// If the source isn't known yet, save the directivity for when it appears
								m_viSourceVector.push_back( std::make_tuple( iSourceID, sDirectivityFilePath ) );
								break;
							}

							std::get<1>( *it ) = sDirectivityFilePath;

							break;

						case ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeHRTF:
							ITA_EXCEPT_NOT_IMPLEMENTED;
							break;
					}
				}

				void CWorkerThread::PushUpdate( std::unique_ptr<CUpdateScene> pUpdate )
				{
					// PROFILER_FUNCTION ( );
					PROFILER_VALUE( "Update to Worker", pUpdate->GetID( ) );

					int iSourceID = pUpdate->GetSourceReceiverPair( ).source->GetId( );

					const auto it = std::find_if( m_viSourceVector.begin(), m_viSourceVector.end(),

							// Find the first tuple in m_viSourceVector, whose first element is equal to iSourceID
                           [iSourceID]( const std::tuple<int, std::string>& tuple ) {
                               return std::get<0>(tuple) == iSourceID;
                           });

					if( it == m_viSourceVector.end( ) )
					{
						m_viSourceVector.push_back( std::make_tuple( iSourceID, "NoDirectivity" ) );
					}

					auto pTask = CreateTaskFromUpdate( std::move( pUpdate ) );

					assert( !IsBusy( ) );
					assert( m_eFieldOfDuty == pTask->eSimulationType );

					// Assume ownership of the task.
					m_pCurrentTask = std::move( pTask );
					m_bBusy        = true;

					// Stop if reset is running.
					if( m_bResetIndicated )
					{
						pTask->oProfiler.eDiscardReason = CSimulationTask::CProfiler::DiscardReason::resetIndicated;

						// todo Do we do something with it?

						m_pCurrentTask = nullptr;

						m_bBusy = false;

						return;
					}

					// Start the loop and simulation.
					m_evTriggerLoop.SignalEvent( );
				}

				void CWorkerThread::Reset( )
				{
					// Signal reset
					m_bResetIndicated = true;

					// Wait for the end of the simulation
					while( IsBusy( ) )
						VistaTimeUtils::Sleep( 100 );

					// Reset the simulator
					m_pSimulator->Reset( );

					// Reset source list
					m_viSourceVector.clear( );

					// Reset the flags
					m_bSceneLoaded    = false;
					m_bResetIndicated = false;
				}

				void CWorkerThread::Shutdown( )
				{
					m_bStopIndicated = true;
					m_evTriggerLoop.SignalEvent( );

					// Wait for the end of the simulation
					while( IsBusy( ) )
						VistaTimeUtils::Sleep( 100 );

					StopGently( true );
				}

				std::unique_ptr<IWorkerInterface> CWorkerThread::CreateWorker( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig,
				                                                               ISchedulerInterface* pParent )
				{
					return std::make_unique<CWorkerThread>( dynamic_cast<const WorkerThreadConfig&>( *pConfig ), pParent );
				}

				std::string CWorkerThread::GetType( ) { return "RavenWorkerThread"; }

				void CWorkerThread::SetSimulator( std::unique_ptr<ISimulatorInterface> pSimulator ) { m_pSimulator = std::move( pSimulator ); }

				bool CWorkerThread::LoopBody( )
				{
					// Wait for trigger
					m_evTriggerLoop.WaitForEvent( true );
					m_evTriggerLoop.ResetThisEvent( );

					PROFILER_FUNCTION( );

					if( m_bStopIndicated )
					{
						IndicateLoopEnd( );
						PROFILER_EVENT_COUNT( "Loop Stop" );
						return false;
					}

					assert( !m_bResetIndicated );

					assert( m_pCurrentTask );

					// Turn atomic ptr to raw ptr
					CSimulationTask* pTask = m_pCurrentTask.get( );

					R_INFO( "raven::WorkerThread:\tStarting simulation on Task {}, SimType {}", pTask->uiID, fmt::underlying( pTask->eSimulationType ) );

					PROFILER_SECTION( "Compute Update/Task" );
					PROFILER_VALUE( "Compute Update/Task", pTask->uiID );
					auto pResult = std::make_unique<CRavenSimulationResult>( );
					try
					{
						// Simulate (blocking)
						DEBUG_PRINTF( "[Thread Worker FOD %d]\t Simulation Started\n", AsInteger( m_eFieldOfDuty ) );
						m_pSimulator->Compute( pTask, pResult.get( ) );
						DEBUG_PRINTF( "[Thread Worker FOD %d]\t Simulation Finished\n", AsInteger( m_eFieldOfDuty ) );
					}
					catch( ITAException& e )
					{
						std::cerr << e.ToString( );
					}
					PROFILER_VALUE( "Update/Task Simulated", pTask->uiID );
					PROFILER_END_SECTION( );

					// Post the result back to the scheduler.
					R_INFO( "raven::WorkerThread:\tSending Task {}, SimType {} to scheduler", pTask->uiID, fmt::underlying( pTask->eSimulationType ) );

					try
					{
						m_pParentScheduler->HandleSimulationFinished( ConvertSimulationResult( std::move( pResult ), pTask ) );
					}
					catch( ITAException& e )
					{
						std::cerr << e.ToString( );
					}

					PROFILER_VALUE( "Result handled", pTask->uiID );
					R_INFO( "raven::WorkerThread:\tTask send to scheduler" );

					// release the current task
					m_pCurrentTask = nullptr;

					m_bBusy = false;

					return true;
				}

				void CWorkerThread::PreLoop( )
				{
#ifdef WITH_PROFILER
					std::string sThreadName;
					switch( m_eFieldOfDuty )
					{
						case RoomAcoustics::FieldOfDuty::directSound:
							sThreadName = "Direct Sound Worker Thread " + to_string( iWorkerMaxID++ );
							break;
						case RoomAcoustics::FieldOfDuty::earlyReflections:
							sThreadName = "Early Reflections Worker Thread " + to_string( iWorkerMaxID++ );
							break;
						case RoomAcoustics::FieldOfDuty::diffuseDecay:
							sThreadName = "Diffuse Decay Worker Thread " + to_string( iWorkerMaxID++ );
							break;
						default:;
					}

					SetThreadName( sThreadName );
					PROFILER_NAME_THREAD( sThreadName );
#endif
				}
			} // namespace Raven
		}     // namespace RoomAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA