// Header include
#include <ITA/SimulationScheduler/RoomAcoustics/room_acoustics_worker_interface.h>

// simulation scheduler include
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/Utils/utils.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			IRoomAcousticsWorkerInterface::RoomAcousticsWorkerInterfaceConfig::RoomAcousticsWorkerInterfaceConfig( std::string sType ) : WorkerConfig( sType ) {}

			VistaPropertyList IRoomAcousticsWorkerInterface::RoomAcousticsWorkerInterfaceConfig::Store( ) const
			{
				auto oProperties = WorkerConfig::Store( );

				oProperties.SetValue( workerFieldOfDuty, AsInteger( eFOD ) );

				return oProperties;
			}

			void IRoomAcousticsWorkerInterface::RoomAcousticsWorkerInterfaceConfig::Load( const VistaPropertyList& oProperties )
			{
				WorkerConfig::Load( oProperties );

				eFOD = static_cast<FieldOfDuty>( oProperties.GetValue<int>( workerFieldOfDuty ) );
			}

			IRoomAcousticsWorkerInterface::IRoomAcousticsWorkerInterface( const RoomAcousticsWorkerInterfaceConfig& oConfig, ISchedulerInterface* pParent )
			    : IWorkerInterface( pParent )
			{
				m_eFieldOfDuty = oConfig.eFOD;
			}

			IRoomAcousticsWorkerInterface::~IRoomAcousticsWorkerInterface( ) {}

			FieldOfDuty IRoomAcousticsWorkerInterface::GetFieldOfDuty( ) const { return m_eFieldOfDuty; }
		} // namespace RoomAcoustics
	}     // namespace SimulationScheduler
} // namespace ITA