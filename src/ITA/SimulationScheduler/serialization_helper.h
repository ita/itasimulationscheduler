#ifndef INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_SERIALIZATION_HELPER
#define INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_SERIALIZATION_HELPER

// ITA
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>

// Vista
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>
#include <VistaBase/VistaQuaternion.h>
#include <VistaBase/VistaVector3D.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		struct SerializationHelper
		{
			///
			/// \brief Write an ITASampleFrame to the given IVistaSerializer.
			/// \param pSerializer the IVistaSerializer to write to.
			/// \param pSampleFrame pointer to the ITASampleFrame that shall be written.
			/// \param iLength number of samples that shall be written.
			/// \param iOffset number of samples that shall be skipped before the requested samples begin.
			/// \return the number of bytes written or -1 on failure.
			///
			static int WriteITASampleFrame( IVistaSerializer& pSerializer, const ITASampleFrame* pSampleFrame, const int iLength, const int iOffset );

			///
			/// \brief Read an ITASampleFrame from the given IVistaDeSerializer.
			/// \param pDeserializer the IVistaDeSerializer to read from.
			/// \param pSampleFrame the ITASampleFrame to save into.
			/// \return the number of bytes written or -1 on failure.
			///
			static int ReadITASampleFrame( IVistaDeSerializer& pDeserializer, ITASampleFrame& pSampleFrame );

			///
			/// \brief Write an ITASampleFrame to the given IVistaSerializer.
			/// \param pSerializer the IVistaSerializer to write to.
			/// \param pBuffer pointer to the ITASampleBuffer that shall be written.
			/// \param iLength number of samples that shall be written.
			/// \param iOffset number of samples that shall be skipped before the requested samples begin.
			/// \return the number of bytes written or -1 on failure.
			///
			static int WriteITASampleBuffer( IVistaSerializer& pSerializer, const ITASampleBuffer* pBuffer, const int iLength, const int iOffset );

			///
			/// \brief Read an ITASampleBuffer from the given IVistaDeSerializer.
			/// \param pDeserializer the IVistaDeSerializer to read from.
			/// \param pBuffer the ITASampleBuffer to save into.
			/// \return the number of bytes written or -1 on failure.
			///
			static int ReadITASampleBuffer( IVistaDeSerializer& pDeserializer, ITASampleBuffer& pBuffer );

			static int WriteITAThirdOctaveFactorMagnitudeSpectrum( IVistaSerializer& pSerializer, const ITABase::CThirdOctaveFactorMagnitudeSpectrum& oSpectrum );
			static int ReadITAThirdOctaveFactorMagnitudeSpectrum( IVistaDeSerializer& pDeserializer, ITABase::CThirdOctaveFactorMagnitudeSpectrum& oSpectrum );

			static int WriteVistaVector3D( IVistaSerializer& pSerializer, const VistaVector3D& v3Vector );
			static int ReadVistaVector3D( IVistaDeSerializer& pDeserializer, VistaVector3D& v3Vector );

			static int WriteVistaQuaternion( IVistaSerializer& pSerializer, const VistaQuaternion& qQuaternion );
			static int ReadVistaQuaternion( IVistaDeSerializer& pDeserializer, VistaQuaternion& qQuaternion );
		};
	} // namespace SimulationScheduler
} // namespace ITA

#endif // INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_ROOM_ACOUSTICS_SERIALIZATION_HELPER
