#include "ITA/SimulationScheduler/dummy_worker.h"

#include "ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h"
#include "ITA/SimulationScheduler/profiler.h"
#include "ITA/SimulationScheduler/scheduler_interface.h"
#include "configuration_keys.h"

#include <VistaBase/VistaTimeUtils.h>
#include <chrono>

namespace ITA
{
	namespace SimulationScheduler
	{
#ifdef WITH_PROFILER
		std::size_t CDummyWorker::iWorkerMaxID = 0;
#endif
		CDummyWorker::DummyWorkerConfig::DummyWorkerConfig( ) : WorkerConfig( GetType( ) ) {}

		VistaPropertyList CDummyWorker::DummyWorkerConfig::Store( ) const
		{
			auto oProperties = WorkerConfig::Store( );

			oProperties.SetValue( dummyWorkerTime, dTime );

			oProperties.SetValue( dummyWorkerReturnResult, sReturnResult );

			return oProperties;
		}

		void CDummyWorker::DummyWorkerConfig::Load( const VistaPropertyList& oProperties )
		{
			WorkerConfig::Load( oProperties );

			oProperties.GetValue( dummyWorkerTime, dTime );

			oProperties.GetValue( dummyWorkerReturnResult, sReturnResult );
		}

		CDummyWorker::CDummyWorker( const DummyWorkerConfig& oConfig, ISchedulerInterface* pParent )
		    : IWorkerInterface( pParent )
		    , m_evTriggerLoop( VistaThreadEvent::NON_WAITABLE_EVENT )
		    , m_bBusy( false )
		    , m_bStopIndicated( false )
		    , m_bResetIndicated( false )
		{
			dTime = oConfig.dTime;

			sReturnResult = oConfig.sReturnResult;

			Run( );
		}

		CDummyWorker::~CDummyWorker( )
		{
			m_bStopIndicated = true;
			m_evTriggerLoop.SignalEvent( );

			// Wait for the end of the simulation
			while( IsBusy( ) )
				VistaTimeUtils::Sleep( 100 );

			StopGently( true );
		}

		bool CDummyWorker::IsBusy( ) { return m_bBusy; }

		void CDummyWorker::PushUpdate( std::unique_ptr<CUpdateScene> pUpdate )
		{
			PROFILER_VALUE( "Update to Worker", pUpdate->GetID( ) );

			m_bBusy     = true;
			m_iUpdateID = pUpdate->GetID( );

			// Stop if reset is running.
			if( m_bResetIndicated )
			{
				m_bBusy = false;

				return;
			}

			// Start the loop and simulation.
			m_evTriggerLoop.SignalEvent( );
		}

		void CDummyWorker::Reset( )
		{
			// Signal reset
			m_bResetIndicated = true;

			// Wait for the end of the simulation
			while( IsBusy( ) )
				VistaTimeUtils::Sleep( 100 );
		}

		void CDummyWorker::Shutdown( )
		{
			m_bStopIndicated = true;
			m_evTriggerLoop.SignalEvent( );

			// Wait for the end of the simulation
			while( IsBusy( ) )
				VistaTimeUtils::Sleep( 100 );

			StopGently( true );
		}

		std::unique_ptr<IWorkerInterface> CDummyWorker::CreateWorker( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
		{
			return std::make_unique<CDummyWorker>( dynamic_cast<const DummyWorkerConfig&>( *pConfig ), pParent );
		}

		std::string CDummyWorker::GetType( ) { return "DummyWorker"; }

		bool CDummyWorker::LoopBody( )
		{
			// Wait for trigger
			m_evTriggerLoop.WaitForEvent( true );
			m_evTriggerLoop.ResetThisEvent( );

			PROFILER_FUNCTION( );

			if( m_bStopIndicated )
			{
				m_bBusy = false;
				IndicateLoopEnd( );
				PROFILER_EVENT_COUNT( "Loop Stop" );
				return false;
			}

			PROFILER_SECTION( "Compute Update/Task" );
			PROFILER_VALUE( "Compute Update", m_iUpdateID )

			const auto start = std::chrono::high_resolution_clock::now( );
			while( std::chrono::duration<double>( std::chrono::high_resolution_clock::now( ) - start ).count( ) < dTime )
			{
			}

			if( !sReturnResult.empty( ) )
			{
				if( sReturnResult == "RIR" )
				{
					auto result                = std::make_unique<RoomAcoustics::CRIRSimulationResult>( );
					result->bSameRoom          = true;
					result->eResultType        = RoomAcoustics::FieldOfDuty::diffuseDecay;
					result->iLeadingZeros      = 42;
					result->iTrailingZeros     = 7;
					result->dTimeStamp         = 3.141;
					result->sourceReceiverPair = { new C3DObject( ), new C3DObject( ) };

					std::array<float, 8> data { 0, 1, 2, 3, 4, 5, 6, 7 };
					result->sfResult.Init( 1, data.size( ), false );
					result->sfResult[0].WriteSamples( data.data( ), data.size( ) );

					m_pParentScheduler->HandleSimulationFinished( std::move( result ) );
				}
			}

			PROFILER_END_SECTION( );

			m_bBusy = false;

			return true;
		}


		void CDummyWorker::PreLoop( )
		{
#ifdef WITH_PROFILER
			const std::string sThreadName = "Dummy Worker Thread " + std::to_string( iWorkerMaxID++ );

			SetThreadName( sThreadName );
			PROFILER_NAME_THREAD( sThreadName );
#endif
		}
	} // namespace SimulationScheduler
} // namespace ITA