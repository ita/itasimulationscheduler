// Header include
#include <ITA/SimulationScheduler/update_config.h>

// simulation scheduler include
#include <ITA/SimulationScheduler/Utils/utils.h>

// Vista include
#include <VistaAspects/VistaDeSerializer.h>
#include <VistaAspects/VistaSerializer.h>
#include <utility>

namespace ITA
{
	namespace SimulationScheduler
	{
		CUpdateConfig::CUpdateConfig( ) : m_eConfigChangeType( ), m_sPayload( ) {}
		CUpdateConfig::CUpdateConfig( const ConfigChangeType type, std::string payload ) : m_eConfigChangeType( type ), m_sPayload( std::move( payload ) ) {}
		CUpdateConfig::ConfigChangeType CUpdateConfig::GetType( ) const { return m_eConfigChangeType; }
		const std::string& CUpdateConfig::GetPayload( ) const { return m_sPayload; }
		int CUpdateConfig::Serialize( IVistaSerializer& pSerializer ) const
		{
			int returnVal = 0;
			returnVal += pSerializer.WriteInt32( AsInteger( m_eConfigChangeType ) );
			returnVal += pSerializer.WriteEncodedString( m_sPayload );
			return returnVal;
		}
		int CUpdateConfig::DeSerialize( IVistaDeSerializer& pDeserializer )
		{
			int returnVal = 0;
			int tmp;
			returnVal += pDeserializer.ReadInt32( tmp );
			m_eConfigChangeType = ConfigChangeType( tmp );
			returnVal += pDeserializer.ReadEncodedString( m_sPayload );
			return returnVal;
		}
		std::string CUpdateConfig::GetSignature( ) const { return "CUpdateConfig"; }
	} // namespace SimulationScheduler
} // namespace ITA