#include <ITA/SimulationScheduler/AudibilityFilter/zone_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>

// ITA includes
#include <ITAException.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CZoneFilter::ZoneFilterConfig::ZoneFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CZoneFilter::ZoneFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				auto counter = 0;
				for( const auto& zone: vpZones )
				{
					oProperties.SetValueInSubList( "Zone" + std::to_string( counter ) + "first", "Zones", zone.first );
					oProperties.SetValueInSubList( "Zone" + std::to_string( counter ) + "second", "Zones", zone.second );
					counter++;
				}

				return oProperties;
			}

			void CZoneFilter::ZoneFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				if( oProperties.HasSubList( "Zones" ) )
				{
					const auto zones = oProperties.GetSubListConstRef( "Zones" );
					for( auto i = 0; i < zones.size( ) / 2; ++i )
					{
						std::pair<VistaVector3D, VistaVector3D> zone;
						zone.first  = zones.GetValue<VistaVector3D>( "Zone" + std::to_string( i ) + "first" );
						zone.second = zones.GetValue<VistaVector3D>( "Zone" + std::to_string( i ) + "second" );
						vpZones.push_back( zone );
					}
				}
				else
					ITA_EXCEPT_INVALID_PARAMETER( "Config is not a compatible zone filter config." );
			}

			CZoneFilter::CZoneFilter( const ZoneFilterConfig& oConfig ) { m_vpZones = oConfig.vpZones; }

			bool CZoneFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				auto source   = newUpdate.GetSourceReceiverPair( ).source;
				auto receiver = newUpdate.GetSourceReceiverPair( ).receiver;
				for( auto& zone: m_vpZones )
				{
					auto sourceInside   = true;
					auto receiverInside = true;
					for( auto i = 0; i < 3; ++i )
					{
						sourceInside &= std::min( zone.first[i], zone.second[i] ) <= source->GetPosition( )[i] &&
						                std::max( zone.first[i], zone.second[i] ) >= source->GetPosition( )[i];
						receiverInside &= std::min( zone.first[i], zone.second[i] ) <= receiver->GetPosition( )[i] &&
						                  std::max( zone.first[i], zone.second[i] ) >= receiver->GetPosition( )[i];
					}
					if( sourceInside || receiverInside )
						return true;
				}
				return false;
			}

			std::string CZoneFilter::GetType( ) { return "ZoneFilter"; }

			std::unique_ptr<IAudibilityFilter> CZoneFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CZoneFilter>( dynamic_cast<const CZoneFilter::ZoneFilterConfig&>( *pConfig ) );
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
