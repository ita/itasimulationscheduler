#include <ITA/SimulationScheduler/AudibilityFilter/filter_network.h>

// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>
#include <ITA/SimulationScheduler/AudibilityFilter/distance_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/perceptive_rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/translation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/zone_filter.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <utility>

// ITA includes
#include "ITA/SimulationScheduler/Utils/utils.h"
#include "ITAException.h"

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			VistaPropertyList CFilterNetwork::FilterNetworkConfig::Store( ) const
			{
				VistaPropertyList oProperties;

				auto counter = 0;

				for( const auto& filterConfig: vpFilterConfigs )
				{
					oProperties.SetValue( filterNetworkFilterSectionName + std::to_string( counter++ ), filterConfig->Store( ) );
				}

				oProperties.SetValue( filterNetworkFilterStartKey, sStartFilter );

				return oProperties;
			}

			void CFilterNetwork::FilterNetworkConfig::Load( const VistaPropertyList& oProperties )
			{
				for( const auto& oProperty: oProperties )
				{
					if( oProperty.first.find( filterNetworkFilterSectionName ) != std::string::npos &&
					    oProperty.second.GetPropertyType( ) == VistaProperty::PROPT_PROPERTYLIST )
					{
						auto configProperties = oProperty.second.GetPropertyListConstRef( );
						auto type             = configProperties.GetValue<std::string>( audibilityFilterTypeKey );

						auto filterConfig = CAudibilityFilterFactory::CreateConfig( type );

						filterConfig->Load( configProperties );
						vpFilterConfigs.push_back( filterConfig );
					}
				}

				oProperties.GetValue( filterNetworkFilterStartKey, sStartFilter );
			}

			CFilterNetwork::CFilterNetwork( const FilterNetworkConfig& oConfig )
			{
				CreateFilters( oConfig.vpFilterConfigs );
				BuildNetwork( oConfig );
			}

			bool CFilterNetwork::EvaluateNetwork( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				if( m_pNetworkTree )
					return m_pNetworkTree->Evaluate( previousState, newUpdate );
				else
					return true;
			}

			CFilterNetwork::NetworkTreeNode::NetworkTreeNode( const IAudibilityFilter* pFilter, std::string sFilterName, IAudibilityFilter::UsageMode eUsageMode,
			                                                  bool bInvertCondition )
			    : m_pFilter( pFilter )
			    , m_eUsageMode( eUsageMode )
			    , m_bInvertCondition( bInvertCondition )
			    , m_sFilterName( std::move( sFilterName ) )
			{
			}

			bool CFilterNetwork::NetworkTreeNode::Evaluate( const CUpdateScene& previousState, const CUpdateScene& newUpdate )
			{
				PROFILER_FUNCTION( );
				bool audible = false;

				if( m_eUsageMode == IAudibilityFilter::UsageMode::filter )
				{
					audible |= m_pFilter->ChangeIsAudible( previousState, newUpdate );

					if( audible )
					{
						PROFILER_VALUE( m_sFilterName + " Audible", newUpdate.GetID( ) )
						return audible;
					}
					PROFILER_VALUE( m_sFilterName + " Inaudible", newUpdate.GetID( ) )
				}
				if( m_eUsageMode == IAudibilityFilter::UsageMode::condition )
				{
					const auto conditionFulfilled = m_pFilter->ChangeIsAudible( previousState, newUpdate ) ^ m_bInvertCondition;

					if( !conditionFulfilled )
					{
						PROFILER_VALUE( m_sFilterName + " Condition Not Fulfilled", newUpdate.GetID( ) )
						return false;
					}
					PROFILER_VALUE( m_sFilterName + " Condition Fulfilled", newUpdate.GetID( ) )
				}

				for( auto childIterator = vChildren.begin( ); audible != true && childIterator != vChildren.end( ); ++childIterator )
				{
					audible |= childIterator->Evaluate( previousState, newUpdate );
				}

				return audible;
			}

			void CFilterNetwork::CreateFilters( const std::vector<std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>>& vpFilterConfigs )
			{
				for( const auto& filterConfig: vpFilterConfigs )
				{
					if( m_mAudibilityFilters.find( filterConfig->sFilterName ) != m_mAudibilityFilters.end( ) )
						ITA_EXCEPT_INVALID_PARAMETER( "Filter name already exist in network" );

					m_mAudibilityFilters.insert( { filterConfig->sFilterName, CAudibilityFilterFactory::CreateFilter( filterConfig ) } );
				}
			}

			void CFilterNetwork::BuildNetwork( const FilterNetworkConfig& oConfig )
			{
				if( !oConfig.vpFilterConfigs.empty( ) && !oConfig.sStartFilter.empty( ) )
				{
					std::map<std::string, std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>> mConfigMap;

					for( const auto& filterConfig: oConfig.vpFilterConfigs )
					{
						mConfigMap.insert( { filterConfig->sFilterName, filterConfig } );
					}

					m_pNetworkTree =
					    std::make_unique<NetworkTreeNode>( m_mAudibilityFilters.at( oConfig.sStartFilter ).get( ), oConfig.sStartFilter,
					                                       mConfigMap.at( oConfig.sStartFilter )->eUsageMode, mConfigMap.at( oConfig.sStartFilter )->bInvertCondition );

					NetworkBuilderHelper( mConfigMap, *m_pNetworkTree, *mConfigMap.at( oConfig.sStartFilter ) );
				}
				else
				{
					m_pNetworkTree = nullptr;
				}
			}

			void CFilterNetwork::NetworkBuilderHelper( const std::map<std::string, std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>>& mConfigMap,
			                                           NetworkTreeNode& oRoot, const IAudibilityFilter::AudibilityFilterConfig& oRootConfig )
			{
				for( const auto& next: oRootConfig.sNextFilter )
				{
					oRoot.vChildren.emplace_back( m_mAudibilityFilters.at( next ).get( ), next, mConfigMap.at( next )->eUsageMode,
					                              mConfigMap.at( next )->bInvertCondition );
					NetworkBuilderHelper( mConfigMap, oRoot.vChildren.back( ), *mConfigMap.at( next ) );
				}
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
