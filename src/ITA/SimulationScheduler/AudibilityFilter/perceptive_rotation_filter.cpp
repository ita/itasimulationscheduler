#include <ITA/SimulationScheduler/AudibilityFilter/perceptive_rotation_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"
#include "ITAException.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <ITA/SimulationScheduler/utils/utils.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CPerceptiveRotationFilter::PerceptiveRotationFilterConfig::PerceptiveRotationFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CPerceptiveRotationFilter::PerceptiveRotationFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				oProperties.SetValue( perceptiveRotationFilterStrengthFactorKey, dStrengthFactor );

				return oProperties;
			}

			void CPerceptiveRotationFilter::PerceptiveRotationFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				oProperties.GetValue( perceptiveRotationFilterStrengthFactorKey, dStrengthFactor );
			}

			CPerceptiveRotationFilter::CPerceptiveRotationFilter( const PerceptiveRotationFilterConfig& oConfig ) { m_dStrengthFactor = oConfig.dStrengthFactor; }

			bool CPerceptiveRotationFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				const auto previousSourceReceiverPair = previousState.GetSourceReceiverPair( );
				const auto newSourceReceiverPair      = newUpdate.GetSourceReceiverPair( );

				// previous angles
				const auto previousReceiverElevationToSource = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    previousSourceReceiverPair.receiver->GetPosition( ), previousSourceReceiverPair.receiver->GetOrientation( ),
				    previousSourceReceiverPair.source->GetPosition( ) );
				const auto previousReceiverAzimuthToSource = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    previousSourceReceiverPair.receiver->GetPosition( ), previousSourceReceiverPair.receiver->GetOrientation( ),
				    previousSourceReceiverPair.source->GetPosition( ) );

				// new angles
				const auto newReceiverElevationToSource = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    newSourceReceiverPair.receiver->GetPosition( ), newSourceReceiverPair.receiver->GetOrientation( ), newSourceReceiverPair.source->GetPosition( ) );
				const auto newReceiverAzimuthToSource = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    newSourceReceiverPair.receiver->GetPosition( ), newSourceReceiverPair.receiver->GetOrientation( ), newSourceReceiverPair.source->GetPosition( ) );

				const std::pair<double, double> uncertainty = FindNearest( int( previousReceiverAzimuthToSource ), int( previousReceiverElevationToSource ) );

				const auto receiverElevationDelta = abs( previousReceiverElevationToSource - newReceiverElevationToSource );
				const auto receiverAzimuthDelta   = abs( previousReceiverAzimuthToSource - newReceiverAzimuthToSource );

				// compare delta against this uncertainty
				// when bigger -> audible
				return receiverAzimuthDelta > uncertainty.first * m_dStrengthFactor || receiverElevationDelta > uncertainty.second * m_dStrengthFactor;
			}

			std::string CPerceptiveRotationFilter::GetType( ) { return "PerceptiveRotationFilter"; }

			std::unique_ptr<IAudibilityFilter> CPerceptiveRotationFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CPerceptiveRotationFilter>( dynamic_cast<const PerceptiveRotationFilterConfig&>( *pConfig ) );
			}

			std::pair<double, double> CPerceptiveRotationFilter::FindNearest( int azimuth, int elevation )
			{
				// PROFILER_FUNCTION ( );
				// condition the arguments for the data as we only have half the azimuth range
				azimuth = azimuth % 180;

				const auto lowerBoundAz = m_mmLocalizationUncertainty.lower_bound( azimuth );

				std::map<int, std::pair<double, double>> elevationMap;

				if( lowerBoundAz == m_mmLocalizationUncertainty.end( ) )
					ITA_EXCEPT_INVALID_PARAMETER( "Not found" );

				if( lowerBoundAz == m_mmLocalizationUncertainty.begin( ) )
				{
					elevationMap = lowerBoundAz->second;
				}
				else
				{
					auto beforeLowerBoundAz = lowerBoundAz;
					--beforeLowerBoundAz;

					if( abs( lowerBoundAz->first - azimuth ) > abs( beforeLowerBoundAz->first - azimuth ) )
					{
						elevationMap = beforeLowerBoundAz->second;
					}
					if( abs( lowerBoundAz->first - azimuth ) <= abs( beforeLowerBoundAz->first - azimuth ) )
					{
						elevationMap = lowerBoundAz->second;
					}
				}

				const auto lowerBoundEl = elevationMap.lower_bound( elevation );

				if( lowerBoundEl == elevationMap.end( ) )
				{
					auto beforeLowerBoundEl = lowerBoundEl;
					--beforeLowerBoundEl;
					return beforeLowerBoundEl->second;
				}

				if( lowerBoundEl == elevationMap.begin( ) )
				{
					return lowerBoundEl->second;
				}
				else
				{
					auto beforeLowerBoundEl = lowerBoundEl;
					--beforeLowerBoundEl;

					if( abs( lowerBoundEl->first - elevation ) > abs( beforeLowerBoundEl->first - elevation ) )
					{
						return beforeLowerBoundEl->second;
					}
					if( abs( lowerBoundEl->first - elevation ) <= abs( beforeLowerBoundEl->first - elevation ) )
					{
						return lowerBoundEl->second;
					}
				}

				return std::pair<double, double>( );
			}

			const std::map<int, std::map<int, std::pair<double, double>>> CPerceptiveRotationFilter::m_mmLocalizationUncertainty = [] {
				std::map<int, std::map<int, std::pair<double, double>>> data {
					{ 0, { { 45, { 3, 7.9 } }, { 25, { 2.3, 4.4 } }, { 5, { 2.9, 3.6 } }, { -5, { 1.9, 3.3 } }, { -25, { 3.1, 7.3 } }, { -45, { 3.6, 7.6 } } } },

					{ 20, { { 45, { 4.8, 7.8 } }, { 25, { 4.0, 5.5 } }, { 5, { 4.1, 3.4 } }, { -5, { 3.9, 4.2 } }, { -25, { 5.2, 6.6 } }, { -45, { 5.7, 7.2 } } } },

					{ 40, { { 45, { 5.2, 6.0 } }, { 25, { 6.0, 6.6 } }, { 5, { 5.5, 3.6 } }, { -5, { 6.3, 4.2 } }, { -25, { 4.1, 5.3 } } } },

					{ 60, { { 25, { 7.2, 7.1 } }, { 5, { 7.1, 4.4 } }, { -5, { 7.3, 4.6 } }, { -25, { 6.2, 4.4 } } } },

					{ 80, { { 5, { 5.9, 3.7 } }, { -5, { 5.9, 5.1 } } } },

					{ 100, { { 5, { 6.1, 6.7 } }, { -5, { 9.0, 3.5 } } } },

					{ 120, { { 25, { 10.6, 6.0 } }, { 5, { 11.4, 7.0 } }, { -5, { 11.1, 5.2 } }, { -25, { 9.0, 4.9 } } } },

					{ 140, { { 25, { 15.8, 9.9 } }, { 5, { 11.0, 5.9 } }, { -5, { 8.6, 6.6 } }, { -25, { 9.5, 5.0 } }, { -45, { 10.0, 5.1 } } } },

					{ 160, { { 45, { 16.6, 9.6 } }, { 25, { 14.7, 12.3 } }, { 5, { 10.8, 8.5 } }, { -5, { 7.6, 6.8 } }, { -25, { 9.7, 5.9 } }, { -45, { 7.4, 5.7 } } } },

					{ 180, { { 0, { 5.5, 9.0 } } } }
				};

				return data;
			}( );
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
