#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>

// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/AudibilityFilter/distance_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/perceptive_rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/translation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/zone_filter.h>
#include <ITA/SimulationScheduler/Utils/utils.h>
#include <ITA/SimulationScheduler/update_scene.h>

// ITA includes
#include <ITAException.h>
#include <ITAStringUtils.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			std::map<std::string, std::pair<CAudibilityFilterFactory::CreateCallback, CAudibilityFilterFactory::ConfigCreateCallback>>
			    CAudibilityFilterFactory::m_mFilters = {
				    { CRateFilter::GetType( ), { CRateFilter::CreateFilter, std::make_shared<CRateFilter::RateFilterConfig> } },
				    { CDistanceFilter::GetType( ), { CDistanceFilter::CreateFilter, std::make_shared<CDistanceFilter::DistanceFilterConfig> } },
				    { CRotationFilter::GetType( ), { CRotationFilter::CreateFilter, std::make_shared<CRotationFilter::RotationFilterConfig> } },
				    { CTranslationFilter::GetType( ), { CTranslationFilter::CreateFilter, std::make_shared<CTranslationFilter::TranslationFilterConfig> } },
				    { CZoneFilter::GetType( ), { CZoneFilter::CreateFilter, std::make_shared<CZoneFilter::ZoneFilterConfig> } },
				    { CPerceptiveRotationFilter::GetType( ),
				      { CPerceptiveRotationFilter::CreateFilter, std::make_shared<CPerceptiveRotationFilter::PerceptiveRotationFilterConfig> } }
			    };


			IAudibilityFilter::AudibilityFilterConfig::AudibilityFilterConfig( ) : sFilterType( m_sFilterType ) { m_sFilterType = "Unknown"; }

			IAudibilityFilter::AudibilityFilterConfig::AudibilityFilterConfig( std::string sType ) : sFilterType( m_sFilterType ) { m_sFilterType = sType; }

			IAudibilityFilter::AudibilityFilterConfig::AudibilityFilterConfig( const AudibilityFilterConfig& other )
			    : IConfig( other )
			    , sFilterName( other.sFilterName )
			    , sFilterType( m_sFilterType )
			    , sNextFilter( other.sNextFilter )
			    , eUsageMode( other.eUsageMode )
			    , bInvertCondition( other.bInvertCondition )
			    , m_sFilterType( other.m_sFilterType )
			{
			}

			IAudibilityFilter::AudibilityFilterConfig& IAudibilityFilter::AudibilityFilterConfig::operator=( const AudibilityFilterConfig& other )
			{
				if( this == &other )
					return *this;
				IConfig::operator=( other );
				sFilterName      = other.sFilterName;
				sNextFilter      = other.sNextFilter;
				eUsageMode       = other.eUsageMode;
				bInvertCondition = other.bInvertCondition;
				m_sFilterType    = other.m_sFilterType;
				return *this;
			}

			VistaPropertyList IAudibilityFilter::AudibilityFilterConfig::Store( ) const
			{
				VistaPropertyList properties;
				properties.SetValue( audibilityFilterNameKey, sFilterName );
				properties.SetValue( audibilityFilterNextFilterKey, sNextFilter );
				properties.SetValue( audibilityFilterTypeKey, m_sFilterType );
				properties.SetValue( audibilityFilterUsageModeKey, AsInteger( eUsageMode ) );
				properties.SetValue( audibilityFilterInvertConditionKey, bInvertCondition );
				return properties;
			}

			void IAudibilityFilter::AudibilityFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				oProperties.GetValue( audibilityFilterNameKey, sFilterName );
				oProperties.GetValue( audibilityFilterNextFilterKey, sNextFilter );
				oProperties.GetValue<std::string>( audibilityFilterTypeKey, m_sFilterType );
				eUsageMode = static_cast<UsageMode>( oProperties.GetValue<int>( audibilityFilterUsageModeKey ) );
				oProperties.GetValue( audibilityFilterInvertConditionKey, bInvertCondition );
			}

			void IAudibilityFilter::CheckUpdateTimestamp( const CUpdateScene& previousState, const CUpdateScene& newUpdate )
			{
				if( previousState.GetTimeStamp( ) > newUpdate.GetTimeStamp( ) )
					ITA_EXCEPT_INVALID_PARAMETER( "The previous state happend before the new update" );
			}

			void CAudibilityFilterFactory::RegisterFilter( const std::string& type, CreateCallback createFunction, ConfigCreateCallback configCreateFunction )
			{
				m_mFilters[type] = { createFunction, configCreateFunction };
			}

			void CAudibilityFilterFactory::UnregisterFilter( const std::string& type ) { m_mFilters.erase( type ); }

			std::unique_ptr<IAudibilityFilter> CAudibilityFilterFactory::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				auto it = m_mFilters.find( pConfig->sFilterType );
				if( it != m_mFilters.end( ) )
				{
					// call the creation callback to construct this derived type
					return it->second.first( pConfig );
				}
				return nullptr;
			}

			std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig> CAudibilityFilterFactory::CreateConfig( const std::string& sType )
			{
				auto it = m_mFilters.find( sType );
				if( it != m_mFilters.end( ) )
				{
					// call the creation callback to construct this derived type
					return it->second.second( );
				}
				return nullptr;
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
