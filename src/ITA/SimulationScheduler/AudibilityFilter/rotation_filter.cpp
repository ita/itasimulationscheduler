#include <ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"
#include "ITAException.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <ITA/SimulationScheduler/utils/utils.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CRotationFilter::RotationFilterConfig::RotationFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CRotationFilter::RotationFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				oProperties.SetValue( rotationFilterSourceThresholdKey, dSourceThreshold );
				oProperties.SetValue( rotationFilterReceiverThresholdKey, dReceiverThreshold );
				oProperties.SetValue( rotationFilterRotationModeKey, AsInteger( eMode ) );

				return oProperties;
			}

			void CRotationFilter::RotationFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				oProperties.GetValue( rotationFilterSourceThresholdKey, dSourceThreshold );
				oProperties.GetValue( rotationFilterReceiverThresholdKey, dReceiverThreshold );
				eMode = static_cast<RotationModes>( oProperties.GetValue<int>( rotationFilterRotationModeKey ) );
			}

			CRotationFilter::CRotationFilter( const RotationFilterConfig& oConfig )
			{
				m_dSourceThreshold   = oConfig.dSourceThreshold;
				m_dReceiverThreshold = oConfig.dReceiverThreshold;
				m_eRotationMode      = oConfig.eMode;
			}

			bool CRotationFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				if( m_eRotationMode == RotationModes::absolute )
					return CheckAbsoluteRotation( previousState, newUpdate );

				if( m_eRotationMode == RotationModes::relative )
					return CheckRelativeRotation( previousState, newUpdate );

				return true;
			}

			std::string CRotationFilter::GetType( ) { return "RotationFilter"; }

			std::unique_ptr<IAudibilityFilter> CRotationFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CRotationFilter>( dynamic_cast<const RotationFilterConfig&>( *pConfig ) );
			}

			bool CRotationFilter::CheckRelativeRotation( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				const auto previousSourceReceiverPair = previousState.GetSourceReceiverPair( );
				const auto newSourceReceiverPair      = newUpdate.GetSourceReceiverPair( );

				// previous angles
				const auto previousSourceElevationToReceiver = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    previousSourceReceiverPair.source->GetPosition( ), previousSourceReceiverPair.source->GetOrientation( ),
				    previousSourceReceiverPair.receiver->GetPosition( ) );
				const auto previousSourceAzimuthToReceiver = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    previousSourceReceiverPair.source->GetPosition( ), previousSourceReceiverPair.source->GetOrientation( ),
				    previousSourceReceiverPair.receiver->GetPosition( ) );

				const auto previousReceiverElevationToSource = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    previousSourceReceiverPair.receiver->GetPosition( ), previousSourceReceiverPair.receiver->GetOrientation( ),
				    previousSourceReceiverPair.source->GetPosition( ) );
				const auto previousReceiverAzimuthToSource = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    previousSourceReceiverPair.receiver->GetPosition( ), previousSourceReceiverPair.receiver->GetOrientation( ),
				    previousSourceReceiverPair.source->GetPosition( ) );

				// new angles
				const auto newSourceElevationToReceiver = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    newSourceReceiverPair.source->GetPosition( ), newSourceReceiverPair.source->GetOrientation( ), newSourceReceiverPair.receiver->GetPosition( ) );
				const auto newSourceAzimuthToReceiver = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    newSourceReceiverPair.source->GetPosition( ), newSourceReceiverPair.source->GetOrientation( ), newSourceReceiverPair.receiver->GetPosition( ) );

				const auto newReceiverElevationToSource = Utils::CalculationUtils::CalculateElevationOnTargetInDegree(
				    newSourceReceiverPair.receiver->GetPosition( ), newSourceReceiverPair.receiver->GetOrientation( ), newSourceReceiverPair.source->GetPosition( ) );
				const auto newReceiverAzimuthToSource = Utils::CalculationUtils::CalculateAzimuthOnTargetInDegree(
				    newSourceReceiverPair.receiver->GetPosition( ), newSourceReceiverPair.receiver->GetOrientation( ), newSourceReceiverPair.source->GetPosition( ) );


				const auto sourceElevationDelta = abs( previousSourceElevationToReceiver - newSourceElevationToReceiver );
				const auto sourceAzimuthDelta   = abs( previousSourceAzimuthToReceiver - newSourceAzimuthToReceiver );

				const auto receiverElevationDelta = abs( previousReceiverElevationToSource - newReceiverElevationToSource );
				const auto receiverAzimuthDelta   = abs( previousReceiverAzimuthToSource - newReceiverAzimuthToSource );

				const auto audible = sourceElevationDelta > m_dSourceThreshold || sourceAzimuthDelta > m_dSourceThreshold ||
				                     receiverElevationDelta > m_dReceiverThreshold || receiverAzimuthDelta > m_dReceiverThreshold;

				return audible;
			}

			bool CRotationFilter::CheckAbsoluteRotation( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				const auto previousSourceReceiverPair = previousState.GetSourceReceiverPair( );
				const auto newSourceReceiverPair      = newUpdate.GetSourceReceiverPair( );

				const auto previousSourceAnglesRad   = previousSourceReceiverPair.source->GetOrientation( ).GetAngles( );
				const auto previousReceiverAnglesRad = previousSourceReceiverPair.receiver->GetOrientation( ).GetAngles( );

				const auto newSourceAnglesRad   = newSourceReceiverPair.source->GetOrientation( ).GetAngles( );
				const auto newReceiverAnglesRad = newSourceReceiverPair.receiver->GetOrientation( ).GetAngles( );

				const auto sourceDelta   = abs( previousSourceAnglesRad - newSourceAnglesRad ) * 180.f / ITAConstants::PI_F;
				const auto receiverDelta = abs( previousReceiverAnglesRad - newReceiverAnglesRad ) * 180.f / ITAConstants::PI_F;

				const auto audible = ( sourceDelta > float( m_dSourceThreshold ) ).max( ) || ( receiverDelta > float( m_dReceiverThreshold ) ).max( );

				return audible;
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
