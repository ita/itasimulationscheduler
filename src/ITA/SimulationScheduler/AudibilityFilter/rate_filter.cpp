#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>

// ITA includes
#include <ITAException.h>

// STD includes
#include <cassert>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CRateFilter::RateFilterConfig::RateFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CRateFilter::RateFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				oProperties.SetValue( rateFilterRateKey, dRate );

				return oProperties;
			}

			void CRateFilter::RateFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				oProperties.GetValue( rateFilterRateKey, dRate );
			}

			CRateFilter::CRateFilter( const RateFilterConfig& oConfig ) { m_dRate = oConfig.dRate; }

			bool CRateFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				const auto dDeltaTime = newUpdate.GetTimeStamp( ) - previousState.GetTimeStamp( );
				assert( abs( dDeltaTime ) > std::numeric_limits<double>::epsilon( ) );
				const auto dCurrentRate = 1 / dDeltaTime;
				return dCurrentRate < m_dRate;
			}

			std::string CRateFilter::GetType( ) { return "RateFilter"; }

			std::unique_ptr<IAudibilityFilter> CRateFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CRateFilter>( dynamic_cast<const RateFilterConfig&>( *pConfig ) );
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
