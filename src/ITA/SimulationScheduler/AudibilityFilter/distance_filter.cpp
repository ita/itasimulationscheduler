#include <ITA/SimulationScheduler/AudibilityFilter/distance_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"

#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>

// ITA includes
#include <ITAException.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CDistanceFilter::DistanceFilterConfig::DistanceFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CDistanceFilter::DistanceFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				oProperties.SetValue( distanceFilterDistanceThresholdKey, dDistanceThreshold );

				return oProperties;
			}

			void CDistanceFilter::DistanceFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				oProperties.GetValue( distanceFilterDistanceThresholdKey, dDistanceThreshold );
			}

			CDistanceFilter::CDistanceFilter( const DistanceFilterConfig& oConfig ) { m_dDistanceThreshold = oConfig.dDistanceThreshold; }

			bool CDistanceFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				const auto newSourceReceiverPair = newUpdate.GetSourceReceiverPair( );

				const auto distance = newSourceReceiverPair.source->GetPosition( ) - newSourceReceiverPair.receiver->GetPosition( );

				const auto distanceLength = distance.GetLength( );

				return distanceLength < m_dDistanceThreshold;
			}

			std::string CDistanceFilter::GetType( ) { return "DistanceFilter"; }

			std::unique_ptr<IAudibilityFilter> CDistanceFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CDistanceFilter>( dynamic_cast<const DistanceFilterConfig&>( *pConfig ) );
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
