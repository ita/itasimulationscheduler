#include <ITA/SimulationScheduler/AudibilityFilter/translation_filter.h>

// simulation scheduler includes
#include "../configuration_keys.h"
#include "ITAException.h"

#include <ITA/SimulationScheduler/Utils/utils.h>
#include <ITA/SimulationScheduler/profiler.h>
#include <ITA/SimulationScheduler/update_scene.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace AudibilityFilter
		{
			CTranslationFilter::TranslationFilterConfig::TranslationFilterConfig( ) : AudibilityFilterConfig( GetType( ) ) {}

			VistaPropertyList CTranslationFilter::TranslationFilterConfig::Store( ) const
			{
				auto oProperties = AudibilityFilterConfig::Store( );

				oProperties.SetValue( translationFilterThresholdKey, dThreshold );
				oProperties.SetValue( translationFilterTranslationModeKey, AsInteger( eMode ) );

				return oProperties;
			}

			void CTranslationFilter::TranslationFilterConfig::Load( const VistaPropertyList& oProperties )
			{
				AudibilityFilterConfig::Load( oProperties );

				oProperties.GetValue( translationFilterThresholdKey, dThreshold );
				eMode = static_cast<TranslationModes>( oProperties.GetValue<int>( translationFilterTranslationModeKey ) );
			}

			CTranslationFilter::CTranslationFilter( const TranslationFilterConfig& oConfig )
			{
				m_dThreshold       = oConfig.dThreshold;
				m_eTranslationMode = oConfig.eMode;
			}

			bool CTranslationFilter::ChangeIsAudible( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const
			{
				PROFILER_FUNCTION( );
				CheckUpdateTimestamp( previousState, newUpdate );

				const auto previousSourceReceiverPair = previousState.GetSourceReceiverPair( );
				const auto newSourceReceiverPair      = newUpdate.GetSourceReceiverPair( );

				if( m_eTranslationMode == TranslationModes::absolute )
				{
					const auto receiverTranslationLength =
					    ( previousSourceReceiverPair.receiver->GetPosition( ) - newSourceReceiverPair.receiver->GetPosition( ) ).GetLength( );
					const auto sourceTranslationLength =
					    ( previousSourceReceiverPair.source->GetPosition( ) - newSourceReceiverPair.source->GetPosition( ) ).GetLength( );

					return receiverTranslationLength >= m_dThreshold || sourceTranslationLength >= m_dThreshold;
				}

				if( m_eTranslationMode == TranslationModes::relative )
				{
					const auto previousDistance = previousSourceReceiverPair.source->GetPosition( ) - previousSourceReceiverPair.receiver->GetPosition( );
					const auto newDistance      = newSourceReceiverPair.source->GetPosition( ) - newSourceReceiverPair.receiver->GetPosition( );

					auto distanceQuotient = newDistance.GetLength( ) / previousDistance.GetLength( );

					if( distanceQuotient < 1 )
						distanceQuotient = 1 / distanceQuotient;

					return distanceQuotient > m_dThreshold;
				}

				ITA_EXCEPT0( MODAL_EXCEPTION );
			}

			std::string CTranslationFilter::GetType( ) { return "TranslationFilter"; }

			std::unique_ptr<IAudibilityFilter> CTranslationFilter::CreateFilter( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
			{
				return std::make_unique<CTranslationFilter>( dynamic_cast<const TranslationFilterConfig&>( *pConfig ) );
			}
		} // namespace AudibilityFilter
	}     // namespace SimulationScheduler
} // namespace ITA
