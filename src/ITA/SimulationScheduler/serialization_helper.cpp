#include "serialization_helper.h"

// std includes
#include <cassert>

int ITA::SimulationScheduler::SerializationHelper::WriteITASampleFrame( IVistaSerializer& pSerializer, const ITASampleFrame* pSampleFrame, const int iLength,
                                                                        const int iOffset )
{
	int returnVal = 0;

	assert( iOffset >= 0 );
	assert( iLength >= 0 );
	assert( iLength + iOffset <= pSampleFrame->length( ) );

	const int iNumChannels = pSampleFrame->channels( );

	returnVal += pSerializer.WriteInt32( iNumChannels );
	returnVal += pSerializer.WriteInt32( iLength );

	for( int i = 0; i < iNumChannels; i++ )
		returnVal += WriteITASampleBuffer( pSerializer, &( *pSampleFrame )[i], iLength, iOffset );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::ReadITASampleFrame( IVistaDeSerializer& pDeserializer, ITASampleFrame& pSampleFrame )
{
	int returnVal = 0;

	int iNumChannels;
	returnVal += pDeserializer.ReadInt32( iNumChannels );

	int iLength;
	returnVal += pDeserializer.ReadInt32( iLength );
	assert( iNumChannels >= 0 );
	assert( iLength >= 0 );

	pSampleFrame.init( iNumChannels, iLength, false );
	for( int i = 0; i < iNumChannels; i++ )
		returnVal += ReadITASampleBuffer( pDeserializer, pSampleFrame[i] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::WriteITASampleBuffer( IVistaSerializer& pSerializer, const ITASampleBuffer* pBuffer, const int iLength,
                                                                         const int iOffset )
{
	int returnVal = 0;

	assert( iOffset >= 0 );
	assert( iLength >= 0 );
	assert( iLength + iOffset <= pBuffer->GetLength( ) );

	returnVal += pSerializer.WriteInt32( iLength );

	for( int i = 0; i < iLength; i++ )
		returnVal += pSerializer.WriteFloat32( ( *pBuffer )[i + iOffset] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::ReadITASampleBuffer( IVistaDeSerializer& pDeserializer, ITASampleBuffer& pBuffer )
{
	int returnVal = 0;

	int iLength;
	returnVal += pDeserializer.ReadInt32( iLength );
	assert( iLength >= 0 );
	for( int i = 0; i < iLength; i++ )
		returnVal += pDeserializer.ReadFloat32( pBuffer[i] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::WriteITAThirdOctaveFactorMagnitudeSpectrum( IVistaSerializer& pSerializer,
                                                                                               const ITABase::CThirdOctaveFactorMagnitudeSpectrum& oSpectrum )
{
	int returnVal = 0;

	for( int i = 0; i < oSpectrum.GetNumBands( ); i++ )
		returnVal += pSerializer.WriteFloat32( oSpectrum[i] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::ReadITAThirdOctaveFactorMagnitudeSpectrum( IVistaDeSerializer& pDeserializer,
                                                                                              ITABase::CThirdOctaveFactorMagnitudeSpectrum& oSpectrum )
{
	int returnVal = 0;

	for( int i = 0; i < oSpectrum.GetNumBands( ); i++ )
		returnVal += pDeserializer.ReadFloat32( oSpectrum[i] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::WriteVistaVector3D( IVistaSerializer& pSerializer, const VistaVector3D& v3Vector )
{
	int returnVal = 0;

	returnVal += pSerializer.WriteFloat32( v3Vector[Vista::X] );
	returnVal += pSerializer.WriteFloat32( v3Vector[Vista::Y] );
	returnVal += pSerializer.WriteFloat32( v3Vector[Vista::Z] );
	returnVal += pSerializer.WriteFloat32( v3Vector[Vista::W] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::ReadVistaVector3D( IVistaDeSerializer& pDeserializer, VistaVector3D& v3Vector )
{
	int returnVal = 0;

	returnVal += pDeserializer.ReadFloat32( v3Vector[Vista::X] );
	returnVal += pDeserializer.ReadFloat32( v3Vector[Vista::Y] );
	returnVal += pDeserializer.ReadFloat32( v3Vector[Vista::Z] );
	returnVal += pDeserializer.ReadFloat32( v3Vector[Vista::W] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::WriteVistaQuaternion( IVistaSerializer& pSerializer, const VistaQuaternion& qQuaternion )
{
	int returnVal = 0;

	returnVal += pSerializer.WriteFloat32( qQuaternion[Vista::X] );
	returnVal += pSerializer.WriteFloat32( qQuaternion[Vista::Y] );
	returnVal += pSerializer.WriteFloat32( qQuaternion[Vista::Z] );
	returnVal += pSerializer.WriteFloat32( qQuaternion[Vista::W] );

	return returnVal;
}

int ITA::SimulationScheduler::SerializationHelper::ReadVistaQuaternion( IVistaDeSerializer& pDeserializer, VistaQuaternion& qQuaternion )
{
	int returnVal = 0;

	returnVal += pDeserializer.ReadFloat32( qQuaternion[Vista::X] );
	returnVal += pDeserializer.ReadFloat32( qQuaternion[Vista::Y] );
	returnVal += pDeserializer.ReadFloat32( qQuaternion[Vista::Z] );
	returnVal += pDeserializer.ReadFloat32( qQuaternion[Vista::W] );

	return returnVal;
}
