#include "ITA/SimulationScheduler/scheduler.h"
#include "remote-worker.grpc.pb.h"

#include <ITA/SimulationScheduler/RoomAcoustics/Raven/worker_thread.h>
#include <ITA/SimulationScheduler/dummy_worker.h>
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/worker_remote.h>
#include <atomic>
#include <chrono>
#include <complex.h>
#include <iostream>
#include <thread>

ITA::SimulationScheduler::RemoteWorkerRPC::SampleFrame convertITASampleFrame( const ITASampleFrame objIn )
{
	ITA::SimulationScheduler::RemoteWorkerRPC::SampleFrame objOut;

	for( int channel = 0; channel < objIn.channels( ); ++channel )
	{
		auto buf = objOut.add_buffers( );
		// ITA::SimulationScheduler::grpc::SampleBuffer buf;

		for( int sample = 0; sample < objIn.length( ); ++sample )
		{
			buf->add_samples( objIn[channel][sample] );
		}

		// objOut.mutable_buffers( channel )->CopyFrom( buf );
	}

	return objOut;
}

void testConvert( )
{
	ITASampleFrame frame( 2, 8 );

	std::array<float, 8> buf1 { 0, 1, 2, 3, 4, 5, 6, 7 };
	std::array<float, 8> buf2 { 7, 6, 5, 4, 3, 2, 1, 0 };

	frame[0].WriteSamples( buf1.data( ), 8 );
	frame[1].WriteSamples( buf2.data( ), 8 );

	auto grpcFrame = convertITASampleFrame( frame );

	std::cout << grpcFrame.DebugString( );
}

struct ResultHandler : public ITA::SimulationScheduler::IResultHandler
{
	void PostResultReceived( std::unique_ptr<ITA::SimulationScheduler::CSimulationResult> pResult ) override { std::cout << "Result received\n"; }
};

int main( )
{
	ITA::SimulationScheduler::CScheduler::LocalSchedulerConfig schedulerConfig;

	const auto workerConfig     = std::make_shared<ITA::SimulationScheduler::CDummyWorker::DummyWorkerConfig>( );
	workerConfig->dTime         = 0.5;
	workerConfig->sReturnResult = "RIR";

	const auto remoteWorkerConfig = std::make_shared<ITA::SimulationScheduler::CWorkerRemote::WorkerRemoteConfig>( );

	remoteWorkerConfig->sRemoteAddress = "localhost:50052";
	remoteWorkerConfig->vpWorkerConfigs.push_back( workerConfig );
	remoteWorkerConfig->vpWorkerConfigs.push_back( workerConfig );

	schedulerConfig.vpWorkerConfigs.push_back( remoteWorkerConfig );

	ITA::SimulationScheduler::CScheduler scheduler( schedulerConfig );

	ResultHandler handler;

	scheduler.AttachResultHandler( &handler );


	std::unique_ptr<ITA::SimulationScheduler::CUpdateScene> update;

	for( int i = 0; i < 10; i++ )
	{
		update = std::make_unique<ITA::SimulationScheduler::CUpdateScene>( );

		update->SetSourceReceiverPair( std::make_unique<ITA::SimulationScheduler::C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 2, 3, 4 },
		                                                                                      ITA::SimulationScheduler::C3DObject::Type::source, 1 ),
		                               std::make_unique<ITA::SimulationScheduler::C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 1.1, 2.2, 3.3 },
		                                                                                      ITA::SimulationScheduler::C3DObject::Type::receiver, 1 ) );

		scheduler.PushUpdate( std::move( update ) );

		std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
	}

	update = std::make_unique<ITA::SimulationScheduler::CUpdateScene>( );
	update->SetSourceReceiverPair( std::make_unique<ITA::SimulationScheduler::C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 2, 3, 4 },
	                                                                                      ITA::SimulationScheduler::C3DObject::Type::source, 1 ),
	                               std::make_unique<ITA::SimulationScheduler::C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 1.1, 2.2, 3.3 },
	                                                                                      ITA::SimulationScheduler::C3DObject::Type::receiver, 1 ) );

	scheduler.PushUpdate( std::move( update ) );

	// Wait for results to come in
	std::this_thread::sleep_for( std::chrono::seconds( 2 ) );


	return 0;
}
