#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_worker_thread.h>
//#include <ITA/SimulationScheduler/worker_interface.h>

#include "mocks.hpp"
#include "test_classes.hpp"

// GTest
#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace OutdoorAcoustics;

struct OutdoorWorkerThreadTest : testing::Test
{
	std::unique_ptr<TestOutdoorWorkerThread> pWorker;
	std::unique_ptr<MockScheduler> pScheduler;

	OutdoorWorkerThreadTest( )
	{
		// overwrite the correct create function.
		CWorkerFactory::RegisterWorker( OutdoorAcoustics::CWorkerThread::GetType( ), TestOutdoorWorkerThread::CreateWorker,
		                                std::make_shared<TestOutdoorWorkerThread::WorkerThreadConfig> );

		pScheduler = std::make_unique<MockScheduler>( );

		auto oConfig = std::make_shared<OutdoorAcoustics::CWorkerThread::WorkerThreadConfig>( );

		pWorker =
		    std::unique_ptr<TestOutdoorWorkerThread>( dynamic_cast<TestOutdoorWorkerThread*>( CWorkerFactory::CreateWorker( oConfig, pScheduler.get( ) ).release( ) ) );
	}

	~OutdoorWorkerThreadTest( )
	{
		// Reset the create function
		CWorkerFactory::RegisterWorker( OutdoorAcoustics::CWorkerThread::GetType( ), OutdoorAcoustics::CWorkerThread::CreateWorker,
		                                std::make_shared<OutdoorAcoustics::CWorkerThread::WorkerThreadConfig> );
	}
};

TEST_F( OutdoorWorkerThreadTest, updateToSimulatorAndResultToScheduler )
{
	auto update   = std::make_unique<CUpdateScene>( 1 );
	auto source   = std::make_unique<C3DObject>( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = std::make_unique<C3DObject>( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

	pWorker->PushUpdate( std::move( update ) );

	// wait for the thread
	VistaTimeUtils::Sleep( 100 );

	EXPECT_TRUE( pWorker->pSimulator->bUpdateReceived );

	EXPECT_TRUE( pWorker->IsBusy( ) );

	pWorker->pSimulator->bFinishSimulation = true;

	// wait for the thread
	VistaTimeUtils::Sleep( 500 );

	EXPECT_EQ( 1, pScheduler->results.size( ) );
}

TEST_F( OutdoorWorkerThreadTest, reset )
{
	pWorker->Reset( );

	EXPECT_TRUE( pWorker->pSimulator->bReset );
}