#pragma once

#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/scheduler_interface.h>
#include <ITA/SimulationScheduler/worker_interface.h>
#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>
#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>

#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulator_interface.h>
#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>

#ifdef WITH_RAVEN
#include <ITA/SimulationScheduler/RoomAcoustics/raven/simulator.h>
#include "ITA/SimulationScheduler/RoomAcoustics/Raven/worker_thread.h"
#endif

#include <ITAException.h>

// Vista
#include <VistaBase/VistaTimeUtils.h>

// STD
#include <memory>

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::RoomAcoustics;

struct MockResultHandler : public IResultHandler
{
	MockResultHandler ( )
	{

	}

	void PostResultReceived ( std::unique_ptr<CSimulationResult> result ) override
	{
		resultVector.push_back ( result.release ( ) );

		const auto rirResult = dynamic_cast< const CRIRSimulationResult* >( resultVector.back ( ) );

		if ( rirResult )
		{
			if ( rirResult->eResultType == FieldOfDuty::directSound )
				dsResultVector.push_back ( rirResult );
			if ( rirResult->eResultType == FieldOfDuty::earlyReflections )
				erResultVector.push_back ( rirResult );
			if ( rirResult->eResultType == FieldOfDuty::diffuseDecay )
				ddResultVector.push_back ( rirResult );
		}

	}

	std::vector<CSimulationResult*> resultVector = std::vector<CSimulationResult*> ( );

	std::vector<const CRIRSimulationResult*> dsResultVector;
	std::vector<const CRIRSimulationResult*> erResultVector;
	std::vector<const CRIRSimulationResult*> ddResultVector;
};

struct MockScheduler : public ISchedulerInterface
{
	MockScheduler ( )
	{

	}

	void PushUpdate ( std::unique_ptr<ITA::SimulationScheduler::IUpdateMessage> pUpdate ) override
	{
		updates.push_back ( pUpdate.release ( ) );

		if ( auto sceneUpdate = dynamic_cast< CUpdateScene* >( updates.back ( ) ) )
		{
			for ( auto handler : handlers )
			{
				auto result = std::make_unique<CRIRSimulationResult> ( );

				result->sourceReceiverPair = sceneUpdate->GetSourceReceiverPair ( );

				handler->PostResultReceived ( std::move ( result ) );
			}
		}
	}

	void AttachResultHandler ( IResultHandler* pHandler ) override
	{
		handlers.push_back ( pHandler );
	}

	void DetachResultHandler ( IResultHandler* ) override
	{ }

	void HandleSimulationFinished ( std::unique_ptr<CSimulationResult> pResult ) override
	{
		results.push_back ( std::move ( pResult ) );
	}

	bool IsBusy() const override
	{
		return false;
	}
	
	std::vector<IResultHandler*> handlers;

	std::vector<IUpdateMessage*> updates;

	std::vector<std::unique_ptr<CSimulationResult>> results;
};

struct MockWorker : public IWorkerInterface
{
	struct MockWorkerConfig : WorkerConfig
	{
		MockWorkerConfig ( ) : WorkerConfig ( "MockWorker" )
		{ }

		VistaPropertyList Store ( ) const override
		{
			return VistaPropertyList ( );
		}

		void Load ( const VistaPropertyList& oProperties ) override
		{

		}
	};

	MockWorker ( const MockWorkerConfig& oConfig, ISchedulerInterface* pParent ) : IWorkerInterface ( pParent )
	{ }

	~MockWorker ( ) override
	{ }

	static std::unique_ptr<IWorkerInterface> createWorker ( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
	{
		return std::make_unique<MockWorker> ( dynamic_cast< const MockWorkerConfig& >( *pConfig ), pParent );
	}

	bool IsBusy ( ) override
	{
		return false;
	}

	void PushUpdate ( std::unique_ptr<CUpdateScene> pUpdateMessage ) override
	{
		vUpdates.push_back ( pUpdateMessage.release ( ) );
		m_pParentScheduler->HandleSimulationFinished ( std::make_unique<CRIRSimulationResult> ( ) );
	}

	void Reset ( ) override
	{
		bReset = true;
	}

	void Shutdown() override
	{
		
	}
	
	bool bReset = false;
	std::vector<CUpdateScene*> vUpdates;
};

struct MockAudibilityFilter : public AudibilityFilter::IAudibilityFilter
{
	enum class Behaviour
	{
		nothingAudible,
		everythingAudible,
		onlySourceID_42_audible
	};

	struct MockFilterConfig : AudibilityFilterConfig
	{
		MockFilterConfig ( ) : AudibilityFilterConfig ( "MockFilter" )
		{ }

		Behaviour behaviour = Behaviour::nothingAudible;

		VistaPropertyList Store ( ) const override
		{
			return VistaPropertyList ( );
		}

		void Load ( const VistaPropertyList& oProperties ) override
		{ }
	};

	MockAudibilityFilter ( const MockFilterConfig& pConfig )
	{
		eBehaviour = pConfig.behaviour;
	}

	static std::unique_ptr<IAudibilityFilter> createFilter ( const std::shared_ptr<IAudibilityFilter::AudibilityFilterConfig>& pConfig )
	{
		return std::make_unique<MockAudibilityFilter> ( dynamic_cast< MockFilterConfig& >( *pConfig ) );
	}

	bool ChangeIsAudible ( const CUpdateScene& previousState, const CUpdateScene& newUpdate ) const override
	{
		switch ( eBehaviour )
		{
			case Behaviour::nothingAudible:
				return false;
			case Behaviour::everythingAudible:
				return true;
			case Behaviour::onlySourceID_42_audible:
				if ( newUpdate.GetSourceReceiverPair().source->GetId() == 42 )
					return true;
				return false;
			default:
				ITA_EXCEPT_NOT_IMPLEMENTED;
		}
	}

	Behaviour eBehaviour = Behaviour::nothingAudible;
};


//----------SIMULATORS----------
//------------------------------

struct MockOutdoorAcousticSimulator : public OutdoorAcoustics::ISimulatorInterface
{
	bool bReset = false;
	bool bUpdateReceived = false;
	ITAAtomicBool bFinishSimulation = false;

	//inline static std::string GetType() { return "MockOutdoorAcousticSimulator"; };
	//inline static std::unique_ptr<ISimulatorInterface> Create(const std::shared_ptr<ISimulatorInterface::SimulatorConfig>& pConfig)
	//{
	//	return std::make_unique< MockOutdoorAcousticSimulator >( );
	//}

	inline void Reset() override
	{
		bReset = true;
	};

	inline std::unique_ptr<OutdoorAcoustics::COutdoorSimulationResult> Compute(std::unique_ptr<CUpdateScene> pSceneUpdate) override
	{
		bUpdateReceived = true;

		while (!bFinishSimulation)
		{
			VistaTimeUtils::Sleep(100);
		}

		auto oPathProperty = OutdoorAcoustics::COutdoorSimulationResult::CPathProperty();
		oPathProperty.dPropagationDelay = 42;
		oPathProperty.dSpreadingLoss = 1.0 / 42.0;
		oPathProperty.iDiffractionOrder = 42;
		oPathProperty.iReflectionOrder = 42;
		oPathProperty.v3SourceWaveFrontNormal = VistaVector3D(1, 1, 1);
		oPathProperty.v3ReceiverWaveFrontNormal = VistaVector3D(1, 1, 1);

		auto pResult = std::make_unique< OutdoorAcoustics::COutdoorSimulationResult >();
		pResult->voPathProperties.push_back(oPathProperty);
		return std::move(pResult);
	};
};


#ifdef WITH_RAVEN

struct MockRavenSimulator : public Raven::ISimulatorInterface
{
	bool bReset = false;

	bool bTaskReceived = false;

	ITAAtomicBool finishSimulation = false;

	inline void Reset() override
	{
		bReset = true;
	};

	inline void Compute ( Raven::CSimulationTask* pTask, Raven::CRavenSimulationResult* pResult ) override
	{
		bTaskReceived = true;

		while ( !finishSimulation )
		{
			VistaTimeUtils::Sleep ( 100 );
		}

		auto pComplexSoundPath = std::make_unique<Raven::CRavenSimulationResult::ComplexSimulationSoundPath> ( );
		pComplexSoundPath->bDirectSoundAudible = true;
		pComplexSoundPath->bEmpty = false;
		pComplexSoundPath->bEntitiesInSameRoom = true;
		pComplexSoundPath->bZerosStripped = false;
		pComplexSoundPath->iLeadingZeros = 10;
		pComplexSoundPath->iTailingZeros = 500;
		pComplexSoundPath->iNumChannels = 2;
		pComplexSoundPath->iReceiverID = 42;
		pComplexSoundPath->iSourceID = 42;
		pComplexSoundPath->psfResult = std::make_unique<ITASampleFrame> ( 2, 128, false );

		pResult->vcspResult.push_back ( std::move ( pComplexSoundPath ) );

		for ( auto i = 0; i < pResult->vcspResult [0]->psfResult->GetLength ( ); ++i )
		{
			pResult->vcspResult [0]->psfResult->operator[]( 0 ) [i] = sin ( 3.14 * i / 128 );
		}
	};
};

#endif