#include "mocks.hpp"

#include <ITA/SimulationScheduler/worker_interface.h>

// GTest
#include "gtest/gtest.h"


using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;

TEST( WorkerFactoryTest, addNewWorkerType )
{
	CWorkerFactory::RegisterWorker( "MockWorker", MockWorker::createWorker, std::make_shared<MockWorker::MockWorkerConfig> );

	const auto config = std::make_shared<MockWorker::MockWorkerConfig>( );

	const auto worker = CWorkerFactory::CreateWorker( config, nullptr );

	ASSERT_NE( worker, nullptr );
}