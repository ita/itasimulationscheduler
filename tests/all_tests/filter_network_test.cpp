#include "mocks.hpp"

#include <ITA/SimulationScheduler/AudibilityFilter/filter_network.h>

// GTest
#include "ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h"
#include "ITA/SimulationScheduler/AudibilityFilter/zone_filter.h"

#include "gtest/gtest.h"
#include <ostream>


using namespace ITA::SimulationScheduler;
using namespace AudibilityFilter;
using namespace std;

struct FilterNetworkTestParam
{
	bool expectedResult;

	bool filter1audible;
	bool filter2audible;
	bool filter3audible;

	bool conditionFulfilled;

	bool invertCondition;

	bool addFilterThreeInSecondBranch;


	friend std::ostream& operator<<( std::ostream& os, const FilterNetworkTestParam& obj )
	{
		return os << boolalpha << "expectedResult: " << obj.expectedResult << " filter1audible: " << obj.filter1audible << " filter2audible: " << obj.filter2audible
		          << " filter3audible: " << obj.filter3audible << " conditionFulfilled: " << obj.conditionFulfilled << " invertCondition: " << obj.invertCondition
		          << " addFilterThreeInSecondBranch: " << obj.addFilterThreeInSecondBranch;
	}
};

struct FilterNetworkTest : public testing::TestWithParam<FilterNetworkTestParam>
{
	std::unique_ptr<CFilterNetwork> filterNetwork;

	FilterNetworkTest( )
	{
		const auto param = GetParam( );

		CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter, std::make_shared<MockAudibilityFilter::MockFilterConfig> );

		const auto filter1 = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		filter1->behaviour = param.filter1audible ? MockAudibilityFilter::Behaviour::everythingAudible : MockAudibilityFilter::Behaviour::nothingAudible;
		const auto filter2 = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		filter2->behaviour = param.filter2audible ? MockAudibilityFilter::Behaviour::everythingAudible : MockAudibilityFilter::Behaviour::nothingAudible;
		const auto filter3 = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		filter3->behaviour = param.filter3audible ? MockAudibilityFilter::Behaviour::everythingAudible : MockAudibilityFilter::Behaviour::nothingAudible;

		const auto condition = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		condition->behaviour = param.conditionFulfilled ? MockAudibilityFilter::Behaviour::everythingAudible : MockAudibilityFilter::Behaviour::nothingAudible;

		filter1->sFilterName = "one";
		filter2->sFilterName = "two";
		filter3->sFilterName = "three";

		condition->sFilterName = "con";

		condition->eUsageMode       = IAudibilityFilter::UsageMode::condition;
		condition->bInvertCondition = param.invertCondition;

		filter1->sNextFilter = { "con", "two" };

		if( param.addFilterThreeInSecondBranch )
			filter2->sNextFilter = { "three" };

		condition->sNextFilter = { "three" };

		//
		//      /- con --  three
		// one -
		//      \- two -- (three)
		//

		CFilterNetwork::FilterNetworkConfig config;

		config.sStartFilter    = "one";
		config.vpFilterConfigs = { filter1, filter2, filter3, condition };

		filterNetwork = std::make_unique<CFilterNetwork>( config );
	}
};

TEST_P( FilterNetworkTest, correctResult )
{
	const auto param = GetParam( );

	const CUpdateScene update1;
	const CUpdateScene update2;

	EXPECT_EQ( param.expectedResult, filterNetwork->EvaluateNetwork( update1, update2 ) );
}

//
//                                                                                                             /- con --  three
//                                                                                                        one -
//                                                                                                             \- two -- (three)
//

//                                                                                                        result, f1   , f2   , f3   , con  , !con?, add f3
INSTANTIATE_TEST_CASE_P( FilterNetworkTest, FilterNetworkTest,
                         testing::Values( FilterNetworkTestParam { false, false, false, false, true, false, false }, // 0
                                          FilterNetworkTestParam { true, true, false, false, true, false, false },   // 1
                                          FilterNetworkTestParam { true, false, true, false, true, false, false },   // 2
                                          FilterNetworkTestParam { true, false, false, true, true, false, false },   // 3
                                          FilterNetworkTestParam { false, false, false, true, false, false, false }, // 4
                                          FilterNetworkTestParam { false, false, false, true, true, true, false },   // 5
                                          FilterNetworkTestParam { true, false, false, true, false, true, false },   // 6
                                          FilterNetworkTestParam { true, false, false, true, false, false, true },   // 7
                                          FilterNetworkTestParam { true, false, false, true, true, false, true }     // 8
                                          ) );

TEST( FilterNetworkTest, configCorrect )
{
	auto config  = std::make_shared<CRotationFilter::RotationFilterConfig>( );
	auto config1 = std::make_shared<CZoneFilter::ZoneFilterConfig>( );

	config->sNextFilter = { "One", "Two" };

	config1->vpZones = { { VistaVector3D( 1, 2, 3 ), VistaVector3D( 4, 5, 6 ) }, { VistaVector3D( 1, 2, 3 ), VistaVector3D( 4, 5, 6 ) } };

	AudibilityFilter::CFilterNetwork::FilterNetworkConfig networkConfig;

	networkConfig.vpFilterConfigs = { config, config1 };

	networkConfig.sStartFilter = "start";

	const auto configProperties = networkConfig.Store( );

	CFilterNetwork::FilterNetworkConfig networkConfig1;

	networkConfig1.Load( configProperties );

	EXPECT_EQ( networkConfig.sStartFilter, networkConfig1.sStartFilter );
	EXPECT_EQ( networkConfig.vpFilterConfigs.size( ), networkConfig1.vpFilterConfigs.size( ) );

	const auto newConfig  = dynamic_pointer_cast<CRotationFilter::RotationFilterConfig>( networkConfig1.vpFilterConfigs[0] );
	const auto newConfig1 = dynamic_pointer_cast<CZoneFilter::ZoneFilterConfig>( networkConfig1.vpFilterConfigs[1] );

	EXPECT_NE( nullptr, newConfig );
	EXPECT_NE( nullptr, newConfig1 );

	EXPECT_EQ( config->sNextFilter.size( ), newConfig->sNextFilter.size( ) );
	EXPECT_EQ( config1->vpZones.size( ), newConfig1->vpZones.size( ) );

	for( auto i = 0; i < config->sNextFilter.size( ); ++i )
	{
		EXPECT_EQ( config->sNextFilter.at( i ), newConfig->sNextFilter.at( i ) );
	}

	for( auto i = 0; i < config1->vpZones.size( ); ++i )
	{
		EXPECT_EQ( config1->vpZones.at( i ), newConfig1->vpZones.at( i ) );
	}
}