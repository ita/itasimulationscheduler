#include "gtest/gtest.h"
#include <ITA/SimulationScheduler/dummy_worker.h>
#include <chrono>

TEST( DummyWorkerTest, factoryCorrect )
{
	auto config = ITA::SimulationScheduler::CWorkerFactory::CreateConfig( "DummyWorker" );

	EXPECT_TRUE( config != nullptr );

	EXPECT_NO_THROW( std::dynamic_pointer_cast<ITA::SimulationScheduler::CDummyWorker::DummyWorkerConfig>( config )->dTime = 2 );

	auto worker = ITA::SimulationScheduler::CWorkerFactory::CreateWorker( config, nullptr );

	EXPECT_TRUE( worker != nullptr );
}

class DummyWorkerTestFixture : public ::testing::TestWithParam<double>
{
};

TEST_P( DummyWorkerTestFixture, fullParameterTest )
{
	ITA::SimulationScheduler::CDummyWorker::DummyWorkerConfig config;
	config.dTime = GetParam( );
	ITA::SimulationScheduler::CDummyWorker worker( config, nullptr );

	worker.PushUpdate( std::make_unique<ITA::SimulationScheduler::CUpdateScene>( 0 ) );

	const auto begin = std::chrono::high_resolution_clock::now( );

	while( worker.IsBusy( ) )
	{
	}

	const auto end = std::chrono::high_resolution_clock::now( );

	const auto duration = std::chrono::duration<double>( end - begin ).count( );

	EXPECT_NEAR( config.dTime, duration, config.dTime * 0.01 );
}

INSTANTIATE_TEST_CASE_P( DummyWorkerParameterFullTest, DummyWorkerTestFixture, ::testing::Range( 0.1, 2.0, 0.2 ) );