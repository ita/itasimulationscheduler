#ifdef WITH_RAVEN

#	include <iostream>
#	include <memory>
#	include <ostream>
#	include <string>
#	include <valarray>

// simulation scheduler includes
#	include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>
#	include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>
#	include <ITA/SimulationScheduler/RoomAcoustics/raven/worker_thread.h>
#	include <ITA/SimulationScheduler/Utils/utils.h>

// ITA includes
#	include "ITAHDFTSpectrum.h"

#	include <ITAAudiofileReader.h>
#	include <ITAAudiofileWriter.h>
#	include <ITAFFT.h>
#	include <ITAHDFTSpectra.h>
#	include <ITATimer.h>

// GTest
#	include "mocks.hpp"

#	include "gtest/gtest.h"
#	include <VistaTools/VistaIniFileParser.h>

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::AudibilityFilter;
using namespace ITA::SimulationScheduler::Utils;
using namespace ITA::SimulationScheduler::RoomAcoustics;
using namespace ITA::SimulationScheduler::RoomAcoustics::Raven;

struct RavenCompleteSchedulingTest
    : public testing::Test
    , public ITATimerEventHandler
{
	std::unique_ptr<CMasterSimulationController> masterController;

	std::unique_ptr<MockResultHandler> resultHandler;

	ITABase::CMultichannelFiniteImpulseResponse dsReference, erReference, ddReference;

	RavenCompleteSchedulingTest( )
	{
		auto oConfig = CMasterSimulationController::MasterSimulationControllerConfig( );

		auto schedulerConfig = std::make_shared<CScheduler::LocalSchedulerConfig>( );

		auto workerConfig                   = std::make_shared<CWorkerThread::WorkerThreadConfig>( );
		workerConfig->sRavenProjectFilePath = "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/referenceNEW.rpf";

		schedulerConfig->vpWorkerConfigs.push_back( workerConfig );

		CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter, std::make_shared<MockAudibilityFilter::MockFilterConfig> );

		auto filterConfig         = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		filterConfig->behaviour   = MockAudibilityFilter::Behaviour::everythingAudible;
		filterConfig->sFilterName = "Filter";

		CFilterNetwork::FilterNetworkConfig networkConfig;
		networkConfig.vpFilterConfigs.push_back( filterConfig );
		networkConfig.sStartFilter = filterConfig->sFilterName;

		schedulerConfig->oFilterNetworkConfig = networkConfig;

		oConfig.oDSSchedulerConfig = schedulerConfig;
		oConfig.oERSchedulerConfig = schedulerConfig;
		oConfig.oDDSchedulerConfig = schedulerConfig;

		VistaIniFileParser::WriteProplistToFile( "CompleteTest_Master_Test_Config.ini", oConfig.Store( ), true );

		ofstream myfile;
		myfile.open( "CompleteTest_Master_Test_Config.json" );
		myfile << Utils::JSONConfigUtils::WriteVistaPropertyListToJSON( oConfig.Store( ) ).dump( 4 );
		myfile.close( );

		masterController = std::make_unique<CMasterSimulationController>( oConfig );

		resultHandler = std::make_unique<MockResultHandler>( );

		masterController->AttachResultHandler( resultHandler.get( ) );

		dsReference.LoadFromFile( "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/DS_reference.wav" );
		erReference.LoadFromFile( "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/ER_reference.wav" );
		ddReference.LoadFromFile( "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/DD_reference.wav" );
	}

	void handleTimerEvent( const ITATimer& tSource ) override
	{
		auto update   = std::make_unique<CUpdateScene>( );
		auto source   = std::make_unique<C3DObject>( VistaVector3D( 1, 1.5, 1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 1 );
		auto receiver = std::make_unique<C3DObject>( VistaVector3D( -0.5, 1.5, -0.5 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 1 );

		update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

		masterController->PushUpdate( std::move( update ) );
	}
};

TEST_F( RavenCompleteSchedulingTest, DISABLED_correctResult )
{
	auto update   = std::make_unique<CUpdateScene>( );
	auto source   = std::make_unique<C3DObject>( VistaVector3D( 1, 1.5, 1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 1 );
	auto receiver = std::make_unique<C3DObject>( VistaVector3D( -0.5, 1.5, -0.5 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 1 );

	update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

	masterController->PushUpdate( std::move( update ) );

	// We need to wait a bit for the workers to start working
	VistaTimeUtils::Sleep( 500 );

	// wait till we have all results
	while( resultHandler->resultVector.size( ) != 3 )
		VistaTimeUtils::Sleep( 250 );

	for( const auto res: resultHandler->resultVector )
	{
		const auto result = dynamic_cast<const CRIRSimulationResult*>( res );

		ITABase::CMultichannelFiniteImpulseResponse reference;

		if( result->eResultType == FieldOfDuty::directSound )
			reference = dsReference;
		if( result->eResultType == FieldOfDuty::earlyReflections )
			reference = erReference;
		if( result->eResultType == FieldOfDuty::diffuseDecay )
			reference = ddReference;

		const auto ir = DataTypeUtils::Convert( result->sfResult );

		auto stdDev = CalculationUtils::CalculateStdDevOfAbsSpectralDifference( reference, { ir } );

		for( const auto& deviation: stdDev )
		{
			if( result->eResultType == FieldOfDuty::diffuseDecay )
				EXPECT_GE( 6.0, deviation ); // accepted >= deviation
			else
				EXPECT_GE( 0.001, deviation ); // accepted >= deviation
		}

		auto correlationVector = CalculationUtils::NormalizedCrossCorrelation( reference, ir );

		for( const auto& correlation: correlationVector )
		{
			if( result->eResultType == FieldOfDuty::diffuseDecay )
				EXPECT_LE( 0.1, abs( correlation ).max( ) ); // threshold <= maxCorr
			else
				EXPECT_LE( 0.9, abs( correlation ).max( ) ); // threshold <= maxCorr
		}
	}

	if( HasFailure( ) || true )
	{
		const auto test_info = testing::UnitTest::GetInstance( )->current_test_info( );

		auto path = std::string( test_info->test_case_name( ) ) + "/" + std::string( test_info->name( ) );

		if( !doesDirectoryExist( "results" ) )
			makeDirectory( "results" );

		size_t index = 0;
		while( true )
		{
			/* Locate the substring to replace. */
			index = path.find( "/", index );
			if( index == std::string::npos )
				break;

			/* Make the replacement. */
			path.replace( index, 1, "_" );

			/* Advance index forward so the next iteration doesn't pick it up as well. */
			index++;
		}

		for( const auto& res: resultHandler->resultVector )
		{
			const auto rirRes = dynamic_cast<CRIRSimulationResult*>( res );
			rirRes->sfResult.Store( "results/" + path + "-FOD_" + to_string( AsInteger( rirRes->eResultType ) ) + ".wav" );
		}
	}
}

TEST_F( RavenCompleteSchedulingTest, realTimeTest )
{
	using namespace std::placeholders;
	// processStream update rate ~300Hz
	const auto updateRate = 300.0;
	ITATimer oTimer( 1 / updateRate );

	oTimer.attach( this );

	if( !oTimer.isActive( ) )
		oTimer.start( );

	VistaTimeUtils::Sleep( 1000 );

	if( oTimer.isActive( ) )
		oTimer.stop( );

	auto prevSize = resultHandler->ddResultVector.size( );

	while( masterController->IsBusy( ) )
	{
		VistaTimeUtils::Sleep( 500 );
	}

	EXPECT_TRUE( resultHandler->resultVector.size( ) > 3 );
	EXPECT_TRUE( !resultHandler->dsResultVector.empty( ) );
	EXPECT_TRUE( !resultHandler->erResultVector.empty( ) );
	EXPECT_TRUE( !resultHandler->ddResultVector.empty( ) );
}

#endif