#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include "../src/ITA/SimulationScheduler/replacement_filter.h"

#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>
#include <ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h>
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/update_config.h>
#include <ITA/SimulationScheduler/update_message.h>
#include <ITA/SimulationScheduler/update_scene.h>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "enum_to_int.cpp"
#include "mocks.hpp"
#include "test_classes.hpp"

#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::RoomAcoustics;


struct MasterSimulationControllerTest : testing::Test
{
	std::unique_ptr<TestMasterSimulationController> controller;

	MockResultHandler* handler;

	MasterSimulationControllerTest( )
	{
		controller = std::make_unique<TestMasterSimulationController>( );

		handler = new MockResultHandler( );

		controller->AttachResultHandler( handler );
	}
};

TEST_F( MasterSimulationControllerTest, uptadeToScheduler )
{
	// std::unique_ptr<CUpdateScene> update;
	for( int i = 0; i < 50; ++i )
	{
		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		controller->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	const auto updates = controller->scheduler.at( 0 )->updates;

	EXPECT_GE( updates.size( ), 1 );
	EXPECT_LE( updates.size( ), 50 );
}

TEST_F( MasterSimulationControllerTest, attachResultHandler )
{
	for( int i = 0; i < 50; ++i )
	{
		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		controller->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	EXPECT_GE( handler->resultVector.size( ), 1 );
	EXPECT_LE( handler->resultVector.size( ), 50 );
}
