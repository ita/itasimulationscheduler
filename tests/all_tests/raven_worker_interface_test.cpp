#ifdef WITH_RAVEN

#	include "mocks.hpp"
#	include "test_classes.hpp"

#	include <ITA/SimulationScheduler/RoomAcoustics/raven/worker_thread.h>
#	include <ITA/SimulationScheduler/update_scene.h>

// GTest
#	include "gtest/gtest.h"


using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;
using namespace Raven;

struct RavenWorkerInterfaceTest : testing::Test
{
	std::unique_ptr<TestRavenWorkerInterface> interface;

	RavenWorkerInterfaceTest( )
	{
		auto config = CWorkerThread::WorkerThreadConfig( );

		interface = std::make_unique<TestRavenWorkerInterface>( config, nullptr );
	}
};

TEST_F( RavenWorkerInterfaceTest, update2Task )
{
	auto update   = std::make_unique<CUpdateScene>( );
	auto source   = std::make_unique<C3DObject>( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = std::make_unique<C3DObject>( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

	const CUpdateScene updateCopy( *update );

	const auto task = interface->createTaskFromUpdateTest( std::move( update ) );

	EXPECT_EQ( updateCopy.GetID( ), task->uiID );
	EXPECT_EQ( FieldOfDuty::directSound, task->eSimulationType );

	auto sourceFormUpdate   = updateCopy.GetSourceReceiverPair( ).source;
	auto receiverFormUpdate = updateCopy.GetSourceReceiverPair( ).receiver;

	EXPECT_EQ( sourceFormUpdate->GetPosition( ), VistaVector3D( task->oScene.GetSourceState( sourceFormUpdate->GetId( ) ).vPos.components ) );
	EXPECT_EQ( receiverFormUpdate->GetPosition( ), VistaVector3D( task->oScene.GetReceiverState( receiverFormUpdate->GetId( ) ).vPos.components ) );

	auto taskSourceOrientation = VistaQuaternion( );
	taskSourceOrientation.SetFromViewAndUpDir( VistaVector3D( task->oScene.GetSourceState( sourceFormUpdate->GetId( ) ).vView.components ),
	                                           VistaVector3D( task->oScene.GetSourceState( receiverFormUpdate->GetId( ) ).vUp.components ) );
	auto taskReceiverOrientation = VistaQuaternion( );
	taskReceiverOrientation.SetFromViewAndUpDir( VistaVector3D( task->oScene.GetReceiverState( sourceFormUpdate->GetId( ) ).vView.components ),
	                                             VistaVector3D( task->oScene.GetReceiverState( receiverFormUpdate->GetId( ) ).vUp.components ) );

	EXPECT_EQ( sourceFormUpdate->GetOrientation( ), taskSourceOrientation );
	EXPECT_EQ( receiverFormUpdate->GetOrientation( ), taskReceiverOrientation );
}

TEST_F( RavenWorkerInterfaceTest, resultConversion )
{
	auto pRavenResult                      = std::make_unique<CRavenSimulationResult>( );
	auto pComplexSoundPath                 = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
	pComplexSoundPath->bDirectSoundAudible = true;
	pComplexSoundPath->bEmpty              = false;
	pComplexSoundPath->bEntitiesInSameRoom = true;
	pComplexSoundPath->bZerosStripped      = false;
	pComplexSoundPath->iLeadingZeros       = 10;
	pComplexSoundPath->iTailingZeros       = 500;
	pComplexSoundPath->iNumChannels        = 2;
	pComplexSoundPath->iReceiverID         = 42;
	pComplexSoundPath->iSourceID           = 42;
	pComplexSoundPath->psfResult           = std::make_unique<ITASampleFrame>( 2, 128, false );

	pRavenResult->vcspResult.push_back( std::move( pComplexSoundPath ) );

	for( auto i = 0; i < pRavenResult->vcspResult[0]->psfResult->GetLength( ); ++i )
	{
		pRavenResult->vcspResult[0]->psfResult->operator[]( 0 )[i] = sin( 3.14 * i / 128 );
	}

	auto pTask             = std::make_unique<CSimulationTask>( );
	pTask->eSimulationType = FieldOfDuty::earlyReflections;

	pTask->oScene.AddSource( 0 );
	auto sourceState  = CRavenScene::CSourceState( );
	sourceState.vPos  = { 1, 2, 3 };
	sourceState.vView = { 0, 1, 0 };
	sourceState.vUp   = { 0, 0, 1 };
	pTask->oScene.SetSourceState( 0, sourceState );
	pTask->oScene.AddReceiver( 0 );
	auto receiverState  = CRavenScene::CReceiverState( );
	receiverState.vPos  = { 1, 2, 3 };
	receiverState.vView = { 0, 1, 0 };
	receiverState.vUp   = { 0, 0, 1 };
	pTask->oScene.SetReceiverState( 0, receiverState );

	// save result for comparison
	auto pRavenResultCopy = std::make_unique<CRavenSimulationResult>( *pRavenResult );

	const auto pResult = TestRavenWorkerInterface::convertSimulationResultTest( std::move( pRavenResult ), pTask.get( ) );

	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->bEntitiesInSameRoom, pResult->bSameRoom );
	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->bZerosStripped, pResult->bZerosStripped );
	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->iTailingZeros, pResult->iTrailingZeros );
	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->iLeadingZeros, pResult->iLeadingZeros );

	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->psfResult->GetLength( ), pResult->sfResult.GetLength( ) );
	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->psfResult->GetNumChannels( ), pResult->sfResult.GetNumChannels( ) );
	EXPECT_EQ( pRavenResultCopy->vcspResult[0]->iNumChannels, pResult->sfResult.GetNumChannels( ) );

	for( auto i = 0; i < pResult->sfResult.GetLength( ); ++i )
	{
		EXPECT_EQ( pRavenResultCopy->vcspResult[0]->psfResult->operator[]( 0 )[i], pResult->sfResult[0][i] );
		EXPECT_EQ( pRavenResultCopy->vcspResult[0]->psfResult->operator[]( 1 )[i], pResult->sfResult[1][i] );
	}

	EXPECT_EQ( pTask->eSimulationType, pResult->eResultType );

	const auto sourceFormResult   = pResult->sourceReceiverPair.source;
	const auto receiverFormResult = pResult->sourceReceiverPair.receiver;

	EXPECT_EQ( VistaVector3D( pTask->oScene.GetSourceState( 0 ).vPos.components ), sourceFormResult->GetPosition( ) );
	EXPECT_EQ( VistaVector3D( pTask->oScene.GetReceiverState( 0 ).vPos.components ), receiverFormResult->GetPosition( ) );

	auto taskSourceOrientation = VistaQuaternion( );
	taskSourceOrientation.SetFromViewAndUpDir( VistaVector3D( pTask->oScene.GetSourceState( sourceFormResult->GetId( ) ).vView.components ),
	                                           VistaVector3D( pTask->oScene.GetSourceState( receiverFormResult->GetId( ) ).vUp.components ) );
	auto taskReceiverOrientation = VistaQuaternion( );
	taskReceiverOrientation.SetFromViewAndUpDir( VistaVector3D( pTask->oScene.GetReceiverState( sourceFormResult->GetId( ) ).vView.components ),
	                                             VistaVector3D( pTask->oScene.GetReceiverState( receiverFormResult->GetId( ) ).vUp.components ) );

	EXPECT_EQ( taskSourceOrientation, sourceFormResult->GetOrientation( ) );
	EXPECT_EQ( taskReceiverOrientation, receiverFormResult->GetOrientation( ) );
}

TEST_F( RavenWorkerInterfaceTest, resultConversionExeptionTestWrongComplexSoundPathCount )
{
	auto pRavenResult       = std::make_unique<CRavenSimulationResult>( );
	auto pComplexSoundPath1 = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
	auto pComplexSoundPath2 = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
	pRavenResult->vcspResult.push_back( std::move( pComplexSoundPath1 ) );
	pRavenResult->vcspResult.push_back( std::move( pComplexSoundPath2 ) );

	EXPECT_ANY_THROW( TestRavenWorkerInterface::convertSimulationResultTest( std::move( pRavenResult ), nullptr ) );
}

TEST_F( RavenWorkerInterfaceTest, resultConversionExeptionTestChannelCount )
{
	auto pRavenResult               = std::make_unique<CRavenSimulationResult>( );
	auto pComplexSoundPath          = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
	pComplexSoundPath->iNumChannels = 2;
	pComplexSoundPath->psfResult    = std::make_unique<ITASampleFrame>( 3, 128, false );
	pRavenResult->vcspResult.push_back( std::move( pComplexSoundPath ) );

	EXPECT_ANY_THROW( TestRavenWorkerInterface::convertSimulationResultTest( std::move( pRavenResult ), nullptr ) );
}

TEST_F( RavenWorkerInterfaceTest, resultConversionExeptionTestEmpty )
{
	auto pRavenResult         = std::make_unique<CRavenSimulationResult>( );
	auto pComplexSoundPath    = std::make_unique<CRavenSimulationResult::ComplexSimulationSoundPath>( );
	pComplexSoundPath->bEmpty = true;
	pRavenResult->vcspResult.push_back( std::move( pComplexSoundPath ) );

	EXPECT_ANY_THROW( TestRavenWorkerInterface::convertSimulationResultTest( std::move( pRavenResult ), nullptr ) );
}

#endif