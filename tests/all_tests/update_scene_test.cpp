#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include <ITA/SimulationScheduler/update_scene.h>

// Vista includes
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "enum_to_int.cpp"

#include "gtest/gtest.h"

namespace simsched = ITA::SimulationScheduler;

struct UpdateSceneTest : testing::Test
{
	std::unique_ptr<simsched::CUpdateScene> update;
	std::unique_ptr<simsched::C3DObject> source;
	std::unique_ptr<simsched::C3DObject> receiver;
	simsched::C3DObject* raw_source;
	simsched::C3DObject* raw_receiver;

	UpdateSceneTest( )
	{
		update   = std::make_unique<simsched::CUpdateScene>( );
		source   = std::make_unique<simsched::C3DObject>( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), simsched::C3DObject::Type::source, 1 );
		receiver = std::make_unique<simsched::C3DObject>( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), simsched::C3DObject::Type::receiver, 1 );

		// save them for later
		raw_source   = source.get( );
		raw_receiver = receiver.get( );

		update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );
	}
};

TEST_F( UpdateSceneTest, getter )
{
	EXPECT_GE( update->GetID( ), 0 );

	auto pair = update->GetSourceReceiverPair( );
	EXPECT_EQ( raw_source, pair.source );
	EXPECT_EQ( raw_receiver, pair.receiver );
}

TEST_F( UpdateSceneTest, operatorEqual )
{
	auto alt1 = simsched::CUpdateScene( );
	alt1.SetSourceReceiverPair( std::make_unique<simsched::C3DObject>( *raw_source ), std::make_unique<simsched::C3DObject>( *raw_receiver ) );

	EXPECT_FALSE( *update == alt1 );
	EXPECT_TRUE( *update == *update );
}

TEST_F( UpdateSceneTest, operatorEqualTol )
{
	auto alt1      = simsched::CUpdateScene( );
	auto altSource = simsched::C3DObject( { 3.14f, 2.5f, 6 }, { 1, 0.05f, 0, 0.1f }, simsched::C3DObject::Type::source, 1 );
	alt1.SetSourceReceiverPair( std::make_unique<simsched::C3DObject>( altSource ), std::make_unique<simsched::C3DObject>( *raw_receiver ) );

	EXPECT_TRUE( update->IsEqualTolerance( alt1, 0.5 ) );
}

TEST_F( UpdateSceneTest, serialization )
{
	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	serializer.WriteSerializable( *update.get( ) );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	auto newUpdate = simsched::CUpdateScene( );

	deserializer.ReadSerializable( newUpdate );

	EXPECT_EQ( *update, newUpdate );
}

TEST_F( UpdateSceneTest, copyConstructor )
{
	auto copy = std::make_unique<simsched::CUpdateScene>( *update );

	EXPECT_EQ( *update, *copy );
}


TEST_F( UpdateSceneTest, relationalOperators )
{
	const auto laterUpdate = std::make_unique<simsched::CUpdateScene>( );

	EXPECT_TRUE( *update < *laterUpdate );
	EXPECT_FALSE( *update > *laterUpdate );
}