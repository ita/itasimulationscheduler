#ifdef WITH_RAVEN

#	include "mocks.hpp"
#	include "test_classes.hpp"

#	include <ITA/SimulationScheduler/RoomAcoustics/raven/worker_thread.h>
#	include <ITA/SimulationScheduler/worker_interface.h>

// GTest
#	include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;
using namespace Raven;

struct RavenWorkerThreadTest : testing::Test
{
	std::unique_ptr<TestRavenWorkerThread> worker;

	std::unique_ptr<MockScheduler> scheduler;

	RavenWorkerThreadTest( )
	{
		// overwrite the correct create function.
		CWorkerFactory::RegisterWorker( CWorkerThread::GetType( ), TestRavenWorkerThread::createWorker, std::make_shared<TestRavenWorkerThread::WorkerThreadConfig> );

		scheduler = std::make_unique<MockScheduler>( );

		auto oConfig                   = std::make_shared<CWorkerThread::WorkerThreadConfig>( );
		oConfig->sRavenProjectFilePath = "";

		worker = std::unique_ptr<TestRavenWorkerThread>( dynamic_cast<TestRavenWorkerThread*>( CWorkerFactory::CreateWorker( oConfig, scheduler.get( ) ).release( ) ) );
	}

	~RavenWorkerThreadTest( )
	{
		// Reset the create function
		CWorkerFactory::RegisterWorker( Raven::CWorkerThread::GetType( ), CWorkerThread::CreateWorker, std::make_shared<CWorkerThread::WorkerThreadConfig> );
	}
};

TEST_F( RavenWorkerThreadTest, updateToSimulatorAndResultToScheduler )
{
	auto update   = std::make_unique<CUpdateScene>( );
	auto source   = std::make_unique<C3DObject>( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = std::make_unique<C3DObject>( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	update->SetSourceReceiverPair( std::move( source ), std::move( receiver ) );

	worker->PushUpdate( std::move( update ) );

	// wait for the thread
	VistaTimeUtils::Sleep( 100 );

	EXPECT_TRUE( worker->simulator->bTaskReceived );

	EXPECT_TRUE( worker->IsBusy( ) );

	worker->simulator->finishSimulation = true;

	// wait for the thread
	VistaTimeUtils::Sleep( 500 );

	EXPECT_EQ( 1, scheduler->results.size( ) );
}

TEST_F( RavenWorkerThreadTest, reset )
{
	worker->Reset( );

	EXPECT_TRUE( worker->simulator->bReset );
}

#endif