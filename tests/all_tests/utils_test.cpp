#include <array>
#include <iostream>
#include <memory>
#include <ostream>
#include <string>
#include <valarray>

// simulation scheduler includes
#include <ITA/SimulationScheduler/utils/utils.h>
// ITA includes


// GTest
#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler::Utils;

TEST( Statistics, mean )
{
	EXPECT_NEAR( 5, Statistics::CalculateMean( { 5, 5, 5, 5, 5, 5 } ), 0.01 );
	EXPECT_NEAR( 5.5, Statistics::CalculateMean( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } ), 0.01 );
	EXPECT_NEAR( 34.3088, Statistics::CalculateMean( { 80, 5, 23, 3.14f, 60.404f } ), 0.01 );
	EXPECT_NEAR( 0.5614, Statistics::CalculateMean( { 0.4387f, 0.3816f, 0.7655f, 0.7952f, 0.1869f, 0.4898f, 0.4456f, 0.6463f, 0.7094f, 0.7547f } ), 0.01 );
}

TEST( Statistics, varianz )
{
	EXPECT_NEAR( 0, Statistics::CalculateVariance( { 5, 5, 5, 5, 5, 5 } ), 0.01 );
	EXPECT_NEAR( 9.1667, Statistics::CalculateVariance( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } ), 0.01 );
	EXPECT_NEAR( 1181.7585072, Statistics::CalculateVariance( { 80, 5, 23, 3.14f, 60.404f } ), 0.01 );
	EXPECT_NEAR( 0.0410, Statistics::CalculateVariance( { 0.4387f, 0.3816f, 0.7655f, 0.7952f, 0.1869f, 0.4898f, 0.4456f, 0.6463f, 0.7094f, 0.7547f } ), 0.01 );
}

TEST( Statistics, stdDev )
{
	EXPECT_NEAR( 0, Statistics::CalculateStandardDeviation( { 5, 5, 5, 5, 5, 5 } ), 0.01 );
	EXPECT_NEAR( 3.0277, Statistics::CalculateStandardDeviation( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } ), 0.01 );
	EXPECT_NEAR( 34.3767, Statistics::CalculateStandardDeviation( { 80, 5, 23, 3.14f, 60.404f } ), 0.01 );
	EXPECT_NEAR( 0.2025, Statistics::CalculateStandardDeviation( { 0.4387f, 0.3816f, 0.7655f, 0.7952f, 0.1869f, 0.4898f, 0.4456f, 0.6463f, 0.7094f, 0.7547f } ), 0.01 );
}

TEST( DataTypeUtils, sampleBuffer )
{
	std::valarray<float> values( { 5, 6, 7, 8, 9, 42 } );
	ITASampleBuffer buffer( values.size( ), true );
	buffer.WriteSamples( &values[0], values.size( ) );

	const auto retRes = DataTypeUtils::ConvertToValueArray( buffer );

	const auto equal = values == retRes;

	EXPECT_EQ( true, equal.min( ) );
}

TEST( DataTypeUtils, sampleFrame )
{
	std::valarray<float> values( { 5, 6, 7, 8, 9, 42 } );
	ITASampleFrame frame( 1, values.size( ), true );
	frame[0].WriteSamples( &values[0], values.size( ) );

	const auto equal = values == DataTypeUtils::ConvertToValueArray( frame )[0];

	EXPECT_EQ( true, equal.min( ) );
}

TEST( DataTypeUtils, spectrumComplex )
{
	std::valarray<std::complex<float>> values { { 5.0f, 3.14f }, { 6.f, 5.f }, { 7.f, 2.f }, { 8.f, 7.f }, { 9.f, 6.f }, { 42.f, 42.f } };
	ITABase::CHDFTSpectrum spectrum( 44100, values.size( ) * 2, true );

	auto i = 0;

	for( auto val: values )
	{
		spectrum.SetCoeffRI( i, val.real( ), val.imag( ) );
		++i;
	}

	const auto equal = values == DataTypeUtils::ConvertToComplexValueArray( spectrum );

	EXPECT_EQ( true, equal.min( ) );
}

TEST( DataTypeUtils, spectrum )
{
	std::valarray<std::complex<float>> values { { 5.0f, 3.14f }, { 6.f, 5.f }, { 7.f, 2.f }, { 8.f, 7.f }, { 9.f, 6.f }, { 42.f, 42.f } };
	std::valarray<float> absValues( values.size( ) );
	ITABase::CHDFTSpectrum spectrum( 44100, values.size( ) * 2, true );

	auto i = 0;

	for( auto val: values )
	{
		absValues[i] = abs( val );
		spectrum.SetCoeffRI( i, val.real( ), val.imag( ) );
		++i;
	}

	const auto equal = absValues == DataTypeUtils::ConvertToValueArray( spectrum );

	EXPECT_EQ( true, equal.min( ) );
}

TEST( DataTypeUtils, spectra )
{
	std::valarray<std::complex<float>> values { { 5.0f, 3.14f }, { 6.f, 5.f }, { 7.f, 2.f }, { 8.f, 7.f }, { 9.f, 6.f }, { 42.f, 42.f } };
	std::valarray<float> absValues( values.size( ) );
	ITABase::CHDFTSpectra spectra( 44100, 1, values.size( ) * 2, true );

	auto i = 0;

	for( auto val: values )
	{
		absValues[i] = abs( val );
		spectra[0]->SetCoeffRI( i, val.real( ), val.imag( ) );
		++i;
	}

	const auto equal = absValues == DataTypeUtils::ConvertToValueArray( spectra )[0];

	EXPECT_EQ( true, equal.min( ) );
}

TEST( DataTypeUtils, buffer2ir )
{
	std::array<float, 5> data { 1, 2, 3, 4, 5 };

	ITASampleBuffer buffer( data.size( ) );

	buffer.WriteSamples( data.data( ), 5 );

	auto ir = DataTypeUtils::Convert( buffer );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], ir.GetData( )[i] );
	}
}

TEST( DataTypeUtils, ir2buffer )
{
	std::array<float, 5> data { 1, 2, 3, 4, 5 };

	ITABase::CFiniteImpulseResponse ir( data.size( ), 44100 );

	ir.WriteSamples( data.data( ), 5 );

	auto buffer = DataTypeUtils::Convert( ir );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], buffer.GetData( )[i] );
	}
}

TEST( DataTypeUtils, frame2irs )
{
	std::array<float, 5> data { 1, 2, 3, 4, 5 };

	ITASampleFrame frame( 1, data.size( ) );

	frame[0].WriteSamples( data.data( ), 5 );

	auto irs = DataTypeUtils::Convert( frame );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], irs[0].GetData( )[i] );
	}
}

TEST( DataTypeUtils, irs2frame )
{
	std::array<float, 5> data { 1, 2, 3, 4, 5 };

	ITABase::CMultichannelFiniteImpulseResponse irs( 1, data.size( ), 44100 );

	irs[0].WriteSamples( data.data( ), 5 );

	auto frame = DataTypeUtils::Convert( irs );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], frame[0].GetData( )[i] );
	}
}

TEST( FFTUtils, fftSingle )
{
	std::array<float, 8> data { 1, 2, 3, 4, 5, 6, 7, 8 };
	std::valarray<std::complex<float>> expectedResult { { 36.f, 0.f }, { -4.f, 9.6569f }, { -4.f, 4.f }, { -4.f, 1.6569f }, { -4.f, 0.f } };

	ITABase::CFiniteImpulseResponse ir( data.size( ), 44100 );

	ir.WriteSamples( data.data( ), data.size( ) );

	const auto tmp = FFTUtils::Transform( ir ).release( );

	const auto spectrum = DataTypeUtils::ConvertToComplexValueArray( *tmp );

	for( auto i = 0; i < spectrum.size( ); ++i )
	{
		EXPECT_NEAR( spectrum[i].real( ), expectedResult[i].real( ), 0.01 );
		EXPECT_NEAR( spectrum[i].imag( ), expectedResult[i].imag( ), 0.01 );
	}
}

TEST( FFTUtils, ifftSingle )
{
	std::valarray<std::complex<float>> data { { 36.f, 0.f }, { -4.f, 9.6569f }, { -4.f, 4.f }, { -4.f, 1.6569f }, { -4.f, 0.f } };
	std::valarray<float> expectedResult { 1, 2, 3, 4, 5, 6, 7, 8 };

	ITABase::CHDFTSpectrum spectrum( 44100, expectedResult.size( ), true );

	for( auto i = 0; i < spectrum.GetSize( ); ++i )
		spectrum.SetCoeffRI( i, data[i].real( ), data[i].imag( ) );

	auto res = FFTUtils::Transform( spectrum );

	const auto tmp = DataTypeUtils::Convert( res );

	const auto ir = DataTypeUtils::ConvertToValueArray( tmp );

	for( auto i = 0; i < ir.size( ); ++i )
	{
		EXPECT_NEAR( ir[i], expectedResult[i], 0.01 );
	}
}

TEST( FFTUtils, fftAndifft )
{
	std::array<float, 8> data { 1, 2, 3, 4, 5, 6, 7, 8 };

	ITABase::CFiniteImpulseResponse ir( data.size( ), 44100 );

	ir.WriteSamples( data.data( ), data.size( ) );

	const auto spectrum = FFTUtils::Transform( ir ).release( );

	const auto result = FFTUtils::Transform( *spectrum );

	const auto spectrumVal = DataTypeUtils::ConvertToValueArray( result );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], spectrumVal[i] );
	}
}

TEST( FFTUtils, fftAndifftMulti )
{
	std::array<float, 8> data { 1, 2, 3, 4, 5, 6, 7, 8 };

	ITABase::CMultichannelFiniteImpulseResponse irs( 1, data.size( ), 44100, true );

	irs[0].WriteSamples( data.data( ), data.size( ) );

	const auto spectra = FFTUtils::Transform( irs ).release( );

	const auto result = FFTUtils::Transform( *spectra );

	const auto spectrumVal = DataTypeUtils::ConvertToValueArray( result );

	for( auto i = 0; i < data.size( ); ++i )
	{
		EXPECT_EQ( data[i], spectrumVal[0][i] );
	}
}

TEST( CalculationUtils, mean )
{
	ITABase::CMultichannelFiniteImpulseResponse irs( 2, 8, 44100, true );

	for( auto i = 0; i < irs.GetLength( ); ++i )
	{
		irs[0][i] = i + 1;
		irs[1][i] = i + 1;
	}

	auto resultOne = CalculationUtils::Mean( { irs } );

	for( auto i = 0; i < irs.GetLength( ); ++i )
	{
		EXPECT_EQ( irs[0][i], resultOne[0][i] );
	}

	auto resultTwo = CalculationUtils::Mean( { irs, irs } );

	for( auto i = 0; i < irs.GetNumChannels( ); ++i )
	{
		for( auto j = 0; j < irs.GetLength( ); ++j )
		{
			EXPECT_EQ( irs[0][j], resultTwo[i][j] );
		}
	}

	std::vector<ITABase::CMultichannelFiniteImpulseResponse> vec;

	vec.emplace_back( 2, 8, 44100 );
	vec.emplace_back( 3, 8, 44100 );

	EXPECT_THROW( CalculationUtils::Mean( vec ), ITAException );
}

TEST( CalculationUtils, FreqDomainDiv )
{
	const auto n                                      = 8;
	std::array<float, n> firstData                    = { 1, 2, 3, 4, 5, 6, 7, 8 };
	std::array<float, n> secondData                   = { 0.625f, 0.125f, -0.125f, 0.125f, 0.125f, 0.125f, -0.125f, 0.125f };
	std::valarray<std::complex<float>> expectedResult = { { 36.f, 0.0000f }, { -8.f, 19.3137f }, { -4.0000f, 4.0000f }, { -8.0000f, 3.3137f }, { 0.0000f, +0.0000f } };

	ITABase::CMultichannelFiniteImpulseResponse first( 1, n, 44100, true );
	ITABase::CMultichannelFiniteImpulseResponse second( 1, n, 44100, true );

	first[0].WriteSamples( firstData.data( ), n );
	second[0].WriteSamples( secondData.data( ), n );

	auto resultOne = *( first / first );

	auto expected = std::complex<float>( 1, 0 );

	for( auto i = 0; i < resultOne[0]->GetSize( ); ++i )
	{
		EXPECT_EQ( expected, resultOne[0]->GetCoeff( i ) );
	}

	auto resultTwo = *( first / second );

	for( auto i = 0; i < resultTwo[0]->GetSize( ); ++i )
	{
		EXPECT_NEAR( expectedResult[i].real( ), resultTwo[0]->GetCoeff( i ).real( ), 0.01 );
		EXPECT_NEAR( expectedResult[i].imag( ), resultTwo[0]->GetCoeff( i ).imag( ), 0.01 );
	}
}

TEST( CalculationUtils, StdDevSpectralDiff )
{
	std::array<float, 8> referenceData = { 0.8147f, 0.9058f, 0.1270f, 0.9134f, 0.6324f, 0.0975f, 0.2785f, 0.5469f };
	std::array<float, 8> resultData    = { 0.9575, 0.9649f, 0.1576f, 0.9706f, 0.9572f, 0.4854f, 0.8003f, 0.1419f };

	ITABase::CMultichannelFiniteImpulseResponse reference( 1, referenceData.size( ), 44100, true );
	ITABase::CMultichannelFiniteImpulseResponse result( 1, resultData.size( ), 44100, true );

	reference[0].WriteSamples( referenceData.data( ), referenceData.size( ) );
	result[0].WriteSamples( resultData.data( ), resultData.size( ) );

	auto stdDev = CalculationUtils::CalculateStdDevOfAbsSpectralDifference( reference, { result, result } );

	EXPECT_NEAR( 0.7193, stdDev.at( 0 ), 0.01 );
}

TEST( CalculationUtils, vectorElemWisePlus )
{
	std::vector<int> one = { 1, 2, 3 };

	auto result = one + one;

	for( auto refIdx: Enumerate( result ) )
	{
		EXPECT_EQ( refIdx.first, 2 * one.at( refIdx.second ) );
	}
}

TEST( DataTypeUtils, makeSameLength )
{
	const auto n                   = 8;
	std::array<float, n> firstData = { 1, 2, 3, 4, 0, 0, 0, 0 };

	ITABase::CMultichannelFiniteImpulseResponse first( 1, n, 44100, true );
	ITABase::CMultichannelFiniteImpulseResponse second( 1, n / 2, 44100, true );

	first[0].WriteSamples( firstData.data( ), first.GetLength( ) );
	second[0].WriteSamples( firstData.data( ), second.GetLength( ) );

	DataTypeUtils::MakeSameLength( first, second );

	EXPECT_EQ( first.GetLength( ), second.GetLength( ) );

	for( auto i = 0; i < n; ++i )
	{
		EXPECT_EQ( second[0][i], firstData[i] );
	}
}

TEST( CalculationUtils, normalize )
{
	const auto n                  = 8;
	std::array<float, n> data     = { -2, 0, 0, 1, 0, 0.5, 0, 0 };
	std::array<float, n> expected = { -0.99f, 0, 0, 0.495, 0, 0.2475, 0, 0 };

	ITABase::CMultichannelFiniteImpulseResponse irs( 1, n, 44100, true );

	irs[0].WriteSamples( data.data( ), irs.GetLength( ) );

	CalculationUtils::Normalize( irs );

	for( auto i = 0; i < n; ++i )
		EXPECT_NEAR( expected[i], irs[0][i], 0.001 );
}

TEST( CalculationUtils, crossCorrelation )
{
	const auto n                  = 8;
	std::array<float, n> data1    = { 1, 2, 3, 4, 5, 6, 7, 8 };
	std::array<float, n> data2    = { 8, 7, 6, 5, 4, 3, 2, 1 };
	std::valarray<float> expected = { 0.00490196078431373f, 0.0196078431372550f, 0.0490196078431373f, 0.0980392156862745f, 0.171568627450980f,
		                              0.274509803921569f,   0.411764705882353f,  0.588235294117647f,  0.720588235294118f,  0.803921568627451f,
		                              0.833333333333333f,   0.803921568627451f,  0.710784313725490f,  0.549019607843137f,  0.313725490196078f };

	auto x = ITABase::CFiniteImpulseResponse( n, 44100 );
	x.WriteSamples( data1.data( ), n );
	auto y = ITABase::CFiniteImpulseResponse( n, 44100 );
	y.WriteSamples( data2.data( ), n );

	auto result = CalculationUtils::NormalizedCrossCorrelation( x, y );

	EXPECT_EQ( expected.size( ), result.size( ) );

	for( auto i = 0; i < result.size( ); ++i )
	{
		EXPECT_NEAR( expected[i], result[i], 0.0001 );
	}
}

TEST( CalculationUtils, crossCorrelation1 )
{
	const auto n                   = 8;
	std::array<float, n> data1     = { 0, 1, 2, 1, 0, 0, 0, 0 };
	std::array<float, n> data2     = { 0, 0, 0, 2, 2, 2, 0, 0 };
	std::valarray<double> expected = { 0, 0, 0, 0.2357, 0.7071, 0.9428, 0.7071, 0.2357, 0, 0, 0, 0, 0, 0, 0 };

	auto x = ITABase::CFiniteImpulseResponse( n, 44100 );
	x.WriteSamples( data1.data( ), n );
	auto y = ITABase::CFiniteImpulseResponse( n, 44100 );
	y.WriteSamples( data2.data( ), n );

	auto result = CalculationUtils::NormalizedCrossCorrelation( x, y );

	EXPECT_EQ( expected.size( ), result.size( ) );

	for( auto i = 0; i < result.size( ); ++i )
	{
		EXPECT_NEAR( expected[i], result[i], 0.0001 );
	}
}

TEST( CalculationUtils, crossCorrelation2 )
{
	std::array<float, 8> data1     = { 0, 1, 2, 1, 0, 0, 0, 0 };
	std::array<float, 8> data2     = { 0, 0, 0, 2, 2, 2, 0, 0 };
	std::valarray<double> expected = { 0, 0, 0, 0.2357, 0.7071, 0.9428, 0.7071, 0.2357, 0, 0, 0, 0, 0, 0, 0 };

	ITABase::CMultichannelFiniteImpulseResponse s1( 1, 8, 44100 );
	ITABase::CMultichannelFiniteImpulseResponse s2( 1, 8, 44100 );

	s1[0].WriteSamples( data1.data( ), 8 );
	s2[0].WriteSamples( data2.data( ), 8 );

	auto result = CalculationUtils::NormalizedCrossCorrelation( s1, s2 );

	EXPECT_EQ( expected.size( ), result[0].size( ) );

	for( auto i = 0; i < result.size( ); ++i )
	{
		EXPECT_NEAR( expected[i], result[0][i], 0.0001 );
	}
}

TEST( CalculationUtils, crossCorrelation3 )
{
	std::valarray<double> data1    = { 0, 1, 2, 1, 0, 0, 0, 0 };
	std::valarray<double> data2    = { 0, 0, 0, 2, 2, 2, 0, 0 };
	std::valarray<double> expected = { 0, 0, 0, 0.2357, 0.7071, 0.9428, 0.7071, 0.2357, 0, 0, 0, 0, 0, 0, 0 };

	auto result = CalculationUtils::NormalizedCrossCorrelation( data1, data2 );

	EXPECT_EQ( expected.size( ), result.size( ) );

	for( auto i = 0; i < result.size( ); ++i )
	{
		EXPECT_NEAR( expected[i], result[i], 0.0001 );
	}
}

TEST( FFTUtils, fftifftNoLib )
{
	std::valarray<double> data                   = { 0, 1, 2, 1 };
	std::valarray<std::complex<double>> expected = { { 4.0000, 0.0000 }, { 0.0000, -3.4142 }, { -2.0000, 0.0000 }, { 0.0000, 0.5858 },
		                                             { 0.0000, 0.0000 }, { 0.0000, -0.5858 }, { -2.0000, 0.0000 }, { 0.0000, 3.41420 } };
	std::valarray<double> expected1              = { 0, 1, 2, 1, 0, 0, 0, 0 };

	const auto resultFFT = FFTUtils::FFT( data, 8 );

	EXPECT_EQ( expected.size( ), resultFFT.size( ) );

	for( auto i = 0; i < resultFFT.size( ); ++i )
	{
		EXPECT_NEAR( expected[i].real( ), resultFFT[i].real( ), 0.0001 );
		EXPECT_NEAR( expected[i].imag( ), resultFFT[i].imag( ), 0.0001 );
	}

	const auto resultIFFT = FFTUtils::IFFT( resultFFT );

	EXPECT_EQ( expected1.size( ), resultIFFT.size( ) );

	for( auto i = 0; i < resultFFT.size( ); ++i )
	{
		EXPECT_NEAR( expected1[i], resultIFFT[i], 0.0001 );
	}
};

TEST( DataTypeUtils, float2double )
{
	const std::valarray<float> in      = { ITAConstants::PI_F, 1 / 3.0f };
	const std::valarray<double> expect = { ITAConstants::PI_F, 1 / 3.0f };

	auto result = DataTypeUtils::Float2double( in );

	for( auto i = 0; i < in.size( ); ++i )
	{
		EXPECT_NEAR( expect[i], result[i], 2e-24 );
	}
}

TEST( CalculationUtils, angleCalculations )
{
	std::vector<double> expectedAngles = { 0.0, 45.0, 90.0, 3.14, 42.0 };

	for( auto i = 0; i < 360; i += 10 )
	{
		const auto expectedAngleRad = i * ITAConstants::PI_D / 180 - ITAConstants::PI_D / 2;

		const auto angle = CalculationUtils::CalculateAzimuthOnTargetInDegree(
		    VistaVector3D( 0, 0, 0 ), VistaQuaternion( VistaAxisAndAngle( VistaVector3D( 0, 1, 0 ), expectedAngleRad ) ), VistaVector3D( 1, 0, 0 ) );
		EXPECT_NEAR( i, angle, 0.001 );
	}

	for( auto i = -90; i <= 90; i += 10 )
	{
		const auto expectedAngleRad = i * ITAConstants::PI_D / 180;

		const auto angle = CalculationUtils::CalculateElevationOnTargetInDegree(
		    VistaVector3D( 0, 0, 0 ), VistaQuaternion( VistaAxisAndAngle( VistaVector3D( 0, 0, -1 ), expectedAngleRad ) ), VistaVector3D( 1, 0, 0 ) );
		EXPECT_NEAR( i, angle, 0.02 );
	}

	for( auto i = 0; i < 10; i += 2 )
	{
		const auto az = CalculationUtils::CalculateAzimuthOnTargetInDegree( VistaVector3D( 0, 0, 0 ), VistaQuaternion( ), VistaVector3D( i, 0, -1 ) );
		const auto el = CalculationUtils::CalculateElevationOnTargetInDegree( VistaVector3D( 0, 0, 0 ), VistaQuaternion( ), VistaVector3D( 1, i, 0 ) );

		const auto expectedAngle = atan2( i, 1 ) * 180 / ITAConstants::PI_D;

		EXPECT_NEAR( expectedAngle, az, 0.01 );
		EXPECT_NEAR( expectedAngle, el, 0.01 );
	}
}

TEST( DataTypeUtils, VistaPropertyListJSONParser )
{
	VistaPropertyList oProp { };
	VistaPropertyList oPropSub { };
	const VistaVector3D vector( 1, 2, 3 );
	oProp.SetValue( "Vector", vector );
	oProp.SetValue( "Bool", false );
	oPropSub.SetValue( "String", "Test" );
	oPropSub.SetValue( "Int", 42 );
	oPropSub.SetValue( "double", 3.14 );
	oProp.SetValue( "PropList", oPropSub );

	const auto jnProps = JSONConfigUtils::WriteVistaPropertyListToJSON( oProp );

	const auto res = nlohmann::json::parse( jnProps.dump( ) );

	const auto resProp = JSONConfigUtils::ReadVistaPropertyListFromJSON( res );

	EXPECT_EQ( oProp, resProp );
}

TEST( DataTypeUtils, convertByteArray )
{
	const double dVal = rand( );

	const auto dRetVal = DataTypeUtils::ConvertFormByteArray<double>( DataTypeUtils::ConvertToByteArray( dVal ) );

	EXPECT_EQ( dVal, dRetVal );

	const int iVal = rand( );

	const auto iRetVal = DataTypeUtils::ConvertFormByteArray<int>( DataTypeUtils::ConvertToByteArray( iVal ) );

	EXPECT_EQ( iVal, iRetVal );
}

TEST( DataTypeUtils, convertByteArrayVectors )
{
	std::vector<double> vecOfRandomDoubles( 10 );
	std::generate( vecOfRandomDoubles.begin( ), vecOfRandomDoubles.end( ), []( ) { return rand( ); } );

	const auto dRetVal = DataTypeUtils::ConvertFormByteArray<double>( DataTypeUtils::ConvertToByteArray( vecOfRandomDoubles ) );

	EXPECT_EQ( vecOfRandomDoubles, dRetVal );

	std::vector<int> vecOfRandomIntegers( 10 );
	std::generate( vecOfRandomIntegers.begin( ), vecOfRandomIntegers.end( ), []( ) { return rand( ) % 100; } );

	const auto iRetVal = DataTypeUtils::ConvertFormByteArray<int>( DataTypeUtils::ConvertToByteArray( vecOfRandomIntegers ) );

	EXPECT_EQ( vecOfRandomIntegers, iRetVal );
}

TEST( DataTypeUtils, convertBase64 )
{
	std::vector<double> vecOfRandomDoubles( 10 );
	std::generate( vecOfRandomDoubles.begin( ), vecOfRandomDoubles.end( ), []( ) { return rand( ); } );

	const auto dRetVal = DataTypeUtils::ConvertFromBase64<double>( DataTypeUtils::ConvertToBase64( vecOfRandomDoubles ) );

	EXPECT_EQ( vecOfRandomDoubles, dRetVal );

	std::vector<int> vecOfRandomIntegers( 10 );
	std::generate( vecOfRandomIntegers.begin( ), vecOfRandomIntegers.end( ), []( ) { return rand( ) % 100; } );

	const auto iRetVal = DataTypeUtils::ConvertFromBase64<int>( DataTypeUtils::ConvertToBase64( vecOfRandomIntegers ) );

	EXPECT_EQ( vecOfRandomIntegers, iRetVal );
}

TEST( JSONConfigUtils, replaceVariables )
{
	nlohmann::json jnRoot;
	jnRoot["WeirdName"]["VariableName"] = 5;
	jnRoot["WeirdName"]["Data"]         = { "One", "One" };
	jnRoot["Conf"]["Some Name"]         = "${Data}";
	jnRoot["Conf"]["Some Var Name"]     = "${VariableName}";

	JSONConfigUtils::ReplaceVariables( jnRoot, "WeirdName" );

	nlohmann::json jnResultRoot;
	jnResultRoot["Conf"]["Some Name"]     = { "One", "One" };
	jnResultRoot["Conf"]["Some Var Name"] = 5;

	EXPECT_EQ( jnRoot, jnResultRoot );
}
