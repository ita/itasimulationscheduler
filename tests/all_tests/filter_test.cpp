#include "ITA/SimulationScheduler/AudibilityFilter/zone_filter.h"

#include <ITA/SimulationScheduler/AudibilityFilter/distance_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/perceptive_rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rate_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/rotation_filter.h>
#include <ITA/SimulationScheduler/AudibilityFilter/translation_filter.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <VistaBase/VistaTimeUtils.h>
#include <array>

// GTest
#include "ITAConstants.h"

#include "gtest/gtest.h"
#include <ITA/SimulationScheduler/Utils/utils.h>
#include <ostream>

using namespace ITA::SimulationScheduler;
using namespace AudibilityFilter;

TEST( RateFilterTest, filterCorrect )
{
	std::array<CUpdateScene, 5> updates;

	for( auto& u: updates )
	{
		u = CUpdateScene( );
		VistaTimeUtils::Sleep( 250 );
	}

	const auto oConfig = CRateFilter::RateFilterConfig( ); // Rate = 1Hz

	CRateFilter rateFilter( oConfig );

	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[0], updates[1] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[0], updates[2] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[0], updates[3] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[1], updates[2] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[1], updates[3] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[1], updates[4] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[2], updates[3] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[2], updates[4] ) );
	EXPECT_FALSE( rateFilter.ChangeIsAudible( updates[3], updates[4] ) );

	EXPECT_TRUE( rateFilter.ChangeIsAudible( updates[0], updates[4] ) );
}

TEST( ZoneFilter, ConfigTest )
{
	std::vector<VistaVector3D> vectors( { VistaVector3D( 1, 1, 1 ), VistaVector3D( 2, 2, 2 ), VistaVector3D( 3, 3, 3 ), VistaVector3D( 4, 4, 4 ) } );
	auto conf    = AudibilityFilter::CZoneFilter::ZoneFilterConfig( );
	conf.vpZones = { { vectors[0], vectors[1] }, { vectors[2], vectors[3] } };

	auto zones = conf.vpZones;

	auto counter = 0;
	for( auto& z: zones )
	{
		EXPECT_EQ( vectors[counter], z.first );
		EXPECT_EQ( vectors[counter + 1], z.second );
		counter += 2;
	}
}

TEST( ZoneFilter, correctResult )
{
	auto conf    = AudibilityFilter::CZoneFilter::ZoneFilterConfig( );
	conf.vpZones = { { VistaVector3D( 0, 0, 0 ), VistaVector3D( 1, 1, 1 ) }, { VistaVector3D( 0, 0, 0 ), VistaVector3D( -1, -1, -1 ) } };

	CZoneFilter filter( conf );

	CUpdateScene update;

	auto sourceInside    = C3DObject( VistaVector3D( 0.5f, 0.5f, 0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto sourceOutside   = C3DObject( VistaVector3D( 1.5f, 5.5f, 4 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto receiverInside  = C3DObject( VistaVector3D( -0.5f, -0.5f, -0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiverOutside = C3DObject( VistaVector3D( -4, -2, -1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );

	update.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceInside ), std::make_unique<C3DObject>( receiverInside ) );
	EXPECT_TRUE( filter.ChangeIsAudible( update, update ) );
	update.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceInside ), std::make_unique<C3DObject>( receiverOutside ) );
	EXPECT_TRUE( filter.ChangeIsAudible( update, update ) );
	update.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceOutside ), std::make_unique<C3DObject>( receiverInside ) );
	EXPECT_TRUE( filter.ChangeIsAudible( update, update ) );
	update.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceOutside ), std::make_unique<C3DObject>( receiverOutside ) );
	EXPECT_FALSE( filter.ChangeIsAudible( update, update ) );
}

TEST( TranslationFilter, correctResultAbsolut )
{
	auto conf       = CTranslationFilter::TranslationFilterConfig( ); // min trans = 1m
	conf.dThreshold = 1;

	CTranslationFilter filter( conf );

	CUpdateScene prevUpdate;
	prevUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );
	CUpdateScene newUpdate;

	auto sourceNear = C3DObject( VistaVector3D( 0.5f, 0.5f, 0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto source1m   = C3DObject( VistaVector3D( 1, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto sourceFar  = C3DObject( VistaVector3D( 1.5f, 5.5f, 4 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );

	auto receiverNear = C3DObject( VistaVector3D( -0.5f, -0.5f, -0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiver1m   = C3DObject( VistaVector3D( 0, 0, -1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiverFar  = C3DObject( VistaVector3D( -4, -2, -1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceNear ), std::make_unique<C3DObject>( receiverNear ) );
	EXPECT_FALSE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceNear ), std::make_unique<C3DObject>( receiver1m ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceNear ), std::make_unique<C3DObject>( receiverFar ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );


	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source1m ), std::make_unique<C3DObject>( receiverNear ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source1m ), std::make_unique<C3DObject>( receiver1m ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source1m ), std::make_unique<C3DObject>( receiverFar ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );


	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceFar ), std::make_unique<C3DObject>( receiverNear ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceFar ), std::make_unique<C3DObject>( receiver1m ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceFar ), std::make_unique<C3DObject>( receiverFar ) );
	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );
}

struct RotationFilterTestParam
{
	bool expectedResult;
	VistaVector3D sourcePos;
	VistaVector3D sourceRotAxis;
	double sourceRotDeg;
	VistaVector3D receiverPos;
	VistaVector3D receiverRotAxis;
	double receiverRotDeg;
	CRotationFilter::RotationModes rotationMode;

	friend std::ostream& operator<<( std::ostream& os, const RotationFilterTestParam& obj )
	{
		return os << "expectedResult: " << obj.expectedResult << " sourcePos: " << obj.sourcePos << " sourceRotAxis: " << obj.sourceRotAxis
		          << " sourceRotDeg: " << obj.sourceRotDeg << " receiverPos: " << obj.receiverPos << " receiverRotAxis: " << obj.receiverRotAxis
		          << " receiverRotDeg: " << obj.receiverRotDeg << " rotationMode: " << AsInteger( obj.rotationMode );
	}
};

struct RotationFilterTest : public testing::TestWithParam<RotationFilterTestParam>
{
	std::unique_ptr<CRotationFilter> filter;
	std::unique_ptr<CUpdateScene> prevUpdate;

	RotationFilterTest( )
	{
		const auto param = GetParam( );

		auto conf               = CRotationFilter::RotationFilterConfig( );
		conf.dReceiverThreshold = 2.5;
		conf.dSourceThreshold   = 5;
		conf.eMode              = param.rotationMode;

		filter = std::make_unique<CRotationFilter>( conf );

		prevUpdate = std::make_unique<CUpdateScene>( );
		prevUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
		                                   std::make_unique<C3DObject>( VistaVector3D( 1, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );
	}
};

TEST_P( RotationFilterTest, correctResult )
{
	const auto param = GetParam( );
	CUpdateScene newUpdate;

	auto source = C3DObject( param.sourcePos, VistaAxisAndAngle( param.sourceRotAxis, param.sourceRotDeg * ITAConstants::PI_D / 180.0 ), C3DObject::Type::source, 0 );
	auto receiver =
	    C3DObject( param.receiverPos, VistaAxisAndAngle( param.receiverRotAxis, param.receiverRotDeg * ITAConstants::PI_D / 180.0 ), C3DObject::Type::receiver, 0 );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_EQ( param.expectedResult, filter->ChangeIsAudible( *prevUpdate, newUpdate ) );
}

INSTANTIATE_TEST_CASE_P(
    RotationFilterTest, RotationFilterTest,
    testing::Values( RotationFilterTestParam { false, { 0, 0, 0 }, { 1, 0, 0 }, 1.5, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },   // 0
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 1, 0, 0 }, 10, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },    // 1
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 1, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },     // 2
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 6, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },      // 3
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 0, 1 }, 2, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },     // 4
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 0, 1 }, 6, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },      // 5
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 1, 0, 0 }, 0.5, CRotationFilter::RotationModes::relative },   // 6
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 1, 0, 0 }, 60, CRotationFilter::RotationModes::relative },    // 7
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 1, CRotationFilter::RotationModes::relative },     // 8
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 3, CRotationFilter::RotationModes::relative },      // 9
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 0, 1 }, 2.5, CRotationFilter::RotationModes::relative },   // 10
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 0, 1 }, 2.51, CRotationFilter::RotationModes::relative },   // 11
                     RotationFilterTestParam { false, { 0, 0.01f, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative }, // 12
                     RotationFilterTestParam { true, { 0, 10, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },     // 13
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0.01f }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative }, // 14
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 42 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::relative },     // 15

                     RotationFilterTestParam { false, { 0, 0, 0 }, { 1, 0, 0 }, 1.5, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },    // 16
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 1, 0, 0 }, 10, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },      // 17
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 1, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },      // 18
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 6, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },       // 19
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 0, 1 }, 2, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },      // 20
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 0, 1 }, 6, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },       // 21
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 1, 0, 0 }, 0.5, CRotationFilter::RotationModes::absolute },    // 22
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 1, 0, 0 }, 60, CRotationFilter::RotationModes::absolute },      // 23
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 1, CRotationFilter::RotationModes::absolute },      // 24
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 3, CRotationFilter::RotationModes::absolute },       // 25
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 0, 1 }, 2.5, CRotationFilter::RotationModes::absolute },    // 26
                     RotationFilterTestParam { true, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 0, 1 }, 2.51, CRotationFilter::RotationModes::absolute },    // 27
                     RotationFilterTestParam { false, { 0, 0.01f, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },  // 28
                     RotationFilterTestParam { false, { 0, 10, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },     // 29
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0.01f }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute },  // 30
                     RotationFilterTestParam { false, { 0, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 42 }, { 0, 1, 0 }, 0, CRotationFilter::RotationModes::absolute } ) ); // 31

TEST( DistanceFilter, correctResult )
{
	auto conf               = CDistanceFilter::DistanceFilterConfig( );
	conf.dDistanceThreshold = 5;

	CDistanceFilter filter( conf );

	CUpdateScene oldUpdate;

	CUpdateScene newUpdate;

	auto sourceInside1  = C3DObject( VistaVector3D( 0.5f, 0.5f, 0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto sourceInside2  = C3DObject( VistaVector3D( 4.99f, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto sourceOutside1 = C3DObject( VistaVector3D( 0, 6, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto sourceOutside2 = C3DObject( VistaVector3D( 5, 5, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );

	auto receiverInside1  = C3DObject( VistaVector3D( 0.5f, 0.5f, 0.5f ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiverInside2  = C3DObject( VistaVector3D( 4.99f, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiverOutside1 = C3DObject( VistaVector3D( 0, 6, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto receiverOutside2 = C3DObject( VistaVector3D( 5, 5, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );

	auto receiver = C3DObject( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 );
	auto source   = C3DObject( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceInside1 ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_TRUE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceInside1 ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_TRUE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceOutside1 ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_FALSE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( sourceOutside1 ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_FALSE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiverInside1 ) );
	EXPECT_TRUE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiverInside2 ) );
	EXPECT_TRUE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiverOutside1 ) );
	EXPECT_FALSE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiverOutside2 ) );
	EXPECT_FALSE( filter.ChangeIsAudible( oldUpdate, newUpdate ) );
}

struct PerceptiveRotationFilterTestParam
{
	bool expectedResult;
	VistaVector3D prevReceiverPos;
	VistaVector3D prevReceiverRotAxis;
	double prevReceiverRotDeg;
	VistaVector3D receiverPos;
	VistaVector3D receiverRotAxis;
	double receiverRotDeg;
	double strength;


	friend std::ostream& operator<<( std::ostream& os, const PerceptiveRotationFilterTestParam& obj )
	{
		return os << "expectedResult: " << obj.expectedResult << " prevReceiverPos: " << obj.prevReceiverPos << " prevReceiverRotAxis: " << obj.prevReceiverRotAxis
		          << " prevReceiverRotDeg: " << obj.prevReceiverRotDeg << " receiverPos: " << obj.receiverPos << " receiverRotAxis: " << obj.receiverRotAxis
		          << " receiverRotDeg: " << obj.receiverRotDeg << " strength: " << obj.strength;
	}
};

struct PerceptiveRotationFilterTest : public testing::TestWithParam<PerceptiveRotationFilterTestParam>
{
	std::unique_ptr<CPerceptiveRotationFilter> filter;
	std::unique_ptr<CUpdateScene> prevUpdate;

	PerceptiveRotationFilterTest( )
	{
		const auto param = GetParam( );

		auto conf            = CPerceptiveRotationFilter::PerceptiveRotationFilterConfig( );
		conf.dStrengthFactor = param.strength;

		filter = std::make_unique<CPerceptiveRotationFilter>( conf );

		prevUpdate = std::make_unique<CUpdateScene>( );
		prevUpdate->SetSourceReceiverPair(
		    std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
		    std::make_unique<C3DObject>( param.prevReceiverPos,
		                                 VistaQuaternion( VistaAxisAndAngle( param.prevReceiverRotAxis, param.prevReceiverRotDeg * ITAConstants::PI_D / 180.0 ) ),
		                                 C3DObject::Type::receiver, 0 ) );
	}
};

TEST_P( PerceptiveRotationFilterTest, correctResult )
{
	const auto param = GetParam( );
	CUpdateScene newUpdate;

	auto source = C3DObject( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 );
	auto receiver =
	    C3DObject( param.receiverPos, VistaAxisAndAngle( param.receiverRotAxis, param.receiverRotDeg * ITAConstants::PI_D / 180.0 ), C3DObject::Type::receiver, 0 );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( source ), std::make_unique<C3DObject>( receiver ) );
	EXPECT_EQ( param.expectedResult, filter->ChangeIsAudible( *prevUpdate, newUpdate ) );
}

INSTANTIATE_TEST_CASE_P( PerceptiveRotationFilterTest, PerceptiveRotationFilterTest,
                         testing::Values( PerceptiveRotationFilterTestParam { false, { 0, 0, 1 }, { 0, 1, 0 }, 0, { 0, 0, 1 }, { 0, 1, 0 }, 2, 1.0 }, // 0
                                          PerceptiveRotationFilterTestParam { true, { 0, 0, 1 }, { 0, 1, 0 }, 0, { 0, 0, 1 }, { 0, 1, 0 }, 3, 1.0 },  // 1
                                          PerceptiveRotationFilterTestParam { true, { 0, 0, 1 }, { 0, 1, 0 }, 0, { 0, 0, 1 }, { 0, 1, 0 }, 2, 0.5 },  // 2

                                          PerceptiveRotationFilterTestParam { false, { 0, 0, 1 }, { 1, 0, 0 }, 0, { 0, 0, 1 }, { 1, 0, 0 }, 3, 1.0 }, // 3
                                          PerceptiveRotationFilterTestParam { true, { 0, 0, 1 }, { 1, 0, 0 }, 0, { 0, 0, 1 }, { 1, 0, 0 }, 4, 1.0 },  // 4
                                          PerceptiveRotationFilterTestParam { true, { 0, 0, 1 }, { 1, 0, 0 }, 0, { 0, 0, 1 }, { 1, 0, 0 }, 3, 0.5 },  // 5

                                          PerceptiveRotationFilterTestParam { false, { 1, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 4, 1.0 }, // 6
                                          PerceptiveRotationFilterTestParam { true, { 1, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 10, 1.0 }, // 7
                                          PerceptiveRotationFilterTestParam { true, { 1, 0, 0 }, { 0, 1, 0 }, 0, { 1, 0, 0 }, { 0, 1, 0 }, 4, 0.5 },  // 8

                                          PerceptiveRotationFilterTestParam { false, { -1, 0, 0 }, { 0, 1, 0 }, 0, { -1, 0, 0 }, { 0, 1, 0 }, 4, 1.0 }, // 9
                                          PerceptiveRotationFilterTestParam { true, { -1, 0, 0 }, { 0, 1, 0 }, 0, { -1, 0, 0 }, { 0, 1, 0 }, 10, 1.0 }, // 10
                                          PerceptiveRotationFilterTestParam { true, { -1, 0, 0 }, { 0, 1, 0 }, 0, { -1, 0, 0 }, { 0, 1, 0 }, 4, 0.5 },  // 11

                                          PerceptiveRotationFilterTestParam { false, { 0, -1, 1 }, { 0, 1, 0 }, 0, { 0, -1, 1 }, { 0, 1, 0 }, 2, 1.0 }, // 12
                                          PerceptiveRotationFilterTestParam { true, { 0, -1, 1 }, { 0, 1, 0 }, 0, { 0, -1, 1 }, { 0, 1, 0 }, 5, 1.0 },  // 13
                                          PerceptiveRotationFilterTestParam { true, { 0, -1, 1 }, { 0, 1, 0 }, 0, { 0, -1, 1 }, { 0, 1, 0 }, 2, 0.5 },  // 14

                                          PerceptiveRotationFilterTestParam { false, { 0, -1, 1 }, { 1, 0, 0 }, 0, { 0, -1, 1 }, { 1, 0, 0 }, 5, 1.0 }, // 15
                                          PerceptiveRotationFilterTestParam { true, { 0, -1, 1 }, { 1, 0, 0 }, 0, { 0, -1, 1 }, { 1, 0, 0 }, 10, 1.0 }, // 16
                                          PerceptiveRotationFilterTestParam { true, { 0, -1, 1 }, { 1, 0, 0 }, 0, { 0, -1, 1 }, { 1, 0, 0 }, 5, 0.5 }   // 17
                                          ) );

TEST( TranslationFilter, correctResultRelative )
{
	auto conf       = CTranslationFilter::TranslationFilterConfig( ); // min trans = 1m
	conf.dThreshold = 3;
	conf.eMode      = CTranslationFilter::TranslationModes::relative;

	CTranslationFilter filter( conf );

	CUpdateScene prevUpdate;
	prevUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D( 0, 0, 1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );
	CUpdateScene newUpdate;
	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                 std::make_unique<C3DObject>( VistaVector3D( 0, 0, 2 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );

	EXPECT_FALSE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                 std::make_unique<C3DObject>( VistaVector3D( 0, 0, 3 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );

	EXPECT_FALSE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                 std::make_unique<C3DObject>( VistaVector3D( 0, 0, 3.1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );

	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );

	newUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                 std::make_unique<C3DObject>( VistaVector3D( 0, 0, 1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );
	prevUpdate.SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D( 0, 0, 0 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D( 0, 0, 3.1 ), VistaQuaternion( 0, 0, 0, 1 ), C3DObject::Type::receiver, 0 ) );

	EXPECT_TRUE( filter.ChangeIsAudible( prevUpdate, newUpdate ) );
}