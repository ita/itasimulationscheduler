#include "mocks.hpp"

#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>

// STD
#include <memory>

// GTest
#include "ITA/SimulationScheduler/AudibilityFilter/rate_filter.h"

#include "gtest/gtest.h"


using namespace ITA::SimulationScheduler;
using namespace AudibilityFilter;

TEST( FilterFactoryTest, addNewFilterType )
{
	CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter, std::make_shared<MockAudibilityFilter::MockFilterConfig> );

	auto filterConfig       = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
	filterConfig->behaviour = MockAudibilityFilter::Behaviour::nothingAudible;

	const auto filter = CAudibilityFilterFactory::CreateFilter( filterConfig );

	ASSERT_NE( filter, nullptr );
}

TEST( FilterFactoryTest, configFactoryTest )
{
	CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter, std::make_shared<MockAudibilityFilter::MockFilterConfig> );

	const auto config = std::dynamic_pointer_cast<MockAudibilityFilter::MockFilterConfig>( CAudibilityFilterFactory::CreateConfig( "MockFilter" ) );
	EXPECT_NE( config, nullptr );

	const auto config2 = std::dynamic_pointer_cast<CRateFilter::RateFilterConfig>( CAudibilityFilterFactory::CreateConfig( CRateFilter::GetType( ) ) );
	EXPECT_NE( config2, nullptr );
}