#include "../src/ITA/SimulationScheduler/replacement_filter.h"

#include <ITA/SimulationScheduler/update_config.h>
#include <ITA/SimulationScheduler/update_scene.h>
#include <list>
#include <memory>

// GTest
#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace RoomAcoustics;

TEST( ReplacementFilter, filterReplace1 )
{
	std::list<std::unique_ptr<IUpdateMessage>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	auto lastUpdate = std::make_unique<CUpdateScene>( );
	lastUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                   std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );

	auto rawLastUpdate = lastUpdate.get( );

	list.push_back( std::move( lastUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 1, list.size( ) );

	auto afterReplace = list.front( ).release( );

	auto ptr1 = dynamic_cast<CUpdateScene*>( rawLastUpdate );
	auto ptr2 = dynamic_cast<CUpdateScene*>( afterReplace );

	EXPECT_EQ( *ptr1, *ptr2 );
}

TEST( ReplacementFilter, filterReplace2 )
{
	std::list<std::unique_ptr<IUpdateMessage>> list;

	list.push_back( std::make_unique<CUpdateConfig>( ) );
	list.push_back( std::make_unique<CUpdateConfig>( ) );
	list.push_back( std::make_unique<CUpdateConfig>( ) );
	list.push_back( std::make_unique<CUpdateConfig>( ) );
	list.push_back( std::make_unique<CUpdateConfig>( ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 5, list.size( ) );
}

TEST( ReplacementFilter, filterReplace3 )
{
	std::list<std::unique_ptr<IUpdateMessage>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 1 ) );
	list.push_back( std::move( tmpUpdate ) );

	list.push_back( std::make_unique<CUpdateConfig>( ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 1 ) );
	list.push_back( std::move( tmpUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 3, list.size( ) );
}

TEST( ReplacementFilter, filterReplace4 )
{
	std::list<std::unique_ptr<CUpdateScene>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	auto lastUpdate = std::make_unique<CUpdateScene>( );
	lastUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                   std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );

	auto rawLastUpdate = lastUpdate.get( );

	list.push_back( std::move( lastUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 1, list.size( ) );

	auto afterReplace = list.front( ).release( );

	auto ptr1 = dynamic_cast<CUpdateScene*>( rawLastUpdate );
	auto ptr2 = dynamic_cast<CUpdateScene*>( afterReplace );

	EXPECT_EQ( *ptr1, *ptr2 );
}

TEST( ReplacementFilter, filterReplaceDeque1 )
{
	std::deque<std::unique_ptr<CUpdateScene>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	auto lastUpdate = std::make_unique<CUpdateScene>( );
	lastUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                   std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );

	auto rawLastUpdate = lastUpdate.get( );

	list.push_back( std::move( lastUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 1, list.size( ) );

	auto afterReplace = list.front( ).release( );

	auto ptr1 = dynamic_cast<CUpdateScene*>( rawLastUpdate );
	auto ptr2 = dynamic_cast<CUpdateScene*>( afterReplace );

	EXPECT_EQ( *ptr1, *ptr2 );
}

TEST( ReplacementFilter, filterReplaceDeque2 )
{
	std::deque<std::unique_ptr<CUpdateScene>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 1 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 1 ) );
	list.push_back( std::move( tmpUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 2, list.size( ) );
}

TEST( ReplacementFilter, filterReplaceDeque3 )
{
	std::deque<std::unique_ptr<CUpdateScene>> list;

	std::unique_ptr<CUpdateScene> tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	tmpUpdate = std::make_unique<CUpdateScene>( );
	tmpUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                  std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );
	list.push_back( std::move( tmpUpdate ) );

	auto lastUpdate = std::make_unique<CUpdateScene>( );
	lastUpdate->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::source, 0 ),
	                                   std::make_unique<C3DObject>( VistaVector3D { 0, 0, 0 }, VistaQuaternion { 0, 0, 0, 1 }, C3DObject::Type::receiver, 0 ) );

	auto rawLastUpdate = lastUpdate.get( );

	list.push_back( std::move( lastUpdate ) );

	CReplacementFilter::FilterReplace( list );

	EXPECT_EQ( 1, list.size( ) );

	auto afterReplace = list.front( ).release( );

	auto ptr1 = dynamic_cast<CUpdateScene*>( rawLastUpdate );
	auto ptr2 = dynamic_cast<CUpdateScene*>( afterReplace );

	EXPECT_EQ( *ptr1, *ptr2 );
}