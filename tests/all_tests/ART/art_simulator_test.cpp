// simulation scheduler includes
#include <ITA/SimulationScheduler/OutdoorAcoustics/ART/art_simulator.h>
#include <ITA/SimulationScheduler/utils/utils.h>
//#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_simulation_result.h>
//#include <ITA/SimulationScheduler/RoomAcoustics/Raven/simulator.h>


// ITA includes
//#include <ITAFFT.h>
//#include <ITAHDFTSpectra.h>
//#include <ITAAudiofileReader.h>
//#include <ITAAudiofileWriter.h>
//#include <ITAHDFTSpectrum.h>


// STD
#include <iostream>
#include <memory>
#include <ostream>
#include <string>
#include <valarray>

// GTest
#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::Utils;
using namespace ITA::SimulationScheduler::OutdoorAcoustics;

// using namespace ITA::SimulationScheduler::RoomAcoustics;
// using namespace ITA::SimulationScheduler::RoomAcoustics::Raven;


struct ARTSimulatorParam : ART::CARTSimulator::ARTSimulatorConfig
{
	// TODO: Think about other parameters
	VistaVector3D v3SourcePosition;
	VistaVector3D v3ReceiverPosition;

	friend std::ostream& operator<<( std::ostream& os, const ARTSimulatorParam& obj )
	{
		return os << "ClientCoordSystem: " << (int)obj.eClientCoordinateSystem << ", "
		          << "JSON: " << obj.sJsonAtmosphere;
	}
};

struct ARTSimulatorTest : testing::Test
{
	std::unique_ptr<ART::CARTSimulator> pSimulator;
	std::unique_ptr<CUpdateScene> pUpdateScene;

	ARTSimulatorTest( )
	{
		ART::CARTSimulator::ARTSimulatorConfig config;
		config.eClientCoordinateSystem = CoordinateSystem::OpenGL;
		config.sJsonAtmosphere         = "";

		pSimulator = std::make_unique<ART::CARTSimulator>( config );

		pUpdateScene = std::make_unique<CUpdateScene>( 0 );
	}

	void SetSourceAndReceiver( const VistaVector3D& v3SourcePos, const VistaVector3D& v3RecPos )
	{
		pUpdateScene->SetSourceReceiverPair( std::make_unique<C3DObject>( v3SourcePos, VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 0 ),
		                                     std::make_unique<C3DObject>( v3RecPos, VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 0 ) );
	}
};


TEST( ARTSimulatorConstructionTest, Constructor )
{
	auto pConfig = std::make_unique<ART::CARTSimulator::ARTSimulatorConfig>( );
	EXPECT_NO_THROW( auto testSim = ART::CARTSimulator::Create( std::move(pConfig) ) );
}
TEST( ARTSimulatorConstructionTest, CreateWithWrongConfig )
{
	auto pWrongConfig = std::make_unique<ISimulatorInterface::SimulatorConfig>( );
	try
	{
		ART::CARTSimulator::Create( std::move( pWrongConfig ) );
	}
	catch( ITAException& err )
	{
		SUCCEED( );
		return;
	}
	FAIL( );
}


TEST_F( ARTSimulatorTest, Compute )
{
	auto v3Source   = VistaVector3D( 1, 0, 0 );
	auto v3Receiver = VistaVector3D( 1, 0, 0 );
	SetSourceAndReceiver( v3Source, v3Receiver );
	auto pResult = pSimulator->Compute( std::move( pUpdateScene ) );

	EXPECT_TRUE( pResult != nullptr );
}


// TEST_P(SimulatorTestParam, CorrectResult)
//{
//	SUCCEED();
//}
//
// INSTANTIATE_TEST_CASE_P ( ARTSimulator, SimulatorTestParam,
//						  testing::Combine ( testing::Values ( SimulatorParam::SceneParam { "",
//															   VistaVector3D ( 1,1.5,1 ), // source
//															   VistaVector3D( -0.5,1.5,-0.5 ) // receiver
//															   } ),
//											 testing::Values ( SimulatorParam::MiscParam { 1, 0.01, 0.9, true },
//															   SimulatorParam::MiscParam { 1, 0.01, 0.9, true },
//															   SimulatorParam::MiscParam { 1, 6.0, 0.15, true } ) ) );
