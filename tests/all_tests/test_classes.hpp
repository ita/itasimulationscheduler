#pragma once

#include <ITA/SimulationScheduler/scheduler_interface.h>
#include <ITA/SimulationScheduler/worker_interface.h>
#include <ITA/SimulationScheduler/scheduler.h>
#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>

#include <ITA/SimulationScheduler/OutdoorAcoustics/outdoor_worker_thread.h>

#ifdef WITH_RAVEN
#include <ITA/SimulationScheduler/RoomAcoustics/raven/worker_thread.h>
#endif

#include "mocks.hpp"

using namespace ITA::SimulationScheduler;
//using namespace ITA::SimulationScheduler::RoomAcoustics;
#ifdef WITH_RAVEN
using namespace ITA::SimulationScheduler::RoomAcoustics::Raven;
#endif

struct TestMasterSimulationController : public RoomAcoustics::CMasterSimulationController
{
	TestMasterSimulationController ( ) : CMasterSimulationController (
		CMasterSimulationController::MasterSimulationControllerConfig ( ) )
	{
		scheduler.push_back ( new MockScheduler );
		scheduler.push_back ( new MockScheduler );
		scheduler.push_back ( new MockScheduler );

		auto ptr1 = std::unique_ptr<ISchedulerInterface> ( scheduler.at ( 0 ) );
		auto ptr2 = std::unique_ptr<ISchedulerInterface> ( scheduler.at ( 1 ) );
		auto ptr3 = std::unique_ptr<ISchedulerInterface> ( scheduler.at ( 2 ) );

		SetScheduler ( std::move ( ptr1 ), std::move ( ptr2 ), std::move ( ptr3 ) );
	}


	std::vector<MockScheduler*> scheduler;
};

struct TestLocalScheduler : public CScheduler
{
	TestLocalScheduler ( const LocalSchedulerConfig& pConfig ) : CScheduler ( pConfig )
	{ }

	~TestLocalScheduler ( ) = default;

	std::vector<IWorkerInterface* > getWorkerInterfaces ( )
	{
		return GetWorker ( );
	}

	AudibilityFilter::CFilterNetwork* getNetwork( )
	{
		return GetFilterNetwork();
	}
};


//----OUTDOOR ACOUSTICS----
//-------------------------

struct TestOutdoorWorkerThread : public OutdoorAcoustics::CWorkerThread
{
	MockOutdoorAcousticSimulator* pSimulator;

	TestOutdoorWorkerThread(const WorkerThreadConfig& oConfig, ISchedulerInterface* pParent) : CWorkerThread(oConfig, pParent)
	{
		auto pSimulatorTmp = std::make_unique<MockOutdoorAcousticSimulator>();
		pSimulator = pSimulatorTmp.get();

		SetSimulator( std::move(pSimulatorTmp) );
	}

	static std::unique_ptr<IWorkerInterface> CreateWorker(const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent)
	{
		return std::make_unique<TestOutdoorWorkerThread>(dynamic_cast<const WorkerThreadConfig&>(*pConfig), pParent);
	}
};



//--------RAVEN--------
//---------------------
#ifdef WITH_RAVEN

struct TestRavenWorkerInterface : public IRavenWorkerInterface
{
	TestRavenWorkerInterface ( const IRavenWorkerInterface::RavenWorkerConfig& oConfig, CScheduler* pParent ) : IRavenWorkerInterface ( oConfig, pParent )
	{ }

	~TestRavenWorkerInterface ( ) override
	{ }

	bool IsBusy ( ) override
	{
		return false;
	}

	void PushUpdate ( std::unique_ptr<CUpdateScene> pUpdateMessage ) override
	{
		vUpdates.push_back ( pUpdateMessage.release ( ) );
		m_pParentScheduler->HandleSimulationFinished ( std::make_unique<CRIRSimulationResult> ( ) );
	}

	void Reset ( ) override
	{
		bReset = true;
	}

	std::unique_ptr<CSimulationTask> createTaskFromUpdateTest ( std::unique_ptr<CUpdateScene> pUpdate ) const
	{
		return CreateTaskFromUpdate ( std::move ( pUpdate ) );
	}

	static std::unique_ptr<CRIRSimulationResult> convertSimulationResultTest ( std::unique_ptr<CRavenSimulationResult> pResult, CSimulationTask* pTask )
	{
		return ConvertSimulationResult ( std::move ( pResult ), pTask );
	}

	void Shutdown() override
	{ }
	
	bool bReset = false;
	std::vector<CUpdateScene*> vUpdates;
};

struct TestRavenWorkerThread : public Raven::CWorkerThread
{
	MockRavenSimulator* simulator;

	TestRavenWorkerThread ( const WorkerThreadConfig& oConfig, ISchedulerInterface* pParent ) : CWorkerThread ( oConfig, pParent )
	{
		auto simulatorTmp = std::make_unique<MockRavenSimulator> ( );
		simulator = simulatorTmp.get ( );

		SetSimulator ( std::move ( simulatorTmp ) );
	}

	static std::unique_ptr<IWorkerInterface> createWorker ( const std::shared_ptr<IWorkerInterface::WorkerConfig>& pConfig, ISchedulerInterface* pParent )
	{
		return std::make_unique<TestRavenWorkerThread> ( dynamic_cast< const WorkerThreadConfig& >( *pConfig ), pParent );
	}


};

#endif