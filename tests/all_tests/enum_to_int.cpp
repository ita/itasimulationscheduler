#ifndef INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_UTILS
#define INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_UTILS

#include <type_traits>

template<typename Enumeration>
auto as_integer( Enumeration const value ) -> typename std::underlying_type<Enumeration>::type
{
	return static_cast<typename std::underlying_type<Enumeration>::type>( value );
}

#endif // INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_UTILS