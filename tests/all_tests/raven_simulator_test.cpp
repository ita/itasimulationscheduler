#ifdef WITH_RAVEN

#	include <iostream>
#	include <memory>
#	include <string>

// simulation scheduler includes
#	include <ITA/SimulationScheduler/RoomAcoustics/raven/simulator.h>
#	include <ITA/SimulationScheduler/utils/utils.h>
#	include <ITA/SimulationScheduler/update_config.h>

// ITA includes
#	include <ITAAudiofileReader.h>
#	include <ITAAudiofileWriter.h>
#	include <ITAFFT.h>
#	include <ITAHDFTSpectra.h>
#	include <ITAException.h>

// JSON Include
#	include <nlohmann/json.hpp>

// GTest
#	include "ITAHDFTSpectrum.h"

#	include "gtest/gtest.h"
#	include <ostream>
#	include <valarray>

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::Utils;
using namespace ITA::SimulationScheduler::RoomAcoustics;
using namespace ITA::SimulationScheduler::RoomAcoustics::Raven;

TEST( SimulatorTest, HandleConfigUpdateChangDirectivity )
{
	auto simulator = std::make_unique<CSimulator>( FieldOfDuty::directSound, std::string( WITH_RAVEN_RESOURCE_DIR "/TestShoeboxRoom/TestShoeboxRoom.rpf" ) );

	CRavenScene::CReceiverState oReceiverState;
	oReceiverState.vPos.init( 0, 1, 0 );
	oReceiverState.vView.init( 0, 0, -1 );
	oReceiverState.vUp.init( 0, 1, 0 );

	CRavenScene::CSourceState oSourceState;
	oSourceState.vPos.init( 0, 1, 0 );
	oSourceState.vView.init( 0, 0, -1 );
	oSourceState.vUp.init( 0, 1, 0 );

	std::string sEmptyPayload = "";
	std::string sNoJsonPayload = "This is not a valid JSON payload";
	nlohmann::json jMissingKeysPayload;
	nlohmann::json jCorrectPayload;
	nlohmann::json jCorrectPayloadNoDFT;

	jMissingKeysPayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyObjId ] = 1;

	jCorrectPayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyDirectivityFilePath ] = "NoDirectivity";
	jCorrectPayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyFilePathDFT ] = "";
	jCorrectPayload[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyObjId ] = 0;

	jCorrectPayloadNoDFT[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyDirectivityFilePath ] = "NoDirectivity";
	jCorrectPayloadNoDFT[ ITA::SimulationScheduler::ConfigUpdateJsonKeys::keyObjId ] = 0;

	std::unique_ptr<ITA::SimulationScheduler::CUpdateConfig> pConfigUpdateEmptyPayload =
		std::make_unique<ITA::SimulationScheduler::CUpdateConfig>( ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity, sEmptyPayload );

	std::unique_ptr<ITA::SimulationScheduler::CUpdateConfig> pConfigUpdateNoJsonPayload =
		std::make_unique<ITA::SimulationScheduler::CUpdateConfig>( ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity, sNoJsonPayload );

	std::unique_ptr<ITA::SimulationScheduler::CUpdateConfig> pConfigUpdateMissingKeysPayload =
		std::make_unique<ITA::SimulationScheduler::CUpdateConfig>( ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity, jMissingKeysPayload.dump( ) );

	std::unique_ptr<ITA::SimulationScheduler::CUpdateConfig> pConfigUpdateCorrectPayload =
		std::make_unique<ITA::SimulationScheduler::CUpdateConfig>( ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity, jCorrectPayload.dump( ) );

	std::unique_ptr<ITA::SimulationScheduler::CUpdateConfig> pConfigUpdateCorrectPayloadNoDFT =
		std::make_unique<ITA::SimulationScheduler::CUpdateConfig>( ITA::SimulationScheduler::CUpdateConfig::ConfigChangeType::changeDirectivity, jCorrectPayloadNoDFT.dump( ) );

	auto oTask = new CSimulationTask;

	oTask->oScene.AddReceiver( 0 );
	oTask->oScene.SetReceiverState( 0, oReceiverState );
	oTask->oScene.AddSource( 0 );
	oTask->oScene.SetSourceState( 0, oSourceState );
	oTask->eSimulationType = FieldOfDuty::earlyReflections;
	oTask->uiID            = 0;

	auto result = new CRavenSimulationResult( );

	simulator->Compute( oTask, result );

	EXPECT_THROW( simulator->HandleConfigUpdate( std::move( pConfigUpdateEmptyPayload ) ), ITAException );
	EXPECT_THROW( simulator->HandleConfigUpdate( std::move( pConfigUpdateNoJsonPayload ) ), ITAException );
	EXPECT_THROW( simulator->HandleConfigUpdate( std::move( pConfigUpdateMissingKeysPayload ) ), ITAException );

	EXPECT_NO_THROW( simulator->HandleConfigUpdate( std::move( pConfigUpdateCorrectPayload ) ) );
	EXPECT_NO_THROW( simulator->HandleConfigUpdate( std::move( pConfigUpdateCorrectPayloadNoDFT ) ) );
}

TEST( SimulatorTest, WriteRPF )
{
	auto simulator = std::make_unique<CSimulator>( FieldOfDuty::directSound, "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/referenceNEW.rpf" );

	CRavenScene::CReceiverState oReceiverState;
	oReceiverState.vPos.init( 0, 1, 0 );
	oReceiverState.vView.init( 0, 0, -1 );
	oReceiverState.vUp.init( 0, 1, 0 );

	CRavenScene::CSourceState oSourceState;
	oSourceState.vPos.init( 0, 1, 0 );
	oSourceState.vView.init( 0, 0, -1 );
	oSourceState.vUp.init( 0, 1, 0 );

	auto oTask = new CSimulationTask;

	oTask->oScene.AddReceiver( 0 );
	oTask->oScene.SetReceiverState( 0, oReceiverState );
	oTask->oScene.AddSource( 0 );
	oTask->oScene.SetSourceState( 0, oSourceState );
	oTask->eSimulationType = FieldOfDuty::earlyReflections;
	oTask->uiID            = 0;

	auto result = new CRavenSimulationResult( );

	simulator->Compute( oTask, result );

	if( !doesDirectoryExist( "results" ) )
		makeDirectory( "results" );

	EXPECT_NO_THROW( simulator->WriteToRpf( "results/WriteRPFTest.rpf" ) );
}


struct SimulatorParam
{
	struct SceneParam
	{
		std::string scene;

		std::string referenceIRPath; // checks for DS_reference.wav, ER_reference.wav, RT_reference.wav

		RG_Vector sourcePosition;

		RG_Vector receiverPosition;

		double irLength; // in seconds

		friend std::ostream& operator<<( std::ostream& os, const SceneParam& obj )
		{
			return os << "scene: " << obj.scene << " referenceIRPath: " << obj.referenceIRPath << " sourcePosition: " << obj.sourcePosition[0] << ", "
			          << obj.sourcePosition[1] << ", " << obj.sourcePosition[2] << " sourcePosition: " << obj.receiverPosition[0] << ", " << obj.receiverPosition[1]
			          << ", " << obj.receiverPosition[2] << " irLength: " << obj.irLength;
		}
	};

	struct MiscParam
	{
		int runs;

		double acceptableStdDev; // ~6

		double correlationThreshold; // >0.9 for deterministic results

		FieldOfDuty fieldOfDuty;

		bool saveResults;


		friend std::ostream& operator<<( std::ostream& os, const MiscParam& obj )
		{
			return os << "runs: " << obj.runs << " acceptableStdDev: " << obj.acceptableStdDev << " correlationThreshold: " << obj.correlationThreshold
			          << " fieldOfDuty: " << AsInteger( obj.fieldOfDuty );
		}
	};
};

struct SimulatorTestParam : testing::TestWithParam<std::tuple<SimulatorParam::SceneParam, SimulatorParam::MiscParam>>
{
	std::unique_ptr<CSimulator> simulator;

	CSimulationTask* oTask;

	ITABase::CMultichannelFiniteImpulseResponse reference;

	SimulatorTestParam( )
	{
		auto paramScene = std::get<0>( GetParam( ) );
		auto paramMisc  = std::get<1>( GetParam( ) );

		simulator = std::make_unique<CSimulator>( paramMisc.fieldOfDuty, paramScene.scene );

		if( !paramScene.referenceIRPath.empty( ) )
		{
			std::string filePath = paramScene.referenceIRPath;
			switch( paramMisc.fieldOfDuty )
			{
				case FieldOfDuty::directSound:
					filePath += "/DS_reference.wav";
					break;

				case FieldOfDuty::earlyReflections:
					filePath += "/ER_reference.wav";
					break;

				case FieldOfDuty::diffuseDecay:
					filePath += "/DD_reference.wav";
					break;
					;
			}
			reference.LoadFromFile( filePath );
		}
		else
		{
			ITA_EXCEPT_NOT_IMPLEMENTED;
		}

		CRavenScene::CReceiverState oReceiverState;
		oReceiverState.vPos = paramScene.receiverPosition;
		oReceiverState.vView.init( 0, 0, -1 );
		oReceiverState.vUp.init( 0, 1, 0 );

		CRavenScene::CSourceState oSourceState;
		oSourceState.vPos = paramScene.sourcePosition;
		oSourceState.vView.init( 0, 0, -1 );
		oSourceState.vUp.init( 0, 1, 0 );

		oTask = new CSimulationTask;

		oTask->oScene.AddReceiver( 0 );
		oTask->oScene.SetReceiverState( 0, oReceiverState );
		oTask->oScene.AddSource( 0 );
		oTask->oScene.SetSourceState( 0, oSourceState );
		oTask->eSimulationType = paramMisc.fieldOfDuty;
		oTask->uiID            = 0;
	}

	static bool resultsAreIdentical( const ITABase::CMultichannelFiniteImpulseResponse& reference, const CRavenSimulationResult& secondResult, FieldOfDuty fieldOfDuty )
	{
		if( fieldOfDuty == FieldOfDuty::earlyReflections || fieldOfDuty == FieldOfDuty::directSound )
		{
			auto secondIR = DataTypeUtils::Convert( *secondResult.vcspResult[0]->psfResult );

			auto referenceCopy = reference;

			CalculationUtils::Normalize( secondIR );

			CalculationUtils::Normalize( referenceCopy );

			DataTypeUtils::MakeSameLength( referenceCopy, secondIR );

			if( referenceCopy.GetLength( ) == secondIR.GetLength( ) && referenceCopy.GetNumChannels( ) == secondIR.GetNumChannels( ) )
			{
				for( auto i = 0; i < referenceCopy.GetNumChannels( ); ++i )
				{
					for( auto j = 0; j < referenceCopy.GetLength( ); ++j )
					{
						const auto difference = abs( referenceCopy[i][j] - secondIR[i][j] );
						if( difference > 0.01 )
							return false;
					}
				}
				return true;
			}
			return false;
		}

		if( fieldOfDuty == FieldOfDuty::diffuseDecay )
		{
			ITA_EXCEPT_INVALID_PARAMETER( "Diffuse decay can not be tested this way." );
		}

		return false;
	}

	static bool compareResults( const ITABase::CMultichannelFiniteImpulseResponse& reference, const std::vector<CRavenSimulationResult>& secondResults,
	                            const FieldOfDuty& fieldOfDuty, const double& acceptedStandardDeviation )
	{
		if( fieldOfDuty == FieldOfDuty::diffuseDecay )
		{
			std::vector<ITABase::CMultichannelFiniteImpulseResponse> irs;

			for( auto& sec: secondResults )
			{
				irs.push_back( DataTypeUtils::Convert( *sec.vcspResult[0]->psfResult ) );
			}

			auto stdDev = CalculationUtils::CalculateStdDevOfAbsSpectralDifference( reference, irs );

			bool acceptable = true;

			for( const auto& deviation: stdDev )
			{
				acceptable &= deviation < acceptedStandardDeviation;
			}

			return acceptable;
		}
		else
		{
			return resultsAreIdentical( reference, secondResults[0], fieldOfDuty );
		}
	}
};

TEST_P( SimulatorTestParam, DISABLED_CorrectResult )
{
	const auto paramMisc = std::get<1>( GetParam( ) );

	std::vector<CRavenSimulationResult> results( paramMisc.runs );

	for( auto i = 0; i < paramMisc.runs; ++i )
	{
		simulator->Compute( oTask, &results.at( i ) );
	}

	if( paramMisc.saveResults )
	{
		const auto test_info = testing::UnitTest::GetInstance( )->current_test_info( );

		auto path = std::string( test_info->test_case_name( ) ) + "/" + std::string( test_info->name( ) );

		if( !doesDirectoryExist( "results" ) )
			makeDirectory( "results" );

		size_t index = 0;
		while( true )
		{
			/* Locate the substring to replace. */
			index = path.find( "/", index );
			if( index == std::string::npos )
				break;

			/* Make the replacement. */
			path.replace( index, 1, "_" );

			/* Advance index forward so the next iteration doesn't pick it up as well. */
			index++;
		}

		for( const auto& resIdx: Enumerate( results ) )
		{
			writeAudiofile( "results/" + path + "_" + "-FOD_" + to_string( AsInteger( oTask->eSimulationType ) ) + "-" + to_string( resIdx.second ) + ".wav",
			                resIdx.first.vcspResult[0]->psfResult.get( ), 44100 );
		}

		simulator->WriteToRpf( "results/" + path + "_" + "-FOD_" + to_string( AsInteger( oTask->eSimulationType ) ) + ".rpf" );
	}

	// Get the irs form the RavenResult
	std::vector<ITABase::CMultichannelFiniteImpulseResponse> irs;

	for( auto& res: results )
	{
		irs.push_back( DataTypeUtils::Convert( *res.vcspResult[0]->psfResult ) );
	}

	auto stdDev = CalculationUtils::CalculateStdDevOfAbsSpectralDifference( reference, irs );

	for( const auto& deviation: stdDev )
	{
		EXPECT_GE( paramMisc.acceptableStdDev, deviation ); // accepted >= deviation
	}

	for( auto& ir: irs )
	{
		auto correlationVector = CalculationUtils::NormalizedCrossCorrelation( reference, ir );

		for( const auto& correlation: correlationVector )
		{
			EXPECT_LE( paramMisc.correlationThreshold, abs( correlation ).max( ) ); // threshold <= maxCorr
		}
	}

	if( HasFailure( ) )
	{
		const auto test_info = testing::UnitTest::GetInstance( )->current_test_info( );

		auto path = std::string( test_info->test_case_name( ) ) + "/" + std::string( test_info->name( ) );

		if( !doesDirectoryExist( "results" ) )
			makeDirectory( "results" );

		size_t index = 0;
		while( true )
		{
			/* Locate the substring to replace. */
			index = path.find( "/", index );
			if( index == std::string::npos )
				break;

			/* Make the replacement. */
			path.replace( index, 1, "_" );

			/* Advance index forward so the next iteration doesn't pick it up as well. */
			index++;
		}

		for( const auto& res: irs )
		{
			res.StoreToFile( "results/" + path + "-FOD_" + to_string( AsInteger( paramMisc.fieldOfDuty ) ) + ".wav" );
		}
	}
}

//,
// SimulatorParam::SceneParam { "$(RavenDataBasePath)/Models/ReverberationChamber/reverbChamber_empty.ac",
//"D:/ScieBo/MasterThesis/RavenDataBase/Models/ReverberationChamber/",
// RG_Vector ( -2,1.5,-1 ), // source
// RG_Vector ( -0.5,1.5,-0.5 ), // receiver
// 0.5 } */

INSTANTIATE_TEST_CASE_P( RavenSimulator, SimulatorTestParam,
                         testing::Combine( testing::Values( SimulatorParam::SceneParam { "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/referenceNEW.rpf",
                                                                                         "D:/ScieBo/MasterThesis/Raven/RavenInput/Cube/",
                                                                                         RG_Vector( 1, 1.5, 1 ),       // source
                                                                                         RG_Vector( -0.5, 1.5, -0.5 ), // receiver
                                                                                         0.5 } ),
                                           testing::Values( SimulatorParam::MiscParam { 1, 0.01, 0.9, FieldOfDuty::directSound, true },
                                                            SimulatorParam::MiscParam { 1, 0.01, 0.9, FieldOfDuty::earlyReflections, true },
                                                            SimulatorParam::MiscParam { 1, 6.0, 0.15, FieldOfDuty::diffuseDecay, true } ) ) );

#endif