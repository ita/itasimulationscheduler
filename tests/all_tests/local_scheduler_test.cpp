#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include <ITA/SimulationScheduler/AudibilityFilter/audibility_filter_interface.h>
#include <ITA/SimulationScheduler/AudibilityFilter/filter_network.h>
#include <ITA/SimulationScheduler/RoomAcoustics/master_simulation_controller.h>
#include <ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h>
#include <ITA/SimulationScheduler/result_handler.h>
#include <ITA/SimulationScheduler/update_config.h>
#include <ITA/SimulationScheduler/update_message.h>
#include <ITA/SimulationScheduler/update_scene.h>

// Vista includes
#include <VistaBase/VistaTimeUtils.h>
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "enum_to_int.cpp"
#include "mocks.hpp"
#include "test_classes.hpp"

#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::RoomAcoustics;

struct LocalSchedulerTest : public testing::Test
{
	std::unique_ptr<TestLocalScheduler> scheduler;

	MockResultHandler* handler;

	MockWorker* worker;

	// MockAudibilityFilter* filter;

	LocalSchedulerTest( )
	{
		AudibilityFilter::CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter,
		                                                            std::make_shared<MockAudibilityFilter::MockFilterConfig> );
		CWorkerFactory::RegisterWorker( "MockWorker", MockWorker::createWorker, std::make_shared<MockWorker::MockWorkerConfig> );

		auto schedulerConfig = CScheduler::LocalSchedulerConfig( );

		const auto workerConfig         = std::make_shared<MockWorker::MockWorkerConfig>( );
		schedulerConfig.vpWorkerConfigs = { workerConfig };

		auto filterConfig         = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
		filterConfig->behaviour   = MockAudibilityFilter::Behaviour::onlySourceID_42_audible;
		filterConfig->sFilterName = "Filter";
		AudibilityFilter::CFilterNetwork::FilterNetworkConfig filterNetworkConfig;
		filterNetworkConfig.vpFilterConfigs = { filterConfig };
		filterNetworkConfig.sStartFilter    = filterConfig->sFilterName;

		schedulerConfig.oFilterNetworkConfig = filterNetworkConfig;

		scheduler = std::make_unique<TestLocalScheduler>( schedulerConfig );

		worker = dynamic_cast<MockWorker*>( scheduler->getWorkerInterfaces( ).front( ) );

		// filter = dynamic_cast< MockAudibilityFilter* >( scheduler->getFilterInterfaces ( ).front ( ) );

		handler = new MockResultHandler( );

		scheduler->AttachResultHandler( handler );
	}
};

TEST( LocalSchedulerCreationTest, WithOneWorker )
{
	CWorkerFactory::RegisterWorker( "MockWorker", MockWorker::createWorker, std::make_shared<MockWorker::MockWorkerConfig> );

	auto schedulerConfig = CScheduler::LocalSchedulerConfig( );

	const auto workerConfig         = std::make_shared<MockWorker::MockWorkerConfig>( );
	schedulerConfig.vpWorkerConfigs = { workerConfig };

	auto scheduler = std::make_unique<TestLocalScheduler>( schedulerConfig );

	ASSERT_EQ( scheduler->getWorkerInterfaces( ).size( ), 1 );
}

TEST( LocalSchedulerCreationTest, WithOneFilter )
{
	AudibilityFilter::CAudibilityFilterFactory::RegisterFilter( "MockFilter", MockAudibilityFilter::createFilter,
	                                                            std::make_shared<MockAudibilityFilter::MockFilterConfig> );

	auto schedulerConfig = CScheduler::LocalSchedulerConfig( );

	auto filterConfig         = std::make_shared<MockAudibilityFilter::MockFilterConfig>( );
	filterConfig->behaviour   = MockAudibilityFilter::Behaviour::nothingAudible;
	filterConfig->sFilterName = "Filter";
	AudibilityFilter::CFilterNetwork::FilterNetworkConfig filterNetworkConfig;
	filterNetworkConfig.vpFilterConfigs = { filterConfig };
	filterNetworkConfig.sStartFilter    = filterConfig->sFilterName;

	schedulerConfig.oFilterNetworkConfig = filterNetworkConfig;

	auto scheduler = std::make_unique<TestLocalScheduler>( schedulerConfig );

	ASSERT_NE( scheduler->getNetwork( ), nullptr );
}

TEST_F( LocalSchedulerTest, UpdateToWorker )
{
	for( int i = 0; i < 50; ++i )
	{
		if( i == 30 )
		{
			VistaTimeUtils::Sleep( 50 );
		}

		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		scheduler->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	const auto updates = worker->vUpdates;

	EXPECT_GE( updates.size( ), 1 );
	EXPECT_LE( updates.size( ), 50 );
}

TEST_F( LocalSchedulerTest, UpdateToHandler )
{
	for( int i = 0; i < 50; ++i )
	{
		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		scheduler->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	EXPECT_GE( handler->resultVector.size( ), 1 );
	EXPECT_LE( handler->resultVector.size( ), 50 );
}

TEST_F( LocalSchedulerTest, UpdateToHandler1 )
{
	auto update = std::make_unique<CUpdateScene>( );
	update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
	                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
	scheduler->PushUpdate( std::move( update ) );

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	EXPECT_EQ( handler->resultVector.size( ), 1 );
}

TEST_F( LocalSchedulerTest, Reset )
{
	scheduler->PushUpdate( std::make_unique<CUpdateConfig>( CUpdateConfig::ConfigChangeType::resetAll ) );

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 1000 );

	EXPECT_EQ( true, worker->bReset );
}

TEST_F( LocalSchedulerTest, Filter )
{
	for( int i = 0; i < 50; ++i )
	{
		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		scheduler->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	// Send another bunch of updates afterwards, so the filter replace is not being used.
	for( int i = 0; i < 50; ++i )
	{
		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		scheduler->PushUpdate( std::move( update ) );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	// The first update is always done
	EXPECT_EQ( handler->resultVector.size( ), 1 );
}

TEST_F( LocalSchedulerTest, correctNumberOfResults )
{
	for( auto i = 0; i < 2; ++i )
	{
		for( auto j = 0; j < 3; ++j )
		{
			auto update = std::make_unique<CUpdateScene>( );
			update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 1 ),
			                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
			scheduler->PushUpdate( std::move( update ) );
		}

		auto update = std::make_unique<CUpdateScene>( );
		update->SetSourceReceiverPair( std::make_unique<C3DObject>( VistaVector3D { 1, 1, 1 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::source, 42 ),
		                               std::make_unique<C3DObject>( VistaVector3D { 3, 6, 2 }, VistaQuaternion { 1, 0, 0, 0 }, C3DObject::Type::receiver, 1 ) );
		scheduler->PushUpdate( std::move( update ) );

		// We have to wait a bit for the thread inside the controller to do its thing. And the second bunch is not replaced by filterReplace.
		VistaTimeUtils::Sleep( 500 );
	}

	// We have to wait a bit for the thread inside the controller to do its thing.
	VistaTimeUtils::Sleep( 250 );

	// The first update is always done
	EXPECT_EQ( handler->resultVector.size( ), 3 );
}