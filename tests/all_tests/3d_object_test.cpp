#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include <ITA/SimulationScheduler/3d_object.h>

// Vista includes
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "enum_to_int.cpp"

#include "gtest/gtest.h"

namespace simsched = ITA::SimulationScheduler;

struct TestC3DObject : testing::Test
{
	std::unique_ptr<simsched::C3DObject> obj1;

	std::unique_ptr<simsched::C3DObject> obj2;

	TestC3DObject( ) = default;
};

struct C3DObjectParam
{
	VistaVector3D position;
	VistaVector3D modifier;
	VistaQuaternion orientation;
	simsched::C3DObject::Type type1;
	simsched::C3DObject::Type type2;
	unsigned int id1;
	unsigned int id2;
	bool successEqual;
	bool successEqualTol;
	double tolerance;

	friend std::ostream& operator<<( std::ostream& os, const C3DObjectParam& obj )
	{
		return os << std::boolalpha << "Position: " << obj.position << "\n"
		          << "modifier: " << obj.modifier << "\n"
		          << "orientation: " << obj.orientation << "\n"
		          << "type1: " << as_integer( obj.type1 ) << "\n"
		          << "type2: " << as_integer( obj.type2 ) << "\n"
		          << "id1: " << obj.id1 << "\n"
		          << "id2: " << obj.id2 << "\n"
		          << "successEqual: " << obj.successEqual << "\n"
		          << "successEqualTol: " << obj.successEqualTol << "\n"
		          << "tolerance: " << obj.tolerance << "\n";
	}
};

struct ParamTestC3DObject
    : TestC3DObject
    , testing::WithParamInterface<C3DObjectParam>
{
	ParamTestC3DObject( )
	{
		auto param = GetParam( );
		obj1       = std::make_unique<simsched::C3DObject>( param.position, param.orientation, param.type1, param.id1 );
		obj2       = std::make_unique<simsched::C3DObject>( param.position + param.modifier, param.orientation, param.type2, param.id2 );
	}
};

TEST_P( ParamTestC3DObject, getter )
{
	auto param = GetParam( );
	EXPECT_EQ( param.position, obj1->GetPosition( ) );
	EXPECT_EQ( param.orientation, obj1->GetOrientation( ) );
	EXPECT_EQ( param.type1, obj1->GetType( ) );
	EXPECT_EQ( param.id1, obj1->GetId( ) );
}

TEST_P( ParamTestC3DObject, TestEqual )
{
	auto param = GetParam( );
	auto res   = *obj1.get( ) == *obj2.get( );
	EXPECT_EQ( param.successEqual, res );
}

TEST_P( ParamTestC3DObject, TestEqualTolerance )
{
	auto param = GetParam( );
	auto o2    = obj2.get( );
	auto res   = obj1->IsEqualTolerance( *o2, param.tolerance );
	EXPECT_EQ( param.successEqualTol, res );
}

INSTANTIATE_TEST_CASE_P(
    C3DObject, ParamTestC3DObject,
    testing::Values(
        C3DObjectParam { { 3, 8, 5 }, { 0, 0, 0 }, { 1, 0, 0, 0 }, simsched::C3DObject::Type::receiver, simsched::C3DObject::Type::receiver, 1, 1, true, true, 0.5 },
        C3DObjectParam { { 3, 8, 5 }, { 0, 0, 0 }, { 1, 0, 0, 0 }, simsched::C3DObject::Type::receiver, simsched::C3DObject::Type::receiver, 1, 2, false, false, 0.5 },
        C3DObjectParam { { 3, 8, 5 }, { 0, 0, 0 }, { 1, 0, 0, 0 }, simsched::C3DObject::Type::receiver, simsched::C3DObject::Type::source, 1, 1, false, false, 0.5 },
        C3DObjectParam { { 3, 8, 5 },
                         { 0, 0.35f, 0.05f },
                         { 1, 0, 0, 0 },
                         simsched::C3DObject::Type::receiver,
                         simsched::C3DObject::Type::receiver,
                         1,
                         1,
                         false,
                         true,
                         0.5 },
        C3DObjectParam { { 3, 8, 5 },
                         { 0, 0.35f, 0.05f },
                         { 1, 0, 0, 0 },
                         simsched::C3DObject::Type::receiver,
                         simsched::C3DObject::Type::receiver,
                         1,
                         1,
                         false,
                         false,
                         0.2 } ) );

TEST( C3DObject, serialization )
{
	auto obj1         = simsched::C3DObject( { 4, 6, 1 }, { 1, 0, 0, 0 }, simsched::C3DObject::Type::receiver, 1 );
	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	serializer.WriteSerializable( obj1 );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	auto obj2 = simsched::C3DObject( );

	deserializer.ReadSerializable( obj2 );

	EXPECT_EQ( true, obj1 == obj2 );
}

TEST( C3DObject, uniqueptr )
{
	auto ptr = std::make_unique<simsched::C3DObject>( VistaVector3D( 1, 2, 3 ), VistaQuaternion( 1, 0, 0, 0 ), simsched::C3DObject::Type::receiver, 1 );
	EXPECT_EQ( VistaVector3D( 1, 2, 3 ), ptr->GetPosition( ) );
	EXPECT_EQ( VistaQuaternion( 1, 0, 0, 0 ), ptr->GetOrientation( ) );
	EXPECT_EQ( simsched::C3DObject::Type::receiver, ptr->GetType( ) );
	EXPECT_EQ( 1, ptr->GetId( ) );
}

TEST( C3DObject, copyConstructor )
{
	auto ptr = std::make_unique<simsched::C3DObject>( VistaVector3D( 1, 2, 3 ), VistaQuaternion( 1, 0, 0, 0 ), simsched::C3DObject::Type::receiver, 1 );

	const auto copy = std::make_unique<simsched::C3DObject>( *ptr );

	EXPECT_EQ( *ptr, *copy );
}