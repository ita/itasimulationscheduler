#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include <ITA/SimulationScheduler/update_config.h>

// Vista includes
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "enum_to_int.cpp"

#include "gtest/gtest.h"

namespace simsched = ITA::SimulationScheduler;

struct UpdateConfigTest : testing::Test
{
	simsched::CUpdateConfig update = simsched::CUpdateConfig( simsched::CUpdateConfig::ConfigChangeType::resetAll, "Hello World!" );

	UpdateConfigTest( ) = default;
};

TEST_F( UpdateConfigTest, getter )
{
	EXPECT_EQ( simsched::CUpdateConfig::ConfigChangeType::resetAll, update.GetType( ) );
	EXPECT_EQ( "Hello World!", update.GetPayload( ) );
}

TEST_F( UpdateConfigTest, serialization )
{
	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	update.Serialize( serializer );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	auto newUpdate = simsched::CUpdateConfig( );

	newUpdate.DeSerialize( deserializer );

	EXPECT_EQ( update.GetType( ), newUpdate.GetType( ) );
	EXPECT_EQ( update.GetPayload( ), newUpdate.GetPayload( ) );
}

TEST_F( UpdateConfigTest, copyConstructor )
{
	const auto copy = std::make_unique<simsched::CUpdateConfig>( update );

	EXPECT_EQ( update.GetType( ), copy->GetType( ) );
	EXPECT_EQ( update.GetPayload( ), copy->GetPayload( ) );
}