#include <iostream>
#include <memory>
#include <string>

// simulation scheduler includes
#include <ITA/SimulationScheduler/RoomAcoustics/rir_simulation_result.h>
#include <ITA/SimulationScheduler/ir_simulation_result.h>
#include <ITA/SimulationScheduler/simulation_result.h>

// Vista includes
#include <VistaInterProcComm/Connections/VistaByteBufferDeSerializer.h>
#include <VistaInterProcComm/Connections/VistaByteBufferSerializer.h>

// GTest
#include "gtest/gtest.h"

using namespace ITA::SimulationScheduler;
using namespace ITA::SimulationScheduler::RoomAcoustics;

TEST( SimulationResultTests, simulationResultSerialize )
{
	auto result   = std::make_unique<CSimulationResult>( );
	auto source   = new C3DObject( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = new C3DObject( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	result->sourceReceiverPair.source   = source;
	result->sourceReceiverPair.receiver = receiver;

	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	auto writeBytes = serializer.WriteSerializable( *result );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	CSimulationResult newResult;

	const auto readBytes = deserializer.ReadSerializable( newResult );

	EXPECT_EQ( writeBytes, readBytes );

	EXPECT_EQ( *result->sourceReceiverPair.source, *newResult.sourceReceiverPair.source );
	EXPECT_EQ( *result->sourceReceiverPair.receiver, *newResult.sourceReceiverPair.receiver );
}

TEST( SimulationResultTests, IRsimulationResultSerialize )
{
	auto result   = std::make_unique<CIRSimulationResult>( );
	auto source   = new C3DObject( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = new C3DObject( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	result->sourceReceiverPair.source   = source;
	result->sourceReceiverPair.receiver = receiver;
	result->iLeadingZeros               = 10;
	result->iTrailingZeros              = 30;
	result->bZerosStripped              = false;

	auto frame = ITASampleFrame( 2, 314, true );

	for( int i = 0; i < frame.GetLength( ); i++ )
		frame[1][i] = sin( 2 * 3.14 * 50 * i );

	result->sfResult = frame;

	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	auto writeBytes = serializer.WriteSerializable( *result );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	CIRSimulationResult newResult;

	const auto readBytes = deserializer.ReadSerializable( newResult );

	EXPECT_EQ( writeBytes, readBytes );

	EXPECT_EQ( result->bZerosStripped, newResult.bZerosStripped );
	EXPECT_EQ( result->iTrailingZeros, newResult.iTrailingZeros );
	EXPECT_EQ( result->iLeadingZeros, newResult.iLeadingZeros );
	EXPECT_EQ( *result->sourceReceiverPair.source, *newResult.sourceReceiverPair.source );
	EXPECT_EQ( *result->sourceReceiverPair.receiver, *newResult.sourceReceiverPair.receiver );
	EXPECT_EQ( result->sfResult.toString( ), newResult.sfResult.toString( ) );
	for( int i = 0; i < newResult.sfResult.GetNumChannels( ); i++ )
		EXPECT_EQ( result->sfResult[i].ValuesToString( ), newResult.sfResult[i].ValuesToString( ) );
}

TEST( SimulationResultTests, RIRsimulationResultSerialize )
{
	auto result   = std::make_unique<CRIRSimulationResult>( );
	auto source   = new C3DObject( VistaVector3D( 3, 2, 6 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::source, 1 );
	auto receiver = new C3DObject( VistaVector3D( 6, 1, 10 ), VistaQuaternion( 1, 0, 0, 0 ), C3DObject::Type::receiver, 1 );

	result->sourceReceiverPair.source   = source;
	result->sourceReceiverPair.receiver = receiver;
	result->iLeadingZeros               = 10;
	result->iTrailingZeros              = 30;
	result->bZerosStripped              = false;
	result->bSameRoom                   = false;
	result->eResultType                 = FieldOfDuty::earlyReflections;

	auto frame = ITASampleFrame( 2, 314, true );

	for( int i = 0; i < frame.GetLength( ); i++ )
		frame[1][i] = sin( 2 * 3.14 * 50 * i );

	result->sfResult = frame;

	auto serializer   = VistaByteBufferSerializer( );
	auto deserializer = VistaByteBufferDeSerializer( );

	auto writeBytes = serializer.WriteSerializable( *result );

	deserializer.SetBuffer( serializer.GetBuffer( ), serializer.GetBufferSize( ) );

	CRIRSimulationResult newResult;

	const auto readBytes = deserializer.ReadSerializable( newResult );

	EXPECT_EQ( writeBytes, readBytes );

	EXPECT_EQ( result->bZerosStripped, newResult.bZerosStripped );
	EXPECT_EQ( result->iTrailingZeros, newResult.iTrailingZeros );
	EXPECT_EQ( result->iLeadingZeros, newResult.iLeadingZeros );
	EXPECT_EQ( *result->sourceReceiverPair.source, *newResult.sourceReceiverPair.source );
	EXPECT_EQ( *result->sourceReceiverPair.receiver, *newResult.sourceReceiverPair.receiver );
	EXPECT_EQ( result->bSameRoom, newResult.bSameRoom );
	EXPECT_EQ( result->eResultType, newResult.eResultType );
	EXPECT_EQ( result->sfResult.toString( ), newResult.sfResult.toString( ) );
	for( int i = 0; i < newResult.sfResult.GetNumChannels( ); i++ )
		EXPECT_EQ( result->sfResult[i].ValuesToString( ), newResult.sfResult[i].ValuesToString( ) );
}