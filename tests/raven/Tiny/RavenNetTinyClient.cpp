/*
 *		RAVEN Network Interface
 *
 *		(c) Copyright Institut f�r Technische Akustik (ITA)
 *			RWTH Aachen (http://www.akustik.rwth-aachen.de)
 *
 *  ---------------------------------------------------------------------------------
 *
 *    Datei:			RavenNetTinyServer.cpp
 *
 *    Zweck:			Test Raven with Vista network functionality
 *
 *    Autor(en):		Jonas Stienen (stienen@akustik.rwth-aachen.de)
 *
 *  ---------------------------------------------------------------------------------
 */

// $Id: VistaNetTest.cpp 2732 2012-06-26 13:38:17Z stienen $

#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <VistaInterProcComm/Connections/VistaConnectionIP.h>
#include <iostream>
#include <string>

static std::string g_sServerName = "10.0.1.25";
static int g_iServerPort         = 12480;

int main( int argc, char** argv )
{
	if( argc == 1 )
		std::cout << "Using default connection, specify connection if needed: VistaNetClient SERVERADDRESS PORTNUMBER" << std::endl;

	std::string sServerName = g_sServerName;
	if( argc >= 2 )
		sServerName = argv[1];

	int iServerPort = g_iServerPort;
	if( argc >= 3 )
		iServerPort = StringToInt( std::string( argv[2] ) );

	std::cout << "Attempting to connect to '" << sServerName << ":" << iServerPort << "'" << std::endl;

	ITAStopWatch sw;
	sw.start( );
	VistaConnectionIP oConnection( VistaConnectionIP::CT_TCP, sServerName, iServerPort );
	if( oConnection.GetIsConnected( ) )
		std::cout << "Connection established in " << timeToString( sw.stop( ) ) << std::endl;
	else
	{
		std::cerr << "Connection error, exiting." << std::endl;
		return 255;
	}

	int iMessageType = 101;
	oConnection.WriteInt32( iMessageType );

	// CConfig oConfig;
	// const void* pBuffer = &oConfig;
	int pBuffer[10];
	for( int i = 0; i < 10; i++ )
		pBuffer[i] = i + 1;
	oConnection.WriteRawBuffer( pBuffer, 10 * sizeof( int ) );
	oConnection.WaitForSendFinish( );

	bool bAck;
	oConnection.ReadBool( bAck );
	std::cout << "Client received acknowledge flag '" << bAck << "'" << std::endl;

	oConnection.Close( false );

	return 0;
}
