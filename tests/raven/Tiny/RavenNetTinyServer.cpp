/*
 *		RAVEN Network Interface
 *
 *		(c) Copyright Institut f�r Technische Akustik (ITA)
 *			RWTH Aachen (http://www.akustik.rwth-aachen.de)
 *
 *  ---------------------------------------------------------------------------------
 *
 *    Datei:			RavenNetTinyServer.cpp
 *
 *    Zweck:			Test Raven over Net using simple Vista network functionality
 *
 *    Autor(en):		Jonas Stienen (stienen@akustik.rwth-aachen.de)
 *
 *  ---------------------------------------------------------------------------------
 */

// $Id: VistaNetTest.cpp 2732 2012-06-26 13:38:17Z stienen $

#include <VistaInterProcComm/Concurrency/VistaThreadLoop.h>
#include <VistaInterProcComm/Connections/VistaConnectionIP.h>
#include <VistaInterProcComm/IPNet/VistaIPAddress.h>
#include <VistaInterProcComm/IPNet/VistaTCPServer.h>
#include <VistaInterProcComm/IPNet/VistaTCPSocket.h>
#include <iostream>
#include <string>

//#include <VistaBase/VistaTimeUtils.h>

static int g_iServerPort = 12480;

int main( int argc, char** argv )
{
	std::cout << "Setting up server listening on all interfaces to TCP port '" << g_iServerPort << "'" << std::endl;
	VistaTCPServer oServer( "0.0.0.0", g_iServerPort, 1 );

	std::cout << "Waiting for connection" << std::endl;
	VistaConnectionIP* pHeadConnection = new VistaConnectionIP( oServer.GetNextClient( ) );

	VistaSocketAddress oAddress;
	VistaIPAddress rAddr;
	std::string sAddrLocal;
	pHeadConnection->GetLocalAddress( ).GetIPAddress( ).GetAddressString( sAddrLocal );
	std::string sAddrPeer = pHeadConnection->GetPeerName( );
	int iPortLocal        = pHeadConnection->GetLocalAddress( ).GetPortNumber( );
	int iPortPeer         = pHeadConnection->GetPeerPort( );
	std::cout << "Connection established from " << sAddrPeer << ":" << iPortPeer << " to " << sAddrLocal << ":" << iPortLocal << std::endl;


	std::cout << "Waiting for data" << std::endl;
	long l = pHeadConnection->WaitForIncomingData( 0 );


	int iMessageType;
	pHeadConnection->ReadInt32( iMessageType );
	std::cout << "Message type is '" << iMessageType << "'" << std::endl;

	if( iMessageType == 101 )
	{
		long l = pHeadConnection->WaitForIncomingData( 0 );
		std::cout << "Received message with length '" << l << "', trying to read config data" << std::endl;
		// CConfig oConfig;
		void* pBuffer = new char[1024];
		pHeadConnection->ReadRawBuffer( pBuffer, l );
		int* pIntBuffer = (int*)pBuffer;
		int l_i         = (int)l / (double)sizeof( int );
		for( int i = 0; i < l_i; i++ )
			std::cout << pIntBuffer[i] << std::endl;
		// std::cout << oConfig.ToString() << std::endl;
		delete[] pBuffer;
	}

	/*
	std::cout << "Server receiving data" << std::endl;
	char* buf = new char[l];
	pHeadConnection->Receive( buf, l );
	std::string sMsg = std::string( buf, l );
	delete[] buf;
	std::cout << "Message content: '" << sMsg << "'" << std::endl;
	*/

	std::cout << "Sending acknowledge flag" << std::endl;
	bool bAck = false;
	pHeadConnection->Send( &bAck, 1 );

	pHeadConnection->Close( false );

	return 0;
}
