#ifndef INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_ROOM_ACOUSTICS_RAVEN_SIMULATOR_INTERFACE
#define INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_ROOM_ACOUSTICS_RAVEN_SIMULATOR_INTERFACE

// std includes
#include <memory>

// API includes
#include <ITA/SimulationScheduler/definitions.h>

// simulation scheduler includes
#include <ITA/SimulationScheduler/update_config.h>
#include <ITA/SimulationScheduler/RoomAcoustics/Raven/raven_worker_interface.h>

namespace ITA
{
	namespace SimulationScheduler
	{
		namespace RoomAcoustics
		{
			namespace Raven
			{
				struct CRavenSimulationResult;
				struct CSimulationTask;

				///
				/// \brief Interface class for simulators.
				/// \remark This interface mainly exists for testing purposes.
				///
				class ITA_SIMULATION_SCHEDULER_API ISimulatorInterface
				{
				public:
					virtual ~ISimulatorInterface( ) = 0 {}

					///
					/// \brief Reset the simulator.
					///
					virtual void Reset( ) = 0;

					///
					/// \brief Simulate the given task.
					/// \param pTask the task to be simulated.
					/// \param[out] pResult the result of the simulation
					///
					virtual void Compute( CSimulationTask* pTask, CRavenSimulationResult* pResult ) = 0;

					///
					/// \brief Register pointer to ParentWorkerThread
					/// \param pParentThread pointer to ParentWorkerThread
					/// 
					virtual void RegisterWorkerThread( IRavenWorkerInterface* pParentThread ) { };

					///
					/// \brief Set pointer to ParentWorkerThread to nullptr
					///
					virtual void DeregisterWorkerThread( ) { };
				};
			} // namespace Raven
		}     // namespace RoomAcoustics
	}         // namespace SimulationScheduler
} // namespace ITA

#endif // INCLUDE_WATCHER_ITA_SIMULATION_SCHEDULER_ROOM_ACOUSTICS_RAVEN_SIMULATOR_INTERFACE